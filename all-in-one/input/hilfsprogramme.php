#<?php
# richtigen Zeichensatz benutzen
header('Content-type: text/html; charset=utf-8');

#
#  INHALTSVERZEICHNIS:
#
#  Darstellfunktionen:
#    pre
#    pre_dump
#    fehlermeldung
#
#  Ueberprueffunktionen:
#    ueberpruefen_ob_seitentextedateien_fehlen_oder_zuviel
#    ueberpruefen_ob_anzahl_zeilen_in_email_durch_zwei_teilbar
#    ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen
#    ueberpruefen_ob_botschaften_zuviel_oder_fehlen
#    ueberpruefen_ob_anzahl_botschaften_uebereinstimmt_mit_summe_botschaften_pro_jahr
#    ueberpruefen_ob_anzahl_themensammlungs_inhaltsangaben_uebereinstimmt_mit_summe_themensammlungs_inhaltsangaben
#
#  Getter:
#    hole_position_sprachkuerzel
#    hole_region_in_grossbuchstaben
#    hole_von_deutscher_sprachbezeichnung_dessen_sprachkuerzel
#    hole_navigationsblock_aktueller_sprache
#    hole_alle_deutschen_sprachbezeichnungen
#    hole_eigensprachliche_sprachbezeichnungen
#    hole_alle_seitenuebersetzungen_texte
#    hole_alle_themensammlungen_inhaltsangaben
#
#  Array-Bearbeitung:
#    array_leere_elemente_entfernen
#    array_sprachtextdatei_bereinigen
#    array_von_array_abziehen
#
#  Ins Gesamtarray eintragen:
#    sprachkuerzel_mit_nur_die_ersten_zwei_zeichen_eintragen
#    eintragen_ob_alternate
#    deutsche_bezeichnung_eintragen
#    englische_bezeichnung_eintragen
#    eigensprachliche_bezeichnung_eintragen
#    eintragen_ob_lateinische_buchstaben
#    beschreibungen_eintragen
#    sprunglinks_eintragen
#    info_eintragen
#    datenschutz_eintragen
#    aktualisiert_begriff_eintragen
#    startseite_begriff_eintragen
#    original_webseiten_begriff_eintragen
#    original_webseite_1_begriff_eintragen
#    original_webseite_2_begriff_eintragen
#    aufklappen_begriff_eintragen
#    untermenue_aufklappen_begriff_eintragen
#    email_texte_eintragen
#    eintragen_ob_internationales_datumsformat
#    datum_neuester_themensammlung_eintragen
#    datum_neuester_botschaft_eintragen
#    anzahl_themensammlungen_eintragen
#    anzahl_botschaften_eintragen
#
#  Ins Navigation-URL-Array eintragen:
#    button_arrays_erschaffen
#    felder_fuer_buttonarrays_erzeugen
#    untermenuebuttons_deklarieren
#    aufklappbuttons_deklarieren
#    hauptmenuebuttons_deklarieren
#    buttons_durchnummerieren
#    button_beschriftungen_eintragen
#    url_links_durchnummerieren
#  - Fuer URL Sonderzeichen ersetzen:
#    liste_der_positionen_deutscher_sprachbezeichnungen_erstellen
#    ueberpruefen_ob_anzahl_sonderzeichen_paarweise_durch_zwei_teilbar
#    array_mit_sprachen_und_deren_zu_ersetzende_sonderzeichen_erstellen
#    url_konforme_liste_erstellen_nichtlateinische_sprachen
#    url_konforme_liste_erstellen_lateinische_sprachen
#    aufklapp_buttons_leeren
#
#  In Botschaften-Arrays eintragen:
#    botschaften_beschriftungen_und_dateinamen_eintragen
#    themensammlungen_beschriftungen_und_dateinamen_eintragen
#


#########################################################################################################
#
#  DARSTELLFUNKTIONEN
#
#########################################################################################################


# Strukturierte Ausgabe
function pre( $ausgabe ) {
  echo "<pre>\n";
  print_r($ausgabe);
  echo "</pre>\n";
}

# Strukturierte Ausgabe
function pre_dump( $ausgabe ) {
  echo "<pre>\n";
  var_dump($ausgabe);
  echo "</pre>\n";
}

# Fehlermeldung ausgeben
function fehlermeldung() {
  echo '<script type="text/javascript">alert("Fehler vorhanden!\nBitte Meldung im Browser lesen.");</script>';
}


#########################################################################################################
#
#  UEBERPRUEFFUNKTIONEN
#
#########################################################################################################


function ueberpruefen_ob_seitentextedateien_fehlen_oder_zuviel( $liste_in_uebersetzungsdaten , $verzeichnis_daten , $verzeichnis_schablonen ) {
  # Zuerst wird ueberprueft, welche Dateien sowohl im Ordner 'daten', als auch im Ordner 'schablonen' vorkommen.
  # Es duerften AUSSCHLIESSLICH "Seiten"-Daten/-Schablonen-Dateien in *Beiden* vorkommen.
  # Diese Liste an "gemeinsamen" Dateien herausfinden.
  # Diese Liste muss auch identisch sein mit der Liste, die in der "uebersetzungsdaten.txt" 'vermerkt' ist.
  # Es darf weder eine Datei fehlen, noch eine zuviel sein.
  $liste_dateien_im_verzeichnis_daten = array();
  $liste_dateien_im_verzeichnis_schablonen = array();
  $liste_dateinamen_die_nicht_ueberall_gleichermassen_vorkommen = array();
  # Hole alle Dateinamen aus dem Verzeichnis "daten":
  if ( is_dir( $verzeichnis_daten ) ) {
    if ( $handle = opendir( $verzeichnis_daten ) ) {
      while ( ( $file = readdir( $handle ) ) !== false ) {
        $liste_dateien_im_verzeichnis_daten[] = $file;
      }
	  closedir( $handle );
    }
  }
  # Hole alle Dateinamen aus dem Verzeichnis "schablonen":
  if ( is_dir( $verzeichnis_schablonen ) ) {
    if ( $handle = opendir( $verzeichnis_schablonen ) ) {
      while ( ( $file = readdir( $handle ) ) !== false ) {
        $liste_dateien_im_verzeichnis_schablonen[] = $file;
      }
	  closedir( $handle );
    }
  }
  # Ermittle alle Dateien, die in BEIDEN Ordnern vorkommen
  $in_beiden_ordnern_vorkommende_dateien = array_intersect( $liste_dateien_im_verzeichnis_daten , $liste_dateien_im_verzeichnis_schablonen );
  # die 2 'Verzeichnis'-Dateien "." und ".." aus der Liste entfernen
  $in_beiden_ordnern_vorkommende_dateien = array_diff( $in_beiden_ordnern_vorkommende_dateien , array( "." , ".." ) );
  # Diese 'Liste' mit den uebriggebliebenen Dateinamen muss UEBEREINSTIMMEN mit der in der "uebersetzungsdaten.txt" hinterlegten Liste:
  $liste_dateinamen_die_nicht_ueberall_gleichermassen_vorkommen = array_merge( array_diff( $in_beiden_ordnern_vorkommende_dateien , $liste_in_uebersetzungsdaten ) , array_diff( $liste_in_uebersetzungsdaten , $in_beiden_ordnern_vorkommende_dateien ) );
  # Falls die Dateien *NICHT* ueberall gleich(ermassen) vorhanden sind: Fehlermeldung:
  if ( count( $liste_dateinamen_die_nicht_ueberall_gleichermassen_vorkommen ) > 0 ) {
    echo "Die Dateien in den Ordnern " . EINGANG . ORDNER_DATEN . " und " . EINGANG . ORDNER_SCHABLONEN . " weichen von der Liste in " . EINGANG . ORDNER_DATEN . ROHDATEN . " in folgenden Dateien ab:<br>";
    pre( $liste_dateinamen_die_nicht_ueberall_gleichermassen_vorkommen );
	fehlermeldung();
  }
}

function ueberpruefen_ob_anzahl_zeilen_in_email_durch_zwei_teilbar( $email_codierung ) {
  If ( ( count( $email_codierung ) % 2 ) > 0 ) {
    echo 'Die Anzahl an Elementen in der Liste "E-Mail-Adresse - codierte Wörter" muss glatt durch 2 teilbar sein.<br>';
    echo 'Eins fuer die Kodierung, eins fuer dessen "Ersetzung".<br>';
	echo 'Es sind aber ' . count( $email_codierung ) . ' Elemente in folgender Liste vorhanden:';
	pre( $email_codierung );
	fehlermeldung();
  }
}

function ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $array ) {
  global $anzahl_sprachen;
  $anzahl_zeilen_im_array = count( $array );
  # Geht die Anzahl an Zeilen - *glatt* - durch die Anzahl an *Sprachen*?
  # (Alle Sprachen muessen die gleiche Anzahl an Zeilen haben. Es muss also 'glatt aufgehen'.)
  # Modulo wird ermittelt.
  $rest_von_array_zeilen_geteilt_durch_anzahl_sprachen = $anzahl_zeilen_im_array % $anzahl_sprachen;
  # Wenn es NICHT glatt aufgeht, fehlt irgendwo eine Zeile oder ist eine zuviel.
  if ( $rest_von_array_zeilen_geteilt_durch_anzahl_sprachen > 0 ) {
    # Fehlermeldung ausgeben
    echo 'Die Anzahl an Sprachen beträgt ' . $anzahl_sprachen . '.<br>';
	echo 'Die Anzahl an Zeilen in der "Liste" unten beträgt ' . $anzahl_zeilen_im_array . '.<br>';
	echo 'Alle Sprachen müssen die GLEICHE Anzahl an ' . "'Zeilen'" . ' haben.<br>';
	echo 'Die Anzahl an Zeilen in der Liste geht nicht - GLATT - durch die Anzahl an Sprachen auf.<br>';
	echo 'Entweder stimmt etwas mit der ' . "'Liste an Sprachen'" . ' nicht, oder mit der unten folgenden Liste:<br>';
    pre( $array );
    fehlermeldung();
  }
}

function ueberpruefen_ob_botschaften_zuviel_oder_fehlen( $liste_der_themensammlungenreihenfolge , $liste_der_botschaftenreihenfolge , $verzeichnis_botschaften ) {
  global $gesamtarray;
  global $liste_aller_botschaftsformate;
  $array_mit_deutschen_sprachbezeichnungen = hole_alle_deutschen_sprachbezeichnungen( $gesamtarray );
  # Ein Array anlegen, welches alle "Nummern" (der Botschaften/Themensammlungen) exakt EIN mal - auch wenn Botschaften 2 mal,
  # sowohl bei Themensammlungen als auch bei Botschaften, vorkommen - , als 'Key-Value' auflisten.
  $liste_aller_nummern = array();
  for( $i = 0 ; $i < count( $liste_der_themensammlungenreihenfolge ) ; $i++ ) {
    $liste_aller_nummern[ ( $liste_der_themensammlungenreihenfolge[$i] + 0 ) ] = $liste_der_themensammlungenreihenfolge[$i];
  }
  for( $i = 0 ; $i < count( $liste_der_botschaftenreihenfolge ) ; $i++ ) {
    $liste_aller_nummern[ ( $liste_der_botschaftenreihenfolge[$i] + 0 ) ] = $liste_der_botschaftenreihenfolge[$i];
  }
  sort( $liste_aller_nummern );
  
  # Gehe alle Botschaftsformate (docx, ...) durch
  for ( $k = 0 ; $k < count( $liste_aller_botschaftsformate ) ; $k++ ) {
    # Gehe alle (deutschsprachigen) Ordner der jeweiligen Sprache durch
    for ( $j = 0 ; $j < count( $array_mit_deutschen_sprachbezeichnungen ) ; $j++ ) {
      $liste_dateien_im_aktuellen_verzeichnis = array();
      
      $aktuelles_verzeichnis = $verzeichnis_botschaften . $liste_aller_botschaftsformate[$k] . "/" . $array_mit_deutschen_sprachbezeichnungen[$j];
      # Hole alle Dateinamen aus dem aktuellen Verzeichnis (ausser "." und ".."):
      if ( is_dir( $aktuelles_verzeichnis ) ) {
        if ( $handle = opendir( $aktuelles_verzeichnis ) ) {
          while ( ( $file = readdir( $handle ) ) !== false ) {
            if ( !( ( $file == "." ) or ( $file == ".." ) ) ) {
              # Nur die ersten 3 Ziffern
              $ersten_drei_ziffern = mb_substr( $file , 0 , 3 );
              
              # Die ersten drei Zeichen muessen ZIFFERN sein
              if ( !( is_numeric( $ersten_drei_ziffern ) ) ) {
                # Fehlermeldung ausgeben
                echo "Im Ordner " . $aktuelles_verzeichnis . " gibt es eine Datei, die nicht mit 3 Ziffern am Anfang des Dateinamens anfängt.<br>";
                echo "Der Name der Datei lautet:<br>";
                echo $file . "<br>";
                fehlermeldung();
              }
              if ( in_array( $ersten_drei_ziffern , $liste_dateien_im_aktuellen_verzeichnis ) ) {
                echo "Im Ordner " . $aktuelles_verzeichnis . " gibt es mehrere Botschaften, die mit der gleichen Ziffernreihenfolge anfangen.<br>";
                echo 'Es sollen aber jede Botschaft ihre EIGENE 3-Ziffern-"Kennung" haben.<br>';
                echo "Es handelt sich um die folgende Botschaftennummer, die mehrfach vorkommt:<br>";
                echo $ersten_drei_ziffern . "<br>";
                fehlermeldung();
              }
              $liste_dateien_im_aktuellen_verzeichnis[] = $ersten_drei_ziffern;
            }
          }
          closedir( $handle );
        }
      }
      sort( $liste_dateien_im_aktuellen_verzeichnis );
	  
      # Ermittle alle Zahlen, die in BEIDEN "Listen", sowohl der Reihenfolgen-Nummern-Liste, als auch in den Dateinamen, vorkommen
      $in_beiden_listen_vorkommende_zahlen = array_intersect( $liste_dateien_im_aktuellen_verzeichnis , $liste_aller_nummern );
      
      $anzahl_in_liste_aller_nummern = count( $liste_aller_nummern );
      $anzahl_in_liste_dateien_im_aktuellen_verzeichnis = count( $liste_dateien_im_aktuellen_verzeichnis );
      $anzahl_in_in_beiden_listen_vorkommende_zahlen = count( $in_beiden_listen_vorkommende_zahlen );
      # Wenn es in der Themensammlungen-/Botschaften-Reihenfolgen-Nummernliste Zahlen gibt, deren Botschaften nicht vorhanden sind
      if ( $anzahl_in_liste_aller_nummern > $anzahl_in_in_beiden_listen_vorkommende_zahlen ) {
        echo "Im Ordner<br>";
        echo $aktuelles_verzeichnis . "<br>";
        echo "ist eine Botschafts-Nummer nicht vorhanden, in einer der Listen <br>";
        echo "'Liste der Themensammlungenreihenfolge'<br>";
        echo "'Liste der Botschaftenreihenfolge'<br>";
        echo "kommt sie aber vor.<br>";
        echo "Entweder fehlt die Botschaft mit dieser Nummer, oder in den Listen ist die Nummer zu viel vorhanden.<br>";
        echo "Die Listen findet man in der Datei<br>";
        echo EINGANG . ORDNER_DATEN . "uebersetzungsdaten.txt<br>";
        echo "ganz unten, bei den beiden Blöcken<br>";
        echo "'Liste der Themensammlungenreihenfolge' und/oder 'Liste der Botschaftenreihenfolge'.<br>";
        echo "Die Nummer, die fehlt, bzw. zu viel ist, lautet:<br>";
        $differenz_array = array();
        $differenz_array = array_diff( $liste_aller_nummern , $in_beiden_listen_vorkommende_zahlen );
        foreach ( $differenz_array as $ueberschuessige_ziffer_bzw_fehlende_botschaft ) {
          echo $ueberschuessige_ziffer_bzw_fehlende_botschaft . "<br>";
        }
        echo "<br>";
        fehlermeldung();
      }
      # Wenn es im Botschaften-Ordner Botschaften gibt, die aber als "Ziffern" in den
      # Reihenfolgen-Nummern der Themensammlungs- bzw. Botschaften-Listen nicht vorkommen
      if ( $anzahl_in_liste_dateien_im_aktuellen_verzeichnis > $anzahl_in_in_beiden_listen_vorkommende_zahlen ) {
        echo "Im Ordner<br>";
        echo $aktuelles_verzeichnis . "<br>";
        echo "ist eine Botschafts-Nummer vorhanden, in einer der Listen <br>";
        echo "'Liste der Themensammlungenreihenfolge'<br>";
        echo "'Liste der Botschaftenreihenfolge'<br>";
        echo "kommt sie aber nicht vor.<br>";
        echo "Entweder existiert die Botschaft mit dieser Nummer fälschlicherweise, oder in den Listen fehlt die Nummer.<br>";
        echo "Die Listen findet man in der Datei<br>";
        echo EINGANG . ORDNER_DATEN . "uebersetzungsdaten.txt<br>";
        echo "ganz unten, bei den beiden Blöcken<br>";
        echo "'Liste der Themensammlungenreihenfolge' und/oder 'Liste der Botschaftenreihenfolge'.<br>";
        echo "Die Nummer, die fehlt, bzw. zu viel ist, lautet:<br>";
        $differenz_array = array();
        $differenz_array = array_diff( $liste_dateien_im_aktuellen_verzeichnis , $in_beiden_listen_vorkommende_zahlen );
        foreach ( $differenz_array as $ueberschuessige_botschaft_bzw_fehlende_ziffer ) {
          echo $ueberschuessige_botschaft_bzw_fehlende_ziffer . "<br>";
        }
        echo "<br>";
        fehlermeldung();
      }
      
      # Ueberpruefen, ob in beiden Listen vorkommende Nummer auch als 'LABEL-Beschriftung' vorhanden ist
	  
    }
  }
}

function ueberpruefen_ob_anzahl_botschaften_uebereinstimmt_mit_summe_botschaften_pro_jahr( $gesamtarray , $anzahl_der_botschaften_pro_jahr ) {
  $anzahl_an_jahren = count( $anzahl_der_botschaften_pro_jahr );
  $summe_aller_botschaften = 0;
  # Die Anzahlen aller Botschaften aller Jahre zusammenzaehlen
  foreach ( $anzahl_der_botschaften_pro_jahr as $anzahl_in_aktuellem_jahr ) {
    $summe_aller_botschaften = $summe_aller_botschaften + $anzahl_in_aktuellem_jahr;
  }
  # Die angegebenen "Gesamt-Anzahlen an Botschften" in JEDER Sprache mit dieser 'ermittelten' vergleichen.
  foreach ( $gesamtarray as $s ) {
    $angegebene_anzahl_der_aktuellen_sprache = $s['anzahl_botschaften'];
    if ( ! ( $summe_aller_botschaften == $angegebene_anzahl_der_aktuellen_sprache ) ) {
      echo "Die Summe aller Botschaften pro Jahr (bei der Botschaftenliste) beträgt: " . $summe_aller_botschaften . ".<br>";
      echo '(Die Liste findet man in der Datei "uebersetzungsdaten.txt" - praktisch ganz unten.)<br>';
      echo "Doch in der Sprache " . $s['deutsch'] . " sind es " . $angegebene_anzahl_der_aktuellen_sprache . " Botschaften.<br>";
      echo "Mögliche Fehlerquellen:<br>";
      echo '- Botschaft fehlt im Botschaften-Ordner (im Ordner "downloads")<br>';
      echo "- in der Reihenfolgen-Liste der Botschaften ist/sind Nummer(n) zuviel/zuwenig<br>";
      echo '- in der "Anzahl an Botschaften pro Jahr"-Liste fehlen Jahre/sind zuviel, oder sind Zahlen zu groß/zu klein<br>';
      fehlermeldung();
    }
  }
}

function ueberpruefen_ob_anzahl_themensammlungs_inhaltsangaben_uebereinstimmt_mit_summe_themensammlungs_inhaltsangaben( $themensammlungen_inhaltsangaben_array , $liste_der_anzahl_der_inhaltsangaben_pro_themensammlung ) {
  global $gesamtarray;
  $anzahl_in_liste = 0;
  # Ermittle die Summe der angegebenen Inhaltsangaben in der LISTE
  for ( $i = 0 ; $i < count( $liste_der_anzahl_der_inhaltsangaben_pro_themensammlung ) ; $i++ ) {
    $anzahl_in_liste = $anzahl_in_liste + $liste_der_anzahl_der_inhaltsangaben_pro_themensammlung[$i];
  }
  # Gehe die Gesamtliste (aller Sprachen) der Inhaltsangaben durch
  foreach ( $themensammlungen_inhaltsangaben_array as $s => $inhaltsangaben_array_aktueller_sprache ) {
    # tatsaechliche Anzahl DIESER AKTUELLEN Sprache
    $tatsaechliche_anzahl = count( $themensammlungen_inhaltsangaben_array[$s] );
    if ( ! ( $tatsaechliche_anzahl == $anzahl_in_liste ) ) {
      echo "MiauDie Summe aller Inahltsangaben (bei der Themensammlungenliste) beträgt: " . $anzahl_in_liste . ".<br>";
      echo '(Die Liste findet man in der Datei "uebersetzungsdaten.txt" - ganz unten.)<br>';
      echo "Doch in der Sprache " . $gesamtarray[$s]['deutsch'] . " sind es " . $tatsaechliche_anzahl . " Inhaltsangaben.<br>";
      echo "Mögliche Fehlerquellen:<br>";
      echo "- in der Themensammlungen-Inhaltsangaben-Liste ist/sind Nummer(n) zuviel/zuwenig<br>";
      echo '- in der Datei "webseite/daten/themensammlungs_inhaltsangaben.txt" fehlen Inhaltsangaben/sind zuviel<br>';
      fehlermeldung();
    }
  }
}


#########################################################################################################
#
#  GETTER
#
#########################################################################################################


# Die Position einen Sprach-Kuerzels in einer Liste zurueckgeben
function hole_position_sprachkuerzel( $gesuchtes_sprachkuerzel , $liste ) {
  global $gesamtarray;
  $anzahl_elemente_pro_sprache = count( $liste ) / count( $gesamtarray );
  # Alle Sprachkuerzel (in der Liste) durchgehen
  for ( $i = 0 ; $i < count( $liste ) ; $i = $i + $anzahl_elemente_pro_sprache ) {
    if ( $liste[$i] == $gesuchtes_sprachkuerzel ) {
      $position = $i;
    }
  }
  return $position;
}

function hole_region_in_grossbuchstaben( $kuerzel ) {
  $kuerzel_mit_region_in_grossbuchstaben = $kuerzel;
  $array = array();
  # Wenn im Kuerzel ein 'Bindestrich' vorhanden ist
  if ( mb_strpos( $kuerzel , "-" ) > 0 ) {
    $array = explode( "-" , $kuerzel );
    $kuerzel_mit_region_in_grossbuchstaben = $array[0] . "-" . mb_strtoupper( $array[1] );
  }
  return $kuerzel_mit_region_in_grossbuchstaben;
}

# Von einer Sprachbezeichnung in deutsch das Sprachkuerzel dieser Sprache zurueckgeben
function hole_von_deutscher_sprachbezeichnung_dessen_sprachkuerzel( $deutsche_bezeichnung ) {
  global $gesamtarray;
  $gesuchtes_sprachkuerzel = "";
  foreach ( $gesamtarray as $s => $spracharray ) {
    if ( $gesamtarray[$s]['deutsch'] == $deutsche_bezeichnung ) {
      $gesuchtes_sprachkuerzel = $s;
    }
  }
  return $gesuchtes_sprachkuerzel;
}

# Aus der 'Liste-aller-Standard-Navigationsseiten' die EINER Sprache zurueckgeben 
function hole_navigationsblock_aktueller_sprache( $liste_aller_navigation_standardseiten , $sprachkuerzel , $anzahl_zeilen_je_sprache ) {

  $block = array();
  for ( $i = 0 ; $i < count( $liste_aller_navigation_standardseiten ) ; $i = $i + $anzahl_zeilen_je_sprache ) {
    # falls Sprachkuerzel in Liste gefunden wurde ...
    if ( $liste_aller_navigation_standardseiten[$i] == $sprachkuerzel ) {
      # Den Block an Zeilen in ein Array packen
      for ( $j = 0 ; $j < $anzahl_zeilen_je_sprache ; $j++ ) {
        $block[] = $liste_aller_navigation_standardseiten[ ( $i + $j ) ];
      }
    }
  }
  return $block;
}

# Aus dem Gesamtarray alle Sprachbezeichnungen AUF DEUTSCH "als Array" zurueckgeben
function hole_alle_deutschen_sprachbezeichnungen( $gesamtarray ) {
  $array_mit_deutschen_sprachbezeichnungen = array();
  foreach ( $gesamtarray as $s => $spracharray ) {
    $array_mit_deutschen_sprachbezeichnungen[] = $gesamtarray[$s]['deutsch'];
  }
  return $array_mit_deutschen_sprachbezeichnungen;
}

# Aus dem Array alle Sprachbezeichnungen in DER JEWEILIGEN SPRACHE SELBST, "als Array" zurueckgeben
function hole_eigensprachliche_sprachbezeichnungen( $array ) {
  global $gesamtarray;
  $array_mit_eigensprachlichen_sprachbezeichnungen = array();
  foreach ( $array as $s ) {
    $array_mit_eigensprachlichen_sprachbezeichnungen[] = $gesamtarray[$s]['in_eigener_sprache'];
  }
  return $array_mit_eigensprachlichen_sprachbezeichnungen;
}

function hole_alle_seitenuebersetzungen_texte( $array_dateinamen ) {
  # leere Elemente aus Array entfernen
  $array_dateinamen = array_leere_elemente_entfernen ( $array_dateinamen );
  $array_seitenuebersetzungen = array();
  # Alle txt-Dateien nacheinander in ein Gesamtarray laden
  for ( $i = 0 ; $i < count( $array_dateinamen ) ; $i++ ) {
    # Eine txt-Datei als Array ins Gesamtarray reinspeichern
    $array_seitenuebersetzungen[$i] = file( EINGANG . ORDNER_DATEN . $array_dateinamen[$i] );
    # gespeichertes Array 'reinigen' (von Leerzeilen, Kommentarzeilen, Zeilenende-Kodierungen)
    $array_seitenuebersetzungen[$i] = array_sprachtextdatei_bereinigen( $array_seitenuebersetzungen[$i] );
  }
  $array_seitenuebersetzungen = array_leere_elemente_entfernen( $array_seitenuebersetzungen );
  return $array_seitenuebersetzungen;
}

function hole_alle_themensammlungen_inhaltsangaben( $datei ) {
  # die Datei laden
  $dateiinhalt = file( $datei );
  # die Datei 'reinigen' (von Leerzeilen, Kommentarzeilen, Zeilenende-Kodierungen)
  $dateiinhalt = array_sprachtextdatei_bereinigen( $dateiinhalt );
  $dateiinhalt = array_leere_elemente_entfernen( $dateiinhalt );
  return $dateiinhalt;
}


#########################################################################################################
#
#  ARRAY-BEARBEITUNG
#
#########################################################################################################


# leere Elemente aus Array entfernen
function array_leere_elemente_entfernen( $array ) {
  $j = 0;
  for ( $i = 0 ; $i < count( $array ) ; $i++ ) {
    $array[$j] = $array[$i];
    if ( $array[$i] == "" ) { } else { $j = $j + 1; }
  }
  $array = array_slice( $array , 0 , $j );
  return $array;
}

# Sprachdateien bereinigen: Leere Zeilen, Kommentarzeilen und Zeilenendekodierungen entfernen
function array_sprachtextdatei_bereinigen( $array ) {
  for ( $i = 0 ; $i < count( $array ) ; $i++ ) {
    # entfernt die Zeilenendenkodierung
    $array[$i] = str_replace( "\n" , "" , $array[$i] );
    $array[$i] = str_replace( "\r" , "" , $array[$i] );
    if ( strlen( $array[$i] ) > 0 ) {
      # Leerzeichen am Anfang und Ende entfernen
      $array[$i] = trim( $array[$i] );
      # entfernt Kommentarzeilen
      if ( mb_substr( $array[$i] , 0 , 1 ) == "#" ) { $array[$i] = ""; }
    }
  }
  # leere Elemente aus Array entfernen
  $array = array_leere_elemente_entfernen ( $array );
  return $array;
}

# Gesamtarray - Teilarray = Restarray
function array_von_array_abziehen( $gesamtarray , $teilarray ) {
  # ARRAYS KOPIEREN
  $originalbackup_gesamtarray = $gesamtarray;
  $originalbackup_teilarray = $teilarray;
  # ARRAYS VONEINANDER ABZIEHEN
  for ( $i = 0 ; $i < count( $teilarray ) ; $i++ ) {
    for ( $j = 0 ; $j < count( $gesamtarray ) ; $j++ ) {
      # Wenn ein Feldinhalt vom Gesamtarray identisch ist mit einem Feldinhalt vom Teilarray, ...
      if ( $gesamtarray[$j] == $teilarray[$i] ) {
        # ... dann leere die Felder
        $gesamtarray[$j] = "";
        $teilarray[$i] = "";
      }
    }
  }
  # FEHLERUEBERPRUEFUNG
  # Es darf keine Sprache im Teilarray vorkommen, die es nicht auch im Gesamtarray gibt.
  # Ueberpruefung, ob alle Elemente aus dem Teilarray 'entfernt' wurden.
  $teilarray = array_leere_elemente_entfernen( $teilarray );
  # Falls im Teilarray nicht'entfernte' Elemente sind, ...
  if ( count( $teilarray ) > 0 ) {
    # Fehlermeldung ausgeben
    echo 'Folgendes Element wurde nicht entfernt:';
    pre( $teilarray );
	echo 'Es kommt in folgender Liste vor:';
	pre( $originalbackup_teilarray );
	echo 'Es kommt aber nicht in folgender Liste vor:';
	pre( $originalbackup_gesamtarray );
    fehlermeldung();
  }
  # RUECKGABE
  # Aus dem Gesamtarray die leeren Felder entfernen
  $rest_array = array_leere_elemente_entfernen( $gesamtarray );
  # Rest des Gesamtarrays (inzwischen ohne Teilarray) zurueckgeben
  return $rest_array;
}


#########################################################################################################
#
#  INS GESAMTARRAY EINTRAGEN
#
#########################################################################################################


# nur die ersten beiden Zeichen (fuer "lang"-Attribut) aus dem Sprachkuerzel heraus'extrahieren'
function sprachkuerzel_mit_nur_die_ersten_zwei_zeichen_eintragen( $gesamtarray ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $gesamtarray[$s]['sprachkuerzel_nur_zwei_zeichen'] = mb_substr( $gesamtarray[$s]['sprachkuerzel'] , 0 , 2 );
  }
  return $gesamtarray;
}

function eintragen_ob_alternate( $gesamtarray , $liste_aller_alternate_sprachcodes ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    foreach ( $liste_aller_alternate_sprachcodes as $alternate_sprachcode ) {
      if ( $gesamtarray[$s]['sprachkuerzel'] == $alternate_sprachcode ) {
        $gesamtarray[$s]['ist_alternate'] = 'rel="alternate" ';
      }
    }
  }
  return $gesamtarray;
}

# die deutsche Bezeichnung der Sprache eintragen
function deutsche_bezeichnung_eintragen( $gesamtarray , $liste_aller_sprachen ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_sprachen );
    $gesamtarray[$s]['deutsch'] = $liste_aller_sprachen[ ( $sprachkuerzelposition + 1 ) ];
  }
  return $gesamtarray;
}

# die englische Bezeichnung der Sprache eintragen
function englische_bezeichnung_eintragen( $gesamtarray , $liste_aller_sprachen ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_sprachen );
    $gesamtarray[$s]['englisch'] = $liste_aller_sprachen[ ( $sprachkuerzelposition + 2 ) ];
  }
  return $gesamtarray;
}

# die englische Bezeichnung der Sprache eintragen
function eigensprachliche_bezeichnung_eintragen( $gesamtarray , $liste_aller_sprachen ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_sprachen );
    $gesamtarray[$s]['in_eigener_sprache'] = $liste_aller_sprachen[ ( $sprachkuerzelposition + 3 ) ];
  }
  return $gesamtarray;
}

# eintragen, ob lateinische Buchstaben 
function eintragen_ob_lateinische_buchstaben( $gesamtarray , $liste_der_sprachcodes_nichtlateinischer_sprachen ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    
    # Schreibe in alle 'ist-latein'-Felder  "ja"  rein.
    $gesamtarray[$s]['ist_latein'] = 'ja';
    
	# Ueberschreibe die Felder der Sprachen mit "nein", die NICHT aus lateinsichen Buchstaben bestehen.
	
    # Gehe alle Elemente in der 'Liste der nicht-lateinischen Sprachcodes' durch
    for ( $i = 0 ; $i < count ( $liste_der_sprachcodes_nichtlateinischer_sprachen ) ; $i++ ) {
      if ( $liste_der_sprachcodes_nichtlateinischer_sprachen[$i] == $s ) {
        # Wenn es sich NICHT um eine Schrift mit lateinischen Buchstaben handelt
        $gesamtarray[$s]['ist_latein'] = 'nein';
      }
    }
  }
  
  return $gesamtarray;
}

# die englische Bezeichnung der Sprache eintragen
function beschreibungen_eintragen( $gesamtarray , $liste_aller_beschreibungen ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_beschreibungen );
    $gesamtarray[$s]['beschreibung'] = $liste_aller_beschreibungen[ ( $sprachkuerzelposition + 2 ) ];
  }
  return $gesamtarray;
}

# Sprunglinks (zum Inhalt) eintragen
function sprunglinks_eintragen( $gesamtarray , $liste_aller_sprunglinks ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_sprunglinks );
    $gesamtarray[$s]['sprunglink_zum_inhalt'] = $liste_aller_sprunglinks[ ( $sprachkuerzelposition + 2 ) ];
  }
  return $gesamtarray;
}

# Info (von Freunden erstellt) eintragen
function info_eintragen( $gesamtarray , $liste_aller_infotexte ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_infotexte );
    $gesamtarray[$s]['info'] = array(
      $liste_aller_infotexte[ ( $sprachkuerzelposition + 2 ) ],
      $liste_aller_infotexte[ ( $sprachkuerzelposition + 3 ) ],
      $liste_aller_infotexte[ ( $sprachkuerzelposition + 4 ) ],
      $liste_aller_infotexte[ ( $sprachkuerzelposition + 5 ) ]
    );
  }
  return $gesamtarray;
}

function datenschutz_eintragen( $gesamtarray , $liste_aller_datenschutztexte ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_datenschutztexte );
    $gesamtarray[$s]['datenschutz'] = array(
      $liste_aller_datenschutztexte[ ( $sprachkuerzelposition + 2 ) ],
      $liste_aller_datenschutztexte[ ( $sprachkuerzelposition + 3 ) ],
      $liste_aller_datenschutztexte[ ( $sprachkuerzelposition + 4 ) ],
      $liste_aller_datenschutztexte[ ( $sprachkuerzelposition + 5 ) ],
      $liste_aller_datenschutztexte[ ( $sprachkuerzelposition + 6 ) ],
      $liste_aller_datenschutztexte[ ( $sprachkuerzelposition + 7 ) ]
    );
  }
  return $gesamtarray;
}

# "aktualisiert"-Begriffe eintragen
function aktualisiert_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_navigation_sonderelemente );
    $gesamtarray[$s]['aktualisiert'] = $liste_aller_navigation_sonderelemente[ ( $sprachkuerzelposition + 5 ) ];
  }
  return $gesamtarray;
}

# "aktualisiert"-Begriffe eintragen
function startseite_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_navigation_sonderelemente );
    $gesamtarray[$s]['startseite'] = $liste_aller_navigation_sonderelemente[ ( $sprachkuerzelposition + 2 ) ];
  }
  return $gesamtarray;
}

# "Originalwebseiten"-Begriffe eintragen
function original_webseiten_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_navigation_sonderelemente );
    $gesamtarray[$s]['original'] = $liste_aller_navigation_sonderelemente[ ( $sprachkuerzelposition + 6 ) ];
  }
  return $gesamtarray;
}

# "Originalwebseite 1"-Begriffe eintragen
function original_webseite_1_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_navigation_sonderelemente );
    $gesamtarray[$s]['original_1'] = $liste_aller_navigation_sonderelemente[ ( $sprachkuerzelposition + 3 ) ];
  }
  return $gesamtarray;
}

# "Originalwebseite 2"-Begriffe eintragen
function original_webseite_2_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_navigation_sonderelemente );
    $gesamtarray[$s]['original_2'] = $liste_aller_navigation_sonderelemente[ ( $sprachkuerzelposition + 4 ) ];
  }
  return $gesamtarray;
}

# "aufklappen"-Begriffe eintragen
function aufklappen_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_navigation_sonderelemente );
    $gesamtarray[$s]['aufklappen'] = $liste_aller_navigation_sonderelemente[ ( $sprachkuerzelposition + 7 ) ];
  }
  return $gesamtarray;
}

# "aufklappen"-Begriffe eintragen
function untermenue_aufklappen_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_navigation_sonderelemente );
    $gesamtarray[$s]['untermenue_aufklappen'] = $liste_aller_navigation_sonderelemente[ ( $sprachkuerzelposition + 8 ) ];
  }
  return $gesamtarray;
}

# Email-Texte eintragen
function email_texte_eintragen( $gesamtarray , $liste_aller_emailtexte , $email_codierung ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $liste_aller_emailtexte );
    
    # Einleitungstext der Email
    $gesamtarray[$s]['email_1'] = $liste_aller_emailtexte[ ( $sprachkuerzelposition + 2 ) ];
    
    # VOR der Zeile mit dem @-Zeichen
    $zu_splitten = $liste_aller_emailtexte[ ( $sprachkuerzelposition + 3 ) ];
    $codewort = trim( $email_codierung[0] );
    $gesplittet = explode( $codewort , $zu_splitten );
    $gesamtarray[$s]['email_2'] = $gesplittet[0];
    $gesamtarray[$s]['email_4'] = $gesplittet[1];
    
    # BEI der Zeile mit dem @-Zeichen
    $zu_splitten = $liste_aller_emailtexte[ ( $sprachkuerzelposition + 4 ) ];
    $codewort = "@";
    $gesplittet = explode( $codewort , $zu_splitten );
    $gesamtarray[$s]['email_5'] = $gesplittet[0];
    $gesamtarray[$s]['email_7'] = $gesplittet[1];
    
    # Das Wort NACH dem @-Zeichen
    $zu_splitten = $liste_aller_emailtexte[ ( $sprachkuerzelposition + 5 ) ];
    $codewort = trim( $email_codierung[2] );
    $gesplittet = explode( $codewort , $zu_splitten );
    $gesamtarray[$s]['email_8'] = $gesplittet[0];
    $gesamtarray[$s]['email_10'] = $gesplittet[1];
    
    # Bei der Zeile mit dem BINDESTRICH (zwischen "liebetroepfchen" und "gottes")
    $zu_splitten = $liste_aller_emailtexte[ ( $sprachkuerzelposition + 6 ) ];
    $codewort = "-";
    $gesplittet = explode( $codewort , $zu_splitten );
    $gesamtarray[$s]['email_11'] = $gesplittet[0];
    $gesamtarray[$s]['email_13'] = $gesplittet[1];
    
    # Das Wort VOR dem .com
    $zu_splitten = $liste_aller_emailtexte[ ( $sprachkuerzelposition + 7 ) ];
    $codewort = trim( $email_codierung[4] );
    $gesplittet = explode( $codewort , $zu_splitten );
    $gesamtarray[$s]['email_14'] = $gesplittet[0];
    $gesamtarray[$s]['email_16'] = $gesplittet[1];
    
    # Bei der Zeile mit dem PUNKT von ".com"
    $zu_splitten = $liste_aller_emailtexte[ ( $sprachkuerzelposition + 8 ) ];
    # Finde Position des ERSTEN vorkommenden Punktes
    $position_von_punkt = mb_strpos( $zu_splitten , "." );
    $vorderer_teil_des_strings = mb_substr( $zu_splitten , 0 , $position_von_punkt );
    $hinterer_teil_des_strings = mb_substr( $zu_splitten , ( $position_von_punkt + 1 ) );
    $gesamtarray[$s]['email_17'] = $vorderer_teil_des_strings;
    $gesamtarray[$s]['email_19'] = $hinterer_teil_des_strings;
    
    # Das Wort NACH dem . (von ".com")
    $zu_splitten = $liste_aller_emailtexte[ ( $sprachkuerzelposition + 9 ) ];
    $codewort = trim( $email_codierung[6] );
    $gesplittet = explode( $codewort , $zu_splitten );
    $gesamtarray[$s]['email_20'] = $gesplittet[0];
    $gesamtarray[$s]['email_22'] = $gesplittet[1];
    
    # Die Kodierungs-Ersetzungen
    $gesamtarray[$s]['email_3'] = trim( $email_codierung[1] );
    $gesamtarray[$s]['email_9'] = trim( $email_codierung[3] );
    $gesamtarray[$s]['email_15'] = trim( $email_codierung[5] );
    $gesamtarray[$s]['email_21'] = trim( $email_codierung[7] );
    
    # Die Standard-Symbole ("@", "-", ".")
    $gesamtarray[$s]['email_6'] = "@";
    $gesamtarray[$s]['email_12'] = "-";
    $gesamtarray[$s]['email_18'] = ".";
    
  }
  return $gesamtarray;
}

# eintragen, ob internationales Datumsformat
function eintragen_ob_internationales_datumsformat( $gesamtarray , $liste_der_sprachcodes_der_sprachen_mit_internationalem_datumsformat ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    
    # Schreibe in alle 'internationales Datumsformat'-Felder  "nein"  rein.
    $gesamtarray[$s]['ist_datumformat_int'] = 'nein';
    
	# Ueberschreibe die Felder der Sprachen mit "ja", die das internationale Datumsformat verwenden.
    
    # Gehe alle Sprachkuerzel in der 'Liste der Sprachen mit internationalem Datumsformat' durch
    for ( $i = 0 ; $i < count ( $liste_der_sprachcodes_der_sprachen_mit_internationalem_datumsformat ) ; $i++ ) {
      if ( $liste_der_sprachcodes_der_sprachen_mit_internationalem_datumsformat[$i] == $s ) {
        # Wenn es sich um eine Sprache mit internationalem Datumsformat handelt
        $gesamtarray[$s]['ist_datumformat_int'] = 'ja';
      }
    }
    
  }
  return $gesamtarray;
}

# Datum neuester Themensammlung eintragen
function datum_neuester_themensammlung_eintragen( $gesamtarray , $themensammlungsdatum ) {
  foreach ( $gesamtarray as $s => $spracharray ) {
    # Wenn die aktuelle Sprache internationales Datumsformat verwendet
    if ( $gesamtarray[$s]['ist_datumformat_int'] == 'ja' ) {
      $gesamtarray[$s]['datum_themensammlung'] = $themensammlungsdatum['jahr'] . "-" . $themensammlungsdatum['monat'] . "-" . $themensammlungsdatum['tag'];
    }
    # Wenn die aktuelle Sprache das (u.a.) deutsche Datumsformat verwendet
    if ( $gesamtarray[$s]['ist_datumformat_int'] == 'nein' ) {
      $gesamtarray[$s]['datum_themensammlung'] = $themensammlungsdatum['tag'] . "." . $themensammlungsdatum['monat'] . "." . $themensammlungsdatum['jahr'];
    }
  }
  return $gesamtarray;
}

# Datum neuester Botschaft eintragen
function datum_neuester_botschaft_eintragen( $gesamtarray , $in_allen_sprachen_das_selbe_botschaften_datum , $liste_jeder_sprache_botschaftsdatum ) {
  
  # Wenn fuer die neueste Botschaft in jeder Sprache das SELBE Datum verwendet wird
  if ( $in_allen_sprachen_das_selbe_botschaften_datum == "ja" ) {
    $botschaft_tag   = $liste_jeder_sprache_botschaftsdatum[2];
    $botschaft_monat = $liste_jeder_sprache_botschaftsdatum[3];
    $botschaft_jahr  = $liste_jeder_sprache_botschaftsdatum[4];
    # Jede Sprache durchgehen
    foreach ( $gesamtarray as $s => $spracharray ) {
      # Wenn die aktuelle Sprache internationales Datumsformat verwendet
      if ( $gesamtarray[$s]['ist_datumformat_int'] == 'ja' ) {
        $gesamtarray[$s]['datum_botschaft'] = $botschaft_jahr . "-" . $botschaft_monat . "-" . $botschaft_tag;
      }
      # Wenn die aktuelle Sprache das (u.a.) deutsche Datumsformat verwendet
      if ( $gesamtarray[$s]['ist_datumformat_int'] == 'nein' ) {
        $gesamtarray[$s]['datum_botschaft'] = $botschaft_tag . "." . $botschaft_monat . "." . $botschaft_jahr;
      }
    }
  }
  
  # Wenn fuer die neueste Botschaft in jeder Sprache ihr EIGENES Datum verwendet wird
  if ( $in_allen_sprachen_das_selbe_botschaften_datum == "nein" ) {
    # Jede Sprache durchgehen
    foreach ( $gesamtarray as $s => $spracharray ) {
      
      # Index-Position aktueller Sprache in Datumsliste
      $sprachkuerzelposition_in_liste = array_search( $s , $liste_jeder_sprache_botschaftsdatum );
      
      $botschaft_tag   = $liste_jeder_sprache_botschaftsdatum[ ( $sprachkuerzelposition_in_liste + 2 ) ];
      $botschaft_monat = $liste_jeder_sprache_botschaftsdatum[ ( $sprachkuerzelposition_in_liste + 3 ) ];
      $botschaft_jahr  = $liste_jeder_sprache_botschaftsdatum[ ( $sprachkuerzelposition_in_liste + 4 ) ];
      
      # Wenn die aktuelle Sprache internationales Datumsformat verwendet
      if ( $gesamtarray[$s]['ist_datumformat_int'] == 'ja' ) {
        $gesamtarray[$s]['datum_botschaft'] = $botschaft_jahr . "-" . $botschaft_monat . "-" . $botschaft_tag;
      }
      # Wenn die aktuelle Sprache das (u.a.) deutsche Datumsformat verwendet
      if ( $gesamtarray[$s]['ist_datumformat_int'] == 'nein' ) {
        $gesamtarray[$s]['datum_botschaft'] = $botschaft_tag . "." . $botschaft_monat . "." . $botschaft_jahr;
      }
    }
  }
  
  return $gesamtarray;
}

# Anzahl Themensammlungen eintragen
function anzahl_themensammlungen_eintragen( $gesamtarray , $liste_der_themensammlungenreihenfolge , $verzeichnis_botschaften ) {
  global $liste_aller_botschaftsformate;
  $array_mit_deutschen_sprachbezeichnungen = hole_alle_deutschen_sprachbezeichnungen( $gesamtarray );
  sort( $liste_der_themensammlungenreihenfolge );
  
  # Gehe alle Botschaftsformate (docx, ...) durch
  for ( $k = 0 ; $k < count( $liste_aller_botschaftsformate ) ; $k++ ) {
    # Gehe alle (deutschsprachigen) Ordner der jeweiligen Sprache durch
    for ( $j = 0 ; $j < count( $array_mit_deutschen_sprachbezeichnungen ) ; $j++ ) {
      $liste_dateien_im_aktuellen_verzeichnis = array();
      
      $aktuelles_verzeichnis = $verzeichnis_botschaften . $liste_aller_botschaftsformate[$k] . "/" . $array_mit_deutschen_sprachbezeichnungen[$j];
      # Hole alle Dateinamen aus dem aktuellen Verzeichnis (ausser "." und ".."):
      if ( is_dir( $aktuelles_verzeichnis ) ) {
        if ( $handle = opendir( $aktuelles_verzeichnis ) ) {
          while ( ( $file = readdir( $handle ) ) !== false ) {
            if ( !( ( $file == "." ) or ( $file == ".." ) ) ) {
              # Nur die ersten 3 Ziffern
              $ersten_drei_ziffern = mb_substr( $file , 0 , 3 );
              $liste_dateien_im_aktuellen_verzeichnis[] = $ersten_drei_ziffern;
            }
          }
          closedir( $handle );
        }
      }
      sort( $liste_dateien_im_aktuellen_verzeichnis );
      
      # Ermittle alle Zahlen, die in BEIDEN "Listen", sowohl der Reihenfolgen-Nummern-Liste, als auch in den Dateinamen, vorkommen
      $in_beiden_listen_vorkommende_zahlen = array_intersect( $liste_dateien_im_aktuellen_verzeichnis , $liste_der_themensammlungenreihenfolge );
      $anzahl_in_in_beiden_listen_vorkommende_zahlen = count( $in_beiden_listen_vorkommende_zahlen );
      
      # Hole Kuerzel aus deutscher Sprachbezeichnung
      $s = hole_von_deutscher_sprachbezeichnung_dessen_sprachkuerzel( $array_mit_deutschen_sprachbezeichnungen[$j] );
      $gesamtarray[$s]['anzahl_themensammlungen'] = $anzahl_in_in_beiden_listen_vorkommende_zahlen;
	}
  }
  return $gesamtarray;
}

# Anzahl Botschaften eintragen
function anzahl_botschaften_eintragen( $gesamtarray , $liste_der_botschaftenreihenfolge , $verzeichnis_botschaften ) {
  global $liste_aller_botschaftsformate;
  $array_mit_deutschen_sprachbezeichnungen = hole_alle_deutschen_sprachbezeichnungen( $gesamtarray );
  sort( $liste_der_botschaftenreihenfolge );
  
  # Gehe alle Botschaftsformate (docx, ...) durch
  for ( $k = 0 ; $k < count( $liste_aller_botschaftsformate ) ; $k++ ) {
    # Gehe alle (deutschsprachigen) Ordner der jeweiligen Sprache durch
    for ( $j = 0 ; $j < count( $array_mit_deutschen_sprachbezeichnungen ) ; $j++ ) {
      $liste_dateien_im_aktuellen_verzeichnis = array();
      
      $aktuelles_verzeichnis = $verzeichnis_botschaften . $liste_aller_botschaftsformate[$k] . "/" . $array_mit_deutschen_sprachbezeichnungen[$j];
      # Hole alle Dateinamen aus dem aktuellen Verzeichnis (ausser "." und ".."):
      if ( is_dir( $aktuelles_verzeichnis ) ) {
        if ( $handle = opendir( $aktuelles_verzeichnis ) ) {
          while ( ( $file = readdir( $handle ) ) !== false ) {
            if ( !( ( $file == "." ) or ( $file == ".." ) ) ) {
              # Nur die ersten 3 Ziffern
              $ersten_drei_ziffern = mb_substr( $file , 0 , 3 );
              $liste_dateien_im_aktuellen_verzeichnis[] = $ersten_drei_ziffern;
            }
          }
          closedir( $handle );
        }
      }
      sort( $liste_dateien_im_aktuellen_verzeichnis );
      
      # Ermittle alle Zahlen, die in BEIDEN "Listen", sowohl der Reihenfolgen-Nummern-Liste, als auch in den Dateinamen, vorkommen
      $in_beiden_listen_vorkommende_zahlen = array_intersect( $liste_dateien_im_aktuellen_verzeichnis , $liste_der_botschaftenreihenfolge );
      $anzahl_in_in_beiden_listen_vorkommende_zahlen = count( $in_beiden_listen_vorkommende_zahlen );
      
      # Hole Kuerzel aus deutscher Sprachbezeichnung
      $s = hole_von_deutscher_sprachbezeichnung_dessen_sprachkuerzel( $array_mit_deutschen_sprachbezeichnungen[$j] );
      $gesamtarray[$s]['anzahl_botschaften'] = $anzahl_in_in_beiden_listen_vorkommende_zahlen;
	}
  }
  return $gesamtarray;
}


#########################################################################################################
#
#  INS NAVIGATIONS-URL-ARRAY EINTRAGEN
#
#########################################################################################################


function button_arrays_erschaffen( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache ) {
  $aktueller_sprachblock = hole_navigationsblock_aktueller_sprache( $liste_aller_navigation_standardseiten , $sprachkuerzel , $anzahl_zeilen_je_sprache );
  
  # Die ersten beiden "Sprach"-Zeilen werden entfernt
  $aktueller_sprachblock = array_slice( $aktueller_sprachblock , 2 );
  
  # Gehe alle 'Button'-Zeilen im Block der aktuellen Sprache durch
  # Fuer jeden Button ein eigenes "Array" erschaffen
  for( $i = 0 ;  $i < count( $aktueller_sprachblock ) ; $i++ ) {
    $navigations_url_array[$sprachkuerzel][] = array();
  }
  return $navigations_url_array;
}

function felder_fuer_buttonarrays_erzeugen( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache ) {
  $aktueller_sprachblock = hole_navigationsblock_aktueller_sprache( $liste_aller_navigation_standardseiten , $sprachkuerzel , $anzahl_zeilen_je_sprache );
  
  # Die ersten beiden "Sprach"-Zeilen werden entfernt
  $aktueller_sprachblock = array_slice( $aktueller_sprachblock , 2 );
  
  # Gehe alle 'Button'-Zeilen im Block der aktuellen Sprache durch
  # In den einzelnen Buttons die jeweiligen Felder erschaffen
  for( $i = 0 ;  $i < count( $aktueller_sprachblock ) ; $i++ ) {
    $navigations_url_array[$sprachkuerzel][$i]['reihenfolge_button'] = "";
    $navigations_url_array[$sprachkuerzel][$i]['beschriftung_button'] = "";
    $navigations_url_array[$sprachkuerzel][$i]['reihenfolge_url'] = "";
    $navigations_url_array[$sprachkuerzel][$i]['beschriftung_url'] = "";
    $navigations_url_array[$sprachkuerzel][$i]['button_typ'] = "";
  }
  return $navigations_url_array;
}

function untermenuebuttons_deklarieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache ) {
  $aktueller_sprachblock = hole_navigationsblock_aktueller_sprache( $liste_aller_navigation_standardseiten , $sprachkuerzel , $anzahl_zeilen_je_sprache );
  
  # Die ersten beiden "Sprach"-Zeilen werden entfernt
  $aktueller_sprachblock = array_slice( $aktueller_sprachblock , 2 );
  
  # Gehe alle 'Button'-Zeilen im Block der aktuellen Sprache durch
  # Untermenue-Buttons als Typ  "unter"  deklarieren
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    # Wenn die ersten beiden 'Zeichen' *Leerzeichen*  ("  ")  sind, dann handelt es sich um ein Untermenue-Button
    if ( mb_substr( $aktueller_sprachblock[$i] , 0 , 2 ) == "  " ) {
      # Als 'Untermenue-Button' deklarieren: [Typ] = "unter"
      $navigations_url_array[$sprachkuerzel][$i]['button_typ'] = "unter";
    }
  }
  return $navigations_url_array;
}

function aufklappbuttons_deklarieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache ) {
  $aktueller_sprachblock = hole_navigationsblock_aktueller_sprache( $liste_aller_navigation_standardseiten , $sprachkuerzel , $anzahl_zeilen_je_sprache );
  
  # Die ersten beiden "Sprach"-Zeilen werden entfernt
  $aktueller_sprachblock = array_slice( $aktueller_sprachblock , 2 );
  
  # Gehe alle 'Button'-Zeilen im Block der aktuellen Sprache durch
  # Aufklapp-Buttons als Typ  "aufklapp"  deklarieren
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    # Wenn der 'aktuelle' Button vom Typ "unter" ist, ...
    if ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "unter" ) {
      # und der *vorherige* "leer" ist, also es sich um ein AUFKLAPP-Button handeln muss, ...
      if ( $navigations_url_array[$sprachkuerzel][ ( $i - 1 ) ]['button_typ'] == "" ) {
        # dann als "Aufklapp-Button" deklarieren: [Typ] = "aufklapp"
        $navigations_url_array[$sprachkuerzel][ ( $i - 1 ) ]['button_typ'] = "aufklapp";
      }
    }
  }
  return $navigations_url_array;
}

function hauptmenuebuttons_deklarieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache ) {
  $aktueller_sprachblock = hole_navigationsblock_aktueller_sprache( $liste_aller_navigation_standardseiten , $sprachkuerzel , $anzahl_zeilen_je_sprache );
  
  # Die ersten beiden "Sprach"-Zeilen werden entfernt
  $aktueller_sprachblock = array_slice( $aktueller_sprachblock , 2 );
  
  # Gehe alle 'Button'-Zeilen im Block der aktuellen Sprache durch
  # Hauptmenue-Buttons als Typ  "haupt"  deklarieren
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    # Da ja alle Untermenue- und Aufklapp-Buttons schon "belegt" sind,
    # wenn das 'Feld'  "button_typ"  noch leer sein sollte, kann es sich nur noch um einen Hauptmenue-Button handeln.
    if ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "" ) {
      # Als 'Hauptmenue-Button' deklarieren: [Typ] = "haupt"
      $navigations_url_array[$sprachkuerzel][$i]['button_typ'] = "haupt";
    }
  }
  return $navigations_url_array;
}

function buttons_durchnummerieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache ) {
  $aktueller_sprachblock = hole_navigationsblock_aktueller_sprache( $liste_aller_navigation_standardseiten , $sprachkuerzel , $anzahl_zeilen_je_sprache );
  
  # Die ersten beiden "Sprach"-Zeilen werden entfernt
  $aktueller_sprachblock = array_slice( $aktueller_sprachblock , 2 );
  
  # Gehe alle 'Button'-Zeilen im Block der aktuellen Sprache durch
  # Buttons in ihrer Reihenfolge durchnummerieren (beginnend bei '1')
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    $navigations_url_array[$sprachkuerzel][$i]['reihenfolge_button'] = $i + 1;
  }
  return $navigations_url_array;
}

function button_beschriftungen_eintragen( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache ) {
  $aktueller_sprachblock = hole_navigationsblock_aktueller_sprache( $liste_aller_navigation_standardseiten , $sprachkuerzel , $anzahl_zeilen_je_sprache );
  
  # Die ersten beiden "Sprach"-Zeilen werden entfernt
  $aktueller_sprachblock = array_slice( $aktueller_sprachblock , 2 );
  
  # Gehe alle 'Button'-Zeilen im Block der aktuellen Sprache durch
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    # Button-Bezeichnungen in ihr jeweiliges Feld  "beschriftung_button"  eintragen.
    $navigations_url_array[$sprachkuerzel][$i]['beschriftung_button'] = trim( $aktueller_sprachblock[$i] );
  }
  return $navigations_url_array;
}

function url_links_durchnummerieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache ) {
  $aktueller_sprachblock = hole_navigationsblock_aktueller_sprache( $liste_aller_navigation_standardseiten , $sprachkuerzel , $anzahl_zeilen_je_sprache );
  
  # Die ersten beiden "Sprach"-Zeilen werden entfernt
  $aktueller_sprachblock = array_slice( $aktueller_sprachblock , 2 );
  
  $anzahl_aufklappbuttons = 0;
  
  # Gehe alle 'Button'-Zeilen im Block der aktuellen Sprache durch
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    # Falls es sich um KEIN Aufklapp-Button handelt (dann *handelt* es sich um einen Link ...)
    if ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] !== "aufklapp" ) {
      # Reihenfolge des "Links" eintragen. Erster gueltiger "Link" faengt mit der Ziffer "2" an ( "1" waere die 'Startseite')
      # Aufklapp-Buttons duerfen nicht hinzugezaehlt werden.
      $navigations_url_array[$sprachkuerzel][$i]['reihenfolge_url'] = $i + 2 - $anzahl_aufklappbuttons;
    } else {
      # WENN es sich um einen Aufklapp-Button handelt
      # erhoehe den Zaehler fuer "Aufklapp-Buttons"
      $anzahl_aufklappbuttons++;
      $navigations_url_array[$sprachkuerzel][$i]['reihenfolge_url'] = "";
    }
  }
  return $navigations_url_array;
}

#-------------------------------------------------------------
# Fuer "sprechende URL" alle 'Sonderzeichen' ersetzen
#-------------------------------------------------------------

function liste_der_positionen_deutscher_sprachbezeichnungen_erstellen( $array_deutsch , $array_sonderzeichen ) {
  $liste_der_positionen_deutscher_sprachbezeichnungen = array();
  $anzahl_deutsche_elemente = count( $array_deutsch );
  $anzahl_sonderzeichen_elemente = count( $array_sonderzeichen );
  # Alle Positionen ermitteln, in denen *deutsche* Bezeichnungen von Sprachen vorkommen. Oder der Begriff "alle".
  for ( $i = 0 ; $i < $anzahl_deutsche_elemente ; $i++ ) {
    for ( $j = 0 ; $j < $anzahl_sonderzeichen_elemente ; $j++ ) {
      if ( $array_deutsch[$i] == $array_sonderzeichen[$j] ) {
        $liste_der_positionen_deutscher_sprachbezeichnungen[] = $j;
      }
    }
  }
  return $liste_der_positionen_deutscher_sprachbezeichnungen;
}

function ueberpruefen_ob_anzahl_sonderzeichen_paarweise_durch_zwei_teilbar( $position_deutscher_bezeichnungen , $sonderzeichenliste ) {
  # Der Abstand zwischen den Bezeichnungen in dt. Sprache muss ermittelt werden. Ob diese eine 'glatte', *paarweise*, Zahl sind.
  # Deshalb wird die Laenge des Sonderzeichen-Arrays hintendrangehaengt.
  $position_deutscher_bezeichnungen[] = count( $sonderzeichenliste );
  # Nun braucht man nur noch im position_deutscher_bezeichnungen-Array die *Abstaende* zwischen
  # den einzelnen "Positionen" auf "Durch-2-Teilbarkeit" zu pruefen:
  # Ueberpruefung, ob an 1. Position eine "Sprachbezeichnung" steht:
  # Wenn ein deutschsprachige Sprachbezeichnung - ganz am Anfang - fehlt (zumindest "alle"
  # sollte dastehen), dann Fehlermeldung:
  if ( $position_deutscher_bezeichnungen[0] !== 0 ) {
    echo "In der Liste der zu ersetzenden Sonderzeichen muss vermerkt werden, bezogen auf *WELCHE* ";
    echo "Sprachen die Sonderzeichen 'anzuwenden sind'.<br> Diese Sprachbezeichnungen müssen immer ";
    echo 'VOR den "Sonderzeichen" stehen, damit diese Sonderzeichen sich auf *DIESE* Sprachen beziehen.<br>';
    echo "Ergo muss ganz am *Anfang der Liste* eine 'Sprachbezeichnung' stehen.<br>";
    echo '(Es dürfte auch das Wort  "alle"  (kleingeschrieben) an erster Stelle stehen, sofern man möchte, ';
    echo "dass alle Zeichenumwandlungen in allen Sprachen durchgeführt werden sollen.)<br>";
    echo 'Erlaubt sind somit deutsche Sprachbezeichnungen aus der "Liste aller Sprachen", und das Wort "alle".<br>';
    echo 'An erster Stelle befindet sich aber stattdessen das Zeichen: "' . $sonderzeichenliste[0] . '"<br>';
    echo "In folgender Liste:";
    pre( $sonderzeichenliste );
    fehlermeldung();
  }
  # Wenn an *letzter* Position in der Sonderzeichenliste eine "Bezeichnung auf deutsch" dasteht, macht das
  # keinen Sinn, weil es keine darauffolgenden 'Sonderzeichen' gibt, die auf die Sprache(n) Anwendung finden wuerden.
  # Die "letzte" Position vermerkt die 'Pseudo'-Position NACH dem letzten Element,
  # die "vor"-letzte Position vermerkt *tatsaechlich* die letzte Position der Sprachbezeichnung.
  $letzte_position = ( count( $position_deutscher_bezeichnungen ) - 1 ) ;
  $vorletzte_position = $letzte_position - 1;
  if ( $position_deutscher_bezeichnungen[$vorletzte_position] == ( ( $position_deutscher_bezeichnungen[$letzte_position] ) - 1 ) ) {
    echo "In der Liste der zu ersetzenden Sonderzeichen muss vermerkt werden, bezogen auf *WELCHE* Sprachen ";
    echo "die Sonderzeichen 'anzuwenden sind'.<br>Diese Sonderzeichen müssen immer auf die Sprachbezeichnungen ";
    echo "FOLGEN, daher macht eine 'Sprachbezeichnung' ganz am *Ende* der Liste keinen Sinn.<br>";
    echo 'An letzter Stelle befindet sich die Sprachbezeichnung "' . $sonderzeichenliste[ ( count( $sonderzeichenliste ) - 1 ) ] . '"<br>';
    echo "In folgender Liste:";
    pre( $sonderzeichenliste );
    fehlermeldung();
  }
  # Sind die Abstaende zwischen den 'Bezeichnungen in deutsch' glatt durch 2 teilbar? Wg. 'von -> zu'-Ersetzung
  for ( $i = 0 ; $i < ( count( $position_deutscher_bezeichnungen ) - 1 ) ; $i++ ) {
    # Die Anzahl ermitteln, die "ZWISCHEN" den jeweiligen deutsch-sprachigen Bezeichnern liegt,
    # ob die Anzahl durch ZWEI teilbar ist, und somit ein "zur-Ersetzung-faehiges-Paar" vorhanden ist.
    # Abstand zwischen 2 einzelnen deutschen Bezeichnungen ermitteln:
    $anzahl_an_elementen_jeweils_zwischen_deutschen_sprachbezeichnungen_in_der_sonderzeichenliste = $position_deutscher_bezeichnungen[ ( $i + 1 ) ] - $position_deutscher_bezeichnungen[$i] - 1 ;
    # Falls der Abstand nicht durch 2 teilbar sein sollte, dann Fehlermeldung:
    if ( $anzahl_an_elementen_jeweils_zwischen_deutschen_sprachbezeichnungen_in_der_sonderzeichenliste % 2 == 1 ) {
	echo "In der 'Liste der zu ersetzenden Sonderzeichen' müssen die Sonderzeichen immer PAARWEISE auftreten.<br>";
    echo "Da ja EIN Sonderzeichen durch ein ANDERES ersetzt werden soll.<br>";
    echo "In der folgenden Liste befinden sich aber zwischen ";
      if ( ( $i + 1 ) == ( count( $position_deutscher_bezeichnungen ) - 1 ) ) {
        echo "dem Wort " . $sonderzeichenliste[$position_deutscher_bezeichnungen[$i]] . " in Zeile ";
        echo $position_deutscher_bezeichnungen[$i] . " und dem *Ende* der Liste ";
      } else {
        echo "den Wörtern " . $sonderzeichenliste[$position_deutscher_bezeichnungen[$i]] . " ";
        echo "in Zeile " . $position_deutscher_bezeichnungen[$i] . " und ";
        echo $sonderzeichenliste[$position_deutscher_bezeichnungen[ ( $i + 1 ) ]] . " in Zeile ";
        echo $position_deutscher_bezeichnungen[ ( $i + 1 ) ] . " ";
      }
      echo "eine UNGERADE Anzahl an 'Sonderzeichen'.<br>";
      echo "Hier die Liste:";
      pre( $sonderzeichenliste );
      fehlermeldung();
    }
  }
}

function array_mit_sprachen_und_deren_zu_ersetzende_sonderzeichen_erstellen( $position_sprachbezeichnungen , $sonderzeichenliste , $liste_sprachcodes ) {
  #
  # Diese Funktion ist 2-geteilt.
  # Der erste Teil sortiert die Daten aus der "uebersetzungsdaten.txt" strukturiert in eine Liste ein,
  # der zweite Teil strukturiert die Daten in dem Array *neu*. So dass es einfacher vom Computer "verarbeitbar/lesbar" ist.
  #
  # Erster Teil:
  #
  $anzahl_sonderzeichenpaare_in_einem_block = array();
  $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken = array();
  # Struktur:
  # In der 'Liste zu ersetzender Sonderzeichen' kommen immer zuerst entweder eine oder mehrere 'Sprachbezeichnungen'.
  # Darunter dann ein "Block" von auf *diese* Sprachen anzuwendenden Sonderzeichen-'Paare'.
  #
  # Jede Sprache bildet mit dem unter ihm befindlichen Sonderzeichen-Paare-Block ein EIGENSTAENDIGES ARRAY.
  # Wenn in der Liste z.B. steht:
  #
  #  "deutsch",
  #  "englisch",
  #  "ä", "ae",
  #  "ö", "ue",
  #  "alle"
  #  "ß", "ss"
  #
  # dann ist:
  #
  # Block 1:
  #   "deutsch",
  #     "ä", "ae",
  #     "ö", "oe",
  #
  # Block 2:
  #   "englisch",
  #     "ä", "ae",
  #     "ö", "oe",
  #
  # Block 3:
  #   "alle",
  #     "ß", "ss"
  #
  #
  # Das 'Gesamt-Array' hat folgenden Aufbau:
  #
  # $gesamtarray = array(
  #   array( "deutsch"  , "ä" , "ae" , "ö" , "oe" )
  #   array( "englisch" , "ä" , "ae" , "ö" , "oe" )
  #   array( "alle"     , "ß" , "ss" )
  # );
  #
  # Dadurch ist es ganz einfach, die Umaenderung der Seitenbezeichnungen fuer URL-konforme Lesbarkeit durchzufuehren.
  #
  # Gesamt-Array erstellen
  $array_zu_ersetzender_sonderzeichen = array();
  # Arrays innerhalb des Gesamtarrays erstellen, soviel wie Sprachbezeichnungen da sind.
  for ( $i = 0 ; $i < count( $position_sprachbezeichnungen ) ; $i++ ) {
    $array_zu_ersetzender_sonderzeichen[] = array( $sonderzeichenliste[$position_sprachbezeichnungen[$i]] );
  }
  # Positionen ermitteln, von wo bis wohin die 'jeweiligen' Sonderzeichenpaar-Blöcke gehen (ohne Sprachen).
  # Die Positionen der Sprachbezeichnungen kennzeichen den Anfang und das Ende von Sonderzeichenpaar-Blöcken.
  # Da der letzte Sonderzeichenpaar-Block da endet, wo auch die Sonderzeichenliste endet, wird die 'kuenstliche'
  # Position *dahinter*, *hinter* dem letzen Sonderzeichen, ebenfalls in der 'Position Sprachbezeichnungen'-Liste vermerkt.
  $position_sprachbezeichnungen[] = count( $sonderzeichenliste );
  # Abstaende zwischen den einzelnen Sprachbezeichnern berechnen
  for ( $i = 0 ; $i < ( count( $position_sprachbezeichnungen ) - 1 ) ; $i++ ) {
    $anzahl_sonderzeichenpaare_in_einem_block[] = $position_sprachbezeichnungen[ ( $i + 1 ) ] - $position_sprachbezeichnungen[$i] - 1 ;
  }
  # Erstes und letztes Sonderzeichen jedes einzelnen "Blocks" in ein Array rein
  for ( $i = 0 ; $i < count( $anzahl_sonderzeichenpaare_in_einem_block ) ; $i++ ) {
    # Wenn das Feld der Sprachbezeichnung 0 Abstand zur naechsten Sprachbezeichnung hat,
    # wird es mit "0" gefuellt, und spaeter "nachgetragen".
    if ( $anzahl_sonderzeichenpaare_in_einem_block[$i] == 0 ) {
      # Eine "0" fuer die Anfangsposition,
      $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[] = "0";
      # Eine "0" fuer die Endposition.
      $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[] = "0";
    } else {
      # Die Anfangsposition (jetztige Position + 1) wird hinzugefuegt
      $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[] = $position_sprachbezeichnungen[$i] + 1;
      # Die Endposition (jetztige Position + Wert im Feld) wird hinzugefuegt
      $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[] = $position_sprachbezeichnungen[$i] + $anzahl_sonderzeichenpaare_in_einem_block[$i];
    }
  }
  # Wenn Sprachbezeichnungen aufeinanderfolgten, wurden die Werte aller bis auf die "letzte" Sprachbezeichnung in der Reihe mit "Nullen" gefuellt.
  # Diese werden hier nachgetragen:
  for ( $i = ( ( count( $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken ) / 2 ) - 1 ) ; $i > -1 ; --$i ) {
    # Falls eine Position "0" sein sollte, wird einfach der Wert von 2 Positionen weiter unten geholt
    if ( $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[ ( $i * 2 ) ] == "0" ) { $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[ ( $i * 2 ) ] = $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[ ( ( $i * 2 ) + 2 ) ]; }
    if ( $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[ ( ( $i * 2 ) + 1 ) ] == "0" ) { $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[ ( ( $i * 2 ) + 1 ) ] = $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[ ( ( ( $i * 2 ) + 1 ) + 2 ) ]; }
  }
  # Jede Sprachbezeichnung bekommt 'IHREN' Block an Sonderzeichen in ihr 'Array' hinzugefuegt, hintendran.
  for ( $i = 0 ; $i < ( ( count( $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken ) ) / 2 ) ; $i++ ) {
    # Jedes einzelne Sonderzeichen je Sprache hinzufuegen
    for ( $j = $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[ ( $i * 2 ) ] ; $j < ( $liste_anfangs_und_endpositionen_von_sonderzeichen_bloecken[ ( ( $i * 2 ) + 1 ) ] + 1 ) ; $j++ ) {
      $array_zu_ersetzender_sonderzeichen[$i][] = $sonderzeichenliste[$j];
    }
  }
  #
  # Zweiter Teil:
  #
  $anzahl_bloecke_im_array_sonderzeichen = count( $array_zu_ersetzender_sonderzeichen );
  # Sprachkuerzel der deutschen Bezeichnung finden
  for ( $i = 0 ; $i < $anzahl_bloecke_im_array_sonderzeichen ; $i++ ) {
    $deutsche_bezeichnung = $array_zu_ersetzender_sonderzeichen[$i][0];
    $aktuelles_sprachkuerzel = hole_von_deutscher_sprachbezeichnung_dessen_sprachkuerzel( $deutsche_bezeichnung );
    # Falls es sich um die Bezeichnung "alle" handeln sollte
    if ( $deutsche_bezeichnung == "alle" ) {
      for ( $j = 0 ; $j < count( $liste_sprachcodes ) ; $j++ ) {
        # Hintendranhaengen der 'zu Ersetzen'-Zeile ans 'Gesamtarray'
        $array_zu_ersetzender_sonderzeichen[] = $array_zu_ersetzender_sonderzeichen[$i];
        # In das ERSTE Feld des - hier - neu hinzugefuegten Arrays kommt das *Sprachkuerzel* rein
        $array_zu_ersetzender_sonderzeichen[ ( count( $array_zu_ersetzender_sonderzeichen ) - 1 ) ][0] = $liste_sprachcodes[$j];
      }
    } else {
      # Falls es sich um eine normale Sprachbezeichnung in 'deutsch' handelt statt dem Begriff "alle"
      # Hintendranhaengen der 'zu Ersetzen'-Zeile ans 'Gesamtarray'
      $array_zu_ersetzender_sonderzeichen[] = $array_zu_ersetzender_sonderzeichen[$i];
      # In das ERSTE Feld des - hier - neu hinzugefuegten Arrays kommt das *Sprachkuerzel* rein
      $array_zu_ersetzender_sonderzeichen[ ( count( $array_zu_ersetzender_sonderzeichen ) - 1 ) ][0] = $aktuelles_sprachkuerzel;
    }
  }
  # Die richtig "codierten" weiter zu bearbeitenden Bloecke wurden ans Array 'hintendrangehaengt'.
  # Da die originalen Bloecke am Anfang *weiterhin* vorhanden sind, nun ueberfluessigerweise, werden diese entfernt.
  $array_zu_ersetzender_sonderzeichen = array_slice( $array_zu_ersetzender_sonderzeichen , $anzahl_bloecke_im_array_sonderzeichen );
  return $array_zu_ersetzender_sonderzeichen;
}

function url_konforme_liste_erstellen_nichtlateinische_sprachen( $url_beschriftungen , $liste_sprachcodes_nichtlateinischer_sprachen , $navigations_url_array ) {
  global $anzahl_sprachen;
  $anzahl_zeilen_pro_sprache = count( $url_beschriftungen ) / $anzahl_sprachen;
  # Gehe alle Sprachen (in der Liste) nacheinander durch 
  for ( $i = 0 ; $i < $anzahl_sprachen ; $i++ ) {
    # Gehe alle Sprachcodes (in der Liste der nichtlateinischen Sprachen) nacheinander durch
    for ( $j = 0 ; $j < count( $liste_sprachcodes_nichtlateinischer_sprachen ) ; $j++ ) {
      # Wenn die 'aktuelle' Sprache in der Liste mit der in der Liste nicht-lateinischer Sprachcodes uebereinstimmt ...
      if ( $url_beschriftungen[ ( $i * $anzahl_zeilen_pro_sprache ) ] == $liste_sprachcodes_nichtlateinischer_sprachen[$j] ) {
        # Wenn es sich um ein Hauptmenue- oder Untermenue-Button handelt, dann durchnummerieren
		# Variable, die sich merkt, wieviele "Aufklapp-Buttons" bisher vorkamen
		$l = 0;
        # Gehe alle Label-Bezeichnungen der Sprache ab der 3. Position (ersten beiden sind "Sprachbezeichnungen") einzeln durch
        for ( $k = 2 ; $k < ( $anzahl_zeilen_pro_sprache ) ; $k++ ) {
          # Wenn DIESER Button in der 'Navigations-URL-Array' den Buttontyp-Vermerk  "aufklapp"  hat
          if ( $navigations_url_array[$liste_sprachcodes_nichtlateinischer_sprachen[$j]][ ( $k - 2 ) ]['button_typ'] == "aufklapp" ) {
            # dann: da der 'Aufklapp-Button' keine URL-Beschriftung hat, leeren, ...
            $url_beschriftungen[ ( ( $i * $anzahl_zeilen_pro_sprache ) + $k ) ] = "";
            # ... "Anzahl bisher vorgekommener Aufklapp-Buttons" um 1 erhoehen und zum naechsten
            $l = $l + 1;
          } else {
            # Wenn es sich um ein Haupt- oder Untermenue-Button handelt
            # die Buttonbezeichnung wird fuer die URL-Konformitaet umbenannt in *Zahlen* (bei nicht-lateinischen Sprachen)
            $url_beschriftungen[ ( ( $i * $anzahl_zeilen_pro_sprache ) + $k ) ] = $k - $l;
          }
        }
      }
    }
  }
  return $url_beschriftungen;
}

function url_konforme_liste_erstellen_lateinische_sprachen( $url_beschriftungen , $liste_sprachcodes_lateinischer_sprachen , $array_zu_ersetzender_sonderzeichen ) {
  global $anzahl_sprachen;
  $anzahl_zeilen_pro_sprache = count( $url_beschriftungen ) / $anzahl_sprachen;
  # Zeichen ersetzen
  # jeden einzelnen 'Sprachkuerzel-Sonderzeichenpaar'-Block nacheinander durchgehen
  for ( $i = 0 ; $i < count ( $array_zu_ersetzender_sonderzeichen ) ; $i++ ) {
    # Alle einzelnen Sprach(-kuerzel) in der URL-Liste auf Uebereinstimmung mit dem Sprachkuerzel pruefen
    for ( $j = 0 ; $j < $anzahl_sprachen ; $j++ ) {
      if ( $url_beschriftungen[ ( $anzahl_zeilen_pro_sprache * $j ) ] == $array_zu_ersetzender_sonderzeichen[$i][0] ) {
        # Wenn man die richtige Sprache gefunden hat, in der die Sonderzeichen ersetzt werden sollen, ...
        # Die Zeilen dieser Sprache (ausser die ersten beiden - sind nur Sprachbezeichnungen -) raussuchen, ...
        for ( $k = 2 ; $k < $anzahl_zeilen_pro_sprache ; $k++ ) {
          # Auf jede einzelne dieser (Button-Beschriftungs-)Zeichen *alle* Sonderzeichen-"Paare" 'anwenden'.
          # Jedes einzelne "Sonderzeichenpaar" durchgehen ...
          for ( $l = 1 ; $l < count( $array_zu_ersetzender_sonderzeichen[$i] ) ; $l = $l + 2 ) {
            # In der Zeile: Ersetze das 'Sonder'-Zeichen durch das 'entschaerfte'.
            $url_beschriftungen[ ( $anzahl_zeilen_pro_sprache * $j + $k ) ] = str_replace( $array_zu_ersetzender_sonderzeichen[$i][$l] , $array_zu_ersetzender_sonderzeichen[$i][$l + 1] , $url_beschriftungen[ ( $anzahl_zeilen_pro_sprache * $j + $k ) ] );
          }
        }
      }
    }
  }
  # trimmen (Leerzeichen am Anfang und Ende entfernen)
  for ( $i = 0 ; $i < count( $url_beschriftungen ) ; $i ++ ) {
    $url_beschriftungen[$i] = trim( $url_beschriftungen[$i] );
  }
  # Alle Zeichen, die NICHT:   a bis z   oder   A bis Z   oder   0 bis 9   oder   - (Bindestrich)   sind, durch Bindestrich ersetzen.
  for ( $i = 0 ; $i < count( $url_beschriftungen ) ; $i ++ ) {
    $url_beschriftungen[$i] = preg_replace( "/([^a-zA-Z0-9]+)/" , "-" , $url_beschriftungen[$i] );
  }
  # Kleinbuchstaben
  for ( $i = 0 ; $i < count( $url_beschriftungen ) ; $i ++ ) {
    $url_beschriftungen[$i] = mb_strtolower( $url_beschriftungen[$i] , 'UTF-8' );
  }
  return $url_beschriftungen;
}

function url_bezeichnungen_ins_navigations_array_eintragen( $navigations_url_array , $url_liste_aller_navigation_standardseiten ) {
  # Gehe alle Sprachen im Gesamtarray durch
  foreach ( $navigations_url_array as $s => $buttonarray ) {
    $sprachkuerzelposition = hole_position_sprachkuerzel( $s , $url_liste_aller_navigation_standardseiten );
    for ( $i = 0 ; $i < count ( $navigations_url_array[$s] ) ; $i++ ) {
      $navigations_url_array[$s][$i]['beschriftung_url'] = $url_liste_aller_navigation_standardseiten[ ( $sprachkuerzelposition + 2 + $i ) ];
    }
  }
  return $navigations_url_array;
}

function aufklapp_buttons_leeren( $navigations_url_array ) {
  # Gehe alle Sprachen im Gesamtarray durch
  foreach ( $navigations_url_array as $s => $buttonarray ) {
    # Gehe alle Buttons einer Sprache einzeln durch
    for ( $i = 0 ; $i < count( $navigations_url_array[$s] ) ; $i++ ) {
      # Wenn es sich um ein "Aufklapp-Button" handelt
      if ( $navigations_url_array[$s][$i]['button_typ'] == "aufklapp" ) {
        # Das Feld 'URL-Beschriftung' leeren, da ein Aufklapp-Button keine aufrufbare URL hat
        $navigations_url_array[$s][$i]['beschriftung_url'] = "";
      }
    }
  }
  return $navigations_url_array;
}


#########################################################################################################
#
#  IN BOTSCHAFTEN-ARRAYS EINTRAGEN
#
#########################################################################################################


function botschaften_beschriftungen_und_dateinamen_eintragen( $liste_botschaften_beschriftungen_und_dateinamen , $verzeichnis_label_beschriftungen , $verzeichnis_dateinamen , $liste_der_botschaftenreihenfolge , $verzeichnis_botschaften ) {
  global $gesamtarray;
  global $liste_aller_sprachcodes;
  $zwischenspeicher_array = array();
  $array_mit_deutschen_sprachbezeichnungen = hole_alle_deutschen_sprachbezeichnungen( $gesamtarray );
  
  # Arrays fuer die jeweiligen Sprachen anlegen
  for ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
    $liste_botschaften_beschriftungen_und_dateinamen[ $liste_aller_sprachcodes[$i] ] = array();
	$zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] = array();
  }
  
  # Labelbeschriftungen reinladen und "reinigen"
  for ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
    # Label-Beschriftungen in die jeweilige Sprache reinladen
    $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] = file( $verzeichnis_label_beschriftungen . $liste_aller_sprachcodes[$i] . ".txt" );
    # gespeichertes Array 'reinigen' (von Leerzeilen, Kommentarzeilen, Zeilenende-Kodierungen)
    $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] = array_sprachtextdatei_bereinigen( $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] );
    $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] = array_leere_elemente_entfernen( $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] );
  }
  
  # Gehe alle (deutschsprachigen) Ordner der jeweiligen Sprache durch
  for ( $i = 0 ; $i < count( $array_mit_deutschen_sprachbezeichnungen ) ; $i++ ) {
    $liste_der_zahlen_aller_botschaften_dieser_sprache = array();
    
    $aktuelles_verzeichnis = $verzeichnis_botschaften . "docx/" . $array_mit_deutschen_sprachbezeichnungen[$i];
    # Hole alle Botschaften-Dateinamen aus dem aktuellen Verzeichnis/Sprache (ausser "." und ".."):
    if ( is_dir( $aktuelles_verzeichnis ) ) {
      if ( $handle = opendir( $aktuelles_verzeichnis ) ) {
        while ( ( $file = readdir( $handle ) ) !== false ) {
          if ( !( ( $file == "." ) or ( $file == ".." ) ) ) {
            # Nur die ersten 3 Ziffern
            $ersten_drei_ziffern = mb_substr( $file , 0 , 3 );
            $liste_der_zahlen_aller_botschaften_dieser_sprache[] = $ersten_drei_ziffern;
          }
        }
        closedir( $handle );
      }
    }
    sort( $liste_der_zahlen_aller_botschaften_dieser_sprache );
    
    # Array, welches alle Zahlen beinhaltet, die 'sowohl' als Botschaft, als auch in der 'Reihenfolgenliste' vorkommt
    $in_beiden_listen_vorkommende_zahlen = array();
    
    # Ermittle alle Zahlen, die in BEIDEN "Listen", sowohl der Reihenfolgen-Nummern-Liste, als auch in den Dateinamen, vorkommen
    foreach ( $liste_der_botschaftenreihenfolge as $nummer_in_reihenfolgenliste ) {
      if ( in_array( $nummer_in_reihenfolgenliste , $liste_der_zahlen_aller_botschaften_dieser_sprache ) ) {
        $in_beiden_listen_vorkommende_zahlen[] = $nummer_in_reihenfolgenliste;
      }
    }
    
    # Jetzt sind im Array alle ZAHLEN vorhanden, die sowohl als BOTSCHAFT vorhanden, als auch in der 'Botschaftenreihenfolge angegeben' sind.
    
    # Liste fuer die Dateinamen aller Botschaften AKTUELLER Sprache erstellen
    $liste_der_dateinamen_der_botschaften_dieser_sprache = array();
    
    # Hole alle Botschaften-Dateinamen aus dem aktuellen Verzeichnis/Sprache (ausser "." und ".."):
    if ( is_dir( $aktuelles_verzeichnis ) ) {
$dateinamen_liste = array_sprachtextdatei_bereinigen( file ( $verzeichnis_dateinamen . $array_mit_deutschen_sprachbezeichnungen[$i] . ".txt" ) );
foreach ( $dateinamen_liste as $aktuelle_zeile ) {
  $liste_der_dateinamen_der_botschaften_dieser_sprache[] = $aktuelle_zeile;
}
/*
      if ( $handle = opendir( $aktuelles_verzeichnis ) ) {
        while ( ( $file = readdir( $handle ) ) !== false ) {
          if ( !( ( $file == "." ) or ( $file == ".." ) ) ) {
#            $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" );
#            $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "ISO-8859-2" );
            switch ( $array_mit_deutschen_sprachbezeichnungen[$i] ) {
              case "bulgarisch":
                $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "ISO-8859-5" );
                break;
#              case "griechisch":
#                $liste_der_dateinamen_der_botschaften_dieser_sprache[] = ""; # mb_convert_encoding( $file , "UTF-8" , "Windows-1252" );
#                break;
              case "griechisch":
                $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "ISO-8859-7" );
                break;
              case "japanisch":
                $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "Windows-1252" );
                break;
              case "rumaenisch":
                $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "ISO-8859-16" );
                break;
              case "russisch":
                $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "ISO-8859-5" );
                break;
              case "chinesisch":
                $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "Windows-1252" );
                break;
              default:
                $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "Windows-1252" );
            }
            #if ( $array_mit_deutschen_sprachbezeichnungen[$i] == "griechisch" ) { $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "CP1252" ); }
#            if ( $array_mit_deutschen_sprachbezeichnungen[$i] == "griechisch" ) { $liste_der_dateinamen_der_botschaften_dieser_sprache[] = ""; } # mb_convert_encoding( $file , "UTF-8" , "UTF16" ); }
#            if ( $array_mit_deutschen_sprachbezeichnungen[$i] == "japanisch" ) { $liste_der_dateinamen_der_botschaften_dieser_sprache[] = ""; } # mb_convert_encoding( $file , "UTF-8" , "Windows-1252" ); }
#            if ( $array_mit_deutschen_sprachbezeichnungen[$i] == "russisch" ) { $liste_der_dateinamen_der_botschaften_dieser_sprache[] = ""; } # mb_convert_encoding( $file , "UTF-8" , "Windows-1252" ); }
#            if ( $array_mit_deutschen_sprachbezeichnungen[$i] == "chinesisch" ) { $liste_der_dateinamen_der_botschaften_dieser_sprache[] = ""; } # mb_convert_encoding( $file , "UTF-8" , "Windows-1252" ); }
#            $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" , "Windows-1252" );
#echo $liste_der_dateinamen_der_botschaften_dieser_sprache[ count($liste_der_dateinamen_der_botschaften_dieser_sprache)-1 ] . "<br>";
          }
        }
        closedir( $handle );
      }
*/
    }
    # sort( $liste_der_dateinamen_der_botschaften_dieser_sprache );
#pre( $liste_der_dateinamen_der_botschaften_dieser_sprache );
#foreach ( $liste_der_dateinamen_der_botschaften_dieser_sprache as $akt_dateiname ) { echo $akt_dateiname . "<br>"; }
    
    # Ins Botschaften-Array Label-Beschriftungen und Dateinamen (aller Sprachen) eintragen.
    for ( $j = 0 ; $j < count( $in_beiden_listen_vorkommende_zahlen ) ; $j++ ) {
      # Label-Beschriftung eintragen
      $liste_botschaften_beschriftungen_und_dateinamen[ $liste_aller_sprachcodes[$i] ]['label_beschriftung'][] = $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ][ ( $in_beiden_listen_vorkommende_zahlen[$j] - 1 ) ];
      # Dateinamen eintragen
      foreach ( $liste_der_dateinamen_der_botschaften_dieser_sprache as $aktueller_botschaftsdateiname ) {
        # Wenn Dateinamen-Ziffern und aktuelle Zahl der Zahl in der Reihenfolgenliste IDENTISCH:
        if ( mb_substr( $aktueller_botschaftsdateiname , 0 , 3 ) == $in_beiden_listen_vorkommende_zahlen[$j] ) {
          $liste_botschaften_beschriftungen_und_dateinamen[ $liste_aller_sprachcodes[$i] ]['dateiname'][] = $aktueller_botschaftsdateiname;
        }
      }
    }
  }
  
  return $liste_botschaften_beschriftungen_und_dateinamen;
}

function themensammlungen_beschriftungen_und_dateinamen_eintragen( $liste_themensammlungen_beschriftungen_und_dateinamen , $verzeichnis_label_beschriftungen , $verzeichnis_dateinamen , $liste_der_themensammlungenreihenfolge , $verzeichnis_botschaften ) {
  global $gesamtarray;
  global $liste_aller_sprachcodes;
  $zwischenspeicher_array = array();
  $array_mit_deutschen_sprachbezeichnungen = hole_alle_deutschen_sprachbezeichnungen( $gesamtarray );
  
  # Arrays fuer die jeweiligen Sprachen anlegen
  for ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
    $liste_themensammlungen_beschriftungen_und_dateinamen[ $liste_aller_sprachcodes[$i] ] = array();
	$zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] = array();
  }
  
  # Labelbeschriftungen reinladen und "reinigen"
  for ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
    # Label-Beschriftungen in die jeweilige Sprache reinladen
    $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] = file( $verzeichnis_label_beschriftungen . $liste_aller_sprachcodes[$i] . ".txt" );
    # gespeichertes Array 'reinigen' (von Leerzeilen, Kommentarzeilen, Zeilenende-Kodierungen)
    $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] = array_sprachtextdatei_bereinigen( $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] );
    $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] = array_leere_elemente_entfernen( $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ] );
  }
  
  # Gehe alle (deutschsprachigen) Ordner der jeweiligen Sprache durch
  for ( $i = 0 ; $i < count( $array_mit_deutschen_sprachbezeichnungen ) ; $i++ ) {
    $liste_der_zahlen_aller_botschaften_dieser_sprache = array();
    
    $aktuelles_verzeichnis = $verzeichnis_botschaften . "docx/" . $array_mit_deutschen_sprachbezeichnungen[$i];
    # Hole alle Botschaften-Dateinamen aus dem aktuellen Verzeichnis/Sprache (ausser "." und ".."):
    if ( is_dir( $aktuelles_verzeichnis ) ) {
      if ( $handle = opendir( $aktuelles_verzeichnis ) ) {
        while ( ( $file = readdir( $handle ) ) !== false ) {
          if ( !( ( $file == "." ) or ( $file == ".." ) ) ) {
            # Nur die ersten 3 Ziffern
            $ersten_drei_ziffern = mb_substr( $file , 0 , 3 );
            $liste_der_zahlen_aller_botschaften_dieser_sprache[] = $ersten_drei_ziffern;
          }
        }
        closedir( $handle );
      }
    }
    sort( $liste_der_zahlen_aller_botschaften_dieser_sprache );
    
    # Array, welches alle Zahlen beinhaltet, die 'sowohl' als Botschaft, als auch in der 'Reihenfolgenliste' vorkommt
    $in_beiden_listen_vorkommende_zahlen = array();
    
    # Ermittle alle Zahlen, die in BEIDEN "Listen", sowohl der Reihenfolgen-Nummern-Liste, als auch in den Dateinamen, vorkommen
    foreach ( $liste_der_themensammlungenreihenfolge as $nummer_in_reihenfolgenliste ) {
      if ( in_array( $nummer_in_reihenfolgenliste , $liste_der_zahlen_aller_botschaften_dieser_sprache ) ) {
        $in_beiden_listen_vorkommende_zahlen[] = $nummer_in_reihenfolgenliste;
      }
    }
    
    # Jetzt sind im Array alle ZAHLEN vorhanden, die sowohl als BOTSCHAFT vorhanden, als auch in der 'Themensammlungenreihenfolge angegeben' sind.
    
    # Liste fuer die Dateinamen aller Botschaften AKTUELLER Sprache erstellen
    $liste_der_dateinamen_der_botschaften_dieser_sprache = array();
    
    # Hole alle Botschaften-Dateinamen aus dem aktuellen Verzeichnis/Sprache (ausser "." und ".."):
    if ( is_dir( $aktuelles_verzeichnis ) ) {
$dateinamen_liste = array_sprachtextdatei_bereinigen( file ( $verzeichnis_dateinamen . $array_mit_deutschen_sprachbezeichnungen[$i] . ".txt" ) );
foreach ( $dateinamen_liste as $aktuelle_zeile ) {
  $liste_der_dateinamen_der_botschaften_dieser_sprache[] = $aktuelle_zeile;
}
/*
      if ( $handle = opendir( $aktuelles_verzeichnis ) ) {
        while ( ( $file = readdir( $handle ) ) !== false ) {
          if ( !( ( $file == "." ) or ( $file == ".." ) ) ) {
            $liste_der_dateinamen_der_botschaften_dieser_sprache[] = mb_convert_encoding( $file , "UTF-8" );
          }
        }
        closedir( $handle );
      }
*/
    }
    # sort( $liste_der_dateinamen_der_botschaften_dieser_sprache );
    
    # Ins Botschaften-Array Label-Beschriftungen und Dateinamen (aller Sprachen) eintragen.
    for ( $j = 0 ; $j < count( $in_beiden_listen_vorkommende_zahlen ) ; $j++ ) {
      # Label-Beschriftung eintragen
      $liste_themensammlungen_beschriftungen_und_dateinamen[ $liste_aller_sprachcodes[$i] ]['label_beschriftung'][] = $zwischenspeicher_array[ $liste_aller_sprachcodes[$i] ][ ( $in_beiden_listen_vorkommende_zahlen[$j] - 1 ) ];
      # Dateinamen eintragen
      foreach ( $liste_der_dateinamen_der_botschaften_dieser_sprache as $aktueller_botschaftsdateiname ) {
        # Wenn Dateinamen-Ziffern und aktuelle Zahl der Zahl in der Reihenfolgenliste IDENTISCH:
        if ( mb_substr( $aktueller_botschaftsdateiname , 0 , 3 ) == $in_beiden_listen_vorkommende_zahlen[$j] ) {
          $liste_themensammlungen_beschriftungen_und_dateinamen[ $liste_aller_sprachcodes[$i] ]['dateiname'][] = $aktueller_botschaftsdateiname;
        }
      }
    }
  }
  
  return $liste_themensammlungen_beschriftungen_und_dateinamen;
}


#?>