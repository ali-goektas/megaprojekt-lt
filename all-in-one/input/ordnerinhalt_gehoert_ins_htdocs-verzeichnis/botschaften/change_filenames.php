<?php

function pre( $ausgabe ) { echo "<pre>\n"; print_r($ausgabe); echo "</pre>\n"; }
function pre_dump( $ausgabe ) { echo "<pre>\n"; var_dump($ausgabe); echo "</pre>\n"; }

/*

$files = scandir( 'E:/botschaften/docx' );
$files = array_diff( $files , array( '.' , '..' ) );
sort( $files );
pre( $files );

echo count( $files );


mkdir( 'F:/botschaften/' );
mkdir( 'F:/botschaften/docx/' );
for ( $i = 0 ; $i < count( $files ) ; $i++ ) {
  mkdir( 'F:/botschaften/docx/' . $files[$i] );
}

*/


# alle Ordnernamen (deutsche ausgeschriebene Sprachbezeichnung) laden
$liste_sprachordner = scandir( 'E:/botschaften/docx/' );
$liste_sprachordner = array_diff( $liste_sprachordner , array( '.' , '..' ) );
sort( $liste_sprachordner );

# Jetzt hat mal alle *Sprachbezeichnungen auf DEUTSCH*
#pre( $liste_sprachordner );



# alle Zeilen (jeder Sprache) laden
$liste_aller_dateinamen_aller_sprachen = array();
foreach ( $liste_sprachordner as $sprache_auf_deutsch ) {
  # alle Dateinamen (jede Sprache nacheinander) laden
  $liste_aller_zeilen_dieser_sprache = scandir( 'E:/botschaften/docx/' . $sprache_auf_deutsch );
  $liste_aller_zeilen_dieser_sprache = array_diff( $liste_aller_zeilen_dieser_sprache , array( '.' , '..' ) );
  sort( $liste_aller_zeilen_dieser_sprache );

  # Jetzt halt man alle *Zeilen DIESER Sprache*
  #pre( $liste_aller_zeilen_dieser_sprache );
  
  # Ins Botschaften-Array eintragen
  $liste_aller_dateinamen_aller_sprachen[$sprache_auf_deutsch] = $liste_aller_zeilen_dieser_sprache;
}

# Jetzt hat man alle *ZEILEN je Sprache*
#pre( $liste_aller_dateinamen_aller_sprachen );




foreach ( $liste_sprachordner as $sprache_auf_deutsch ) {
pre( $sprache_auf_deutsch );

  foreach ( $liste_aller_dateinamen_aller_sprachen[$sprache_auf_deutsch] as $dateinamen_aktueller_sprache ) {
  
  # Jetzt hat man alle *Zeilen nacheinander* - aller Sprachen
  pre( $dateinamen_aktueller_sprache );



  }
}



# Die Zahl am Anfang des Dateinamens vom Rest trennen
$erste_4_zeichen = mb_substr( $liste_sprachordner , 0 , 4 );
$rest = trim( mb_substr( $liste_sprachordner , 4 ) );
pre( $erste_4_zeichen );
pre( $rest );


# neuen String (Datum) erstellen

$neue_zeichen = str_replace( "001 " , "02-04-15" , $erste_4_zeichen );

$neue_zeichen = str_replace( "002 " , "03-01-06" , $erste_4_zeichen );
$neue_zeichen = str_replace( "003 " , "03-03-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "004 " , "03-08-17" , $erste_4_zeichen );
$neue_zeichen = str_replace( "005 " , "03-08-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "006 " , "03-10-22" , $erste_4_zeichen );
$neue_zeichen = str_replace( "007 " , "03-11-26" , $erste_4_zeichen );
$neue_zeichen = str_replace( "008 " , "03-12-10" , $erste_4_zeichen );

$neue_zeichen = str_replace( "009 " , "04-01-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "010 " , "04-03-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "011 " , "04-04-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "012 " , "04-06-18" , $erste_4_zeichen );
$neue_zeichen = str_replace( "013 " , "04-07-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "014 " , "04-07-30" , $erste_4_zeichen );
$neue_zeichen = str_replace( "015 " , "04-10-02" , $erste_4_zeichen );
$neue_zeichen = str_replace( "016 " , "04-10-25" , $erste_4_zeichen );
$neue_zeichen = str_replace( "017 " , "04-10-26" , $erste_4_zeichen );
$neue_zeichen = str_replace( "018 " , "04-11-25" , $erste_4_zeichen );
$neue_zeichen = str_replace( "019 " , "04-12-18" , $erste_4_zeichen );

$neue_zeichen = str_replace( "020 " , "05-01-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "021 " , "05-01-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "022 " , "05-01-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "023 " , "05-01-20" , $erste_4_zeichen );
$neue_zeichen = str_replace( "024 " , "05-02-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "025 " , "05-03-31" , $erste_4_zeichen );
$neue_zeichen = str_replace( "026 " , "05-04-02" , $erste_4_zeichen );
$neue_zeichen = str_replace( "027 " , "05-04-22" , $erste_4_zeichen );
$neue_zeichen = str_replace( "028 " , "05-06-14" , $erste_4_zeichen );
$neue_zeichen = str_replace( "029 " , "05-07-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "030 " , "05-07-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "031 " , "05-07-08" , $erste_4_zeichen );
$neue_zeichen = str_replace( "032 " , "05-08-21" , $erste_4_zeichen );
$neue_zeichen = str_replace( "033 " , "05-08-25" , $erste_4_zeichen );
$neue_zeichen = str_replace( "034 " , "05-09-17" , $erste_4_zeichen );
$neue_zeichen = str_replace( "035 " , "05-10-26" , $erste_4_zeichen );
$neue_zeichen = str_replace( "036 " , "05-11-29" , $erste_4_zeichen );

$neue_zeichen = str_replace( "037 " , "06-01-02" , $erste_4_zeichen );
$neue_zeichen = str_replace( "038 " , "06-01-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "039 " , "06-01-12" , $erste_4_zeichen );
$neue_zeichen = str_replace( "040 " , "06-01-14" , $erste_4_zeichen );
$neue_zeichen = str_replace( "041 " , "06-01-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "042 " , "06-01-20" , $erste_4_zeichen );
$neue_zeichen = str_replace( "043 " , "06-02-21" , $erste_4_zeichen );
$neue_zeichen = str_replace( "044 " , "06-03-12" , $erste_4_zeichen );
$neue_zeichen = str_replace( "045 " , "06-03-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "046 " , "06-03-28" , $erste_4_zeichen );
$neue_zeichen = str_replace( "047 " , "06-04-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "048 " , "06-04-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "049 " , "06-04-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "050 " , "06-04-29" , $erste_4_zeichen );
$neue_zeichen = str_replace( "051 " , "06-04-30" , $erste_4_zeichen );
$neue_zeichen = str_replace( "052 " , "06-05-04" , $erste_4_zeichen );
$neue_zeichen = str_replace( "053 " , "06-07-02" , $erste_4_zeichen );
$neue_zeichen = str_replace( "054 " , "06-08-08" , $erste_4_zeichen );
$neue_zeichen = str_replace( "055 " , "06-09-14" , $erste_4_zeichen );
$neue_zeichen = str_replace( "056 " , "06-09-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "057 " , "06-10-06" , $erste_4_zeichen );
$neue_zeichen = str_replace( "058 " , "06-10-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "059 " , "06-10-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "060 " , "06-10-20" , $erste_4_zeichen );
$neue_zeichen = str_replace( "061 " , "06-11-14" , $erste_4_zeichen );

$neue_zeichen = str_replace( "062 " , "07-01-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "063 " , "07-01-28" , $erste_4_zeichen );
$neue_zeichen = str_replace( "064 " , "07-01-29" , $erste_4_zeichen );
$neue_zeichen = str_replace( "065 " , "07-01-30" , $erste_4_zeichen );
$neue_zeichen = str_replace( "066 " , "07-01-31" , $erste_4_zeichen );
$neue_zeichen = str_replace( "067 " , "07-03-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "068 " , "07-03-18" , $erste_4_zeichen );
$neue_zeichen = str_replace( "069 " , "07-03-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "070 " , "07-04-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "071 " , "07-04-06" , $erste_4_zeichen );
$neue_zeichen = str_replace( "072 " , "07-08-02" , $erste_4_zeichen );
$neue_zeichen = str_replace( "073 " , "07-08-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "074 " , "07-08-29" , $erste_4_zeichen );
$neue_zeichen = str_replace( "075 " , "07-09-03" , $erste_4_zeichen );
$neue_zeichen = str_replace( "076 " , "07-09-04" , $erste_4_zeichen );
$neue_zeichen = str_replace( "077 " , "07-09-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "078 " , "07-10-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "079 " , "07-11-24" , $erste_4_zeichen );
$neue_zeichen = str_replace( "080 " , "07-11-25" , $erste_4_zeichen );
$neue_zeichen = str_replace( "081 " , "07-11-26" , $erste_4_zeichen );
$neue_zeichen = str_replace( "082 " , "07-12-01" , $erste_4_zeichen );

$neue_zeichen = str_replace( "083 " , "08-01-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "084 " , "08-03-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "085 " , "08-03-06" , $erste_4_zeichen );
$neue_zeichen = str_replace( "086 " , "08-03-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "087 " , "08-03-08" , $erste_4_zeichen );
$neue_zeichen = str_replace( "088 " , "08-03-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "089 " , "08-03-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "090 " , "08-03-12" , $erste_4_zeichen );
$neue_zeichen = str_replace( "091 " , "08-03-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "092 " , "08-03-17" , $erste_4_zeichen );
$neue_zeichen = str_replace( "093 " , "08-04-17" , $erste_4_zeichen );
$neue_zeichen = str_replace( "094 " , "08-04-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "095 " , "08-07-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "096 " , "08-07-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "097 " , "08-07-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "098 " , "08-08-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "099 " , "08-08-23" , $erste_4_zeichen );
$neue_zeichen = str_replace( "100 " , "08-08-28" , $erste_4_zeichen );
$neue_zeichen = str_replace( "101 " , "08-09-04" , $erste_4_zeichen );
$neue_zeichen = str_replace( "102 " , "08-09-24" , $erste_4_zeichen );
$neue_zeichen = str_replace( "103 " , "08-09-25" , $erste_4_zeichen );
$neue_zeichen = str_replace( "104 " , "08-10-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "105 " , "08-12-31" , $erste_4_zeichen );

$neue_zeichen = str_replace( "106 " , "09-01-21" , $erste_4_zeichen );
$neue_zeichen = str_replace( "107 " , "09-01-25" , $erste_4_zeichen );
$neue_zeichen = str_replace( "108 " , "09-03-12" , $erste_4_zeichen );
$neue_zeichen = str_replace( "109 " , "09-04-27" , $erste_4_zeichen );
$neue_zeichen = str_replace( "110 " , "09-04-30" , $erste_4_zeichen );
$neue_zeichen = str_replace( "111 " , "09-05-20" , $erste_4_zeichen );
$neue_zeichen = str_replace( "112 " , "09-11-12" , $erste_4_zeichen );
$neue_zeichen = str_replace( "113 " , "09-12-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "114 " , "09-12-24" , $erste_4_zeichen );

$neue_zeichen = str_replace( "115 " , "10-01-04" , $erste_4_zeichen );
$neue_zeichen = str_replace( "116 " , "10-01-08" , $erste_4_zeichen );
$neue_zeichen = str_replace( "117 " , "10-02-02" , $erste_4_zeichen );
$neue_zeichen = str_replace( "118 " , "10-08-21" , $erste_4_zeichen );
$neue_zeichen = str_replace( "119 " , "10-09-17" , $erste_4_zeichen );
$neue_zeichen = str_replace( "120 " , "10-12-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "121 " , "10-12-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "122 " , "10-12-29" , $erste_4_zeichen );

$neue_zeichen = str_replace( "123 " , "11-02-18" , $erste_4_zeichen );
$neue_zeichen = str_replace( "124 " , "11-03-24" , $erste_4_zeichen );
$neue_zeichen = str_replace( "125 " , "11-05-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "126 " , "11-05-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "127 " , "11-05-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "128 " , "11-07-23" , $erste_4_zeichen );
$neue_zeichen = str_replace( "129 " , "11-07-25" , $erste_4_zeichen );
$neue_zeichen = str_replace( "130 " , "11-09-18" , $erste_4_zeichen );
$neue_zeichen = str_replace( "131 " , "11-09-27" , $erste_4_zeichen );
$neue_zeichen = str_replace( "132 " , "11-11-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "133 " , "11-12-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "134 " , "11-12-18" , $erste_4_zeichen );

$neue_zeichen = str_replace( "135 " , "12-02-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "136 " , "12-02-13" , $erste_4_zeichen );
$neue_zeichen = str_replace( "137 " , "12-02-14" , $erste_4_zeichen );
$neue_zeichen = str_replace( "138 " , "12-02-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "139 " , "12-02-18" , $erste_4_zeichen );
$neue_zeichen = str_replace( "140 " , "12-02-20" , $erste_4_zeichen );
$neue_zeichen = str_replace( "141 " , "12-03-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "142 " , "12-03-06" , $erste_4_zeichen );
$neue_zeichen = str_replace( "143 " , "12-04-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "144 " , "12-04-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "145 " , "12-05-08" , $erste_4_zeichen );
$neue_zeichen = str_replace( "146 " , "12-06-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "147 " , "12-08-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "148 " , "12-09-14" , $erste_4_zeichen );
$neue_zeichen = str_replace( "149 " , "12-09-24" , $erste_4_zeichen );
$neue_zeichen = str_replace( "150 " , "12-10-30" , $erste_4_zeichen );
$neue_zeichen = str_replace( "151 " , "12-11-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "152 " , "12-11-21" , $erste_4_zeichen );

$neue_zeichen = str_replace( "153 " , "13-01-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "154 " , "13-02-21" , $erste_4_zeichen );
$neue_zeichen = str_replace( "155 " , "13-04-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "156 " , "13-05-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "157 " , "13-07-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "158 " , "13-08-22" , $erste_4_zeichen );
$neue_zeichen = str_replace( "159 " , "13-09-27" , $erste_4_zeichen );
$neue_zeichen = str_replace( "160 " , "13-10-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "161 " , "13-11-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "162 " , "13-12-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "163 " , "13-12-28" , $erste_4_zeichen );

$neue_zeichen = str_replace( "164 " , "14-01-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "165 " , "14-08-18" , $erste_4_zeichen );
$neue_zeichen = str_replace( "166 " , "14-08-29" , $erste_4_zeichen );
$neue_zeichen = str_replace( "167 " , "14-09-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "168 " , "14-10-04" , $erste_4_zeichen );
$neue_zeichen = str_replace( "169 " , "14-10-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "170 " , "14-11-04" , $erste_4_zeichen );
$neue_zeichen = str_replace( "171 " , "14-11-12" , $erste_4_zeichen );
$neue_zeichen = str_replace( "172 " , "14-11-23" , $erste_4_zeichen );
$neue_zeichen = str_replace( "173 " , "14-12-17" , $erste_4_zeichen );
$neue_zeichen = str_replace( "174 " , "14-12-18" , $erste_4_zeichen );
$neue_zeichen = str_replace( "175 " , "14-12-25" , $erste_4_zeichen );
$neue_zeichen = str_replace( "176 " , "14-12-26" , $erste_4_zeichen );

$neue_zeichen = str_replace( "177 " , "15-02-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "178 " , "15-03-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "179 " , "15-04-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "180 " , "15-04-21" , $erste_4_zeichen );
$neue_zeichen = str_replace( "181 " , "15-09-17" , $erste_4_zeichen );
$neue_zeichen = str_replace( "182 " , "15-11-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "183 " , "15-12-20" , $erste_4_zeichen );
$neue_zeichen = str_replace( "184 " , "15-12-23" , $erste_4_zeichen );
$neue_zeichen = str_replace( "185 " , "15-12-30" , $erste_4_zeichen );
$neue_zeichen = str_replace( "186 " , "15-12-31" , $erste_4_zeichen );

$neue_zeichen = str_replace( "187 " , "16-01-28" , $erste_4_zeichen );
$neue_zeichen = str_replace( "188 " , "16-03-26" , $erste_4_zeichen );
$neue_zeichen = str_replace( "189 " , "16-04-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "190 " , "16-04-02" , $erste_4_zeichen );
$neue_zeichen = str_replace( "191 " , "16-08-23" , $erste_4_zeichen );
$neue_zeichen = str_replace( "192 " , "16-09-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "193 " , "16-09-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "194 " , "16-09-20" , $erste_4_zeichen );
$neue_zeichen = str_replace( "195 " , "16-09-21" , $erste_4_zeichen );
$neue_zeichen = str_replace( "196 " , "16-11-21" , $erste_4_zeichen );
$neue_zeichen = str_replace( "197 " , "16-12-17" , $erste_4_zeichen );

$neue_zeichen = str_replace( "198 " , "17-01-04" , $erste_4_zeichen );

$neue_zeichen = str_replace( "199 " , "18-10-12" , $erste_4_zeichen );
$neue_zeichen = str_replace( "200 " , "18-12-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "201 " , "18-12-28" , $erste_4_zeichen );

$neue_zeichen = str_replace( "202 " , "19-01-08" , $erste_4_zeichen );
$neue_zeichen = str_replace( "203 " , "19-01-28" , $erste_4_zeichen );
$neue_zeichen = str_replace( "204 " , "19-02-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "205 " , "19-03-05" , $erste_4_zeichen );
$neue_zeichen = str_replace( "206 " , "19-03-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "207 " , "19-03-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "208 " , "19-03-13" , $erste_4_zeichen );
$neue_zeichen = str_replace( "209 " , "19-04-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "210 " , "19-04-13" , $erste_4_zeichen );
$neue_zeichen = str_replace( "211 " , "19-04-27" , $erste_4_zeichen );
$neue_zeichen = str_replace( "212 " , "19-05-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "213 " , "19-06-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "214 " , "19-12-28" , $erste_4_zeichen );
$neue_zeichen = str_replace( "215 " , "19-12-31" , $erste_4_zeichen );

$neue_zeichen = str_replace( "216 " , "20-02-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "217 " , "20-03-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "218 " , "20-03-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "219 " , "20-03-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "220 " , "20-03-25" , $erste_4_zeichen );
$neue_zeichen = str_replace( "221 " , "20-03-27" , $erste_4_zeichen );
$neue_zeichen = str_replace( "222 " , "20-06-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "223 " , "20-06-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "224 " , "20-06-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "225 " , "20-08-07" , $erste_4_zeichen );
$neue_zeichen = str_replace( "226 " , "20-08-08" , $erste_4_zeichen );
$neue_zeichen = str_replace( "227 " , "20-08-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "229 " , "20-08-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "228 " , "20-08-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "230 " , "20-08-13" , $erste_4_zeichen );
$neue_zeichen = str_replace( "231 " , "20-08-14" , $erste_4_zeichen );
$neue_zeichen = str_replace( "232 " , "20-08-15" , $erste_4_zeichen );
$neue_zeichen = str_replace( "240 " , "20-08-16" , $erste_4_zeichen );
$neue_zeichen = str_replace( "233 " , "20-08-19" , $erste_4_zeichen );
$neue_zeichen = str_replace( "234 " , "20-09-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "235 " , "20-12-15" , $erste_4_zeichen );

$neue_zeichen = str_replace( "236 " , "21-01-21" , $erste_4_zeichen );
$neue_zeichen = str_replace( "237 " , "21-02-03" , $erste_4_zeichen );
$neue_zeichen = str_replace( "238 " , "21-02-09" , $erste_4_zeichen );
$neue_zeichen = str_replace( "239 " , "21-02-10" , $erste_4_zeichen );
$neue_zeichen = str_replace( "241 " , "21-02-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "244 " , "21-05-01" , $erste_4_zeichen );
$neue_zeichen = str_replace( "242 " , "21-05-11" , $erste_4_zeichen );
$neue_zeichen = str_replace( "243 " , "21-05-28" , $erste_4_zeichen );
$neue_zeichen = str_replace( "245 " , "21-10-12" , $erste_4_zeichen );

pre( $neue_zeichen );



#alles nochmal durchkontrollieren, ob Zahlen richtig

# neu zusammengesetzt
$neu = $neue_zeichen . "   " . $rest;
pre( $neu );

?>