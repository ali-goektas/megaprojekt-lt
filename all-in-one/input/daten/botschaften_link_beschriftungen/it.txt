Piccoli animali - i nostri parassiti

Chiamate di ammonizione e di aiuto da parte di Dio
Esperienze dell'acqua nel letto del fiume
raggi terrestri nocivi attraverso i passaggi d'acqua sotterranei
L'indicibile sofferenza del mondo animale
Movimento del corpo nella danza
vita esagerata e attenta alla salute
Indirizzare le cellule del corpo in caso di indisposizione

mio e tuo - il modo di vivere vincolante degli esseri che cadono
comportamento esagerato quando si mangia
Vedere una cascata dalla prospettiva degli esseri celesti
dipendenza dal tabacco
i lunghi e faticosi preparativi degli esseri celesti per la salvezza della creazione e le loro agonizzanti incarnazioni
effetti devastanti sul corpo e sull'anima attraverso la mortificazione e la costrizione per motivi religiosi
Affrontare le cellule del corpo durante le malattie gravi
Affrontare le cellule del corpo prima della chirurgia
false idee sul cammino interiore verso Dio
La comprensione degli eventi cosmici invisibili e incomprensibili
Origine delle allergie cutanee

al di là delle anime terrene e la loro influenza invisibile sulla vita umana
Istruzione divina per diventare consapevoli e controllare i propri pensieri
Come capire la dolcezza divina
Rimpianto di sé e umore depressivo
Il tragico destino delle vittime dello tsunami
Riconoscere, analizzare e rimediare alle proprie finzioni
Ipotesi come utili sentimenti sgradevoli per vedere attraverso le persone ambigue
Vivere in questo mondo di inganni
L'offerta di Dio per aumentare l'energia vitale
Schiavi e guidati dal mondo del lavoro
Parte 1 - Paura degli inquinanti nel cibo
Parte 2 - Paura delle sostanze nocive negli alimenti
Cosa significa la libertà e l'indipendenza degli esseri cosmici?
drammatico stato energetico della terra con conseguenze devastanti per tutta la vita
strette relazioni degli atomi sottili primordiali con gli atomi alterati e condensati dell'universo materiale
modestia giustamente intesa per vivere vicino alla giustizia divina degli esseri celesti
assunzione di cibo più energico in relazione all'elemento fuoco

la difficile liberazione dai principi di vita mondani e conferenzieri, così come le composizioni musicali senza suono celeste
congedo di un'anima divina dal corpo umano nell'aldilà
parole confortanti e utili dallo spirito universale dell'amore per il riconoscimento di sé per le persone anziane e sole
Come afferrare più lungimirantemente la comprensione di Dio delle nostre trasgressioni della legge e il loro perdono
Dove sono i tuoi tesori raccolti, lì sei attratto ancora e ancora
Autoconoscenza del suono della parola, kinesiologia e autoguarigione da parte di medici laici
importanti istruzioni dallo Spirito di Dio per i traduttori delle gocce d'amore Io Sono in altre lingue
cuore chiuso
possibile via d'uscita dalla propria freddezza di cuore per vivere di nuovo nel calore del cuore degli esseri celesti duali
Media aurea nel comportamento alimentare
L'umanità interiore può ancora evitare gli eventi apocalittici sulla terra e la fine prematura del mondo predetta dai primi veggenti?
Distruzione della natura da parte di micro-animali senza energia
L'onestà verso se stessi smaschera la menzogna
spietata erosione mentale dei reati della legge nel corpo umano
l'intolleranza a certi cibi e spezie nelle persone divine e sensibili - la fedeltà, il prerequisito per una partnership felice
gli esseri della natura preoccupati raccontano la loro difficile situazione di vita e i loro compiti nei nostri giardini
finta fedeltà nel matrimonio e nell'unione - doppia vita degli ipocriti dal cuore freddo
Dove sono finite le tue qualità di essere celeste cordiale e nobile dal genio divino?
Elevata perdita di energia vitale attraverso incontri regolari con persone mondane e supponenti.
Consapevolezza della vita umana
Onestà, fedeltà e senso dell'onore - le qualità nobili più importanti per una partnership armoniosa e felice
Affrontare le cellule del corpo per il dolore articolare
Parte 1 - L'emergere delle religioni e delle comunità di fede e la fanatica credenza in Dio che ne è scaturita, con infinite sofferenze per gli esseri umani e le anime ultraterrene
Parte 2 - L'origine delle religioni e delle comunità religiose e la conseguente credenza fanatica in Dio, con infinite sofferenze per gli esseri umani e le anime nell'aldilà.
Perché alcune persone che amano Dio non sopportano di parlare seriamente

Il cammino verso la spiritualizzazione e la libertà celeste
Autoresponsabilità per la propria vita - l'elemento del fuoco e la cremazione del corpo
Parte 1 - Ragioni per presunte malattie incurabili e loro possibilità di cura da un punto di vista divino - così come la creazione di mondi adiacenti e interconnessi di modi di vivere completamente diversi
Parte 2 - Ragioni delle presunte malattie incurabili e delle loro possibilità di guarigione da un punto di vista divino - così come la creazione di mondi adiacenti e interconnessi di modi di vivere completamente diversi
Parte 3 - Le ragioni delle malattie presunte incurabili e le loro possibilità di essere curate da un punto di vista divino - così come la creazione di mondi adiacenti e interconnessi di un modo di vivere completamente diverso.
Rivolgersi alle cellule del corpo per attivare le loro funzioni
Parte 1 - Grazia - remissione della pena da parte del popolo dominante - erroneamente associata dai credenti alla benevolenza della divinità
Parte 2 - Misericordia - Punizione del popolo dominante - erroneamente associata dai credenti alla benevolenza della divinità
Le nostre buone azioni - sono davvero quello che pensiamo che siano?
Perché gli esseri duali celesti a volte si separano e formano nuove unioni
I consigli di Dio che danno speranza dopo la diagnosi di una malattia grave e gli indizi sul comportamento delle cellule del corpo che ci sono ancora sconosciuti
Aumento di tempeste devastanti, inondazioni, fasi di calore e siccità sulla terra - forieri di una fine lenta ma inesorabile della vita terrena
Parte 1 - fasi insopportabili nella vita dei veri araldi di Dio
Parte 2 - fasi insopportabili nella vita dei veri araldi di Dio
Ragioni per la creazione dell'universo di materia solida e per la creazione dell'essere umano imperfetto
L'amore dal cuore - come possono le persone interiori capirlo e viverlo meglio?
Comunicazione accorata di una mela sulle esperienze sul suo albero madre
immagine, colore e linguaggio sonoro degli esseri celesti in contrasto con la comunicazione umana
importante raccomandazione di preghiera dallo spirito di Dio per le persone spiritualmente mature
Matrimoni e unioni dalla prospettiva delle leggi celesti della vita
Pendoli - un'attività apparentemente utile con pericoli inaspettati

Il Natale cristiano e il simbolo della croce - sono davvero la volontà divina e di grande importanza per chi ritorna nel regno dei cieli?
Parte 1 - stile di vita vegetariano - dal punto di vista del principio celeste della vita e altri argomenti
Parte 2 - stile di vita vegetariano - dal punto di vista del principio celeste della vita e altri argomenti
Parte 3 - stile di vita vegetariano - dal punto di vista del principio celeste della vita e altri argomenti
Parte 4 - stile di vita vegetariano - dal punto di vista del principio celeste della vita e altri argomenti
Parte 1 - uno stile di vita interiore armonioso, amante della pace ed equilibrato - un obiettivo degno di nota
Parte 2 - uno stile di vita interiore armonioso, amante della pace ed equilibrato - un obiettivo degno di nota
Il perfezionismo umano da un punto di vista celeste
la corretta gestione dei mezzi di pagamento e dei regali, così come gli elementi
Parte 1 - La generazione di energia attraverso la fissione nucleare e il funzionamento delle centrali nucleari da una prospettiva celeste
Parte 2 - La generazione di energia attraverso la fissione nucleare e il funzionamento delle centrali nucleari da una prospettiva celeste
Elezione di un politico - dalla prospettiva dello spirito di Dio
Profonda autoconoscenza dei difetti e delle debolezze - il compito più importante del giorno per chi ritorna al celeste per la crescita spirituale
Parte 1 - La vita evolutiva degli esseri celesti e la loro connessione del cuore alla Divinità impersonale Io Sono
Parte 2 - La vita evolutiva degli esseri celesti e la loro connessione del cuore alla Divinità impersonale Io Sono
Parte 3 - La vita evolutiva degli Esseri Celesti e la loro connessione del cuore alla Divinità Impersonale Io Sono
Parte 1 - malsana vita personale di esseri profondamente caduti
Parte 2 - la vita malsana degli esseri profondamente caduti che esalta la loro personalità
Quale tipo di gioia apprezzi di più, quella della gente rumorosa o quella che sale nel silenzio esterno dall'interno della tua anima eterna?
Parte 1 - lunghi percorsi erronei di persone credenti e anime ultraterrene attraverso insegnamenti religiosi, credi e costumi.
Parte 2 - lunghi percorsi erronei di persone credenti e anime ultraterrene attraverso insegnamenti religiosi, credi e costumi.
Parte 3 - lunghi percorsi sbagliati di persone credenti e anime ultraterrene attraverso insegnamenti religiosi, credi e costumi
Situazione finanziaria ed economica devastante - come dovrebbero comportarsi i rimpatriati celesti in questo tempo preoccupante

Parte 1 - Una malattia o un evento angosciante può essere ricondotto a cause stabilite in precedenza?
Parte 2 - Una malattia o un evento angosciante può essere ricondotto a cause precedenti?
Istruzioni divine per la via interiore di ritorno in una vita sensibile e beata della nostra casa di luce
Parte 1 - la conoscenza sconosciuta sugli esseri celesti ed extraterrestri così come l'adulazione - e cosa c'è dietro
Parte 2 - la conoscenza sconosciuta sugli esseri celesti ed extraterrestri così come l'adulazione - e cosa c'è dietro
Parte 3 - la conoscenza sconosciuta sugli esseri celesti ed extraterrestri e l'adulazione - e cosa c'è dietro
Risposta dello Spirito di Dio ai lettori che erano preoccupati per le gocce d'amore
Parte 1 - Ciò che l'uomo sperimenta nella fase del morire e dopo la morte l'anima nell'aldilà
Parte 2 - Ciò che l'essere umano sperimenta nella fase del morire e dopo la morte l'anima nell'aldilà

Il significato dell'Io Sono da una prospettiva celeste
Parte 3 - Ciò che l'essere umano sperimenta nella fase del morire e dopo la morte l'anima nell'aldilà
Parte 4 - Ciò che l'essere umano sperimenta nella fase del morire e dopo la morte l'anima nell'aldilà
Parte 5 - Ciò che l'essere umano sperimenta nella fase del morire e dopo la morte l'anima nell'aldilà
Parte 1 - Differenza tra la vita impersonale degli esseri celesti e la vita personale degli esseri umani
Parte 2 - Differenza tra la vita impersonale degli esseri celesti e la vita personale degli esseri umani
Parte 3 - Differenza tra la vita impersonale degli esseri celesti e la vita personale degli esseri umani
Visione e applicazione sbagliata del pensiero positivo con conseguenze disastrose

Parte 1 - Dipendenze - le loro cause e come superarle dal punto di vista celeste - e altri argomenti
Parte 2 - Dipendenze - le loro cause e come superarle dal punto di vista celeste - e altri argomenti
Spiegazione dello spirito universale dell'amore riguardo alle domande dei lettori
Gesù è morto sulla croce?
Gli araldi sono obbligati a ricevere messaggi divini in qualsiasi momento?
Parte 1 - L'origine degli atti di culto rituale e i pericoli che rappresentano per le persone e le anime da una prospettiva celeste
Parte 2 - L'origine degli atti rituali di culto e i pericoli che rappresentano per le persone e le anime da una prospettiva celeste
Cosa sono le Gocce d'Amore dell'Io Sono di Dio? - Fonte dei messaggi divini
Affrontare le cellule dell'occhio nelle malattie lievi e gravi
Perché le persone impazienti e irrequiete che amano Dio non riescono a stabilire una relazione interiore con la sottile vita celeste della loro anima
L'adulazione - un modo di essere poco attraente e ingannevole
La musica degli esseri celesti rispetto alle composizioni mondane

Uso del termine legge o regolarità nei messaggi
Parte 1 - Particelle minerali sottili - Primi elementi di vita per la creazione dell'universo
Parte 2 - Particelle minerali sottili - Primi elementi di vita per la creazione dell'universo
Perché i messaggi delle gocce d'amore divine cambiano di volta in volta nel loro messaggio e nella loro espressione?
Incentivo a una vita di umiltà celeste
Creazione dell'essere più geniale, di cuore, impersonale - Dio - da parte di esseri celesti di luce
Comprendere i messaggi solo nel loro significato
Senso e scopo dei messaggi divini attraverso gli araldi
Terapia di guarigione con siero di veleno di serpente dal punto di vista celeste
Ignoranza spirituale dell'umanità con conseguenze tragiche
Impianti spirituali - il loro significato e le loro funzioni insidiose
Cristo parla della sua nascita e della giusta uguaglianza di tutti gli esseri cosmici
Il calcio - un gioco combattivo - illuminato dal punto di vista celeste
Parte 1 - L'umiltà vissuta - la chiave spirituale per il ritorno al cielo e per una vita beata nella giusta uguaglianza
Parte 2 - Umiltà vissuta - la chiave spirituale per il ritorno al cielo e una vita beata di giusta uguaglianza
Raccolta amorevole e preparazione dei doni della natura
Parte 1 - Perché gli esseri extraterrestri compiono azioni cosmicamente significative nel nostro sistema solare con le loro astronavi
Parte 2 - Perché gli esseri extraterrestri compiono azioni cosmicamente significative nel nostro sistema solare con le loro astronavi

Cibi dolci e i loro effetti collaterali indesiderati
un discorso forte ed energico - un comportamento dominante che non tollera le risposte
Parte 1 - grande clamore su presunti rappresentanti di Dio in questo mondo - un mondo di inganni
Parte 2 - grande clamore su presunti rappresentanti di Dio in questo mondo - un mondo di inganni
istruzioni serie da un extraterrestre
forte ritiro dell'energia vitale da parte di persone e anime povere di energia
La vita di vagabondaggio e di fuga di Gesù Cristo
un desiderio fanatico di libertà e giustizia con gravi conseguenze
Parte 1 - saggezza cosmica e suggerimenti per il riorientamento spirituale
Parte 2 - saggezza cosmica e suggerimenti per il riorientamento spirituale
Parte 1 - sé interiore - energia e immagazzinamento di dati del nostro corpo di luce interiore (anima) così come ampie misure per la creazione della salvezza

Parte 2 - sé interiore - energia e immagazzinamento di dati del nostro corpo di luce interiore (anima) così come ampie misure per la creazione della salvezza
Possibilità di guarigione attraverso l'attivazione delle cellule degenerate
Falsa immagine di Dio attraverso araldi fuorviati
Perché anche le malattie maligne sono curabili
Perché non ci sarà una vita paradisiaca sulla terra
L'orgoglio e l'arroganza bloccano il ritorno al cielo
Breve introduzione per i nuovi lettori di Love Drops
Preghiera del cuore di un tipo spiritualmente più elevato
falsa immagine dell'uomo con inaspettate conseguenze tragiche
Parte 1 - Perché l'anima incarnata plasma già il carattere del bambino
Parte 2 - Perché l'anima incarnata plasma già il carattere del bambino
La fine della vita extra-celeste inizia ora
Appello del cuore dello Spirito d'Amore Universale a tutti gli esseri extra-mondani

Il perfezionismo dalla prospettiva del Dio-Spirito e di un filosofo
previsione ottimistica del giorno con un effetto piacevole
Connessione energetica degli esseri incarnati del piano di salvezza con i loro fratelli e sorelle planetari celesti.
Visione del Regno della pace - il grande errore dei credenti in Dio e delle anime ultraterrene
Parte 1 - conoscenza sconosciuta della vita terrena e celeste di Gesù Cristo
convivenza difficile con persone dominanti, supponenti e polemiche
pericoli inimmaginabili da anime invadenti per persone curiose con capacità psichiche
Parte 2 - conoscenza sconosciuta della vita terrena e celeste di Gesù Cristo
Dove si trovano oggi alcune persone medianiche che ricevono annunci su eventi mondiali vicini e preoccupanti?
Auguri di cuore dagli esseri celesti a tutti i rimpatriati celesti per il tempo a venire sulla terra.

Messaggio di un essere extraterrestre all'umanità
La doppia vita degli esseri celesti nello stesso stato di coscienza e la costante armonia dei cuori
Parte 1 - Significato dei giudizi - positivi e negativi - per l'origine della creazione e la doppia vita celeste
Parte 2 - Significato dei giudizi - positivi e negativi - per l'emergere della creazione e della doppia vita celeste
Le ragioni del massiccio afflusso di rifugiati - una tragedia dal punto di vista celeste-divino
Parte 3 - conoscenza sconosciuta della vita terrena e celeste di Gesù Cristo
Perché una ferma fede in Dio e le preghiere del cuore da sole non sono sufficienti per avvicinare l'uomo e la sua anima al Regno dei Cieli
Perché è così importante chiedere con il cuore, ringraziare e ricevere il grazie degli altri
Intuizioni di un puro essere di luce dalla sua vita terrena, per l'auto-esplorazione per i ritorni celesti
Parte 1 - Il mal d'amore del malato di cuore
Parte 2 - Lo strazio dell'essere umano del cuore

inesauribile e illimitato dono disinteressato dell'amore divino - come può essere meglio compreso

Superare la vita indifferente e il disinteresse spirituale
Salto del palo - cause e preavvisi dello spirito di Dio
Chiamata dello spirito d'amore - non sprecare energie vitali

Chiamata di risveglio dello Spirito-Dio a tutti gli esseri incarnati
Parte 1 - Le interpretazioni dei cartomanti sul futuro un'impresa rischiosa e pericolosa
Parte 2 - Le interpretazioni future degli indovini un'impresa rischiosa e pericolosa
Attività della Cosmic Rescue Alliance nei sistemi solari materiali
Parte 1 - Come si è arrivati alla colonizzazione della terra con piante ed esseri viventi
Parte 2 - Come è avvenuta la colonizzazione con piante ed esseri viventi sulla Terra
Il significato dell'altruismo da un punto di vista celeste
Programmazione dei nuclei delle cellule materiali per ricevere energie divine bipolari
L'avventura infruttuosa degli esseri profondamente caduti con sofferenze insuperabili sta per finire
Che possibilità ha lo spirito di Dio di prevedere e scongiurare situazioni di pericolo cosmico e planetario.
Parte 1 - Messaggio da un cespuglio di rose
Parte 2 - Comunicazione di un cespuglio di rose
Parte 3 - Messaggio da un cespuglio di rose
I desideri del cuore per l'anno 2020

Umiltà celeste - cosa ne hanno fatto gli apostati caduti
Risposta dello spirito d'amore alle traduzioni dei messaggi e alle regole di vita celesti
Istruzione dello spirito dell'amore in colori pastello celestiali e creazione di siti web di gocce d'amore
Superare la paura della persecuzione
Indirizzare le cellule del corpo in caso di infezioni simil-influenzali e per la protezione contro gli invasori di virus
Raccomandazioni dello spirito d'amore per i tempi terreni di disastro e di emergenza
Grande gioia nell'essere celeste per un evento terreno significativo
Approfondimento della vita imperfetta degli araldi e dei cosiddetti profeti di Dio
Effetti delle frequenze elettromagnetiche dei dispositivi di guarigione sulle cellule del corpo e sull'anima da un punto di vista celeste-divino
Parte 1 - Perché il progresso spirituale e tecnico non dovrebbe essere rifiutato
Parte 2 - Perché il progresso spirituale e tecnico non dovrebbe essere rifiutato
Opera della nostra Madre della Creazione Originale nella Caduta e il suo prematuro ritorno al cielo
Introduzione alla collezione di temi
A cosa serve la collezione di temi?
Collezione di temi 1
Raccolta di temi 2
Raccolta di argomenti 3
Perché alcune persone sono infastidite da alcune parole del messaggio
Parte 4 - conoscenza sconosciuta della vita terrena e celeste di Gesù Cristo
Parte 5 - conoscenza sconosciuta della vita terrena e celeste di Gesù Cristo

Parte 6 - conoscenza sconosciuta della vita terrena e celeste di Gesù Cristo
Parte 7 - conoscenza sconosciuta della vita terrena e celeste di Gesù Cristo
Parte 8 - conoscenza sconosciuta della vita terrena e celeste di Gesù Cristo
Incontro con un extraterrestre
Raccolta di argomenti 4
Cristo descrive il suo bellissimo ritorno celeste
Parte 1 - trasformazione interiore in un essere personalmente poco appariscente e cordiale di origine celeste
Parte 2 - trasformazione interiore in un essere personalmente poco appariscente e cordiale di origine celeste
Come si è arrivati alle prime creazioni e procreazioni di esseri da parte dei genitori originali nella pre-creazione celeste
Parte 3 - trasformazione interiore in un essere personalmente poco appariscente e cordiale di origine celeste
