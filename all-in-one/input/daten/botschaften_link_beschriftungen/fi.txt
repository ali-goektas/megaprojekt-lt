Pieneläimet - tuholaisemme

Jumalan kehotukset ja avunpyynnöt
Kokemuksia joenuomassa olevasta vedestä
haitalliset maasäteet maanalaisten vesireittien kautta.
Eläinmaailman sanoinkuvaamaton kärsimys
Kehon liikkeet tanssissa
liioiteltu, terveystietoinen elämä
Kehon soluihin puuttuminen huonovointisuuden yhteydessä

minun ja sinun - putoavien olentojen sitova elämäntapa
liioiteltu käyttäytyminen syömisen yhteydessä
Vesiputouksen tarkastelu taivaallisten olentojen näkökulmasta
riippuvuus tupakasta
taivaallisten olentojen pitkät ja vaivalloiset valmistelut luomakunnan pelastamiseksi ja heidän tuskalliset inkarnaationsa.
tuhoisat vaikutukset ruumiiseen ja sieluun, jotka johtuvat uskonnollisista syistä tapahtuvasta siveellisestä kuolemanrangaistuksesta ja pakottamisesta.
Kehon solujen käsittely vakavan sairauden aikana
Kehon solujen käsittely ennen leikkausta
vääriä käsityksiä sisäisestä polusta Jumalan luo
näkemys näkymättömistä ja käsittämättömistä kosmisista tapahtumista.
Ihoallergioiden alkuperä

maallisten sielujen maanpäällisyys ja niiden näkymätön vaikutus ihmiselämään.
Jumalallinen ohje tiedostaa ja hallita omia ajatuksiaan.
Miten ymmärtää jumalallista lempeyttä
Itsesyytökset ja masennus
Tsunamin uhrien traaginen kohtalo
Omien teeskentelyjen tunnistaminen, analysointi ja korjaaminen.
Oletukset hyödyllisinä epämiellyttäviä tunteita nähdä läpi monitulkintaisia ihmisiä
Elämä tässä petoksen maailmassa
Jumalan tarjous lisätä elämänenergiaa
Työelämän orjia ja ajureita
Osa 1 - Pelko elintarvikkeiden sisältämistä epäpuhtauksista
Osa 2 - Elintarvikkeiden sisältämien haitallisten aineiden pelko
Mitä tarkoittaa kosmisten olentojen vapaus ja riippumattomuus?
maapallon dramaattinen energiatila, jolla on tuhoisia seurauksia kaikelle elämälle.
hienovaraisten alkuatomien läheiset suhteet aineellisen maailmankaikkeuden muuttuneisiin, tiivistettyihin atomeihin.
oikein ymmärretty vaatimattomuus, jotta voidaan elää lähellä taivaallisten olentojen jumalallista oikeudenmukaisuutta.
energisempi ruoan saanti tulielementin yhteydessä.

vaikea vapautuminen maallisista ja luennoivista elämänperiaatteista sekä musiikilliset sävellykset ilman taivaallista ääntä.
jumalallisen sielun erottaminen ihmisruumiista tuonpuoleiseen.
lohduttavia ja auttavia sanoja universaalista rakkauden hengestä itsetuntemukseen vanhuksille ja yksinäisille ihmisille.
Miten ymmärtää kaukonäköisemmin Jumalan käsitys rikkomuksistamme lakia vastaan ja niiden anteeksiantamisesta
Missä kerätyt aarteet ovat, sinne sinut vedetään yhä uudelleen ja uudelleen.
Lääketieteellisten maallikoiden itsetuntemus puheäänestä, kinesiologiasta ja omasta tahdosta parantamisesta
tärkeitä ohjeita Jumalan Hengeltä Minä olen rakkauden pisaroiden kääntäjille muille kielille.
suljettu sydän
mahdollinen tie ulos omasta sydämen viileydestä elää jälleen taivaallisten kaksoisolentojen sydämen lämmössä.
Kultainen keskiarvo syömiskäyttäytymisessä
Voiko sisäinen ihmiskunta vielä estää maailmanlopun tapahtumat maan päällä ja maailman ennenaikaisen lopun, jonka aikaisemmat näkijät ennustivat?
Energiattomat mikroeläimet tuhoavat luontoa
Rehellisyys paljastaa valheen
lainrikkomusten armoton henkinen rapautuminen ihmiskehossa.
tiettyjen ruoka-aineiden ja mausteiden sietämättömyys hurskailla ja herkillä ihmisillä - uskollisuus, joka on onnellisen parisuhteen edellytys.
huolestuneet luonto-olennot kertovat vaikeasta elämäntilanteestaan ja tehtävistään puutarhoissamme.
teeskennelty uskollisuus avioliitossa ja parisuhteessa - sydämen kylmettyneiden tekopyhien kaksoiselämää
Minne ovat kadonneet teidän sydämelliset ja jalot taivaalliset olento-ominaisuutenne, jotka ovat peräisin jumalallisesta nerokkuudesta?
Suuri elämänenergian menetys säännöllisten kohtaamisten kautta maallisten, omapäisten ihmisten kanssa.
Tietoisuus ihmiselämästä
Rehellisyys, uskollisuus ja kunniantunto - harmonisen ja onnellisen parisuhteen tärkeimmät jalot ominaisuudet.
Kehon nivelkipua aiheuttavien solujen käsitteleminen
Osa 1 - Uskontojen ja uskonyhteisöjen synty ja niistä syntynyt fanaattinen usko Jumalaan, johon liittyy ihmisten ja tuonpuoleisten sielujen loputon kärsimys.
Osa 2 - Uskontojen ja uskonnollisten yhteisöjen synty ja siitä johtuva fanaattinen usko Jumalaan, johon liittyy ihmisten ja tuonpuoleisten sielujen loputon kärsimys.
Miksi jotkut Jumalaa rakastavat ihmiset eivät kestä puhua vakavasti?

Tie henkistymiseen ja taivaalliseen vapauteen
Itsevastuu omasta elämästä - tulen elementti ja ruumiin polttaminen
Osa 1 - Syyt väitetysti parantumattomiksi väitetyille sairauksille ja niiden paranemismahdollisuuksille jumalallisesta näkökulmasta - sekä vierekkäisten ja toisiinsa liittyvien, täysin erilaisten elintapojen maailmojen luominen.
Osa 2 - Syyt väitetyille parantumattomille sairauksille ja niiden paranemismahdollisuuksille jumalallisesta näkökulmasta - sekä vierekkäisten ja toisiinsa liittyvien täysin erilaisten elintapojen maailmojen luominen.
Osa 3 - Syyt väitetysti parantumattomiksi väitetyille sairauksille ja niiden paranemismahdollisuuksille jumalallisesta näkökulmasta - sekä vierekkäisten ja toisiinsa liittyvien, täysin erilaisen elämäntavan maailmojen luominen.
Kehon soluihin vaikuttaminen niiden toimintojen aktivoimiseksi.
Osa 1 - Armo - rangaistuksen anteeksiantaminen hallitsevan kansan toimesta - uskovat liittävät sen virheellisesti jumaluuden hyväntahtoisuuteen.
Osa 2 - Armo - Hallitsevan kansan rankaiseminen - uskovaiset liittävät sen virheellisesti jumaluuden hyväntahtoisuuteen.
Hyvät tekomme - ovatko ne todella sitä, mitä luulemme niiden olevan?
Miksi taivaalliset kaksoisolennot joskus eroavat toisistaan ja muodostavat uusia liittoja?
Jumalan toivoa antavat neuvot vakavan sairauden diagnosoinnin jälkeen ja meille vielä tuntemattomat vihjeet elimistön solujen käyttäytymisestä.
tuhoisien myrskyjen, tulvien, kuumuuden ja kuivuuden lisääntyminen maapallolla - hitaasti mutta vääjäämättömästi etenevän maanpäällisen elämänajan lopun airut.
Osa 1 - sietämättömät vaiheet Jumalan todellisten sanansaattajien elämässä
Osa 2 - sietämättömät vaiheet Jumalan todellisten sanansaattajien elämässä
Syyt kiinteästä aineesta koostuvan maailmankaikkeuden ja epätäydellisen ihmisen luomiseen
Rakkaus sydämestä - miten sisäiset ihmiset voivat ymmärtää ja elää sitä paremmin?
Sydämellinen viestintä omena kokemuksia sen emäpuu
taivaallisten olentojen kuva-, väri- ja äänikieli, toisin kuin ihmisten kommunikointi
Jumalan Hengen antama tärkeä rukoussuositus hengellisesti kypsille ihmisille.
Avioliitot ja parisuhteet elämän taivaallisten lakien näkökulmasta katsottuna.
Heilurit - oletettavasti hyödyllinen toiminta, johon liittyy odottamattomia vaaroja.

Kristillinen joulu ja ristin symboli - ovatko ne todella jumalallista tahtoa ja ovatko ne erittäin tärkeitä taivasten valtakuntaan palaaville?
Osa 1 - Kasvissyöjän elämäntapa - elämän taivaallisen periaatteen näkökulmasta ja muita aiheita
Osa 2 - Kasvissyöjän elämäntapa - elämän taivaallisen periaatteen näkökulmasta ja muita aiheita
Osa 3 - Kasvissyöjän elämäntapa - elämän taivaallisen periaatteen näkökulmasta ja muita aiheita
Osa 4 - Kasvissyöjän elämäntapa - elämän taivaallisen periaatteen näkökulmasta ja muita aiheita
Osa 1 - harmoninen, rauhaa rakastava ja tasapainoinen sisäinen elämäntapa - kannattava tavoite
Osa 2 - harmoninen, rauhaa rakastava ja tasapainoinen sisäinen elämäntapa - kannattava tavoite
Ihmisen perfektionismi taivaallisesta näkökulmasta katsottuna
maksuvälineiden ja lahjojen asianmukainen käsittely sekä osatekijät
Osa 1 - Energiantuotanto ydinfissiolla ja ydinvoimalaitosten toiminta taivaallisesta näkökulmasta katsottuna.
Osa 2 - Energiantuotanto ydinfissiolla ja ydinvoimalaitosten toiminta taivaallisesta näkökulmasta katsottuna.
Poliitikon valinta - Jumalan hengen näkökulmasta katsottuna
Syvällinen itsetuntemus virheistä ja heikkouksista - päivän tärkein tehtävä taivaallisille paluumuuttajille, jotka pyrkivät henkiseen kasvuun.
Osa 1 - Taivaallisten olentojen evoluutioelämä ja heidän sydänyhteytensä persoonattomaan Minä olen -jumaluuteen.
Osa 2 - Taivaallisten olentojen evoluutioelämä ja heidän sydänyhteytensä persoonattomaan Minä olen -jumaluuteen.
Osa 3 - Taivaallisten olentojen evoluutioelämä ja heidän sydämen yhteytensä persoonattomaan Minä olen -jumaluuteen.
Osa 1 - syvästi langenneiden olentojen epäterveellinen henkilökohtainen elämää parantava elämä
Osa 2 - syvästi langenneiden olentojen epäterveellinen elämä, joka lisää heidän persoonallisuuttaan.
Kumpaa iloa arvostat enemmän - sitä, joka tulee äänekkäistä ihmisistä, vai sitä, joka nousee ulkoisessa hiljaisuudessa ikuisen sielusi sisältä?
Osa 1 - uskovien ihmisten ja tuonpuoleisten sielujen pitkät virheelliset polut uskonnollisten opetusten, uskontunnustusten ja tapojen kautta.
Osa 2 - uskovien ihmisten ja tuonpuoleisten sielujen pitkät harhapolut uskonnollisten opetusten, uskontunnustusten ja tapojen kautta.
Osa 3 - uskovien ihmisten ja tuonpuoleisten sielujen pitkät harhapolut uskonnollisten opetusten, uskontunnustusten ja tapojen kautta.
tuhoisa rahoitus- ja taloustilanne - miten taivaallisten paluumuuttajien pitäisi käyttäytyä tänä huolestuttavana aikana?

Osa 1 - Voidaanko sairaus tai ahdistava tapahtuma jäljittää aiemmin asetettuihin syihin?
Osa 2 - Voidaanko sairaus tai ahdistava tapahtuma jäljittää aikaisempiin syihin?
Jumalalliset ohjeet sisäiselle tielle takaisin herkkään autuaalliseen elämään valon kodissamme
Osa 1 - tuntematon tieto taivaallisista ja maan ulkopuolisista olennoista sekä imartelu - ja mitä sen takana on
Osa 2 - tuntematon tieto taivaallisista ja maan ulkopuolisista olennoista sekä imartelu - ja mitä sen takana on
Osa 3 - tuntematon tieto taivaallisista ja maan ulkopuolisista olennoista sekä imartelu - ja mitä sen takana on
Jumalan Hengen vastaus lukijoille, jotka olivat huolissaan rakkauden pisaroista.
Osa 1 - Mitä ihminen kokee kuolemanvaiheessa ja kuoleman jälkeen sielu tuonpuoleisessa elämässä
Osa 2 - Mitä ihminen kokee kuolemanvaiheessa ja kuoleman jälkeen sielu tuonpuoleisessa elämässä

Minä olen -olemuksen merkitys taivaallisesta näkökulmasta katsottuna
Osa 3 - Mitä ihminen kokee kuolemanvaiheessa ja kuoleman jälkeen sielu tuonpuoleisessa elämässä
Osa 4 - Mitä ihminen kokee kuolemanvaiheessa ja kuoleman jälkeen sielu tuonpuoleisessa elämässä
Osa 5 - Mitä ihminen kokee kuolemanvaiheessa ja kuoleman jälkeen sielu tuonpuoleisessa elämässä
Osa 1 - Taivaallisten olentojen persoonattoman elämän ja ihmisten henkilökohtaisen elämän välinen eroavaisuus
Osa 2 - Taivaallisten olentojen persoonattoman elämän ja ihmisten henkilökohtaisen elämän välinen ero.
Osa 3 - Taivaallisten olentojen persoonattoman elämän ja ihmisten henkilökohtaisen elämän välinen eroavaisuus
Positiivisen ajattelun väärä näkemys ja soveltaminen, jolla on katastrofaaliset seuraukset.

Osa 1 - Riippuvuudet - niiden syyt ja niiden voittaminen taivaallisesta näkökulmasta - ja muita aiheita.
Osa 2 - Riippuvuudet - niiden syyt ja niiden voittaminen taivaallisesta näkökulmasta - ja muita aiheita.
Yleismaailmallisen rakkauden hengen selitys lukijan kysymyksiin liittyen
Kuoliko Jeesus ristillä?
Ovatko sanansaattajat velvollisia vastaanottamaan jumalallisia viestejä milloin tahansa?
Osa 1 - Rituaalisten kulttien alkuperä ja niiden ihmisille ja sieluille aiheuttamat vaarat taivaallisesta näkökulmasta katsottuna
Osa 2 - Rituaalisten palvontatoimien alkuperä ja niiden ihmisille ja sieluille aiheuttamat vaarat taivaallisesta näkökulmasta katsottuna
Mitä ovat Jumalan Minä olen -rakkauspisarat? - Jumalallisten viestien lähde
Silmän solujen käsittely lievissä ja vaikeissa sairauksissa
Miksi kärsimättömät ja levottomat Jumalaa rakastavat ihmiset eivät onnistu luomaan sisäistä suhdetta sielunsa hienovaraisen taivaallisen elämän kanssa?
Imartelu - epämiellyttävä petollinen tapa olla
Taivaallisten olentojen musiikki verrattuna maallisiin sävellyksiin

Termien laki tai säännönmukaisuus käyttö viesteissä.
Osa 1 - Hienovaraiset mineraalihiukkaset - Elämän ensimmäiset elementit maailmankaikkeuden luomista varten.
Osa 2 - Hienovaraiset mineraalihiukkaset - Elämän ensimmäiset alkuaineet maailmankaikkeuden luomista varten.
Miksi jumalallisen rakkauden pisaraviestien sanoma ja ilmaisu muuttuvat aika ajoin?
Kannustin taivaallisen nöyryyden elämään
Nerokkaimman, sydämellisimmän, persoonattomimman olennon - Jumalan - luominen taivaallisten valo-olentojen toimesta.
Viestien ymmärtäminen vain niiden merkityksessä
Sanansaattajien välittämien jumalallisten viestien merkitys ja tarkoitus
Parantava hoito käärmeen myrkkyseerumin avulla taivaallisesta näkökulmasta katsottuna
Ihmiskunnan henkinen tietämättömyys, jolla on traagiset seuraukset.
Henkiset implantit - niiden merkitys ja salakavalat toiminnot
Kristus puhuu syntymisestään ja kaikkien kosmisten olentojen oikeudenmukaisesta tasa-arvosta.
Jalkapallo - taisteleva peli - taivaallisesta näkökulmasta valaistuna
Osa 1 - Eletty nöyryys - hengellinen avain taivaalliseen paluuseen ja autuaalliseen elämään oikeudenmukaisessa tasa-arvoisuudessa
Osa 2 - Eletty nöyryys - hengellinen avain taivaalliseen paluuseen ja autuaalliseen elämään vanhurskaassa tasa-arvossa.
Luonnon lahjojen rakastava sadonkorjuu ja valmistelu
Osa 1 - Miksi maan ulkopuoliset olennot suorittavat avaruusaluksillaan kosmisesti merkittäviä toimia aurinkokunnassamme?
Osa 2 - Miksi maan ulkopuoliset olennot suorittavat avaruusaluksillaan kosmisesti merkittäviä toimia aurinkokunnassamme?

Makeat elintarvikkeet ja niiden ei-toivotut sivuvaikutukset
kovaääninen, energinen puhe - dominoiva käytös, joka ei siedä vastapuheita.
Osa 1 - suuri kohu Jumalan väitetyistä edustajista tässä maailmassa - petosten maailma
Osa 2 - suuri kohu Jumalan väitetyistä edustajista tässä maailmassa - petosten maailma
vakavia ohjeita avaruusolennolta
energiapuutteisten ihmisten ja sielujen voimakas elämänenergian vetäytyminen.
Jeesuksen Kristuksen elämä vaelluksen ja pakenemisen merkeissä
fanaattinen vapauden ja oikeudenmukaisuuden vaatimus, jolla on vakavia seurauksia.
Osa 1 - Kosminen viisaus ja vihjeitä henkiseen uudelleensuuntautumiseen.
Osa 2 - Kosminen viisaus ja vinkkejä henkiseen uudelleensuuntautumiseen.
Osa 1 - sisäinen minämme - sisäisen valokehomme (sielumme) energia- ja tietovarastot sekä laajat toimenpiteet luomiseksi pelastukseksi.

Osa 2 - sisäinen minämme - sisäisen valokehomme (sielumme) energia- ja tietovarastot sekä laajat toimenpiteet luomiseksi pelastukseksi.
Mahdollisuus parantua rappeutuneiden solujen aktivoinnin kautta.
Väärä kuva Jumalasta harhaanjohdettujen sanansaattajien kautta
Miksi pahanlaatuiset sairaudet ovat myös parannettavissa
Miksi maan päällä ei tule olemaan paratiisimaista elämää?
Ylpeys ja ylimielisyys estävät taivaallisen paluun.
Lyhyt esittely Love Dropsin uusille lukijoille
Henkisesti korkeampi sydämen rukous
väärä kuva ihmisestä, jolla on odottamattomat traagiset seuraukset
Osa 1 - Miksi inkarnoitunut sielu muokkaa jo lapsen luonnetta?
Osa 2 - Miksi ruumiillistunut sielu muokkaa jo lapsen luonnetta?
Avaruuden ulkopuolisen elämän loppu alkaa nyt.
Universaalisen Rakkauden Hengen sydämen kutsu kaikille maallisuuden ulkopuolisille olennoille

Perfektionismi Jumala-Hengen ja filosofin näkökulmasta
optimistinen ennuste päivän miellyttävä vaikutus
Pelastussuunnitelman inkarnoituneiden olentojen energeettinen yhteys taivaallisiin planeettaveljiinsä ja -sisaruksiinsa.
Rauhan valtakunnan visio - Jumalaan uskovien ihmisten ja tuonpuoleisten sielujen suuri erehdys
Osa 1 - tuntematon tieto Jeesuksen Kristuksen maallisesta ja taivaallisesta elämästä.
vaikea rinnakkaiselo hallitsevien, omapäisten ja riitaisien ihmisten kanssa.
uteliaille ihmisille, joilla on psyykkiset kyvyt, on odottamattomia vaaroja tunkeilevien sielujen taholta.
Osa 2 - tuntematon tieto Jeesuksen Kristuksen maallisesta ja taivaallisesta elämästä.
Mistä jotkut meedionistiset ihmiset saavat nykyään ilmoituksia läheisistä huolestuttavista maailmantapahtumista?
Taivaallisten olentojen sydämelliset toivotukset kaikille taivaallisille paluumuuttajille tulevaa aikaa varten maan päällä.

Maan ulkopuolisen olennon viesti ihmiskunnalle
Taivaallisten olentojen kaksoiselämä samassa tietoisuuden tilassa ja sydämien jatkuva harmonia.
Osa 1 - Tuomioiden - myönteisten ja kielteisten - merkitys luomakunnan alkuperälle ja taivaalliselle kaksoiselämälle.
Osa 2 - Tuomioiden - myönteisten ja kielteisten - merkitys luomakunnan ja taivaallisen kaksoiselämän syntymiselle.
Syyt valtavaan pakolaisvirtaan - tragedia taivaallis-jumalallisesta näkökulmasta katsottuna
Osa 3 - tuntematon tieto Jeesuksen Kristuksen maallisesta ja taivaallisesta elämästä.
Miksi luja usko Jumalaan ja sydämen rukoukset eivät yksin riitä tuomaan ihmistä ja hänen sieluaan lähemmäksi taivasten valtakuntaa?
Miksi on niin tärkeää pyytää sydämestä, kiittää ja ottaa vastaan toisten kiitos.
Puhtaan valo-olennon oivalluksia maallisesta elämästään, itsetutkiskelua varten taivaallisille paluumuuttajille.
Osa 1 - Sydänsairaan rakkaussairaus
Osa 2 - Sydämen ihmisen sydänsuruja

jumalallisen rakkauden ehtymätön ja rajaton epäitsekäs antaminen - miten se voidaan ymmärtää paremmin?

Välinpitämättömän elämän ja hengellisen välinpitämättömyyden voittaminen
Seiväshyppy - Jumalan hengen syyt ja ennakkovaroitus
Rakkauden hengen kutsu - älä tuhlaa elämänenergiaa.

Jumala-Hengen herätyskutsu kaikille ruumiillistuneille olennoille.
Osa 1 - Ennustajien tulkinnat tulevaisuudesta riskialtis ja vaarallinen yritys
Osa 2 - Tulevaisuuden tulkinnat ennustajista riskialtista ja vaarallista toimintaa
Kosmisen pelastusliiton toiminta aineellisissa aurinkokunnissa
Osa 1 - Miten maa asutettiin kasveilla ja elävillä olennoilla?
Osa 2 - Miten kasveilla ja elävillä olennoilla tapahtuva kolonisaatio tapahtui maapallolla?
Epäitsekkyyden merkitys taivaallisesta näkökulmasta katsottuna.
Materiaalisolujen ytimien ohjelmointi vastaanottamaan kaksinapaista jumalallista energiaa.
Syvästi langenneiden olentojen epäonnistunut seikkailu, jossa on ylitsepääsemätöntä kärsimystä, lähestyy loppuaan.
Millaisia mahdollisuuksia Jumalan hengellä on ennakoida ja torjua kosmisia ja planetaarisia vaaratilanteita.
Osa 1 - Viesti ruusupensaasta
Osa 2 - Ruusupensaan viestintä
Osa 3 - Viesti ruusupensaasta
Sydämen toiveet vuodelle 2020

Taivaallinen nöyryys - mitä luopuneet langenneet olennot tekivät siitä.
Rakkauden hengen vastaus sanoman käännöksiin ja taivaallisiin elämänsääntöihin
Rakkauden hengen opettaminen taivaallisissa pastelliväreissä ja rakkauspisaran verkkosivustojen luominen.
Vainon pelon voittaminen
Kehon soluihin kohdistuvat toimet flunssan kaltaisten infektioiden varalta ja suojaamiseksi virusten tunkeutujia vastaan.
Rakkauden hengen suositukset maallisia katastrofi- ja hätäkausia varten.
Suuri ilo taivaallisessa olennossa merkittävästä maallisesta tapahtumasta.
Oivallus Jumalan sanansaattajien ja niin kutsuttujen profeettojen epätäydellisestä elämästä.
Parantavien laitteiden sähkömagneettisten taajuuksien vaikutukset kehon soluihin ja sieluun taivaallis-jumalallisesta näkökulmasta katsottuna
Osa 1 - Miksi henkistä ja teknistä kehitystä ei pidä torjua?
Osa 2 - Miksi henkistä ja teknistä kehitystä ei pidä torjua?
Alkuperäisen luomakuntaäidin työ syntiinlankeemuksessa ja hänen ennenaikainen taivaallinen paluunsa.
Johdanto teemakokoelmaan
Mitä varten teemakokoelma on?
Teemakokoelma 1
Teemakokoelma 2
Aihekokoelma 3
Miksi jotkut sanomat häiritsevät joitakin ihmisiä
Osa 4 - tuntematon tieto Jeesuksen Kristuksen maallisesta ja taivaallisesta elämästä.
Osa 5 - tuntematon tieto Jeesuksen Kristuksen maallisesta ja taivaallisesta elämästä.

Osa 6 - tuntematon tieto Jeesuksen Kristuksen maallisesta ja taivaallisesta elämästä.
Osa 7 - tuntematon tieto Jeesuksen Kristuksen maallisesta ja taivaallisesta elämästä.
Osa 8 - tuntematon tieto Jeesuksen Kristuksen maallisesta ja taivaallisesta elämästä.
Kohtaaminen avaruusolennon kanssa
Aihekokoelma 4
Kristus kuvaa kaunista taivaallista paluutaan
Osa 1 - sisäinen muodonmuutos henkilökohtaisesti huomaamattomaksi ja sydämelliseksi taivaallista alkuperää olevaksi olennoksi.
Osa 2 - sisäinen muodonmuutos henkilökohtaisesti huomaamattomaksi ja sydämelliseksi taivaallista alkuperää olevaksi olennoksi.
Miten alkuperäiset vanhemmat ovat tulleet ensimmäisiin luomuksiin ja olentojen lisääntymisiin taivaallisessa esiluonnossa.
Osa 3 - sisäinen muodonmuutos henkilökohtaisesti huomaamattomaksi ja sydämelliseksi taivaallista alkuperää olevaksi olennoksi.
