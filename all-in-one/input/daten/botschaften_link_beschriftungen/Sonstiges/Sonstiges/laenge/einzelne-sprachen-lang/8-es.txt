El mensaje "instrucciones serias de un extraterrestre" se incluye ahora en el volumen de 2013
Pequeños animales: nuestras plagas

Llamadas de atención y ayuda de Dios
Experiencias del agua en el cauce del río
rayos terrestres nocivos a través de cruces de agua subterráneos
El indecible sufrimiento del mundo animal
El movimiento del cuerpo en la danza
vida exagerada y consciente de la salud
Dirigirse a las células del cuerpo en caso de indisposición

la mía y la tuya - la forma de vida vinculante de los seres que caen
comportamiento exagerado al comer
Ver una cascada desde la perspectiva de los seres celestiales
adicción al tabaco
largos y arduos preparativos de los seres celestiales para la salvación de la creación y sus agónicas encarnaciones
efectos devastadores en el cuerpo y el alma por la mortificación y la compulsión por motivos religiosos
Dirigirse a las células del cuerpo durante una enfermedad grave
Dirigirse a las células del cuerpo antes de la cirugía
falsas ideas sobre el camino interior hacia Dios
La visión de los acontecimientos cósmicos invisibles e incomprensibles
Origen de las alergias cutáneas

terrenal más allá de las almas mundanas y su influencia invisible en la vida humana
Instrucción divina para tomar conciencia y controlar los propios pensamientos
Cómo entender la dulzura divina
Arrepentimiento y estado de ánimo depresivo
Trágico destino de las víctimas del tsunami
Reconocer, analizar y remediar las propias pretensiones
Las suposiciones como sentimientos desagradables útiles para ver a través de las personas ambiguas
Vivir en este mundo de engaños
La oferta de Dios para aumentar la energía vital
Esclavos y conducidos por el mundo del trabajo
Parte 1 - El miedo a los contaminantes en los alimentos
Parte 2 - El miedo a las sustancias nocivas en los alimentos
¿Qué significa la libertad e independencia de los seres cósmicos?
estado energético dramático de la tierra con consecuencias devastadoras para toda la vida
relaciones estrechas de los átomos sutiles primordiales con los átomos alterados y condensados del universo material
la modestia bien entendida para vivir cerca de la justicia divina de los seres celestiales
ingesta de alimentos más energéticos en relación con el elemento fuego

la difícil liberación de los principios mundanos y sermoneadores de la vida, así como las composiciones musicales sin sonido celestial
la salida de un alma piadosa del cuerpo humano hacia el más allá
palabras reconfortantes y útiles del espíritu universal del amor para el auto reconocimiento de las personas mayores y solitarias
Cómo captar con mayor amplitud de miras la comprensión de Dios sobre nuestras transgresiones de la ley y su perdón
Donde están tus tesoros recogidos, allí te atraen una y otra vez
Autoconocimiento del sonido del habla, kinesiología y curación por voluntad propia de los médicos legos
importantes instrucciones del Espíritu de Dios para los traductores de las gotas de amor Yo Soy a otros idiomas
corazón cerrado
posible salida de la propia frialdad del corazón para volver a vivir en el calor del corazón de los seres duales celestiales
La media de oro en el comportamiento alimentario
¿Puede la humanidad interior evitar aún los acontecimientos apocalípticos en la tierra y el prematuro fin del mundo predicho por los primeros videntes?
Destrucción de la naturaleza por microanimales sin energía
La sinceridad expone la mentira
despiadada erosión mental de las infracciones de la ley en el cuerpo humano
la intolerancia a ciertos alimentos y especias en personas piadosas y sensibles - la fidelidad, requisito para una pareja feliz
los seres de la naturaleza preocupados informan sobre su difícil situación vital y sus tareas en nuestros jardines
fidelidad fingida en el matrimonio y la pareja - doble vida de los hipócritas de corazón
¿Adónde han ido a parar tus cualidades de ser celestial, cordial y noble, de genio divino?
Elevada pérdida de energía vital por los encuentros regulares con personas mundanas y de opinión.
Conciencia de la vida humana
Honestidad, fidelidad y sentido del honor: las cualidades nobles más importantes para una pareja armoniosa y feliz
Dirigirse a las células del cuerpo para el dolor articular
Parte 1 - Surgimiento de las religiones y comunidades de fe y la creencia fanática en Dios que surgió de ellas, con un sufrimiento interminable para los seres humanos y las almas de otro mundo
Parte 2 - El origen de las religiones y las comunidades religiosas y la consiguiente creencia fanática en Dios, con un sufrimiento infinito para los seres humanos y las almas del más allá.
Por qué algunas personas que aman a Dios no pueden soportar hablar en serio

El camino de la espiritualización y la libertad celestial
La responsabilidad de la propia vida - el elemento del fuego y la cremación del cuerpo
Parte 1 - Razones de las enfermedades supuestamente incurables y sus posibilidades de curación desde un punto de vista divino - así como la creación de mundos adyacentes y entrelazados de formas de vida completamente diferentes
Parte 2 - Razones de las supuestas enfermedades incurables y sus posibilidades de curación desde un punto de vista divino - así como la creación de mundos adyacentes y entrelazados de formas de vida completamente diferentes
Parte 3 - Razones de las enfermedades supuestamente incurables y sus posibilidades de curación desde un punto de vista divino - así como la creación de mundos adyacentes y entrelazados de una forma de vida completamente diferente.
Dirigirse a las células del cuerpo para activar sus funciones
Parte 1 - Gracia - remisión del castigo por parte del pueblo gobernante - erróneamente asociada por los creyentes con la benevolencia de la deidad
Parte 2 - Misericordia - Castigo del pueblo gobernante - asociado erróneamente por los creyentes con la benevolencia de la deidad
Nuestras buenas acciones, ¿son realmente lo que creemos que son?
Por qué los seres duales celestiales a veces se separan y forman nuevas uniones
El consejo esperanzador de Dios tras el diagnóstico de una enfermedad grave y las pistas sobre el comportamiento de las células del cuerpo que aún desconocemos
Aumento de las tormentas devastadoras, las inundaciones, el calor y las fases de sequía en la tierra, presagios de un lento pero inexorable final de la vida terrenal
Parte 1 - fases insoportables en la vida de los verdaderos heraldos de Dios
Parte 2 - fases insoportables en la vida de los verdaderos heraldos de Dios
Razones para la creación del universo de materia sólida y para la creación del ser humano imperfecto
El amor de corazón: ¿cómo pueden entenderlo y vivirlo mejor las personas del interior?
Comunicación sincera de una manzana sobre las experiencias en su árbol madre
lenguaje de imágenes, colores y sonidos de los seres celestiales en contraste con la comunicación humana
importante recomendación de oración del espíritu de Dios para personas espiritualmente maduras
Los matrimonios y las parejas desde la perspectiva de las leyes celestiales de la vida
Péndulos: una actividad supuestamente útil con peligros inesperados

La Navidad cristiana y el símbolo de la cruz, ¿son realmente la voluntad divina y de gran importancia para los que vuelven al reino de los cielos?
Parte 1 - Modo de vida vegetariano - desde el punto de vista del principio celestial de la vida y otros temas
Parte 2 - Modo de vida vegetariano - desde el punto de vista del principio celestial de la vida y otros temas
Parte 3 - Modo de vida vegetariano - desde el punto de vista del principio celestial de la vida y otros temas
Parte 4 - Modo de vida vegetariano - desde el punto de vista del principio celestial de la vida y otros temas
Parte 1 - Un estilo de vida interior armonioso, amante de la paz y equilibrado: un objetivo que merece la pena
Parte 2 - Un estilo de vida interior armonioso, amante de la paz y equilibrado: un objetivo que merece la pena
El perfeccionismo humano desde el punto de vista celestial
manejo correcto de los medios de pago y los regalos, así como los elementos
Parte 1 - La generación de energía mediante la fisión nuclear y el funcionamiento de las centrales nucleares desde una perspectiva celestial
Parte 2 - La generación de energía mediante la fisión nuclear y el funcionamiento de las centrales nucleares desde una perspectiva celestial
Elección de un político - desde la perspectiva del espíritu de Dios
Profundo autoconocimiento de las faltas y debilidades - la tarea más importante del día para los retornados celestiales al crecimiento espiritual
Parte 1 - La vida evolutiva de los seres celestiales y su conexión cardíaca con la Deidad impersonal Yo Soy
Parte 2 - La vida evolutiva de los seres celestiales y su conexión cardíaca con la Deidad impersonal Yo Soy
Parte 3 - La Vida Evolutiva de los Seres Celestiales y su Conexión de Corazón con la Deidad Impersonal Yo Soy
Parte 1 - persona insana-apreciar la vida de los seres profundamente caídos
Parte 2 - la vida malsana de los seres profundamente caídos que realza su persona
¿Qué tipo de alegría valoras más, la de la gente ruidosa o la que surge en el silencio exterior desde el interior de tu alma eterna?
Parte 1 - largos caminos erróneos de las personas creyentes y de las almas del otro mundo a través de las enseñanzas religiosas, los credos y las costumbres.
Parte 2 - largos caminos erróneos de las personas creyentes y de las almas del otro mundo a través de las enseñanzas religiosas, los credos y las costumbres.
Parte 3 - largos caminos equivocados de personas creyentes y almas de otro mundo a través de enseñanzas religiosas, credos y costumbres
La devastadora situación financiera y económica: cómo deben comportarse los retornados celestiales en esta preocupante época

Parte 1 - ¿Puede una enfermedad o un acontecimiento angustioso remontarse a causas establecidas anteriormente?
Parte 2 - ¿Puede una enfermedad o un acontecimiento angustioso remontarse a causas anteriores?
Instrucciones divinas para el camino interior de vuelta a una vida sensible y dichosa de nuestro hogar de luz
Parte 1 - Conocimientos desconocidos sobre los seres celestes y extraterrestres, así como la adulación - y lo que hay detrás
Parte 2 - Conocimientos desconocidos sobre los seres celestes y extraterrestres, así como la adulación - y lo que hay detrás
Parte 3 - Conocimientos desconocidos sobre los seres celestes y extraterrestres, así como la adulación - y lo que hay detrás
Respuesta del Espíritu de Dios a los lectores que estaban preocupados por las gotas de amor
Parte 1 - Lo que el hombre experimenta en la fase de muerte y después de la muerte el alma en el más allá
Parte 2 - Lo que el ser humano experimenta en la fase de muerte y después de la muerte el alma en el más allá

El significado del Yo Soy desde una perspectiva celestial
Parte 3 - Lo que el ser humano experimenta en la fase de muerte y después de la muerte el alma en el más allá
Parte 4 - Lo que el ser humano experimenta en la fase de muerte y después de la muerte el alma en el más allá
Parte 5 - Lo que el ser humano experimenta en la fase de muerte y después de la muerte el alma en el más allá
Parte 1 - Diferencia entre la vida impersonal de los seres celestiales y la vida personal de los seres humanos
Parte 2 - Diferencia entre la vida impersonal de los seres celestiales y la vida personal de los seres humanos
Parte 3 - Diferencia entre la vida impersonal de los seres celestiales y la vida personal de los seres humanos
Visión y aplicación errónea del pensamiento positivo con consecuencias desastrosas

Parte 1 - Adicciones - sus causas y cómo superarlas desde el punto de vista celestial - y otros temas
Parte 2 - Adicciones - sus causas y cómo superarlas desde el punto de vista celestial - y otros temas
Explicación del espíritu universal del amor en relación con las preguntas de los lectores
¿Murió Jesús en la cruz?
¿Están los heraldos obligados a recibir mensajes divinos en cualquier momento?
Parte 1 - El origen de los actos de culto ritual y los peligros que suponen para las personas y las almas desde una perspectiva celestial
Parte 2 - El origen de los actos rituales de culto y los peligros que suponen para las personas y las almas desde una perspectiva celestial
¿Qué son las Gotas de Amor Yo Soy de Dios? - Fuente de los mensajes divinos
Tratamiento de los glóbulos oculares en las enfermedades leves y graves
Por qué las personas impacientes e inquietas que aman a Dios no logran establecer una relación interior con la sutil vida celestial de su alma
Adulación: una forma de ser poco atractiva y engañosa
La música de los seres celestiales comparada con las composiciones mundanas

Uso del término ley o regularidad en los mensajes
Parte 1 - Partículas minerales sutiles - Primeros elementos de la vida para la creación del universo
Parte 2 - Partículas minerales sutiles - Primeros elementos de vida para la creación del universo
¿Por qué los mensajes de las gotas de amor divino cambian en su mensaje y expresión de vez en cuando?
Incentivo a una vida de humildad celestial
Creación del ser más ingenioso, sincero e impersonal -Dios- por seres celestiales de luz
Entender los mensajes sólo en su significado
Sentido y finalidad de los mensajes divinos a través de los heraldos
Terapia curativa con suero de veneno de serpiente desde el punto de vista celestial
Ignorancia espiritual de la humanidad con trágicas consecuencias
Implantes espirituales: su significado y sus funciones insidiosas
Cristo habla de su nacimiento y de la justa igualdad de todos los seres cósmicos
El fútbol - un juego combativo - iluminado desde el punto de vista celestial
Parte 1 - La humildad vivida - la clave espiritual para el retorno celestial y para una vida dichosa en justa igualdad
Parte 2 - La humildad vivida - la clave espiritual para el regreso al cielo y una vida dichosa de justa igualdad
Cosechar y preparar con amor los dones de la naturaleza
Parte 1 - Por qué los seres extraterrestres realizan acciones de importancia cósmica en nuestro sistema solar con sus naves espaciales
Parte 2 - Por qué los seres extraterrestres realizan acciones de importancia cósmica en nuestro sistema solar con sus naves espaciales

Los alimentos dulces y sus efectos secundarios indeseables
discurso fuerte y enérgico - un comportamiento dominante que no tolera las réplicas
Parte 1 - gran alboroto por los supuestos representantes de Dios en este mundo - un mundo de engaños
Parte 2 - gran alboroto por los supuestos representantes de Dios en este mundo - un mundo de engaños
instrucciones serias de un extraterrestre
fuerte retirada de energía vital por parte de las personas y almas pobres en energía
La vida errante y huidiza de Jesucristo
un afán fanático de libertad y justicia con graves consecuencias
Parte 1: sabiduría cósmica y consejos para la reorientación espiritual
Parte 2: sabiduría cósmica y consejos para la reorientación espiritual
Parte 1 - el yo interior - la energía y el almacenamiento de datos de nuestro cuerpo de luz interior (alma), así como amplias medidas para la creación de la salvación

Parte 2 - el yo interior - la energía y el almacenamiento de datos de nuestro cuerpo de luz interior (alma), así como amplias medidas para la creación de la salvación
Posibilidad de curación mediante la activación de las células degeneradas
Falsa imagen de Dios a través de heraldos equivocados
Por qué las enfermedades malignas también son curables
Por qué no habrá vida paradisíaca en la tierra
El orgullo y la arrogancia bloquean el retorno celestial
Breve introducción para los nuevos lectores de Gotas de Amor
Oración del corazón de un tipo espiritualmente más elevado
falsa imagen del hombre con inesperadas consecuencias trágicas
Parte 1 - Por qué el alma encarnada ya forma el carácter del niño
Parte 2 - Por qué el alma encarnada ya forma el carácter del niño
El fin de la vida extraceleste comienza ahora
Llamada del corazón del Espíritu de Amor Universal a todos los seres extramundanos

El perfeccionismo desde la perspectiva del Espíritu de Dios y de un filósofo
previsión optimista del día con un efecto agradable
Conexión energética de los seres encarnados del plan de salvación con sus hermanos planetarios celestiales.
Visión del reino de la paz - el gran error de los creyentes en Dios y las almas de otro mundo
Parte 1 - Conocimiento desconocido sobre la vida terrenal y celestial de Jesucristo
convivencia difícil con personas dominantes, opinantes y discutidoras
peligros inimaginables de las almas intrusas para los curiosos con capacidades psíquicas
Parte 2 - Conocimiento desconocido sobre la vida terrenal y celestial de Jesucristo
¿De dónde sacan algunos médiums hoy en día los anuncios sobre los preocupantes acontecimientos mundiales cercanos?
Deseos de corazón de los seres celestiales a todos los retornados celestiales para el próximo tiempo en la tierra.

Mensaje de un ser extraterrestre a la humanidad
La vida dual de los seres celestiales en el mismo estado de conciencia y la armonía constante de los corazones
Parte 1 - Importancia de los juicios -positivos y negativos- para el origen de la creación y la vida dual celestial
Parte 2 - Importancia de los juicios -positivos y negativos- para el surgimiento de la creación y la vida dual celestial
Razones de la afluencia masiva de refugiados: una tragedia desde el punto de vista celestial-divino
Parte 3 - Conocimiento desconocido sobre la vida terrenal y celestial de Jesucristo
Por qué una fe firme en Dios y las oraciones del corazón no son suficientes para acercar al hombre y su alma al Reino de los Cielos
Por qué es tan importante pedir de corazón, dar las gracias y recibir el agradecimiento de los demás
Perspectivas de un ser puro de luz desde su vida terrenal, para la autoexploración de los retornados celestiales
Parte 1 - El mal de amores de los enfermos del corazón
Parte 2 - El desamor del ser humano de corazón

la inagotable e ilimitada entrega desinteresada del amor divino - cómo se puede entender mejor

Superar la vida indiferente y el desinterés espiritual
Salto de pértiga - causas y previsión del espíritu de Dios
Llamada del espíritu del amor - no desperdiciar las energías vitales

Llamada de atención del Dios-Espíritu a todos los seres encarnados
Parte 1 - Las interpretaciones de los adivinos sobre el futuro, una empresa arriesgada y peligrosa
Parte 2 - Las futuras interpretaciones de los adivinos, una empresa arriesgada y peligrosa
Actividades de la Alianza de Rescate Cósmico en los sistemas solares materiales
Parte 1 - Cómo se llegó a la colonización de la tierra con plantas y seres vivos
Parte 2 - Cómo se produjo la colonización con plantas y seres vivos en la Tierra
El significado del desinterés desde el punto de vista celestial
Programación de los núcleos celulares materiales para recibir energías divinas bipolares
La aventura infructuosa de los seres profundamente caídos con un sufrimiento insuperable se acerca a su fin
Qué posibilidades tiene el espíritu de Dios de prever y evitar situaciones de peligro cósmicas y planetarias.
Parte 1 - Mensaje de un rosal
Parte 2 - Comunicación de un rosal
Parte 3 - Mensaje de un rosal
Los deseos del corazón para el año 2020

Humildad celestial - lo que los seres caídos apóstatas hicieron de ella
Respuesta del espíritu del amor a las traducciones de los mensajes y a las reglas de vida celestiales
Instrucción del espíritu del amor en colores pastel celestiales y creación de páginas web de gotas de amor
Superar el miedo a la persecución
Dirigirse a las células del cuerpo en caso de infecciones gripales y para la protección contra los invasores virales
Recomendaciones del espíritu del amor para los tiempos terrenales de desastre y emergencia
Gran alegría en el ser celestial por un acontecimiento terrenal importante
Una visión de la vida imperfecta de los heraldos y supuestos profetas de Dios
Efectos de las frecuencias electromagnéticas de los dispositivos de curación en las células del cuerpo y el alma desde un punto de vista celestial-divino
Parte 1 - Por qué no hay que rechazar el progreso espiritual y técnico
Parte 2 - Por qué no hay que rechazar el progreso espiritual y técnico
Obra de nuestra Madre de la Creación Original en la Caída y su prematuro retorno celestial
Introducción a la colección de temas
¿Para qué sirve la colección temática?
Colección de temas 1
Colección de temas 2
Colección de temas 3
Por qué a algunas personas les molestan algunas palabras de los mensajes
Parte 4 - Conocimiento desconocido sobre la vida terrenal y celestial de Jesucristo
Parte 5 - Conocimiento desconocido sobre la vida terrenal y celestial de Jesucristo

Parte 6 - Conocimiento desconocido sobre la vida terrenal y celestial de Jesucristo
Parte 7 - Conocimiento desconocido sobre la vida terrenal y celestial de Jesucristo
Parte 8 - Conocimiento desconocido sobre la vida terrenal y celestial de Jesucristo
Encuentro con un extraterrestre
Colección de temas 4
Enlace a la lista de colecciones temáticas
Cristo describe su hermoso retorno celestial
Parte 1 - Transformación interior en un ser personalmente discreto y cordial de origen celestial
Parte 2 - Transformación interior en un ser personalmente discreto y cordial de origen celestial
Cómo se produjeron las primeras creaciones y procreaciones de seres por los padres originales en la precreación celestial
Parte 3 - Transformación interior en un ser personalmente discreto y cordial de origen celestial
