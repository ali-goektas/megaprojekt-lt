A mensagem "instruções sérias de um extraterrestre" está agora incluída no volume de 2013
Pequenos animais - as nossas pragas

Apelos de admoestação e ajuda de Deus
Experiências da água no leito do rio
raios de terra nocivos através de travessias de águas subterrâneas
O sofrimento indescritível do mundo animal
Movimento corporal na dança
vida exagerada, consciente da saúde
Dirigir-se às células do corpo em caso de indisposição

o meu e o teu - o modo de vida obrigatório dos seres em queda
comportamento exagerado ao comer
Ver uma queda de água da perspectiva dos seres celestiais
vício do tabaco
longas e árduas preparações dos seres celestiais para a salvação da criação e das suas encarnações agonizantes
efeitos devastadores no corpo e na alma através da mortificação e da compulsão por motivos religiosos
Dirigir-se às células do corpo durante doenças graves
Dirigir-se às células do corpo antes da cirurgia
ideias falsas sobre o caminho interior para Deus
Percepção dos eventos cósmicos invisíveis e incompreensíveis
Origem das alergias cutâneas

terrestres para além das almas mundanas e da sua influência invisível na vida humana
Instrução divina para tomar consciência e controlar os próprios pensamentos
Como compreender a doçura divina
Auto-regulação e humor depressivo
Trágico destino das vítimas do tsunami
Reconhecer, analisar e corrigir as próprias pretensões
Pressupostos como sentimentos desagradáveis úteis a ver através de pessoas ambíguas
Viver neste mundo de decepção
A oferta de Deus para aumentar a energia vital
Escravos e movidos pelo mundo do trabalho
Parte 1 - Medo de poluentes nos alimentos
Parte 2 - Medo de substâncias nocivas nos alimentos
O que significa a liberdade e a independência dos seres cósmicos?
estado dramático da energia da terra com consequências devastadoras para toda a vida
relações estreitas dos átomos primordiais subtis com os átomos alterados e condensados do universo material
compreendeu correctamente a modéstia para viver perto da justiça divina dos seres celestiais
consumo alimentar mais enérgico em ligação com o elemento fogo

difícil libertação dos princípios mundanos e de ensino da vida, bem como composições musicais sem som celestial
despedimento de uma alma piedosa do corpo humano para o além
palavras reconfortantes e úteis do espírito universal de amor pelo auto-reconhecimento para pessoas idosas e solitárias
Como compreender com mais clarividência a compreensão de Deus sobre as nossas transgressões da lei e o seu perdão
Onde estão os seus tesouros recolhidos, aí é sempre atraído de novo
Autoconhecimento no som da fala, cinesiologia e cura voluntária por leigos médicos
importantes instruções do Espírito de Deus para os tradutores do Eu Sou, que caiem de amor noutras línguas
coração fechado
possível saída da própria frieza de coração para voltar a viver no calor do coração dos seres duais celestiais
Média dourada no comportamento alimentar
Poderá a humanidade interior ainda evitar os acontecimentos apocalípticos na Terra e o fim prematuro do mundo previsto pelos videntes anteriores?
Destruição da natureza por microanimais sem energia
A auto-honestidade expõe a mentira
a erosão mental impiedosa dos delitos da lei no corpo humano
intolerância a certos alimentos e especiarias em pessoas piedosas e sensíveis - fidelidade, pré-requisito para uma parceria feliz
seres naturais preocupados relatam a sua difícil situação de vida e as suas tarefas nos nossos jardins
fidelidade fingida no casamento e na parceria - vida dupla dos hipócritas de coração frio
Para onde foram as suas qualidades de ser celestial nobre e caloroso do génio divino?
Elevada perda de energia vital através de encontros regulares com pessoas mundanas e com opiniões.
Sensibilização para a vida humana
Honestidade, fidelidade e sentido de honra - as mais importantes qualidades nobres para uma parceria harmoniosa e feliz
Abordagem das células do corpo para as dores articulares
Parte 1 - Emergência de religiões e comunidades de fé e a crença fanática em Deus que delas surgiu, com sofrimento interminável para os seres humanos e outras almas do mundo
Parte 2 - A origem das religiões e comunidades religiosas e a resultante crença fanática em Deus, com sofrimento interminável para os seres humanos e as almas no além.
Porque é que algumas pessoas que amam Deus não suportam falar a sério

O caminho para a espiritualização e a liberdade celestial
Auto-responsabilidade pela própria vida - o elemento de fogo e a cremação do corpo
Parte 1 - Razões para doenças alegadamente incuráveis e suas possibilidades de cura de um ponto de vista divino - bem como a criação de mundos adjacentes e interligados de formas de vida completamente diferentes
Parte 2 - Razões para alegadas doenças incuráveis e as suas hipóteses de cura de um ponto de vista divino - bem como a criação de mundos adjacentes e interligados de formas de vida completamente diferentes
Parte 3 - Razões para doenças alegadamente incuráveis e as suas hipóteses de serem curadas de um ponto de vista divino - bem como a criação de mundos adjacentes e interligados de um modo de vida completamente diferente.
Abordar as células do corpo para activar as suas funções
Parte 1 - Graça - remissão da punição pelo povo dominante - erroneamente associada pelos crentes com a benevolência da divindade
Parte 2 - Misericórdia - Castigo do povo dominante - erroneamente associado pelos crentes com a benevolência da divindade
As nossas boas acções - são realmente o que pensamos que são?
Porque é que os seres duais celestes por vezes se separam e formam novas uniões
O conselho de Deus que dá esperança depois de uma doença grave ter sido diagnosticada e pistas sobre o comportamento das células do corpo que ainda nos são desconhecidas
Aumento de tempestades devastadoras, inundações, calor e fases de seca na terra - presságios de um fim lento mas inexorável da vida terrena
Parte 1 - fases insuportáveis na vida dos verdadeiros mensageiros de Deus
Parte 2 - fases insuportáveis na vida dos verdadeiros mensageiros de Deus
Razões para a criação do universo da matéria sólida e para a criação do ser humano imperfeito
Amor do coração - como podem as pessoas interiores compreendê-lo e vivê-lo melhor?
Comunicação sincera de uma maçã sobre as experiências na sua árvore-mãe
imagem, cor e linguagem sonora dos seres celestiais em contraste com a comunicação humana
importante recomendação de oração do espírito de Deus para pessoas espiritualmente maduras
Casamentos e parcerias a partir da perspectiva das leis celestiais da vida
Pêndulos - uma actividade supostamente útil com perigos inesperados

O Natal cristão e o símbolo da cruz - são realmente a vontade divina e de grande importância para aqueles que regressam ao reino dos céus?
Parte 1 - modo de vida vegetariano - do ponto de vista do princípio celestial da vida e outros tópicos
Parte 2 - modo de vida vegetariano - do ponto de vista do princípio celestial da vida e outros tópicos
Parte 3 - modo de vida vegetariano - do ponto de vista do princípio celestial da vida e outros tópicos
Parte 4 - modo de vida vegetariano - do ponto de vista do princípio celestial da vida e outros tópicos
Parte 1 - modo de vida interior harmonioso, amante da paz e equilibrado - um objectivo que vale a pena
Parte 2 - modo de vida interior harmonioso, amante da paz e equilibrado - um objectivo que vale a pena
Perfeccionismo humano de um ponto de vista celestial
o tratamento correcto dos meios de pagamento e dos presentes, bem como dos elementos
Parte 1 - Produção de energia através da fissão nuclear e o funcionamento de centrais nucleares numa perspectiva celestial
Parte 2 - Produção de energia através da fissão nuclear e o funcionamento de centrais nucleares numa perspectiva celestial
Eleição de um político - da perspectiva do espírito de Deus
Autoconhecimento profundo de falhas e fraquezas - a tarefa mais importante do dia para os retornados celestiais ao crescimento espiritual
Parte 1 - Vida evolutiva dos seres celestiais e a sua ligação cardíaca com a Deidade impessoal Eu Sou
Parte 2 - Vida evolutiva dos seres celestiais e a sua ligação cardíaca com a Deidade impessoal Eu Sou
Parte 3 - Vida Evolutiva dos Seres Celestiais e a sua Ligação do Coração à Divindade Impessoal Eu Sou
Parte 1 - a vida não saudável de seres profundamente caídos
Parte 2 - a vida não saudável de seres profundamente caídos que realça a sua personalidade
Que tipo de alegria valoriza mais - a das pessoas barulhentas ou a que surge no silêncio exterior de dentro da sua alma eterna?
Parte 1 - longos caminhos errados de pessoas crentes e almas de outros mundos através de ensinamentos religiosos, credos e costumes.
Parte 2 - longos caminhos errados de pessoas crentes e almas de outros mundos através de ensinamentos religiosos, credos e costumes.
Parte 3 - longos caminhos mal orientados de pessoas crentes e almas de outros mundos através de ensinamentos religiosos, credos e costumes
situação financeira e económica devastadora - como se devem comportar os celestes regressados a casa neste tempo preocupante

Parte 1 - Pode uma doença ou evento angustiante ser rastreado até causas estabelecidas mais cedo?
Parte 2 - Pode uma doença ou um acontecimento angustiante ser rastreado até causas anteriores?
Instruções divinas para o caminho interior de regresso a uma vida sensível e feliz do nosso lar luminoso
Parte 1 - conhecimento desconhecido sobre seres celestiais e extraterrestres, bem como sobre a lisonja - e o que está por detrás dela
Parte 2 - conhecimento desconhecido sobre seres celestiais e extraterrestres, bem como sobre a lisonja - e o que está por detrás dela
Parte 3 - conhecimento desconhecido sobre seres celestiais e extraterrestres, bem como sobre a lisonja - e o que está por detrás dela
Resposta do Espírito de Deus aos leitores que estavam preocupados com as gotas de amor
Parte 1 - O que o homem experimenta na fase da morte e após a morte a alma no além
Parte 2 - O que o ser humano experimenta na fase da morte e após a morte a alma no além

O Significado do Eu Sou de uma Perspectiva Celestial
Parte 3 - O que o ser humano experimenta na fase da morte e após a morte a alma no além
Parte 4 - O que o ser humano experimenta na fase da morte e após a morte a alma no além
Parte 5 - O que o ser humano experimenta na fase da morte e após a morte a alma no além
Parte 1 - Diferença entre a vida impessoal dos seres celestiais e a vida pessoal dos seres humanos
Parte 2 - Diferença entre a vida impessoal dos seres celestiais e a vida pessoal dos seres humanos
Parte 3 - Diferença entre a vida impessoal dos seres celestiais e a vida pessoal dos seres humanos
Visão e aplicação erradas de pensamento positivo com consequências desastrosas

Parte 1 - Vícios - as suas causas e como superá-las do ponto de vista celestial - e outros tópicos
Parte 2 - Vícios - as suas causas e como superá-las do ponto de vista celestial - e outros tópicos
Explicação do espírito universal de amor em relação às perguntas dos leitores
Será que Jesus morreu na cruz?
Os arautos são obrigados a receber mensagens divinas em qualquer altura?
Parte 1 - A origem dos actos de culto ritual e os perigos que eles representam para as pessoas e as almas numa perspectiva celestial
Parte 2 - A origem dos actos rituais de culto e os perigos que representam para as pessoas e almas numa perspectiva celestial
O que são as Gotas de Amor de Deus I Am? - Fonte das mensagens divinas
Abordar as células oculares em doenças leves e graves
Porque é que pessoas impacientes e inquietas, amantes de Deus, não conseguem estabelecer uma relação interior com a vida subtil e celestial da sua alma
Lisonja - uma forma pouco atractiva e enganosa de ser
Música de seres celestiais comparada com composições do mundo

Utilização do termo lei ou regularidade nas mensagens
Parte 1 - Partículas Minerais Subtis - Primeiros Elementos de Vida para a Criação do Universo
Parte 2 - Partículas Minerais Subtis - Primeiros Elementos de Vida para a Criação do Universo
Por que é que as mensagens de amor divino gotejam na sua mensagem e expressão de tempos a tempos?
Incentivo a uma vida de humildade celestial
Criação do ser mais engenhoso, sincero e impessoal - Deus - por seres celestiais de luz
Compreender as mensagens apenas no seu significado
Sentido e propósito das mensagens divinas através de mensageiros
Terapia de cura com soro venenoso de serpente do ponto de vista celestial
A ignorância espiritual da humanidade com consequências trágicas
Implantes espirituais - o seu significado e funções insidiosas
Cristo fala sobre o seu nascimento e sobre a justa igualdade de todos os seres cósmicos
Futebol - um jogo combativo - iluminado do ponto de vista celestial
Parte 1 - A humildade vivida - a chave espiritual para o regresso celestial e para uma vida abençoada em justa igualdade
Parte 2 - A humildade vivida - a chave espiritual para o regresso celestial e uma vida feliz de igualdade justa
Adoração da colheita e preparação dos dons da natureza
Parte 1 - Porque é que os seres extraterrestres realizam acções cosmicamente significativas no nosso sistema solar com as suas naves espaciais
Parte 2 - Porque é que os seres extraterrestres realizam acções cosmicamente significativas no nosso sistema solar com as suas naves espaciais

Alimentos doces e os seus efeitos secundários indesejáveis
discurso alto e enérgico - um comportamento dominador que não tolera o recuo
Parte 1 - grande alarido sobre alegados representantes de Deus neste mundo - um mundo de enganos
Parte 2 - grande alarido sobre alegados representantes de Deus neste mundo - um mundo de enganos
instruções sérias de um extraterrestre
forte retirada da energia vital por pessoas e almas pobres em energia
A vida de vaguear e voar de Jesus Cristo
impulso fanático pela liberdade e justiça com graves consequências
Parte 1 - sabedoria cósmica e dicas para a reorientação espiritual
Parte 2 - sabedoria cósmica e dicas para a reorientação espiritual
Parte 1 - eu interior - energia e armazenamento de dados do nosso corpo de luz interior (alma), bem como medidas extensivas para a salvação da criação

Parte 2 - eu interior - energia e armazenamento de dados do nosso corpo de luz interior (alma), bem como medidas extensivas para a salvação da criação
Possibilidade de cura através da activação de células degeneradas
Falsa imagem de Deus através de mensageiros mal orientados
Porque é que as doenças malignas também são curáveis
Porque não haverá vida paradisíaca na Terra
Orgulho e arrogância bloqueiam o regresso celestial
Breve introdução para os novos leitores de Love Drops
Oração do coração de um tipo espiritualmente superior
falsa imagem do homem com consequências trágicas inesperadas
Parte 1 - Porque é que a alma encarnada já molda o carácter da criança
Parte 2 - Porque é que a Alma Encarnada já Molda o Carácter da Criança
O fim da vida extra-celestial começa agora
Apelo do Espírito Universal do Amor a todos os Seres Extra-Mundane

Perfeccionismo na perspectiva do Deus-Espírito e de um filósofo
previsão optimista do dia com um efeito agradável
Ligação energética dos seres encarnados do plano de salvação com os seus irmãos e irmãs planetários celestiais.
A visão do Reino da paz - o grande erro do povo crente em Deus e de outras almas do mundo
Parte 1 - conhecimento desconhecido sobre a vida terrena e celestial de Jesus Cristo
coexistência difícil com pessoas dominantes, opinantes e argumentativas
perigos inimagináveis de almas intrusivas para pessoas curiosas com capacidades psíquicas
Parte 2 - conhecimento desconhecido sobre a vida terrena e celestial de Jesus Cristo
Onde é que algumas pessoas mediúnicas de hoje recebem anúncios sobre eventos mundiais preocupantes nas proximidades?
Desejos sinceros dos seres celestiais a todos os retornados celestiais para o tempo vindouro na terra.

Mensagem de um ser extraterrestre para a humanidade
Vida dupla de seres celestes no mesmo estado de consciência e harmonia constante dos corações
Parte 1 - Significado dos Julgamentos - Positivo e Negativo - para a Origem da Criação e da Dupla Vida Celestial
Parte 2 - Significado dos Julgamentos - Positivos e Negativos - para o surgimento da Criação e da Dupla Vida Celestial
Razões para o afluxo maciço de refugiados - uma tragédia de um ponto de vista divino celestial
Parte 3 - conhecimento desconhecido sobre a vida terrena e celestial de Jesus Cristo
Porque uma fé firme em Deus e orações do coração não são suficientes para aproximar o homem e a sua alma do Reino dos Céus
Porque é tão importante pedir do coração, agradecer e receber o agradecimento dos outros
Visões de um ser puro de luz da sua vida terrena, para auto-exploração dos retornados celestiais
Parte 1 - Doença de amor do coração
Parte 2 - O desgosto do coração do ser humano do coração

doação inesgotável e ilimitada e abnegada do amor divino - como pode ser melhor compreendido

Superação da vida indiferente e do desinteresse espiritual
Salto de vara - causas e prenúncio do espírito de Deus
Apelo do espírito de amor - não desperdiçar energias vitais

Chamado de despertar do Espírito-Deus para todos os seres encarnados
Parte 1 - As interpretações dos adivinhos sobre o futuro - um empreendimento arriscado e perigoso
Parte 2 - Interpretações futuras dos adivinhadores uma empresa arriscada e perigosa
Actividades da Cosmic Rescue Alliance in Material Solar Systems
Parte 1 - Como chegou à colonização da terra com plantas e seres vivos
Parte 2 - Como surgiu a colonização com plantas e seres vivos na Terra
O significado de altruísmo do ponto de vista celestial
Programação dos núcleos das células materiais para receber as energias divinas bipolares
A aventura mal sucedida dos seres profundamente caídos com sofrimento insuperável está a aproximar-se do seu fim
Que possibilidades tem o espírito de Deus para prever e evitar situações cósmicas e planetárias perigosas.
Parte 1 - Mensagem de um arbusto de rosas
Parte 2 - Comunicação de uma Rose Bush
Parte 3 - Mensagem de uma Rose Bush
Os desejos do coração para o ano 2020

Humildade celestial - o que os seres apóstatas dela fizeram
Resposta do espírito amoroso às traduções de mensagens e às regras celestiais da vida
Instrução do espírito do amor em cores pastel celestiais e criação de sites de gota de amor
Superar o medo de perseguição
Abordagem das células do corpo em caso de infecções semelhantes às da gripe e para protecção contra os invasores do vírus
Recomendações do espírito de amor pelos tempos terrenos de catástrofe e emergência
Grande alegria no ser celestial por um acontecimento terreno significativo
A percepção da vida imperfeita dos mensageiros e dos chamados profetas de Deus
Efeitos das frequências electromagnéticas dos dispositivos de cura nas células do corpo e da alma de um ponto de vista divino celestial
Parte 1 - Porque é que o progresso espiritual e técnico não deve ser rejeitado
Parte 2 - Porque é que o progresso espiritual e técnico não deve ser rejeitado
Trabalho da nossa Criação Original Mãe no Outono e o seu regresso celestial prematuro
Introdução à colecção de temas
Para que é a colecção temática?
Colecção Temática 1
Colecção temática 2
Colecção temática 3
Porque é que algumas pessoas são incomodadas por algumas palavras de mensagem
Parte 4 - conhecimento desconhecido sobre a vida terrena e celestial de Jesus Cristo
Parte 5 - conhecimento desconhecido sobre a vida terrena e celestial de Jesus Cristo

Parte 6 - conhecimento desconhecido sobre a vida terrena e celestial de Jesus Cristo
Parte 7 - conhecimento desconhecido sobre a vida terrena e celestial de Jesus Cristo
Parte 8 - conhecimento desconhecido sobre a vida terrena e celestial de Jesus Cristo
Encontro com um extraterrestre
Colecção temática 4
Link para a lista de colecções temáticas
Cristo descreve o seu belo regresso celestial
Parte 1 - transformação interior para um ser de origem celestial, pessoalmente inconspícuo e sincero
Parte 2 - transformação interior para um ser de origem celestial, pessoalmente inconspícuo e sincero
Como chegaram às primeiras criações e procriações de seres pelos pais originais na pré-criação celestial
Parte 3 - transformação interior num ser de origem celestial, pessoalmente inconspícuo e sincero
