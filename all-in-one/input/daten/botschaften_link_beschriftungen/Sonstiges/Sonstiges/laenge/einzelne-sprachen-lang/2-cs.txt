Zpráva "vážné instrukce od mimozemšťana" je nyní zahrnuta ve svazku z roku 2013.
Malá zvířata - naši škůdci

Boží napomenutí a volání o pomoc
zážitky z vody v korytě řeky
škodlivé zemské paprsky přes podzemní vodní přechody.
Nevýslovné utrpení světa zvířat
Pohyb těla v tanci
přehnaný, zdravý život.
Oslovení tělesných buněk v případě indispozice

můj a tvůj - závazný způsob života padlých bytostí
přehnané chování při jídle
Pohled na vodopád z perspektivy nebeských bytostí
závislost na tabáku
dlouhé a namáhavé přípravy nebeských bytostí na spásu stvoření a jejich strastiplná vtělení.
ničivé účinky na tělo i duši v důsledku umrtvování a nucení z náboženských pohnutek.
Řešení tělesných buněk během závažného onemocnění
Řešení tělesných buněk před operací
falešné představy o vnitřní cestě k Bohu
vhled do neviditelných a nepochopitelných vesmírných událostí.
Původ kožních alergií

pozemské duše a jejich neviditelný vliv na lidský život.
Božský pokyn, jak si uvědomit a ovládat vlastní myšlenky.
Jak porozumět božské mírnosti
Sebelítost a depresivní nálada
Tragický osud obětí tsunami
Rozpoznání, analýza a náprava vlastních přetvářek
Domněnky jako nepříjemné pocity užitečné pro odhalení nejednoznačných lidí
Žít v tomto světě klamu
Boží nabídka na zvýšení životní energie
Otroci a poháněni světem práce
Část 1 - Strach ze škodlivin v potravinách
Část 2 - Strach ze škodlivých látek v potravinách
Co znamená svoboda a nezávislost vesmírných bytostí?
dramatický energetický stav Země s ničivými důsledky pro veškerý život.
úzké vztahy jemných praatomů k pozměněným, kondenzovaným atomům hmotného vesmíru.
správně chápaná skromnost, abychom žili v blízkosti božské spravedlnosti nebeských bytostí.
Jíst více energetických potravin ve spojení s ohnivým prvkem

obtížné osvobození od světských a poučujících zásad života, stejně jako hudební skladby bez nebeského zvuku.
propuštění zbožné duše z lidského těla na onen svět.
útěšná a užitečná slova univerzálního ducha lásky pro sebepoznání starších a osamělých lidí
Jak prozíravěji pochopit Boží chápání našich přestupků zákona a jejich odpuštění.
Tam, kde jsou tvé nashromážděné poklady, tě to táhne znovu a znovu.
Sebepoznání v oblasti zvuku řeči, kineziologie a léčení vlastní vůlí lékařských laiků
důležité pokyny od Božího Ducha pro překladatele kapek lásky Já jsem do jiných jazyků.
uzavřené srdce
možná cesta z vlastního srdečního chladu, aby člověk znovu žil v teple srdce nebeských dvojjediných bytostí.
Zlatá střední cesta ve stravovacím chování
Může vnitřní lidstvo ještě odvrátit apokalyptické události na Zemi a předčasný konec světa, který předpověděli dřívější věštci?
Ničení přírody mikroživočichy bez energie
Upřímnost k sobě samému odhaluje lež
nemilosrdná duševní eroze zákonných přestupků v lidském těle.
nesnášenlivost některých potravin a koření u zbožných a citlivých lidí - věrnost, předpoklad šťastného partnerství
znepokojené přírodní bytosti informují o své obtížné životní situaci a svých úkolech v našich zahradách.
předstíraná věrnost v manželství a partnerství - dvojí život pokrytců s chladným srdcem
Kde zůstaly vaše srdečné a vznešené nebeské vlastnosti, které pocházejí z božského génia?
vysoká ztráta životní energie v důsledku pravidelného setkávání se světskými lidmi s vyhraněným názorem
Povědomí o lidském životě
Poctivost, věrnost a smysl pro čest - nejdůležitější ušlechtilé vlastnosti pro harmonické a šťastné partnerství.
promlouvání k tělesným buňkám v případě bolesti kloubů
1. část - Vznik náboženství a náboženských společenství a z nich vzešlá fanatická víra v Boha s nekonečným utrpením lidských bytostí i duší na onom světě.
2. část - Vznik náboženství a náboženských komunit a z toho vyplývající fanatická víra v Boha s nekonečným utrpením lidských bytostí a duší na onom světě.
Proč někteří lidé, kteří jsou spojeni s Bohem, nesnesou, aby se o nich mluvilo vážně?

Cesta ke spiritualizaci a nebeské svobodě
Odpovědnost za vlastní život - živel ohně a kremace těla
1. část - Důvody údajně nevyléčitelných nemocí a šance na jejich vyléčení z božského hlediska - a také vytvoření sousedních a vzájemně se prolínajících světů zcela odlišných způsobů života.
2. část - Důvody údajných nevyléčitelných nemocí a šance na jejich vyléčení z božského hlediska - a také vytvoření sousedních a vzájemně propojených světů zcela odlišných způsobů života.
3. část - Důvody údajně nevyléčitelných nemocí a šance na jejich vyléčení z božského hlediska - a také vytvoření sousedních a vzájemně propojených světů zcela odlišného způsobu života.
Oslovení tělesných buněk pro aktivaci jejich funkcí
Část 1 - Milosrdenství - odpuštění trestu vládnoucími osobami - věřícími mylně spojováno s dobrotivostí Božství
Část 2 - Milosrdenství - odpuštění trestu vládnoucími osobami - věřícími mylně spojováno s dobrotivostí Božství
Naše dobré skutky - jsou opravdu takové, jaké si myslíme, že jsou?
Proč se nebeské duální bytosti někdy rozcházejí a vytvářejí nové svazky?
Boží rada, která dává naději po zjištění vážné nemoci, a vodítka k chování tělesných buněk, která jsou nám dosud neznámá.
Nárůst ničivých bouří, záplav, veder a sucha na Zemi - předzvěst pomalého, ale neúprosného konce pozemského života.
1. část - nesnesitelné etapy v životě pravých Božích zvěstovatelů
2. část - nesnesitelné etapy v životě pravých Božích zvěstovatelů
Důvody stvoření vesmíru z pevné hmoty a stvoření nedokonalého člověka
Láska ze srdce - jak ji mohou vnitřní lidé lépe pochopit a prožít?
Srdečné sdělení jablka o zážitcích na mateřském stromě
obrazová, barevná a zvuková řeč nebeských bytostí na rozdíl od lidské komunikace.
důležité modlitební doporučení Božího ducha pro duchovně zralé lidi
Manželství a partnerství z pohledu nebeských zákonů života
Kyvadla - údajně užitečná činnost s nečekaným nebezpečím

Křesťanské Vánoce a symbol kříže - jsou skutečně Boží vůlí a mají velký význam pro ty, kdo se vracejí do nebeského království?
1. část - vegetariánský způsob života - z pohledu nebeského principu života a další témata
2. část - vegetariánský způsob života - z pohledu nebeského principu života a další témata
3. část - vegetariánský způsob života - z pohledu nebeského principu života a další témata
4. část - vegetariánský způsob života - z pohledu nebeského principu života a další témata
Část 1 - harmonický, mírumilovný a vnitřně vyrovnaný způsob života - hodnotný cíl
2. část - harmonický, mírumilovný a vyrovnaný vnitřní způsob života - hodnotný cíl
Lidský perfekcionismus z nebeského pohledu
správné nakládání s platebními prostředky a dary, jakož i s prvky
Část 1 - Výroba energie jaderným štěpením a provoz jaderných elektráren z nebeského pohledu
Část 2 - Výroba energie jaderným štěpením a provoz jaderných elektráren z nebeského pohledu
Volba politika - z pohledu Božího ducha
hluboké sebepoznání chyb a slabostí - nejdůležitější úkol dne pro ty, jejichž cílem je nebe
1. část - Evoluční život nebeských bytostí a jejich srdeční spojení s neosobním Božstvím Já Jsem.
2. část - Evoluční život nebeských bytostí a jejich srdeční spojení s neosobním Božstvím Já Jsem.
Část 3 - Evoluční život nebeských bytostí a jejich srdeční spojení s neosobním Božstvím Já Jsem
1. část - nedobrý život hluboce padlých bytostí, který posiluje jejich osobnost
2. část - nedobrý život hluboce padlých bytostí, který posiluje jejich osobnost
Kterého druhu radosti si ceníte více - radosti hlučných lidí nebo té, která vychází ve vnějším tichu z nitra vaší věčné duše?
1. část - dlouhá odysea věřících lidí a nadpozemských duší skrze náboženské nauky, vyznání a zvyky
2. část - dlouhá odysea věřících lidí a nadpozemských duší skrze náboženské nauky, vyznání a zvyky
3. část - dlouhá odysea věřících lidí a nadpozemských duší skrze náboženské nauky, vyznání a zvyky
Zničující finanční a ekonomická situace - jak se mají v této znepokojivé době chovat ti, jejichž cílem je nebe?

Část 1 - Lze nemoc nebo nepříjemnou událost vysledovat k dříve stanoveným příčinám?
2. část - Lze u nemoci nebo nepříjemné události vysledovat její dřívější příčiny?
Božské pokyny pro vnitřní cestu zpět do citlivého blaženého života v našem světelném domově.
1. část - neznámé vědomosti o nebeských a mimozemských bytostech a lichotky - a co se za nimi skrývá
2. část - neznámé znalosti o nebeských a mimozemských bytostech a lichotky - a co se za nimi skrývá
3. část - neznámé vědomosti o nebeských a mimozemských bytostech a lichotky - a co se za nimi skrývá
Odpověď Ducha Božího čtenářům, kteří se bojí o kapky lásky
Část 1 - Co prožívá člověk ve fázi smrti a duše v posmrtném životě
2. část - Co prožívá člověk ve fázi smrti a duše v posmrtném životě

Význam Já jsem z nebeské perspektivy
Část 3 - Co prožívá člověk ve fázi smrti a duše v posmrtném životě
Část 4 - Co prožívá člověk ve fázi smrti a duše v posmrtném životě
Část 5 - Co prožívá člověk ve fázi smrti a duše v posmrtném životě
Část 1 - Rozdíl mezi neosobním životem nebeských bytostí a osobním životem lidí
2. část - Rozdíl mezi neosobním životem nebeských bytostí a osobním životem lidí
Část 3 - Rozdíl mezi neosobním životem nebeských bytostí a osobním životem lidí
Špatný pohled a aplikace pozitivního myšlení s katastrofálními důsledky

1. část - Závislosti - jejich příčiny a jak je překonat z nebeského pohledu - a další témata
2. část - Závislosti - jejich příčiny a jak je překonat z nebeského hlediska - a další témata
Prohlášení univerzálního ducha lásky k otázkám čtenářů
Zemřel Ježíš na kříži?
Jsou zvěstovatelé povinni přijímat božská poselství kdykoli?
1. část - Původ rituálních kultů a jejich nebezpečí pro lidi a duše z nebeské perspektivy
2. část - Původ rituálních úkonů uctívání a jejich nebezpečí pro lidi a duše z nebeské perspektivy
Co jsou kapky lásky Já jsem Bůh? - Zdroj božských poselství
Promlouvání k očním buňkám v případě mírných a závažných onemocnění
Proč netrpěliví a neklidní lidé milující Boha nedokážou navázat vnitřní vztah s jemným nebeským životem své duše?
Lichocení - nepřitažlivý klamavý způsob bytí
Hudba nebeských bytostí ve srovnání se světskými skladbami

Použití pojmu zákon nebo pravidelnost ve zprávách
Část 1 - Jemné minerální částice - první prvky života pro stvoření vesmíru
2. část - Jemné minerální částice - první prvky života pro stvoření vesmíru
Proč se poselství božských kapek lásky čas od času mění ve svém poselství a vyjádření?
Pobídka k nebesky pokornému životu
Stvoření nejgeniálnější, srdečné, neosobní bytosti - Boha - nebeskými bytostmi světla.
Chápání zpráv pouze v jejich významu
Smysl a účel božských poselství prostřednictvím zvěstovatelů
Léčebná terapie sérem hadího jedu z nebeského pohledu
Duchovní nevědomost lidstva s tragickými následky
Duchovní implantáty - jejich význam a zákeřné funkce
Kristus mluví o svém narození a o spravedlivé rovnosti všech vesmírných bytostí.
Fotbal - bojová hra - osvětlená z nebeského hlediska
1. část - Prožívaná pokora - duchovní klíč k nebeskému návratu a k blaženému životu ve spravedlivé rovnosti
2. část - Prožívaná pokora - duchovní klíč k nebeskému návratu a blaženému životu ve spravedlivé rovnosti
sklizeň a příprava darů přírody s láskou
1. část - Proč mimozemské bytosti provádějí v naší sluneční soustavě pomocí svých vesmírných lodí kosmicky významné akce?
2. část - Proč mimozemské bytosti provádějí v naší sluneční soustavě pomocí svých vesmírných lodí kosmicky významné akce?

Sladké potraviny a jejich nežádoucí vedlejší účinky
hlasitý, energický projev - dominantní chování, které nesnese odmlouvání.
1. část - velký rozruch kolem údajných zástupců Boha v tomto světě - svět podvodů
2. část - velký rozruch kolem údajných zástupců Boha v tomto světě - svět podvodů
vážné pokyny od mimozemšťana
silný odběr životní energie lidmi a dušemi s nedostatkem energie.
Život Ježíše Krista v putování a útěku
fanatická touha po svobodě a spravedlnosti s vážnými důsledky.
1. část - kosmická moudrost a rady pro duchovní přeorientování
2. část - kosmická moudrost a rady pro duchovní přeorientování
1. část - vnitřní já - energie a datové úložiště našeho vnitřního světelného těla (duše), jakož i rozsáhlá opatření ke spáse stvoření.

2. část - vnitřní já - energie a datové úložiště našeho vnitřního světelného těla (duše) a také rozsáhlá opatření ke spáse stvoření.
Možnost uzdravení prostřednictvím aktivace degenerovaných buněk
Falešný obraz Boha prostřednictvím zavádějících hlasatelů
Proč jsou i zhoubné nemoci léčitelné
Proč nebude na zemi rajský život?
Pýcha a arogance brání návratu do nebeských světů
Krátký úvod pro nové čtenáře Kapek lásky
Srdcová modlitba duchovně vyššího druhu
falešný obraz člověka s nečekanými tragickými následky.
1. část - Proč již inkarnovaná duše utváří charakter dítěte?
2. část - Proč vtělená duše již utváří charakter dítěte?
konec života mimo nebe začíná nyní
Srdcová výzva univerzálního ducha lásky všem bytostem mimo nebe

Perfekcionismus z pohledu Božího ducha a filozofa
optimistická předpověď dne s příjemným účinkem
Energetické spojení inkarnovaných bytostí plánu spásy s jejich nebeskými planetárními bratry a sestrami.
Vize Království míru - velký omyl lidí věřících v Boha a duší z jiného světa
1. část - neznámé poznatky o pozemském a nebeském životě Ježíše Krista
obtížné soužití s dominantními, svéhlavými a hádavými lidmi.
nepředstavitelné nebezpečí ze strany dotěrných duší pro zvědavé lidi s psychickými schopnostmi
2. část - neznámé poznatky o pozemském a nebeském životě Ježíše Krista
Odkud dnes někteří okultisté získávají zprávy o blízkých znepokojivých světových událostech?
Srdečné přání nebeských bytostí všem nebeským navrátilcům pro nadcházející čas na zemi.

Poselství mimozemské bytosti lidstvu
Duální život nebeských bytostí na stejné úrovni vědomí a v neustálé jednotě srdcí
Část 1 - Význam prvotních částic - pozitivních a negativních - pro genezi stvoření a nebeský duální život
Část 2 - Význam prvotních částic - pozitivních a negativních - pro genezi stvoření a nebeský duální život
Důvody masového přílivu uprchlíků - tragédie z nebesko-božského hlediska
3. část - neznámé poznatky o pozemském a nebeském životě Ježíše Krista
Proč jen pevná víra v Boha a modlitby srdce nestačí k tomu, aby se člověk a jeho duše přiblížili nebeskému království?
Proč je tak důležité prosit ze srdce, děkovat a přijímat díky druhých?
Poznatky čisté bytosti světla z pozemského života, pro sebepoznání nebeských navrátilců.
Část 1 - Milostná nemoc lidí srdce
Část 2 - Milostná nemoc lidí srdce

nevyčerpatelné a neomezené nezištné dávání božské lásky - jak ji lze lépe pochopit.

Překonání lhostejného života a duchovního nezájmu
Obrácení pólů - příčiny a varování Ducha Božího
Výzva Ducha lásky - neplýtvejte životní energií

Výzva k probuzení od Božího ducha všem inkarnovaným bytostem
Část 1 - Budoucí výklady věštců jsou riskantní a nebezpečné
Část 2 - Budoucí výklady věštců jsou riskantní a nebezpečné
Činnost Kosmické záchranné aliance v hmotných slunečních systémech
Část 1 - Jak došlo k osídlení Země rostlinami a živými bytostmi
2. část - Jak došlo k osídlení Země rostlinami a živými bytostmi
Význam nesobeckosti z nebeského hlediska
Programování hmotných buněčných jader pro příjem bipolárních božských energií
Neúspěšné dobrodružství hluboce padlých bytostí s nepřekonatelným utrpením se blíží ke konci.
Jaké možnosti má Boží duch, aby předvídal a odvracel vesmírné a planetární nebezpečné situace.
Část 1 - Poselství z růžového keře
Část 2 - Komunikace růžového keře
Část 3 - Poselství z růžového keře
Přání srdce pro rok 2020

Nebeská pokora - co z ní udělaly odpadlé padlé bytosti
Reakce ducha lásky na překlady poselství a nebeská pravidla života
Pokyn ducha lásky v nebeských pastelových barvách a tvorba webových stránek s kapkou lásky.
Překonání strachu z pronásledování
Oslovení tělesných buněk v případě chřipkových infekcí a pro ochranu před virovými útočníky.
doporučení ducha lásky pro pozemské katastrofy a nouzové situace
velká radost v nebeské existenci z významné pozemské události
Vhled do nedokonalého života zvěstovatelů a takzvaných Božích proroků
Účinky elektromagnetických frekvencí léčivých přístrojů na buňky těla a duši z nebesko-božského hlediska
Část 1 - Proč bychom neměli odmítat duchovní a technický pokrok
2. část - Proč bychom neměli odmítat duchovní a technický pokrok
Působení naší Matky původního stvoření při pádu a její předčasný návrat do nebe
Úvod do sbírky témat
K čemu slouží tematická sbírka?
Tématická kolekce 1
Tématická kolekce 2
Sbírka témat 3
Proč některým lidem vadí některá slova ve zprávách
4. část - neznámé poznatky o pozemském a nebeském životě Ježíše Krista
5. část - neznámé poznatky o pozemském a nebeském životě Ježíše Krista

6. část - neznámé poznatky o pozemském a nebeském životě Ježíše Krista
7. část - neznámé poznatky o pozemském a nebeském životě Ježíše Krista
8. část - neznámé poznatky o pozemském a nebeském životě Ježíše Krista
Setkání s mimozemšťanem
Sbírka témat 4
Odkaz na seznam tematických sbírek
Kristus popisuje svůj krásný návrat do nebeských světů
1. část - vnitřní proměna v osobně nenápadnou a srdečnou bytost nebeského původu
2. část - vnitřní proměna v osobně nenápadnou a srdečnou bytost nebeského původu
Jak došlo k prvnímu stvoření a zplození bytostí původními rodiči v nebeském předtvoření?
3. část - vnitřní proměna v osobně nenápadnou a srdečnou bytost nebeského původu
