Pranešimas "rimti nurodymai iš nežemiškos būtybės" dabar įtrauktas į 2013 m. tomą
Smulkūs gyvūnai - mūsų kenkėjai

Dievo įspėjimo ir pagalbos šauksmai
Vandens patirtis upės vagoje
kenksmingi žemės spinduliai per požemines vandens perėjas.
Neapsakomos gyvūnų pasaulio kančios
Kūno judesiai šokyje
perdėtas, sveikuoliškas gyvenimas
Kreipimasis į kūno ląsteles esant negalavimui

mano ir tavo - privalomas krintančių būtybių gyvenimo būdas
perdėtas elgesys valgant.
Žvilgsnis į krioklį iš dangaus būtybių perspektyvos
priklausomybė nuo tabako
ilgas ir sunkus dangaus būtybių pasiruošimas kūrinijos išgelbėjimui ir jų kankinantys įsikūnijimai
pražūtingas poveikis kūnui ir sielai dėl numarinimo ir prievartos iš religinių paskatų.
Pagalba organizmo ląstelėms sunkios ligos metu
Kūno ląstelių apsauga prieš operaciją
klaidingos idėjos apie vidinį kelią į Dievą.
Įžvalga apie nematomus ir nesuprantamus kosminius įvykius
Odos alergijų kilmė

žemiškųjų sielų ir jų nematomos įtakos žmogaus gyvenimui.
Dieviškasis nurodymas pažinti ir kontroliuoti savo mintis
Kaip suprasti dieviškąjį švelnumą
Savęs gailėjimasis ir depresinė nuotaika
Tragiškas cunamio aukų likimas
Atpažinti, analizuoti ir ištaisyti savo pretenzijas
Prielaidos kaip naudingi nemalonūs jausmai, kuriuos padeda įžvelgti dviprasmiški žmonės
Gyvenimas šiame apgaulės pasaulyje
Dievo pasiūlymas padidinti gyvybinę energiją
Darbo pasaulio vergai ir vergai
1 dalis - Baimė dėl maiste esančių teršalų
2 dalis - Baimė dėl kenksmingų medžiagų maiste
Ką reiškia kosminių būtybių laisvė ir nepriklausomybė?
dramatiška Žemės energetinė būklė, turinti pražūtingų pasekmių visai gyvybei.
glaudūs subtilių pirmapradžių atomų ryšiai su pakitusiais, kondensuotais materialios visatos atomais.
teisingai suprastas kuklumas, kad gyventume arti dieviškojo teisingumo dangiškųjų būtybių.
energingesnis maisto vartojimas, susijęs su ugnies elementu.

sunkus išsilaisvinimas iš žemiškų ir pamokančių gyvenimo principų, taip pat muzikinės kompozicijos be dangiško skambesio.
dieviškos sielos išėjimas iš žmogaus kūno į pomirtinį gyvenimą.
paguodžiantys ir padedantys visuotinės meilės dvasios žodžiai savęs pažinimui pagyvenusiems ir vienišiems žmonėms.
Kaip įžvalgiau suvokti Dievo supratimą apie mūsų nusižengimus įstatymui ir jų atleidimą
Ten, kur yra jūsų sukaupti lobiai, jus vėl ir vėl traukia
Savęs pažinimas apie kalbos garsą, kineziologiją ir savęs išgydymą, kurį atlieka medicinos neprofesionalai
svarbūs Dievo Dvasios nurodymai "Aš esu meilės lašų" vertėjams į kitas kalbas
uždara širdis
galimas būdas išeiti iš savo širdies vėsos ir vėl gyventi dangiškųjų dvigubų būtybių širdies šilumoje.
Auksinis valgymo elgsenos vidurkis
Ar vidinė žmonija vis dar gali išvengti apokaliptinių įvykių žemėje ir ankstyvųjų aiškiaregių išpranašautos pasaulio pabaigos?
Gamtos naikinimas energijos neturinčiais mikro-gyvūnais
Sąžiningumas atskleidžia melą
negailestinga psichinė teisės pažeidimų erozija žmogaus kūne.
tam tikrų maisto produktų ir prieskonių netoleravimas dievobaimingiems, jautriems žmonėms - ištikimybė, būtina laimingos partnerystės sąlyga
susirūpinusios gamtos būtybės pasakoja apie savo sunkią gyvenimo situaciją ir užduotis mūsų soduose.
apsimestinė ištikimybė santuokoje ir partnerystėje - šaltos širdies veidmainių dvigubas gyvenimas
Kur dingo jūsų nuoširdžios ir kilnios dangiškosios būtybės savybės iš dieviškojo genijaus?
Didelis gyvybinės energijos praradimas dėl nuolatinių susidūrimų su žemiškais, savo nuomonę turinčiais žmonėmis.
Žmogaus gyvybės suvokimas
Sąžiningumas, ištikimybė ir garbės jausmas - svarbiausi kilnūs bruožai darniai ir laimingai partnerystei.
Sąnarių skausmą sukeliančių organizmo ląstelių šalinimas
1 dalis - Religijų ir tikėjimo bendruomenių atsiradimas ir iš jų kilęs fanatiškas tikėjimas į Dievą su nesibaigiančiomis žmonių ir nežemiškų sielų kančiomis.
2 dalis - Religijų ir religinių bendruomenių kilmė ir iš to kylantis fanatiškas tikėjimas į Dievą, nesibaigiančios žmonių kančios ir sielos anapusybėje.
Kodėl kai kurie Dievą mylintys žmonės negali pakęsti rimtai kalbėti

Kelias į dvasingumą ir dangiškąją laisvę
Atsakomybė už savo gyvenimą - ugnies stichija ir kūno kremavimas
1 dalis - tariamai nepagydomų ligų priežastys ir jų išgydymo galimybės dieviškuoju požiūriu, taip pat gretimų ir tarpusavyje susijusių visiškai skirtingų gyvenimo būdų pasaulių sukūrimas.
2 dalis - tariamų nepagydomų ligų priežastys ir jų išgydymo galimybės dieviškuoju požiūriu, taip pat gretimų ir tarpusavyje susijusių visiškai skirtingų gyvenimo būdų pasaulių sukūrimas.
3 dalis - tariamai nepagydomų ligų priežastys ir jų išgydymo galimybės dieviškuoju požiūriu, taip pat gretimų ir tarpusavyje susijusių visiškai kitokio gyvenimo būdo pasaulių sukūrimas.
Kreipimasis į kūno ląsteles, siekiant suaktyvinti jų funkcijas
1 dalis - Malonė - valdančiųjų atleidimas nuo bausmės - tikinčiųjų klaidingai siejama su dievybės geranoriškumu
2 dalis - Gailestingumas - valdančiųjų bausmė - tikinčiųjų klaidingai siejama su dievybės geranoriškumu
Ar mūsų geri darbai iš tiesų yra tokie, kokie mums atrodo?
Kodėl dangiškosios dvilypės būtybės kartais atsiskiria ir sudaro naujas sąjungas
Dievo patarimai, suteikiantys viltį diagnozavus sunkią ligą, ir mums dar nežinomos užuominos apie organizmo ląstelių elgesį.
Žemę vis dažniau užklumpa pražūtingos audros, potvyniai, karščiai ir sausros - lėtos, bet nenumaldomos žemiškojo gyvenimo pabaigos pranašai
1 dalis - nepakeliami tikrų Dievo skelbėjų gyvenimo etapai
2 dalis - nepakeliami tikrų Dievo skelbėjų gyvenimo etapai
Kietos medžiagos visatos sukūrimo ir netobulo žmogaus sukūrimo priežastys
Meilė iš širdies - kaip vidiniai žmonės gali geriau ją suprasti ir išgyventi?
Nuoširdus obuolio pasakojimas apie motininio medžio išgyvenimus
dangaus būtybių vaizdų, spalvų ir garsų kalba, priešingai nei žmonių bendravimas.
svarbi Dievo Dvasios rekomendacija dvasiškai brandiems žmonėms maldai
Santuokos ir partnerystė iš dangiškųjų gyvenimo dėsnių perspektyvos
Švytuoklės - tariamai naudinga veikla su netikėtais pavojais

Krikščioniškosios Kalėdos ir kryžiaus simbolis - ar jie iš tiesų atitinka Dievo valią ir yra labai svarbūs grįžtantiems į dangaus karalystę?
1 dalis - vegetariškas gyvenimo būdas dangiškojo gyvenimo principo požiūriu ir kitos temos
2 dalis - vegetariškas gyvenimo būdas dangiškojo gyvenimo principo požiūriu ir kitos temos
3 dalis - vegetariškas gyvenimo būdas dangiškojo gyvenimo principo požiūriu ir kitos temos
4 dalis - vegetariškas gyvenimo būdas dangiškojo gyvenimo principo požiūriu ir kitos temos
1 dalis - harmoningas, taikus ir subalansuotas vidinis gyvenimo būdas - vertas dėmesio tikslas
2 dalis - harmoningas, taikus ir subalansuotas vidinis gyvenimo būdas - vertas dėmesio tikslas
Žmogiškasis perfekcionizmas dangaus požiūriu
teisingai elgtis su mokėjimo priemonėmis ir dovanomis, taip pat su elementais.
1 dalis - Energijos gamyba branduolių dalijimosi būdu ir branduolinių elektrinių eksploatacija iš dangaus perspektyvos
2 dalis - Energijos gamyba branduolių dalijimosi būdu ir branduolinių elektrinių eksploatavimas dangaus požiūriu
Politiko rinkimai iš Dievo dvasios perspektyvos
Gilus savęs pažinimas apie klaidas ir silpnybes - svarbiausia šios dienos užduotis dangiškiesiems sugrįžėliams, siekiantiems dvasinio tobulėjimo.
1 dalis - Evoliucinis dangaus būtybių gyvenimas ir jų širdies ryšys su beasmene Aš Esu Dievybe
2 dalis - Evoliucinis dangaus būtybių gyvenimas ir jų širdies ryšys su beasmene Aš Esu Dievybe
3 dalis - Dangiškųjų būtybių evoliucinis gyvenimas ir jų širdies ryšys su beasmene Aš Esu Dievybe
1 dalis - nedoras žmogus, vertinantis giliai puolusių būtybių gyvenimą
2 dalis - nedoras giliai puolusių būtybių gyvenimas, stiprinantis jų asmenybę
Kokį džiaugsmą labiau vertinate - triukšmingų žmonių džiaugsmą ar tą, kuris kyla išorinėje tyloje iš jūsų amžinosios sielos vidaus?
1 dalis - ilgi klaidingi tikinčių žmonių ir nežemiškų sielų keliai per religinius mokymus, tikėjimus ir papročius.
2 dalis - ilgi klaidingi tikinčių žmonių ir nežemiškų sielų keliai per religinius mokymus, tikėjimus ir papročius.
3 dalis - ilgas klaidingas tikinčių žmonių ir nežemiškų sielų kelias per religinius mokymus, tikėjimus ir papročius
katastrofiška finansinė ir ekonominė padėtis - kaip turėtų elgtis dangaus sugrįžėliai šiuo nerimą keliančiu laikotarpiu?

1 dalis - Ar ligą arba varginantį įvykį galima susieti su anksčiau nustatytomis priežastimis?
2 dalis - Ar ligą arba varginantį įvykį galima sieti su ankstesnėmis priežastimis?
Dieviškieji nurodymai, kaip grįžti į jautrų palaimingą gyvenimą mūsų šviesos namuose.
1 dalis - nežinomos žinios apie dangiškąsias ir nežemiškas būtybes, taip pat liaupsės - ir kas už jų slypi
2 dalis - nežinomos žinios apie dangiškąsias ir nežemiškas būtybes, taip pat liaupsės - ir kas už jų slypi
3 dalis - nežinomos žinios apie dangiškąsias ir nežemiškas būtybes, taip pat liaupsės - ir kas už jų slypi
Dievo Dvasios atsakymas skaitytojams, kuriems rūpėjo meilės lašai
1 dalis - Ką žmogus patiria mirštant, o po mirties - sielą pomirtiniame gyvenime
2 dalis - Ką žmogus patiria mirštant, o po mirties - sielą pomirtiniame gyvenime

"Aš esu" reikšmė iš Dangaus perspektyvos
3 dalis - Ką žmogus patiria mirštant, o po mirties - sielą pomirtiniame gyvenime
4 dalis - Ką žmogus patiria mirštant, o po mirties - sielą pomirtiniame gyvenime
5 dalis - Ką žmogus patiria mirštant, o po mirties - sielą pomirtiniame gyvenime
1 dalis - Skirtumas tarp beasmenio dangaus būtybių gyvenimo ir asmeninio žmonių gyvenimo
2 dalis - Skirtumas tarp beasmenio dangaus būtybių gyvenimo ir asmeninio žmonių gyvenimo
3 dalis - Skirtumas tarp beasmenio dangaus būtybių gyvenimo ir asmeninio žmonių gyvenimo
Neteisingas požiūris į pozityvų mąstymą ir jo taikymas, turintis pražūtingų pasekmių

1 dalis - Priklausomybės - jų priežastys ir kaip jas įveikti dangiškuoju požiūriu - ir kitos temos
2 dalis - Priklausomybės - jų priežastys ir kaip jas įveikti dangiškuoju požiūriu - ir kitos temos
Visuotinės meilės dvasios paaiškinimas apie skaitytojų klausimus
Ar Jėzus mirė ant kryžiaus?
Ar pranašautojai privalo bet kada gauti dieviškąsias žinutes?
1 dalis - Ritualinių kulto veiksmų kilmė ir pavojus, kurį jie kelia žmonėms ir sieloms iš dangaus perspektyvos
2 dalis - Ritualinių garbinimo veiksmų kilmė ir pavojai, kuriuos jie kelia žmonėms ir sieloms iš dangaus perspektyvos
Kas yra Dievo meilės lašai Aš Esu? - Dieviškųjų pranešimų šaltinis
Akių ląstelių gydymas sergant lengvomis ir sunkiomis ligomis
Kodėl nekantrūs ir neramūs Dievą mylintys žmonės nesugeba užmegzti vidinio ryšio su subtiliu dangiškuoju sielos gyvenimu
Paglostymas - nepatrauklus apgaulingas buvimo būdas
Dangiškųjų būtybių muzika, palyginti su žemiškais kūriniais

Teisės arba dėsningumo termino vartojimas pranešimuose
1 dalis - Subtiliosios mineralinės dalelės - pirmieji gyvybės elementai Visatos sukūrimui
2 dalis - Subtiliosios mineralinės dalelės - pirmieji gyvybės elementai Visatos sukūrimui
Kodėl dieviškųjų meilės lašelių žinutės kartkartėmis keičia savo žinią ir išraišką?
Skatinimas gyventi dangiško nuolankumo gyvenimą
Išradingiausios, širdingiausios, beasmenės būtybės - Dievo - sukūrimas dangaus šviesos būtybių.
Suprasti pranešimus tik pagal jų prasmę
Dieviškųjų žinučių per pranašautojus prasmė ir tikslas
Gydymo terapija gyvatės nuodų serumu dangaus požiūriu
Dvasinis žmonijos neišprusimas su tragiškomis pasekmėmis
Dvasiniai implantai - jų reikšmė ir klastingos funkcijos
Kristus kalba apie savo gimimą ir teisingą visų kosminių būtybių lygybę
Futbolas - kovingas žaidimas - nušviestas iš dangaus perspektyvos
1 dalis - Gyvas nuolankumas - dvasinis raktas į dangiškąjį sugrįžimą ir palaimingą gyvenimą teisingoje lygybėje
2 dalis - Gyvenamas nuolankumas - dvasinis raktas į sugrįžimą į dangų ir palaimingą teisingos lygybės gyvenimą
Meilės kupinas gamtos dovanų rinkimas ir ruošimas
1 dalis - Kodėl nežemiškos būtybės savo erdvėlaiviais mūsų Saulės sistemoje atlieka kosmiškai svarbius veiksmus
2 dalis - Kodėl nežemiškos būtybės savo erdvėlaiviais mūsų Saulės sistemoje atlieka kosmiškai svarbius veiksmus

Saldūs maisto produktai ir jų nepageidaujamas šalutinis poveikis
garsus, energingas kalbėjimas - dominuojantis elgesys, netoleruojantis kalbų iš nugaros.
1 dalis - didelis triukšmas dėl tariamų Dievo atstovų šiame pasaulyje - apgaulės pasaulis
2 dalis - didelis triukšmas dėl tariamų Dievo atstovų šiame pasaulyje - apgaulės pasaulis
rimti nežemiškos kilmės nurodymai
stiprus gyvybinės energijos atsisakymas iš energijos stokojančių žmonių ir sielų.
Jėzaus Kristaus gyvenimas klajojant ir bėgant
fanatiškas laisvės ir teisingumo siekis, turintis rimtų pasekmių.
1 dalis - kosminė išmintis ir dvasinio persiorientavimo patarimai
2 dalis - kosminė išmintis ir dvasinio persiorientavimo patarimai
1 dalis - vidinė savastis - mūsų vidinio šviesaus kūno (sielos) energijos ir duomenų saugykla, taip pat išsamios priemonės, skirtos išgelbėjimui sukurti.

2 dalis - vidinė savastis - mūsų vidinio šviesaus kūno (sielos) energijos ir duomenų saugykla, taip pat išsamios priemonės, skirtos išgelbėjimui sukurti.
Gydymo galimybė aktyvinant degeneravusias ląsteles
Klaidingas Dievo įvaizdis per klaidingus skelbėjus
Kodėl piktybinės ligos taip pat išgydomos
Kodėl žemėje nebus rojaus gyvenimo
Puikybė ir arogancija užkerta kelią dangiškajam sugrįžimui
Trumpa įžanga naujiems "Meilės lašų" skaitytojams
Širdies malda - dvasiškai aukštesnės rūšies malda
klaidingas žmogaus įvaizdis su netikėtomis tragiškomis pasekmėmis.
1 dalis - Kodėl įsikūnijusi siela jau formuoja vaiko charakterį
2 dalis - Kodėl įsikūnijusi siela jau formuoja vaiko charakterį
Dabar prasideda nežemiškojo gyvenimo pabaiga
Visuotinės Meilės Dvasios širdies kvietimas visoms nežemiškoms būtybėms

Perfekcionizmas iš Dievo Dvasios ir filosofo perspektyvos
optimistinė dienos prognozė su maloniu poveikiu
Energetinis išganymo plane įsikūnijusių būtybių ryšys su jų dangiškaisiais planetiniais broliais ir seserimis.
Taikos karalystės vizija - didžioji Dievą tikinčių žmonių ir nežemiškų sielų klaida
1 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
sudėtingas sugyvenimas su dominuojančiais, savo nuomonę turinčiais ir ginčijančiais žmonėmis.
neįsivaizduojami įkyrių sielų pavojai smalsiems žmonėms, turintiems aiškiaregystės gebėjimų.
2 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
Iš kur kai kurie mediumistai šiandien gauna pranešimus apie artimiausius nerimą keliančius pasaulio įvykius?
Nuoširdūs dangaus būtybių linkėjimai visiems dangaus sugrįžėliams ateinančiu laiku žemėje.

Nežemiškos būtybės žinutė žmonijai
Dvejopas dangiškųjų būtybių gyvenimas toje pačioje sąmonės būsenoje ir nuolatinė širdžių harmonija
1 dalis - Teigiamų ir neigiamų nuosprendžių reikšmė kūrinijos atsiradimui ir dangiškajam dvigubam gyvenimui
2 dalis - Teigiamų ir neigiamų nuosprendžių reikšmė kūrinijos ir dangiškojo dvigubo gyvenimo atsiradimui
Masinio pabėgėlių antplūdžio priežastys - tragedija dangiškuoju ir dieviškuoju požiūriu
3 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
Kodėl vien tik tvirto tikėjimo Dievu ir širdies maldų nepakanka, kad žmogus ir jo siela priartėtų prie Dangaus karalystės?
Kodėl taip svarbu prašyti iš širdies, dėkoti ir priimti kitų padėką?
Švarios šviesos būtybės įžvalgos iš žemiškojo gyvenimo, skirtos dangaus sugrįžėlių savęs pažinimui
1 dalis - Sergančiųjų meilės liga
2 dalis - Širdies žmogaus širdgėla

neišsenkantis ir neribotas nesavanaudiškas dieviškosios meilės dovanojimas - kaip jį geriau suprasti

Abejingo gyvenimo ir dvasinio nesidomėjimo įveikimas
Šuolis su kartimi - Dievo dvasios priežastys ir perspėjimas
Meilės dvasios kvietimas - nešvaistykite gyvybinės energijos

Dievo Dvasios kvietimas visoms įsikūnijusioms būtybėms
1 dalis. 1 dalis - Ateities aiškinimai - rizikingas ir pavojingas užsiėmimas
2 dalis - Ateities pranašų aiškinimai - rizikinga ir pavojinga užduotis
Kosminio gelbėjimo aljanso veikla materialiose Saulės sistemose
1 dalis - Kaip Žemė buvo apgyvendinta augalais ir gyvomis būtybėmis
2 dalis - Kaip Žemėje prasidėjo kolonizacija augalais ir gyvomis būtybėmis
Nesavanaudiškumo reikšmė dangiškuoju požiūriu
Medžiaginių ląstelių branduolių programavimas priimti dvipolę dieviškąją energiją
Nesėkmingas giliai puolusių būtybių nuotykis su nenugalima kančia artėja prie pabaigos
Kokias galimybes Dievo dvasia turi numatyti ir išvengti kosminių ir planetinių pavojingų situacijų.
1 dalis - Žinutė iš rožių krūmo
2 dalis - Rožių krūmo bendravimas
3 dalis - Žinutė iš rožių krūmo
Širdies linkėjimai 2020 metams

Dangaus nuolankumas - ką iš jo padarė atkritusios puolusios būtybės
Meilės dvasios atsakas į pranešimų vertimus ir dangiškąsias gyvenimo taisykles
Meilės dvasios instrukcija dangiškomis pastelinėmis spalvomis ir meilės lašelių svetainių kūrimas
Persekiojimo baimės įveikimas
Kreipimasis į organizmo ląsteles gripo infekcijų atveju ir siekiant apsisaugoti nuo virusų įsibrovėlių
Meilės dvasios rekomendacijos žemiškiems nelaimių ir ekstremalių situacijų laikotarpiams
Didelis dangiškosios būties džiaugsmas dėl reikšmingo žemiškojo įvykio
Įžvalga apie netobulą Dievo skelbėjų ir vadinamųjų pranašų gyvenimą
Gydymo prietaisų elektromagnetinių dažnių poveikis kūno ląstelėms ir sielai dangiškuoju-dieviškuoju požiūriu
1 dalis - Kodėl nereikėtų atmesti dvasinės ir techninės pažangos
2 dalis - Kodėl nereikėtų atmesti dvasinės ir techninės pažangos
Mūsų pirmapradės kūrinijos Motinos darbas nuopuolio metu ir jos ankstyvas sugrįžimas į dangų
Įvadas į temų rinkinį
Kam skirta teminė kolekcija?
1 temų rinkinys
2 temų rinkinys
Temų rinkinys 3
Kodėl kai kuriuos žmones trikdo kai kurie pranešimo žodžiai
4 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
5 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą

6 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
7 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
8 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
Susitikimas su nežemiška būtybe
Temų rinkinys 4
Nuoroda į temų kolekcijų sąrašą
Kristus aprašo savo gražų dangiškąjį sugrįžimą
1 dalis - vidinė transformacija į asmeniškai nepastebimą ir nuoširdžią dangiškos kilmės būtybę
2 dalis - vidinė transformacija į asmeniškai nepastebimą ir nuoširdžią dangiškos kilmės būtybę
Kaip atsirado pirmieji pirmapradžių tėvų kūriniai ir būtybių palikuonys dangiškoje pirmykštėje kūryboje
3 dalis - vidinė transformacija į asmeniškai nepastebimą ir nuoširdžią dangiškos kilmės būtybę
