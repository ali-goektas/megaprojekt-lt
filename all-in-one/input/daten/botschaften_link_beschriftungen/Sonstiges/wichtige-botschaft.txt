bulgarisch
Бог, безличното, най-гениално и сърдечно същество, е създаден от небесни същества от светлина.



tschechisch
Bůh, nejgeniálnější, nejsrdečnější, neosobní bytost, byl stvořen nebeskými bytostmi světla.



dänisch
Gud, det upersonlige, mest geniale og hjertelige væsen, blev skabt af himmelske lysvæsener.



deutsch
„Erschaffung des genialsten und herzlichsten unpersönlichen Wesens - Gott - durch himmlische Lichtwesen“
Übersetzungsvorlage:
Gott, das unpersönliche, genialste und herzlichste Wesen, wurde erschaffen von himmlischen Lichtwesen.



XXX
griechisch
Ο Θεός, το απρόσωπο, το πιο ιδιοφυές και εγκάρδιο ον, δημιουργήθηκε από ουράνια όντα του φωτός.



englisch - uk
God, the impersonal, most ingenious and most cordial being, was created by heavenly beings of light.



englisch - us
God, the impersonal, most ingenious and most cordial being, was created by heavenly beings of light.



spanisch
Dios, el ser impersonal, más ingenioso y más cordial, fue creado por seres celestiales de luz.



(xxx)
estnisch
Jumal, isikupäratu, kõige geniaalsem ja südamlikum olend, loodi taevaste valgusolendite poolt.



(xxx)
finnisch
Jumala, persoonaton, nerokkain ja ystävällisin olento, luotiin taivaallisten valo-olentojen toimesta.



französisch
Dieu, l'être impersonnel, le plus ingénieux et le plus cordial, a été créé par des êtres célestes de lumière.



(xxx)
ungarisch
Istent, a személytelen, legzseniálisabb és legszívélyesebb lényt a fény égi lényei teremtették.



(xxx)
italienisch
Dio, l'essere impersonale, più geniale e più cordiale, è stato creato da esseri celesti di luce.



(xxx)
japanisch
最も属人的で、最も心のこもった、人間離れした存在である神は、光の天人たちによって創造された。



xxx
litauisch
Dievą, beasmenę, išradingiausią ir širdingiausią būtybę, sukūrė dangaus šviesos būtybės.



xxx
lettisch
Dievu, bezpersonisko būtni, visģeniālāko un vismīlošāko, radīja debesu gaismas būtnes.



niederländisch
God, het onpersoonlijke, meest geniale en meest hartelijke wezen, werd geschapen door hemelse wezens van licht.



xxx
polnisch
Bóg, bezosobowa, najbardziej genialna i najserdeczniejsza istota, został stworzony przez niebiańskie istoty światła.



portugiesisch
Deus, o ser impessoal, mais genial e mais cordial, foi criado por seres celestiais de luz.



portugiesisch - br
Deus, o ser impessoal, mais genial e mais cordial, foi criado por seres celestes de luz.



xxx
rumänisch
Dumnezeu, ființa impersonală, cea mai ingenioasă și mai inimoasă, a fost creat de ființe celeste de lumină.



(xxx)
russisch
Бог, безличное, самое гениальное и самое сердечное существо, был создан небесными существами света.



slowakisch
Boha, neosobnú, najgeniálnejšiu a najsrdečnejšiu bytosť, stvorili nebeské bytosti svetla.



slowenisch
Boga, najbolj genialno, najbolj srčno, neosebno bitje, so ustvarila nebesna bitja svetlobe.



schwedisch
Gud, den opersonliga, mest geniala och hjärtligaste varelsen, skapades av himmelska ljusvarelser.



chinesisch
上帝，这个非个人的、最巧妙的、最衷心的存在，是由天人合一的光明生物创造的。



