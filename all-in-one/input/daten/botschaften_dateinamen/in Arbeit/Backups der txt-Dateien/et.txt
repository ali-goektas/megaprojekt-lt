Väikeloomad - meie kahjurid
Jumala appihüüded ja manitsused
Vesi - kogemused jõesängis
kahjulikud maapinnalähedased kiired maa-aluste vee läbipääsude tõttu
loomamaailma sõnulseletamatuid kannatusi
keha liikumine tantsimise ajal
liialdatud terviseteadlik elu
keharakkudega rääkimine - haigestumise korral
minu ja sinu - siduv elu langevad olendid
liialdatud käitumine söömisel
veeuputus - taevaste olendite vaatenurgast vaadatuna
tubakasõltuvus
ettevalmistused loodu päästmiseks - taevaste olendite piinarikkad kehastused
Usulistest motiividest tingitud mortifitseerimise ja sundimise laastav mõju
Somaatiliste rakkudega rääkimine - raske haiguse korral
rääkides keharakkudega - enne operatsiooni
sisemine tee Jumala juurde - valed ideed
nähtamatud ja arusaamatud kosmilised sündmused
nahaallergia tekkimine
maiste hingede ülestõusmispõlve - nende mõju inimese elule
Oma mõtete teadvustamine ja kontrollimine
jumalik õrnus
Enesevigastus ja depressiivne meeleolu
tsunami tõttu kannatanute traagiline saatus
omaenda teesklused - nende äratundmine, analüüsimine ja parandamine
eeldused - kasulikud ebameeldivad tunded, et näha läbi kahevahelised inimesed
elades selles pettuse maailmas
eluenergia suurenemine
Orjad ja töö ajendatuna
1. osa - Toit - hirm kahjulike ainete ees
2. osa - toit - hirm saasteainete ees
Kosmiliste olendite vabadus ja sõltumatus
Maa dramaatiline energiaseisund - laastavad tagajärjed kogu elule
Suhted peente aatomite ja kondenseeritud aine aatomite vahel
Alandlikkuse mõistmine - elamine taevaste olendite õigluses
Rohkem energiat sisaldav toit - tuleelement
maised ja õpetlikud elupõhimõtted - muusika ilma taevaliku helita
hing, mis läheb inimkehast välja tulevikku.
lohutavad ja abistavad sõnad - eneseteadmised eakatele ja üksildastele inimestele
Jumala mõistmine - meie seaduserikkumiste eest ja nende andeksandmine
Kus on sinu kogutud aarded, sinna tõmbab sind ikka ja jälle
Eneseteadvus - kõnekeel - kinesioloogia - omapärane tervendamine ilmikute poolt
Juhised love drop sõnumite tõlkijatele teistesse keeltesse tõlkimiseks
suletud süda
tee välja südame jahedusest - elamine taevaste kaksikolendite südamesoojuses
kuldne kesktee söömiskäitumises
Apokalüpsis ja enneaegne maailmalõpp
Looduse hävitamine energiata mikroloomade poolt
Enese ausus paljastab vale
Õigusrikkumised - täitmine inimkehas - hinge kaudu
toit ja vürtsid - kokkusobimatus - truudus - õnnelik partnerlus
mures looduse olendid - raske elu olukord - aiad
teeseldud truudus abielus ja partnerluses - südamekülma silmakirjatseja topeltelu
südamlik - üllas - taevalik
suur eluenergia kadu arvamuslike inimeste tõttu
teadvustada inimelu
ausus - ustavus - aupaklikkus - harmooniline ja õnnelik partnerlus
keha rakkudega rääkimine - liigesevalu puhul
1. osa - Religioonide ja fanaatiliste uskumuste tekkimine - inimeste ja hingede kannatused
2. osa - Religioonide ja fanaatiliste uskumuste tekkimine - Inimeste ja hingede kannatused
Jumalat armastavad inimesed ja tõsine kõne
Tee spirituaalsuse ja taevase vabaduse poole
Omavastutus oma elu eest - tule element - tuhastus
1. osa - väidetavalt ravimatud haigused - paranemisvõimalused - erinevad maailmad
2. osa - väidetavalt ravimatud haigused - paranemisvõimalused - erinevad maailmad
3. osa - väidetavalt ravimatud haigused - ravivõimalused - erinevad maailmad
Räägivad keharakkudega - nende funktsioonide aktiveerimiseks
1. osa - armu - karistuste andmine valitsejate poolt - ekslikult Jumala heategevusega segi ajades
2. osa - halastus - karistuste andmine valitsejate poolt - ekslikult Jumala heatahtlikkusega segi ajades
Meie head teod - kuidas me neid hindame - kuidas nad tegelikult on
Miks taevased kaksikolendid mõnikord eralduvad ja moodustavad uusi liite
Nõustamine tõsise haiguse korral - keharakkude tundmatu käitumine
Äikesetormid - üleujutused - kuumus - põud - maise elu lõpu lähenemine
1. osa - talumatud etapid Jumala kuulutajate elus
2. osa - talumatud etapid Jumala kuulutajate elus
Tahke universumi tekkimine - ebatäiusliku inimese loomine
Armastus südamest - mõistmine ja elamine
Õun ja selle kogemused puu otsas
taevased olendid - pildikeel - värvikeel - helikeel - helikeel
Palve soovitus vaimselt küpsetele inimestele
Abielud ja partnerlused - taevaste eluseaduste vaatenurgast vaadatuna
pendelränne - näiliselt kasulik - ootamatud ohud
Kristlikud jõulud ja risti sümbol
1. osa - taimetoitlaste eluviis - taevaliku elu vaatenurgast
2. osa - taimetoitlaste eluviis - taevaliku elu vaatenurgast
3. osa - taimetoitlaste eluviis - taevaliku elu vaatenurgast
4. osa - taimetoiduline eluviis - taevase elu seisukohalt
1. osa - elada harmooniliselt, rahulikult ja tasakaalustatult - väärt eesmärk
2. osa - elada harmooniliselt, rahulikult ja tasakaalustatult - väärt eesmärk
Inimlik perfektsionism taevasest vaatepunktist
maksevahendite, kingituste ja elementide õige käitlemine
1. osa - Tuumalõhustumine ja tuumaelektrijaamad - taevalikust vaatenurgast
2. osa - Tuumalõhustumine ja tuumaelektrijaamad - taevalikust vaatenurgast
Poliitiku valimine - Jumala vaimu seisukohalt
Oma vigade ja nõrkuste sügav äratundmine.
1. osa - Taevalike olendite evolutsiooniline elu ja nende südamega seotud seos Jumalaga
2. osa - Taevaste olendite evolutsiooniline elu ja nende südameühendus Jumalaga
3. osa - Taevalike olendite evolutsiooniline elu ja nende südamega seotud seos Jumalaga
1. osa - isiku kõrgemaks väärtustamine - ebapuhtad
2. osa - isiku kõrgemaks väärtustamine - ebapuhtad
Rõõm läbi valjude inimeste väljastpoolt või rõõm hinges seestpoolt
1. osa - Usklike inimeste ja teispoolsete hingede odüsseia läbi religioossete õpetuste
2. osa - Usklike inimeste ja teispoolsete hingede odüsseia läbi religioossete õpetuste
3. osa - Usklike ja teispoolsete hingede odüsseia läbi religioossete õpetuste
Laastav finants- ja majandusolukord - kuidas käituda sel ajal
1. osa - Haigused ja kurvad sündmused - kas nende põhjused on minevikus?
2. osa - Haigused ja kurvad sündmused - kas nende põhjused on minevikus?
Juhised - tagasitee - tundlik õndsas elu - meie kerge kodu
1. osa - Teadmised taevaste ja maaväliste olendite kohta - meelitamine
2. osa - Teadmised taevaste ja maaväliste olendite kohta - meelitamine
3. osa - teadmised taevaste ja maaväliste olendite kohta - meelitamine
Jumala vastus lugejatele, kes olid mures armastuse tilkade pärast
1. osa - Kogemused - inimene suremisfaasis - pärast surma hing edasises elus
2. osa - Kogemused - inimene suremisfaasis - pärast surma hing edasises elus
Mina Olen tähendus taevasest vaatepunktist lähtudes
3. osa - Kogemused - inimene suremisfaasis - pärast surma hing edasises elus
4. osa - Kogemused - inimene suremisfaasis - pärast surma hing edasises elus
5. osa - Kogemused - inimene suremisfaasis - pärast surma hing edasises elus
1. osa - taevaste olendite impersonaalne elu - inimeste isiklik elu
2. osa - taevaste olendite impersonaalne elu - inimeste isiklik elu
3. osa - taevaste olendite impersonaalne elu - inimeste isiklik elu
Positiivne mõtlemine - vale arusaamine ja rakendamine - katastroofilised tagajärjed
1. osa - Sõltuvused - põhjused ja toimetulek - taevasest vaatepunktist
2. osa - Sõltuvused - põhjused ja toimetulek - Taevasest vaatenurgast vaadatuna
Selgitus armastuse vaimust seoses lugeja küsimustega
Kas Jeesus suri ristil?
Kas kuulutajad on kohustatud igal ajal jumalikke sõnumeid vastu võtma?
1. osa - rituaalsed riitused - oht inimestele ja hingedele - taevasest vaatepunktist
2. osa - rituaalsed riitused - oht inimestele ja hingedele - taevasest vaatenurgast
Millised on Mina Olen Jumaluse armastuse tilgad? - Jumalike sõnumite allikas
Silmarakkudega rääkimine - kergete ja raskete haiguste puhul
Miks kannatamatud rahutud inimesed ei suuda luua suhet oma hingega
Lemmiklikkus - ebameeldiv petlik omadus
Taevalike olendite muusika - maiste kompositsioonid
Õiguse ja korrapärasuse mõisted sõnumitesse
1. osa - peened mineraalsed osakesed - universumi loomise elemendid
2. osa - Peened mineraalsed osakesed - Universumi loomise elemendid
Miks sõnumite väljendusviis muutub aja jooksul.
Taevalik ja alandlik elu
Kõige geniaalsem, südamlikum, isikupäratu Jumal on loodud taevaste valgusolendite poolt
Sõnumite mõistmine ainult nende tähenduses
Jumalike sõnumite mõte ja eesmärk
Tervendav ravi mürkseerumiga - taevasest vaatepunktist lähtuvalt
Inimkonna vaimne teadmatus traagiliste tagajärgedega
Vaimsed implantaadid - nende tähendus ja salakavalad funktsioonid
Kristuse sünd - kõigi kosmiliste olendite õiglane võrdsus
Jalgpall - võitluslik mäng - valgustatud taevasest vaatepunktist
1. osa - Alandlikkus - võti taevasse ja õndsasse ellu
2. osa - Alandlikkus - võti taevasse ja õndsasse ellu
Looduslike andide armastav koristamine ja valmistamine
1. osa - Maavälised olendid sooritavad olulisi tegevusi Päikesesüsteemis
2. osa - Maavälised olendid sooritavad olulisi tegevusi Päikesesüsteemis
Magusad toidud ja nende soovimatud kõrvalmõjud
kõva ja energiline rääkimine - domineeriv käitumine, mis ei talu tagarääkimist
1. osa - suur kära Jumala väidetavate esindajate ümber
2. osa - suur kära Jumala väidetavate esindajate ümber
tõsised juhised maaväliselt inimeselt
Madala energiaga inimesed ja hinged ammutavad palju eluenergiat
Jeesus Kristus rändamisest ja põgenemisest
Fanaatiline soov vabaduse ja õigluse järele, millel on tõsised tagajärjed
1. osa - kosmiline tarkus ja nõuanded vaimseks ümberorienteerumiseks
2. osa - kosmiline tarkus ja nõuanded vaimseks ümberorienteerumiseks
1. osa - sisemine mina - energia ja andmete säilitamine - meetmed loomingu säästmiseks
2. osa - sisemine mina - energia ja andmete säilitamine - meetmed loomise säästmiseks
Võimalus paranemiseks degeneratsioonirakkude aktiveerimise teel
Vale ettekujutus Jumalast ekslike kuulutajate kaudu
Miks pahaloomulised haigused on ka ravitavad
Miks ei tule paradiislikku elu maa peal
Uhkus ja ülbus blokeerivad tee tagasi taevasse
Lühike sissejuhatus uutele lugejatele Love Drops'ile
Vaimselt kõrgemat laadi südamepalve
vale kuvand inimesest - ootamatud traagilised tagajärjed
1. osa - Miks kehastunud hing kujundab juba lapse iseloomu
2. osa - Miks kehastunud hing kujundab juba lapse iseloomu
Elu lõpp väljaspool taevast algab nüüd
Jumala südame üleskutse - kõigile olenditele väljaspool taevast
Perfektsionism Jumala ja filosoofi seisukohalt
Optimistlik päevaprognoos meeldiva mõjuga
Päästeplaani kehastunud olendite seotus nende taevaste planeetide vendade ja õdedega.
Visioon rahuriigist - Jumalasse uskuva rahva eksimus ja hinged edasises elus
1. osa - Teadmised Jeesuse Kristuse maapealsest ja taevasest elust
raske kooselu domineerivate, omapäraste, argumenteerivate inimestega
pealetükkivad hinged - oht uudishimulikele inimestele, kellel on selgeltnägemisvõimed
2. osa - Teadmised Jeesuse Kristuse maapealsest ja taevasest elust
Kust tulevad sõnumid maailma murettekitavate sündmuste kohta
Südamlikud soovid eelseisvaks ajaks kõigile neile, kes naasevad koju taevasse
Maavälise inimese sõnum inimkonnale
Taevane kaksikelu - sama teadvuse tase - üksmeel südametes
1. osa - Positiivsed ja negatiivsed osakesed - Loomise päritolu - Topeltelu
2. osa - Positiivsed ja negatiivsed osakesed - Loomise tekkimine - Topeltelu
Massiivne põgenikevoog - tragöödia taevalikust vaatepunktist
3. osa - Teadmised Jeesuse Kristuse maapealsest ja taevasest elust
Südamepalvetest ja kindlast usust Jumalasse ei piisa taevasse pääsemiseks.
Südamest - palumine, tänamine, teiste tänamise vastuvõtmine
Valgusolendi arusaamade kasutamine eneseuurimiseks
1. osa - Südamehaigused
2. osa - Südamepiinad
ammendamatu piiramatu, piiramatu ja omakasupüüdmatu jumalik armastus
Ükskõiksuse ja vaimse huvipuuduse ületamine
Magnetpooluse ümberpööramine - põhjused - Jumala Vaimu eelhoiatus
Jumala üleskutse - ärge raisake eluenergiat
Jumala äratuskõne kõigile kehastunud olenditele
1. osa - Ennustajate tulevikutõlgendused - riskantne ja ohtlik
2. osa - Ennustajate tulevikutõlgendused - riskantne ja ohtlik
Kosmilise Päästeliidu tegevus materiaalsetes päikesesüsteemides
1. osa - Kuidas jõudis taime ja elusolenditega maa koloniseerimiseni
2. osa - Kuidas toimus koloniseerimine taimede ja elusolenditega Maal
Eneseväärikuse tähendus taevasest vaatepunktist lähtudes
Materiaalsete raku tuumade programmeerimine ja bipolaarse energia neeldumine
sügavalt langenud olendid - nende seiklus täis kannatusi - ebaõnnestunud - lõpeb varsti
Jumala võimalused kosmiliste ohtude ettenägemiseks ja ärahoidmiseks
1. osa - Sõnum roosipõõsast
2. osa - Sõnum roosipõõsast
3. osa - Sõnum roosipõõsast
Südame soovid aastaks 2020
Taevane alandlikkus - mida langenud langenud olendid sellest tegid
sõnumite tõlked - taevased elureeglid
taevased pastellid - veebilehtede loomine armastuse tilkade kohta
Tagakiusamishirmu ületamine
Räägib keha rakkudega - gripilaadsete infektsioonide ja viiruse sissetungijate eest kaitsmiseks
Jumala soovitused maiste katastroofide ja hädaolukordade jaoks
suur rõõm taevas olulise maise sündmuse üle
Jumala kuulutajate ja nn prohvetite ebatäiuslik elu
Tervendavad seadmed - elektromagnetilised sagedused - mõju kehale ja hingele
1. osa - Miks vaimset ja tehnilist arengut ei tohiks tagasi lükata
2. osa - Miks vaimset ja tehnilist arengut ei tohiks tagasi lükata
Algne ema sügisel - tema enneaegne tagasipöördumine taevasse
Sissejuhatus teemakogumikku
Milleks on teemakogumikud mõeldud
Teemakogumik 1
Teemakollektsioon 2
Teemakogumik 3
Miks mõned inimesed on häiritud mõnest sõnumist sõnumitest
4. osa - tundmatud teadmised Jeesuse Kristuse maapealsest ja taevasest elust
5. osa - tundmatud teadmised Jeesuse Kristuse maapealsest ja taevasest elust
6. osa - tundmatud teadmised Jeesuse Kristuse maapealsest ja taevasest elust
7. osa - tundmatud teadmised Jeesuse Kristuse maapealsest ja taevasest elust
8. osa - tundmatud teadmised Jeesuse Kristuse maapealsest ja taevasest elust
Kohtumine maavälise olendiga
Teemakogumik 4
Kristus kirjeldab oma ilusat tagasipöördumist taevasse
1. osa - sisemine ümberkujundamine taevase päritoluga südamlikuks olendiks
2. osa - sisemine ümberkujundamine taevase päritoluga südamlikuks olendiks
ürgvanemad eelloomingus - nende esimesed loodud ja sigitatud lapsed
3. osa - sisemine ümberkujundamine taevase päritoluga südamlikuks olendiks