Kleinsttiere - unsere Plagegeister
Mahn- und Hilferufe Gottes
Erlebnisse des Wassers im Flussbett
schaedlich wirkende Erdstrahlen durch unterirdische Wasserkreuzungen
unsagbares Leid der Tierwelt
Koerperbewegung im Tanz
uebertriebenes gesundheitsbewusstes Leben
Ansprache der Koerperzellen bei einem Unwohlsein
mein und dein - die bindende Lebensweise der Fallwesen
ueberzogene Verhaltensweise beim Essen
Betrachtung eines Wasserfalls aus der Sicht der himmlischen Wesen
Tabaksucht
lange und muehevolle Vorbereitungen der himmlischen Wesen zur Schoepfungserrettung und ihre qualvollen Inkarnationen
verheerende Auswirkungen auf Koerper und Seele durch Kasteiung und Zwang aus religioesen Motiven
Ansprache der Koerperzellen bei schwerer Erkrankung
Ansprache der Koerperzellen vor einem operativen Eingriff
falsche Vorstellungen vom inneren Weg zu Gott
Einblick in das unsichtbare und unfassbare kosmische Geschehen
Entstehung von Hautallergien
erdgebundenes Jenseits der weltbezogenen Seelen und ihr unsichtbarer Einfluss auf das menschliche Leben
goettliche Weisung zur Bewusstwerdung und Kontrolle eigener Gedanken
Wie ist die goettliche Sanftmut zu verstehen
Selbstbedauern und depressive Stimmung
tragisches Schicksal der Tsunami-Betroffenen
eigene Verstellungskuenste erkennen analysieren und beheben
Vermutungen als hilfreiche unangenehme Gefuehle um zweideutige Menschen zu durchschauen
Leben in dieser Welt der Taeuschung
Gottes Angebot zur Vermehrung der Lebensenergie
Sklaven und Getriebene der Arbeitswelt
Teil 1 - Angst vor Schadstoffen in der Nahrung
Teil 2 - Angst vor Schadstoffen in der Nahrung
Was bedeutet die Freiheit und Unabhaengigkeit der kosmischen Wesen
dramatischer Energiezustand der Erde mit verheerenden Folgen fuer das ganze Leben
enge Beziehungen der feinstofflichen Ur-Atome zu den veraenderten verdichteten Atomen des materiellen Weltalls
richtig verstandene Bescheidenheit um nahe der goettlichen Gerechtigkeit der himmlischen Wesen zu leben
energiereichere Nahrungsaufnahme in Verbindung mit dem Feuerelement
schwierige Befreiung von den weltlichen und belehrenden Lebensprinzipien sowie Musikkompositionen ohne himmlischen Klang
Abberufung einer gottverbundenen Seele aus dem menschlichen Koerper ins Jenseits
troestende und hilfreiche Worte vom universellen Liebegeist zur Selbsterkennung fuer aeltere und einsame Menschen
Wie koennen wir das Verstaendnis Gottes fuer unsere Gesetzesvergehen und deren Vergebung weitsichtiger erfassen
Wo eure gesammelten Schaetze sind zieht es euch immer wieder hin
Selbsterkenntnis im Sprachklang - Kinesiologie - eigenwilliges Heilen durch medizinische Laien
wichtige Anweisungen vom Gottesgeist fuer Uebersetzer der Ich Bin-Liebetroepfchen in andere Sprachen
verschlossenes Herz
moeglicher Weg aus der eigenen Herzenskuehle herauszufinden um wieder in der Herzenswaerme himmlischer Dualwesen zu leben
goldener Mittelweg im Essenverhalten
Kann die innere Menscheit das von den frueheren Sehern vorausgesagte apokalyptische Geschehen auf der Erde noch abwenden
Zerstoerung der Natur durch energielose Kleinsttiere
Selbstehrlichkeit entlarvt die Luege
unbarmherzige seelische Abtragung der Gesetzesvergehen im menschlichen Koerper
Unvertraeglichkeit bestimmter Speisen und Gewuerze bei gottverbundenen Menschen - Treue - fuer eine glueckliche Partnerschaft
besorgte Naturwesen berichten ueber ihre schwierige Lebenssituation und ihre Aufgaben in unseren Gaerten
vorgetaeuschte Treue in Ehe und Partnerschaft - Doppelleben der herzenskalten Heuchler
Wo sind eure herzlichen und edlen himmlischen Wesenseigenschaften aus der goettlichen Genialitaet geblieben
hoher Lebensenergieverlust durch regelmaessige Begegnungen mit weltlich bezogenen rechthaberischen Menschen
Bewusstwerdung des menschlichen Lebens
Ehrlichkeit Treue und Ehrgefuehl - die wichtigsten edlen Eigenschaften fuer eine harmonische und glueckliche Partnerschaft
Ansprache der Koerperzellen bei Gelenkschmerzen
Teil 1 - Entstehung der Religionen und Glaubensgemeinschaften - und daraus fanatischem Gottesglauben - mit endlosem Leid
Teil 2 - Entstehung der Religionen und Glaubensgemeinschaften - und daraus fanatischem Gottesglauben - mit endlosem Leid
Warum manche gottverbundene Menschen ernstes Sprechen nicht ertragen koennen
Der Weg zur Vergeistigung und himmlischen Freiheit
Selbstverantwortung fuer das eigene Leben - das Element Feuer und die Feuerbestattung des Koerpers
Teil 1 - Gruende fuer angeblich unheilbare Krankheiten und ihre Heilungschancen - Erschaffung voellig unterschiedlicher Welten
Teil 2 - Gruende fuer angeblich unheilbare Krankheiten und ihre Heilungschancen - Erschaffung voellig unterschiedlicher Welten
Teil 3 - Gruende fuer angeblich unheilbare Krankheiten und ihre Heilungschancen - Erschaffung voellig unterschiedlicher Welten
Ansprache der Koerperzellen zur Aktivierung ihrer Funktionen
Teil 1 - Gnade - Straferlass der Herrschenden - irrtuemlich von Glaeubigen mit dem Wohlwollen Gottes in Verbindung gebracht
Teil 2 - Gnade - Straferlass der Herrschenden - irrtuemlich von Glaeubigen mit dem Wohlwollen Gottes in Verbindung gebracht
unsere guten Taten - sind sie wirklich so wie wir sie einschaetzen
Warum sich himmlische Dualwesen manchmal trennen und neue Verbindungen eingehen
Gottes hoffnungsgebender Rat nach Feststellung einer schweren Erkrankung - bisher unbekanntes Verhalten der Koerperzellen
Zunahme verheerender Unwetter Ueberschwemmungen Hitze- und Duerrephasen auf der Erde - baldiges Ende des irdischen Lebens
Teil 1 - unertraegliche Phasen im Leben der wahren Kuender Gottes
Teil 2 - unertraegliche Phasen im Leben der wahren Kuender Gottes
Gruende fuer die Entstehung des feststofflichen Universums und fuer die Erschaffung des unvollkommenen Menschen
Liebe aus dem Herzen - wie koennen die inneren Menschen sie besser verstehen und leben
herzliche Mitteilung eines Apfels ueber die Erlebnisse auf seinem Mutterbaum
Bild- Farb- und Klangsprache der himmlischen Wesen im Gegensatz zur menschlichen Verstaendigung
wichtige Gebetsempfehlung vom Gottesgeist fuer geistig weit gereifte Menschen
Ehen und Partnerschaften aus der Sicht der himmlischen Lebensgesetze
Pendeln - eine vermeintlich hilfreiche Betaetigung mit ungeahnten Gefahren
christliche Weihnachten und das Kreuz - sind sie wirklich der goettliche Wille und wichtig fuer die Rueckkehr ins Himmelreich
Teil 1 - vegetarische Lebensweise - aus der Sicht des himmlischen Lebensprinzips und andere Themen
Teil 2 - vegetarische Lebensweise - aus der Sicht des himmlischen Lebensprinzips und andere Themen
Teil 3 - vegetarische Lebensweise - aus der Sicht des himmlischen Lebensprinzips und andere Themen
Teil 4 - vegetarische Lebensweise - aus der Sicht des himmlischen Lebensprinzips und andere Themen
Teil 1 - harmonische friedliebende und ausgeglichene innere Lebensweise - ein lohnenswertes Ziel
Teil 2 - harmonische friedliebende und ausgeglichene innere Lebensweise - ein lohnenswertes Ziel
menschlicher Perfektionismus aus himmlischer Sicht
rechter Umgang mit Zahlungsmitteln und Schenkungen sowie den Elementen
Teil 1 - Energiegewinnung durch Kernspaltung und das Betreiben von Atomkraftwerken aus himmlischer Sicht
Teil 2 - Energiegewinnung durch Kernspaltung und das Betreiben von Atomkraftwerken aus himmlischer Sicht
Wahl eines Politikers - aus der Sicht des Gottesgeistes
tiefgruendige Selbsterkenntnis der Fehler und Schwaechen - die wichtigste Aufgabe des Tages fuer himmlische Rueckkehrer
Teil 1 - Evolutionsleben der himmlischen Wesen und ihre Herzensverbindung zur unpersoenlichen Ich Bin-Gottheit
Teil 2 - Evolutionsleben der himmlischen Wesen und ihre Herzensverbindung zur unpersoenlichen Ich Bin-Gottheit
Teil 3 - Evolutionsleben der himmlischen Wesen und ihre Herzensverbindung zur unpersoenlichen Ich Bin-Gottheit
Teil 1 - unheilbringendes personenaufwertendes Leben tief gefallener Wesen
Teil 2 - unheilbringendes personenaufwertendes Leben tief gefallener Wesen
Welche Art der Freude schaetzt ihr mehr - die der lauten Menschen oder die in der Stille aus dem Inneren eurer Seele aufsteigt
Teil 1 - lange Irrwege glaeubiger Menschen und jenseitiger Seelen durch religioese Lehren Glaubensbekenntnisse und Braeuche
Teil 2 - lange Irrwege glaeubiger Menschen und jenseitiger Seelen durch religioese Lehren Glaubensbekenntnisse und Braeuche
Teil 3 - lange Irrwege glaeubiger Menschen und jenseitiger Seelen durch religioese Lehren Glaubensbekenntnisse und Braeuche
verheerende Finanz- und Wirtschaftslage - wie sollen sich in dieser beunruhigenden Zeit die himmlischen Heimkehrer verhalten
Teil 1 - Koennen eine Krankheit oder ein leidvolles Ereignis auf frueher gesetzte Ursachen zurueckgefuehrt werden
Teil 2 - Koennen eine Krankheit oder ein leidvolles Ereignis auf frueher gesetzte Ursachen zurueckgefuehrt werden
goettliche Weisungen fuer den inneren Rueckweg in ein feinfuehliges glueckseliges Leben unserer Lichtheimat
Teil 1 - unbekanntes Wissen ueber himmlische und ausserirdische Wesen sowie Schmeicheleien - und was dahintersteckt
Teil 2 - unbekanntes Wissen ueber himmlische und ausserirdische Wesen sowie Schmeicheleien - und was dahintersteckt
Teil 3 - unbekanntes Wissen ueber himmlische und ausserirdische Wesen sowie Schmeicheleien - und was dahintersteckt
Antwort des Gottesgeistes an Leser die sich um die Liebetroepfchen sorgten
Teil 1 - Was der Mensch in der Sterbephase und nach dem Tod die Seele im Jenseits erleben
Teil 2 - Was der Mensch in der Sterbephase und nach dem Tod die Seele im Jenseits erleben
Bedeutung des Ich Bin aus himmlischer Sicht
Teil 3 - Was der Mensch in der Sterbephase und nach dem Tod die Seele im Jenseits erleben
Teil 4 - Was der Mensch in der Sterbephase und nach dem Tod die Seele im Jenseits erleben
Teil 5 - Was der Mensch in der Sterbephase und nach dem Tod die Seele im Jenseits erleben
Teil 1 - Unterschied zwischen dem unpersoenlichen Leben der himmlischen Wesen und dem personenbezogenen Leben der Menschen
Teil 2 - Unterschied zwischen dem unpersoenlichen Leben der himmlischen Wesen und dem personenbezogenen Leben der Menschen
Teil 3 - Unterschied zwischen dem unpersoenlichen Leben der himmlischen Wesen und dem personenbezogenen Leben der Menschen
falsche Anschauung und Anwendung des positiven Denkens mit verhaengnisvollen Folgen
Teil 1 - Suechte - ihre Ursachen und deren Bewaeltigung aus der himmlischen Sicht - und weitere Themen
Teil 2 - Suechte - ihre Ursachen und deren Bewaeltigung aus der himmlischen Sicht - und weitere Themen
Erklaerung des universellen Liebegeistes bezueglich Leserfragen
Ist Jesus am Kreuz gestorben
Sind Kuender verpflichtet jederzeit goettliche Botschaften aufzunehmen
Teil 1 - Entstehung ritueller Kulthandlungen - davon ausgehende Gefahren fuer Menschen und Seelen aus himmlischer Sicht
Teil 2 - Entstehung ritueller Kulthandlungen - davon ausgehende Gefahren fuer Menschen und Seelen aus himmlischer Sicht
Was sind die Ich Bin-Liebetroepfchen Gottes - Quelle der goettlichen Botschaften
Ansprache der Augenzellen bei leichten und schweren Erkrankungen
Warum ungeduldige und rastlose gottverbundene Menschen scheitern eine innere Beziehung zu ihrer himmlischen Seele aufzubauen
Schmeichelei - eine unschoene taeuschende Wesensart
Musik himmlischer Wesen im Vergleich zu den weltlichen Kompositionen
Verwendung des Begriffs Gesetz beziehungsweise Gesetzmaessigkeit in den Botschaften
Teil 1 - feinstoffliche Mineralteilchen - erste Lebenselemente zur Erschaffung des Universums
Teil 2 - feinstoffliche Mineralteilchen - erste Lebenselemente zur Erschaffung des Universums
Warum veraendern sich von Zeit zu Zeit die goettlichen Liebetroepfchen-Botschaften in ihrer Aussage und Ausdrucksweise
Ansporn zu einem Leben in himmlischer demuetiger Weise
Erschaffung des genialsten herzlichsten unpersoenlichen Wesens - Gott - durch himmlische Lichtwesen
Botschaften nur sinngemaess verstehen
Sinn und Zweck goettlicher Botschaften durch Kuender
Heiltherapie mit Schlangengiftserum aus der himmlischen Sicht
geistige Unwissenheit der Menschheit mit tragischen Folgen
geistige Implantate - ihre Bedeutung und heimtueckischen Funktionen
Christus spricht ueber seine Geburt und ueber die gerechte Gleichheit aller kosmischen Wesen
Fussball - ein kaempferisches Spiel - beleuchtet aus der himmlischen Sicht
Teil 1 - Die gelebte Demut - geistiger Schluessel fuer die Heimkehr und ein glueckseliges Leben in gerechter Wesensgleichheit
Teil 2 - Die gelebte Demut - geistiger Schluessel fuer die Heimkehr und ein glueckseliges Leben in gerechter Wesensgleichheit
liebevolles Ernten und Zubereiten der Naturgaben
Teil 1 - Warum ausserirdische Wesen mit ihren Raumschiffen kosmisch bedeutsame Aktionen in unserem Sonnensystem durchfuehren
Teil 2 - Warum ausserirdische Wesen mit ihren Raumschiffen kosmisch bedeutsame Aktionen in unserem Sonnensystem durchfuehren
suesse Speisen und ihre unerwuenschten Nebenwirkungen
lautes energisches Sprechen - ein herrschsuechtiges Verhalten das keine Widerrede duldet
Teil 1 - grosses Aufsehen um angebliche Stellvertreter Gottes in dieser Welt - eine Welt der Taeuschungen
Teil 2 - grosses Aufsehen um angebliche Stellvertreter Gottes in dieser Welt - eine Welt der Taeuschungen
ernste Weisungen eines Ausserirdischen
starker Lebensenergieentzug durch energiearme Menschen und Seelen
Jesu Christi Leben auf der Wanderschaft und Flucht
fanatischer Freiheits- und Gerechtigkeitsdrang mit folgenschweren Auswirkungen
Teil 1 - kosmische Weisheiten und Hinweise zur geistigen Neuorientierung
Teil 2 - kosmische Weisheiten und Hinweise zur geistigen Neuorientierung
Teil 1 - inneres Selbst - Energie- und Datenspeicher unseres inneren Lichtkoerpers Seele - Massnahmen zur Schoepfungserrettung
Teil 2 - inneres Selbst - Energie- und Datenspeicher unseres inneren Lichtkoerpers Seele - Massnahmen zur Schoepfungserrettung
Heilungsmoeglichkeit durch Aktivierung entarteter Zellen
falsches Gottesbild durch fehlgeleitete Kuender
Warum auch boesartige Erkrankungen heilbar sind
Warum es auf Erden kein paradiesisches Leben geben wird
Hochmut und Stolz versperren die himmlische Rueckkehr
kurze Einweisung fuer die neuen Liebetroepfchen-Leser
Herzensgebet einer geistig-hoeheren Art
falsches Menschenbild mit unerwarteten tragischen Folgen
Teil 1 - Warum die einverleibte Seele schon den Charakter des Kindes praegt
Teil 2 - Warum die einverleibte Seele schon den Charakter des Kindes praegt
Das Ende ausserhimmlischen Lebens beginnt jetzt
Herzensruf des universellen Liebegeistes an alle ausserhimmlischen Wesen
Perfektionismus aus der Sicht des Gottesgeistes und einer Philosophin
optimistische Tagesvorschau mit erfreulicher Auswirkung
energetische Verbundenheit der inkarnierten Heilsplanwesen mit ihren himmlischen Planetengeschwistern
Friedensreichvision - der grosse Irrtum gottglaeubiger Menschen und jenseitiger Seelen
Teil 1 - unbekanntes Wissen ueber das irdische und himmlische Leben Jesu Christi
schwieriges Zusammenleben mit dominanten rechthaberischen und streitbaren Menschen
ungeahnte Gefahren durch aufdringliche Seelen fuer neugierige Menschen mit uebersinnlichen Faehigkeiten
Teil 2 - unbekanntes Wissen ueber das irdische und himmlische Leben Jesu Christi
Woher beziehen heute manche mediale Menschen Durchgaben ueber nahestehende besorgniserregende Weltereignisse
herzliche Wuensche der himmlischen Wesen an alle himmlischen Heimkehrer fuer die kommende Erdenzeit
Botschaft eines ausserirdischen Wesens an die Menschheit
Dualleben himmlischer Wesen in gleichem Bewusstseinsstand und stetigem Herzensgleichklang
Teil 1 - Bedeutung der Urteilchen - Positiv und Negativ - fuer die Entstehung der Schoepfung und das himmlische Dualleben
Teil 2 - Bedeutung der Urteilchen - Positiv und Negativ - fuer die Entstehung der Schoepfung und das himmlische Dualleben
Gruende fuer den massiven Fluechtlingsstrom - eine Tragoedie aus himmlisch-goettlicher Sicht
Teil 3 - unbekanntes Wissen ueber das irdische und himmlische Leben Jesu Christi
Warum ein fester Gottesglaube und Herzensgebete nicht genuegen den Menschen und seine Seele dem Himmelreich naeher zu bringen
Warum es so wichtig ist aus dem Herzen zu bitten und zu danken und den Dank anderer entgegenzunehmen
Erkenntnisse eines reinen Lichtwesens aus seinen irdischen Leben - zur Selbsterforschung fuer die himmlischen Rueckkehrer
Teil 1 - Liebeskummer der Herzensmenschen
Teil 2 - Liebeskummer der Herzensmenschen
unerschoepfliche und unbegrenzte selbstlose sich verschenkende goettliche Liebe - wie sie besser verstanden werden kann
gleichgueltiges Leben und geistige Interesselosigkeit ueberwinden
Polsprung - Ursachen und Vorwarnung des Gottesgeistes
Aufruf des Liebegeistes - keine Lebensenergien vergeuden
Weckruf des Gottesgeistes an alle inkarnierten Wesen
Teil 1 - Zukunftsdeutungen der Wahrsager ein risikoreiches und gefahrvolles Unternehmen
Teil 2 - Zukunftsdeutungen der Wahrsager ein risikoreiches und gefahrvolles Unternehmen
Aktivitaeten der kosmischen Rettungsallianz in materiellen Sonnensystemen
Teil 1 - Wie es auf der Erde zur Besiedelung mit Pflanzen und Lebewesen kam
Teil 2 - Wie es auf der Erde zur Besiedelung mit Pflanzen und Lebewesen kam
Bedeutung der Selbstlosigkeit aus himmlischer Sicht
Programmierung der materiellen Zellkerne zur Aufnahme zweipoliger goettlicher Energien
misslungenes Abenteuer der tiefstgefallenen Wesen mit nicht zu ueberbietendem Leid naehert sich seinem Ende
Welche Moeglichkeiten der Gottesgeist innehat kosmische und planetarische Gefahrensituationen vorauszuschauen und abzuwenden
Teil 1 - Mitteilung eines Rosenstrauches
Teil 2 - Mitteilung eines Rosenstrauches
Teil 3 - Mitteilung eines Rosenstrauches
Herzenswuensche fuer das Jahr 2020
himmlische Demut - was abtruennige tiefgefallene Wesen aus ihr machten
Antwort des Liebegeistes zu Botschaftsuebersetzungen und himmlischen Lebensregeln
Einweisung des Liebegeistes in himmlische Pastellfarben und Erstellung von Liebetroepfchen-Webseiten
Angst vor Verfolgung ueberwinden
Ansprache der Koerperzellen bei grippalen Infekten und zum Schutz vor Viren-Eindringlingen
Empfehlungen des Liebegeistes fuer irdische Katastrophen- und Notfallzeiten
grosse Freude im himmlischen Sein ueber ein bedeutendes irdisches Ereignis
Einblick in das unvollkommene Leben der Kuender und sogenannter Gottespropheten
Wirkungen elektromagnetischer Frequenzen von Heilgeraeten auf Koerperzellen und Seele aus himmlisch-goettlicher Sicht
Teil 1 - Warum geistiger und technischer Fortschritt nicht abgelehnt werden sollte
Teil 2 - Warum geistiger und technischer Fortschritt nicht abgelehnt werden sollte
Wirken unserer Ur-Schoepfungsmutter im Fallgeschehen und ihre vorzeitige himmlische Rueckkehr
Einweisung in die Themensammlung
Wofuer Themensammlung
Themensammlung 1
Themensammlung 2
Themensammlung 3
Warum sich manche Menschen an einigen Botschaftsworten stoeren
Teil 4 - unbekanntes Wissen ueber das irdische und himmlische Leben Jesu Christi
Teil 5 - unbekanntes Wissen ueber das irdische und himmlische Leben Jesu Christi
Teil 6 - unbekanntes Wissen ueber das irdische und himmlische Leben Jesu Christi
Teil 7 - unbekanntes Wissen ueber das irdische und himmlische Leben Jesu Christi
Teil 8 - unbekanntes Wissen ueber das irdische und himmlische Leben Jesu Christi
Begegnung mit einem Ausserirdischen
Themensammlung 4
Christus schildert seine schoene himmlische Rueckkehr
Teil 1 - innere Wandlung zu einem persoenlich unauffaelligen und herzlichen Wesen himmlischen Ursprungs
Teil 2 - innere Wandlung zu einem persoenlich unauffaelligen und herzlichen Wesen himmlischen Ursprungs
Wie es zu den ersten Erschaffungen und Zeugungen der Wesen durch die Ureltern in der himmlischen Vorschoepfung kam
Teil 3 - innere Wandlung zu einem persoenlich unauffaelligen und herzlichen Wesen himmlischen Ursprungs