Smulkūs gyvūnai - mūsų kenkėjai
Dievo pagalbos ir pamokymo šauksmai
Vanduo - patirtis upės vagoje
kenksmingi žemės spinduliai dėl požeminių vandens perėjų.
neapsakomos gyvūnų pasaulio kančios
kūno judesiai šokant
perdėtai sveikuoliškas gyvenimas
kalbėjimas su kūno ląstelėmis - esant negalavimui
mano ir tavo - privalomas krintančių būtybių gyvenimas
perdėtas elgesys valgant.
krioklys - dangaus būtybių požiūriu
priklausomybė nuo tabako
pasiruošimas kūrinijos išgelbėjimui - kankinantys dangaus būtybių įsikūnijimai
Naikinantis numarinimo ir prievartos iš religinių paskatų poveikis
Kalbėjimas su somatinėmis ląstelėmis - sunkios ligos atveju
kalbėjimas su kūno ląstelėmis - prieš operaciją
vidinis kelias į Dievą - klaidingos idėjos
nematomi ir nesuprantami kosminiai įvykiai.
odos alergijos išsivystymas
pasauliečių sielos pomirtiniame gyvenime - jų įtaka žmogaus gyvenimui.
Savo minčių pažinimas ir kontrolė
dieviškas švelnumas
Savęs gailėjimasis ir depresinė nuotaika
tragiškas cunamio paveiktų žmonių likimas
savo apsimetinėjimą - atpažinti, analizuoti ir ištaisyti.
prielaidos - naudingi nemalonūs jausmai, padedantys įžvelgti dviprasmiškus žmones.
gyventi šiame apgaulės pasaulyje
gyvybinės energijos padidėjimas
Vergai ir darbo varomi
1 dalis - Maistas - kenksmingų medžiagų baimė
2 dalis - Maistas - teršalų baimė
Kosminių būtybių laisvė ir nepriklausomybė
dramatiška Žemės energetinė būklė - pražūtingos pasekmės visai gyvybei.
Santykis tarp subtiliųjų atomų ir kondensuotos medžiagos atomų
Suprasti nuolankumą - gyventi pagal dangiškųjų būtybių teisingumą
Daugiau energijos turintis maistas - ugnies elementas
pasaulietiški ir pamokantys gyvenimo principai - muzika be dangiško skambesio
iš žmogaus kūno į pomirtinį gyvenimą išeinanti siela.
paguodžiantys ir padedantys žodžiai - savęs pažinimas pagyvenusiems ir vienišiems žmonėms.
Dievo supratimas - dėl mūsų nusižengimų įstatymui ir jų atleidimo
Ten, kur yra jūsų sukaupti lobiai, jus vėl ir vėl traukia
Savęs pažinimas - kalbos garsas - kineziologija - neprofesionalų gydymas
Instrukcijos meilės lašo žinučių vertėjams į kitas kalbas
uždara širdis
išėjimas iš širdies vėsos - gyvenimas dangiškųjų dvilypių būtybių širdies šilumoje
aukso vidurys mitybos elgsenoje
Apokalipsė ir ankstyva pasaulio pabaiga
Gamtos naikinimas energijos neturinčiais mikro-gyvūnais
Sąžiningumas atskleidžia melą
Įstatymo pažeidimai - įvykdymas žmogaus kūne - per sielą
maistas ir prieskoniai - nesuderinamumas - ištikimybė - laiminga partnerystė
susirūpinusios gamtos būtybės - sunki gyvenimo situacija - sodai
apsimestinė ištikimybė santuokoje ir partnerystėje - šaltos širdies veidmainių dvigubas gyvenimas
nuoširdus - kilnus - dangiškas
didelis gyvybinės energijos praradimas dėl savo nuomonę turinčių žmonių
suvokti žmogaus gyvenimą.
sąžiningumas - ištikimybė - garbės jausmas - darni ir laiminga partnerystė
kalbėjimas su kūno ląstelėmis - nuo sąnarių skausmo
1 dalis - Religijų ir fanatiškų įsitikinimų atsiradimas - žmonių ir sielų kančios
2 dalis - Religijų ir fanatiškų įsitikinimų atsiradimas - žmonių ir sielų kančios
Dievą mylintys žmonės ir rimta kalba
Kelias į dvasingumą ir dangiškąją laisvę
Atsakomybė už savo gyvenimą - ugnies stichija - kremavimas
1 dalis - tariamai nepagydomos ligos - išgijimo galimybės - skirtingi pasauliai
2 dalis - tariamai nepagydomos ligos - išgijimo galimybės - skirtingi pasauliai
3 dalis - tariamai nepagydomos ligos - galimybės išgydyti - skirtingi pasauliai
kalbėti su kūno ląstelėmis - suaktyvinti jų funkcijas
1 dalis - Malonė - valdovų atleidimas nuo bausmės - supainiota su Dievo geranoriškumu
2 dalis - Gailestingumas - valdovų atleidimas nuo bausmės - painiojamas su Dievo geranoriškumu
Mūsų geri darbai - kaip mes juos vertiname - kokie jie yra iš tikrųjų
Kodėl dangiškosios dvilypės būtybės kartais atsiskiria ir sudaro naujas sąjungas
Patarimai sunkios ligos atveju - nežinomas organizmo ląstelių elgesys
Audros - potvyniai - karštis - sausra - artėjanti žemiškojo gyvenimo pabaiga
1 dalis - nepakeliami Dievo skelbėjų gyvenimo etapai
2 dalis - nepakeliami Dievo skelbėjų gyvenimo etapai
Kietosios visatos kilmė - netobulo žmogaus sukūrimas
Meilė iš širdies - suprasti ir gyventi
Obuolys ir jo patirtis ant medžio
dangaus būtybės - vaizdų kalba - spalvų kalba - garsų kalba
Maldos rekomendacija dvasiškai brandiems žmonėms
Santuoka ir partnerystė - iš dangiškųjų gyvenimo dėsnių perspektyvos
važiavimas į darbą ir atgal - iš pažiūros naudingas - netikėti pavojai
Krikščioniškos Kalėdos ir kryžiaus simbolis
1 dalis - vegetariškas gyvenimo būdas iš dangiškojo gyvenimo perspektyvos
2 dalis - vegetariškas gyvenimo būdas iš dangiškojo gyvenimo perspektyvos
3 dalis - vegetariškas gyvenimo būdas iš dangiškojo gyvenimo perspektyvos
4 dalis - vegetariškas gyvenimo būdas dangiškojo gyvenimo požiūriu
1 dalis - gyventi harmoningai, taikiai ir subalansuotai - vertingas tikslas
2 dalis - harmoningas, taikus ir subalansuotas gyvenimas - vertingas tikslas
Žmogiškasis perfekcionizmas dangaus požiūriu
teisingas elgesys su mokėjimo priemonėmis, dovanomis ir elementais.
1 dalis - Branduolio dalijimasis ir atominės elektrinės dangaus požiūriu
2 dalis - Branduolio dalijimasis ir atominės elektrinės dangaus požiūriu
Politiko rinkimai Dievo dvasios požiūriu
Gilus savo klaidų ir silpnybių pripažinimas
1 dalis - Evoliucinis dangaus būtybių gyvenimas ir jų širdies ryšys su Dievu
2 dalis - Evoliucinis dangiškųjų būtybių gyvenimas ir jų širdies ryšys su Dievu
3 dalis - Evoliucinis dangaus būtybių gyvenimas ir jų širdies ryšys su Dievu
1 dalis - labiau vertinti asmenį - nenaudinga
2 dalis - labiau vertinti asmenį - nenaudinga
Džiaugsmas per garsiai besireiškiančius žmones išorėje ar džiaugsmas iš sielos vidaus
1 dalis - Tikinčių žmonių ir nežemiškų sielų odisėja per religinius mokymus
2 dalis - Tikinčių žmonių ir nežemiškų sielų odisėja per religinius mokymus
3 dalis - Tikinčiųjų ir nežemiškų sielų odisėja per religinius mokymus
Pražūtinga finansinė ir ekonominė padėtis - kaip elgtis šiuo metu
1 dalis - Ligos ir liūdni įvykiai - ar jų priežastys glūdi praeityje?
2 dalis - Ligos ir liūdni įvykiai - ar jų priežastys glūdi praeityje?
Instrukcijos - kelias atgal - jautrus palaimingas gyvenimas - mūsų šviesos namai
1 dalis - Žinios apie dangiškąsias ir nežemiškas būtybes - liaupsinimas
2 dalis - Žinios apie dangiškąsias ir nežemiškas būtybes - liaupsinimas
3 dalis - Žinios apie dangiškąsias ir nežemiškas būtybes - paglostymas
Dievo atsakymas skaitytojams, kuriems rūpėjo meilės lašai
1 dalis - Patirtis - žmogus mirštant - po mirties siela pomirtiniame gyvenime
2 dalis - Patirtys - žmogus mirštant - po mirties siela pomirtiniame gyvenime
Aš esu dangaus požiūriu
3 dalis - Patirtys - žmogus mirštant - po mirties siela pomirtiniame gyvenime
4 dalis - Patirtys - žmogus mirštant - po mirties siela pomirtiniame gyvenime
5 dalis - Patirtys - žmogus mirštant - po mirties siela pomirtiniame gyvenime
1 dalis - beasmenis dangaus būtybių gyvenimas - asmeninis žmonių gyvenimas
2 dalis - beasmenis dangaus būtybių gyvenimas - asmeninis žmonių gyvenimas
3 dalis - beasmenis dangaus būtybių gyvenimas - asmeninis žmonių gyvenimas
Pozityvus mąstymas - neteisingas supratimas ir taikymas - pražūtingos pasekmės
1 dalis - Priklausomybės - priežastys ir įveikimas - iš dangaus perspektyvos
2 dalis - Priklausomybės - priežastys ir įveikimo būdai - iš Dangaus perspektyvos
Meilės dvasios paaiškinimas dėl skaitytojų klausimų
Ar Jėzus mirė ant kryžiaus?
Ar pranašautojai privalo bet kada gauti dieviškąsias žinutes?
1 dalis - ritualinės apeigos - pavojus žmonėms ir sieloms - iš dangaus perspektyvos
2 dalis - ritualinės apeigos - pavojus žmonėms ir sieloms - iš dangaus perspektyvos
Kokie yra Aš Esu Dievybės meilės lašai? - Dieviškųjų žinučių šaltinis
Pokalbis su akių ląstelėmis - lengvose ir sunkiose ligose
Kodėl nekantrūs ir neramūs žmonės nesugeba užmegzti ryšio su savo siela
Pagyrūniškumas - nepatraukli apgaulinga savybė
Dangiškųjų būtybių muzika - žemiškos kompozicijos
Teisės ir taisyklingumo sąvokos pranešimuose
1 dalis - subtilios mineralinės dalelės - visatos kūrimo elementai
2 dalis - Subtiliosios mineralinės dalelės - Visatos sukūrimo elementai
Kodėl laikui bėgant keičiasi žinučių išraiška.
Dangiškas ir nuolankus gyvenimas
Išradingiausią, širdingą, beasmenį Dievą sukūrė dangaus šviesos būtybės.
Suprasti pranešimus tik pagal jų prasmę
Dieviškųjų pranešimų prasmė ir tikslas
Gydomoji terapija gyvatės nuodų serumu - iš dangaus taško
Dvasinis žmonijos neišprusimas su tragiškomis pasekmėmis
Dvasiniai implantai - jų reikšmė ir klastingos funkcijos
Kristaus gimimas - teisinga visų kosminių būtybių lygybė
Futbolas - kovingas žaidimas - nušviestas iš dangaus perspektyvos
1 dalis - Nuolankumas - raktas į dangų ir palaimingą gyvenimą
2 dalis - Nuolankumas - raktas į dangų ir palaimingą gyvenimą
Meilės kupinas gamtos dovanų rinkimas ir ruošimas
1 dalis - Nežemiškos būtybės atlieka svarbius veiksmus Saulės sistemoje
2 dalis - Nežemiškos būtybės atlieka svarbius veiksmus Saulės sistemoje
Saldūs maisto produktai ir jų nepageidaujamas šalutinis poveikis
kalbėti garsiai ir energingai - dominuojantis elgesys, kuris netoleruoja kalbų iš nugaros.
1 dalis - didelis triukšmas dėl tariamų Dievo atstovų
2 dalis - didelis triukšmas dėl tariamų Dievo atstovų
rimti nežemiškos kilmės nurodymai
Mažai energijos turintys žmonės ir sielos išeikvoja daug gyvybinės energijos
Jėzus Kristus apie klajones ir skrydį
Fanatiškas laisvės ir teisingumo troškimas su rimtomis pasekmėmis
1 dalis - kosminė išmintis ir patarimai, kaip dvasiškai persiorientuoti
2 dalis - kosminė išmintis ir patarimai, kaip dvasiškai persiorientuoti
1 dalis - vidinė savastis - energijos ir duomenų saugojimas - taupymo priemonės
2 dalis - vidinis "aš" - energijos ir duomenų saugojimas - taupymo priemonės
Galimybė išgydyti suaktyvinant degeneravusias ląsteles
Klaidingas Dievo įvaizdis per klaidingus skelbėjus
Kodėl piktybinės ligos taip pat išgydomos
Kodėl žemėje nebus rojaus gyvenimo
Puikybė ir arogancija užkerta kelią atgal į dangų
Trumpa įžanga naujiems "Meilės lašelių" skaitytojams
Širdies malda - dvasiškai aukštesnės rūšies malda
neteisingas žmogaus įvaizdis - netikėtos tragiškos pasekmės
1 dalis - Kodėl įsikūnijusi siela jau formuoja vaiko charakterį
2 dalis - Kodėl įsikūnijusi siela jau formuoja vaiko charakterį
Gyvenimo už dangaus ribų pabaiga prasideda dabar
Dievo širdies kvietimas - visoms būtybėms už dangaus ribų
Perfekcionizmas Dievo ir filosofo požiūriu
Optimistinė dienos prognozė su maloniu poveikiu
Išganymo plane įsikūnijusių būtybių ryšys su jų dangiškaisiais planetiniais broliais ir seserimis
Taikos karalystės vizija - Dievą tikinčių žmonių klaida ir sielos pomirtiniame gyvenime
1 dalis - Žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
sudėtingas sugyvenimas su dominuojančiais, savo nuomonę turinčiais, ginčijamais žmonėmis.
įkyrios sielos - pavojus smalsiems žmonėms, turintiems aiškiaregystės gebėjimų.
2 dalis - Žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
Iš kur ateina pranešimai apie nerimą keliančius pasaulio įvykius
Nuoširdžiausi linkėjimai artėjančio laiko proga visiems, kurie grįžta namo į dangų
Nežemiškos kilmės žmogaus žinutė žmonijai
Dangiškasis dvigubas gyvenimas - tas pats sąmonės lygis - vienybė širdyse
1 dalis - Teigiamos ir neigiamos dalelės - Kūrimo kilmė - Dvigubas gyvenimas
2 dalis - Teigiamos ir neigiamos dalelės - Kūrinijos atsiradimas - Dvigubas gyvenimas
Masinis pabėgėlių srautas - tragedija iš dangiškojo taško
3 dalis - Jėzaus Kristaus žemiškojo ir dangiškojo gyvenimo pažinimas
Širdies maldų ir tvirto tikėjimo Dievu nepakanka, kad patektum į dangų
Iš širdies - prašyti, dėkoti, priimti kitų padėką
Šviesos būtybės įžvalgų naudojimas savęs pažinimui
1 dalis - Širdies meilė
2 dalis - Širdies skausmas
neišsemiama neribota nesavanaudiška dieviškoji meilė
Abejingo gyvenimo ir dvasinio nesidomėjimo įveikimas
Magnetinių polių pasikeitimas - priežastys - Dievo dvasios perspėjimas
Dievo kvietimas - nešvaistykite gyvenimo energijos
Dievo skambutis visoms įsikūnijusioms būtybėms
1 dalis - Ateities aiškinimai - rizikingi ir pavojingi
2 dalis - Aiškiaregių ateities aiškinimai - rizikinga ir pavojinga
Kosminio gelbėjimo aljanso veikla materialiose Saulės sistemose
1 dalis - Kaip Žemė buvo apgyvendinta augalais ir gyvomis būtybėmis
2 dalis - Kaip Žemėje prasidėjo kolonizacija augalais ir gyvomis būtybėmis
Nesavanaudiškumo reikšmė dangiškuoju požiūriu
Medžiagų ląstelių branduolių programavimas ir dvipolių energijų sugėrimas
giliai puolusios būtybės - jų nuotykis kupinas kančių - nepavyko - netrukus baigsis
Dievo galimybės numatyti ir išvengti kosminių pavojų
1 dalis - Žinutė iš rožių krūmo
2 dalis - Žinutė iš rožių krūmo
3 dalis - Žinutė iš rožių krūmo
Širdies linkėjimai 2020 metams
Dangaus nuolankumas - ką iš jo padarė atkritusios puolusios būtybės
žinutės vertimai - dangiškos gyvenimo taisyklės
dangiškos pastelės - meilės lašų tinklalapių kūrimas
Persekiojimo baimės įveikimas
kalbėjimas su organizmo ląstelėmis - gripo infekcijoms ir apsaugai nuo virusų įsibrovėlių
Dievo rekomendacijos dėl žemiškų nelaimių ir ekstremalių situacijų
didelis džiaugsmas danguje dėl reikšmingo žemiško įvykio.
netobulas pranašų ir vadinamųjų Dievo pranašų gyvenimas
Gydymo prietaisai - elektromagnetiniai dažniai - poveikis kūnui ir sielai
1 dalis - Kodėl nereikėtų atmesti dvasinės ir techninės pažangos
2 dalis - Kodėl nereikėtų atmesti dvasinės ir techninės pažangos
Pradinė Motina rudenį - jos ankstyvas sugrįžimas į dangų
Įvadas į temų rinkinį
Kam skirtos temų kolekcijos
Temų rinkinys 1
2 temų rinkinys
3 temų rinkinys
Kodėl kai kuriuos žmones trikdo kai kurie žinutėse esantys žodžiai
4 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
5 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
6 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
7 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
8 dalis - nežinomos žinios apie žemiškąjį ir dangiškąjį Jėzaus Kristaus gyvenimą
Susitikimas su nežemiška būtybe
Temų rinkinys 4
Kristus aprašo savo gražų sugrįžimą į dangų
1 dalis - vidinė transformacija į širdingą dangiškos kilmės būtybę
2 dalis - vidinė transformacija į širdingą dangiškos kilmės būtybę
pirmykščiai tėvai prieš sukūrimą - jų pirmieji kūriniai ir palikuonys.
3 dalis - vidinė transformacija į širdingą dangiškos kilmės būtybę