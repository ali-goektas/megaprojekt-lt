#
# Diese Datei MUSS im Format   UTF-8 ohne BOM   abgespeichert werden.
# Die Zeilenenden in dieser Datei MUESSEN im (Windows)-Format   CR+LF   abgespeichert werden.
#
# Die Reihenfolge der Sprachen in dieser Datei (siehe 'Kaestchen' weiter unten: "Sprachbezeichnung auf
# Deutsch - Sprachregionskuerzel") muss IDENTISCH sein mit der Reihenfolge in der "Liste aller Sprachen".
# (siehe "uebersetzungsdaten.txt"-Datei im "daten"-Ordner)
#

#
# Egal, in welcher Sprache:
# Reihenfolge der Werte:
#
# 1. Zeile: 'Titel' der Seite, welcher im Tab (Reiter) des Browsers erscheint
#
# Danach 3 Zeilen Keywörter (je Keywort eine Zeile). Für SEO - Suchmaschinenoptimierung:
# 2. Zeile: Erstes Keywort einer 'Seite'. 
# 3. Zeile: Zweites Keywort einer 'Seite'.
# 4. Zeile: Drittes Keywort einer 'Seite'.
#
# Darunter dann die Überschrift der Seite und die Textinhalte
# Alle Sprachen müssen GLEICHVIEL Zeilen haben. (Leerzeilen zählen nicht.)
#


#########################################################################################################
#
# BULGARISCH - BG
#
#########################################################################################################



Капчици Божия любов

Капчици любов
Съвети за живота
Бог

Капчици Божия любов

Свободният универсален безличен дух на любовта - БОГ - дава на сърдечните хора в днешно време, които търсят божествената истина, чрез един вестител - който живее скромно и далеч от обществото и не принадлежи към никоя религия или духовна или светска група по интереси - ново знание за разширяване на съзнанието им, което навлиза в различни земни и небесни сфери на живота и досега е било отчасти непознато на човечеството.

Препоръка за особено важно послание:

"Бог, безличното, най-гениално и сърдечно същество, е създаден от небесни същества от светлина."

кратък брифинг



#########################################################################################################
#
# TSCHECHISCH - CS
#
#########################################################################################################



Kapky Boží lásky

Kapky lásky
Životní rady
Bůh

Kapky Boží lásky

Svobodný univerzální neosobní duch lásky - BŮH - dává v současné době srdnatým lidem, kteří hledají božskou pravdu, prostřednictvím hlasatele - který žije pokorně a daleko od veřejnosti a nepatří k žádnému náboženství ani k žádné duchovní či světské zájmové skupině - nové poznatky pro rozšíření jejich vědomí, které zasahují do různých pozemských i nebeských oblastí života a které byly lidstvu dosud částečně neznámé.

Doporučení zvláště důležitého sdělení:

"Bůh, nejgeniálnější, nejsrdečnější, neosobní bytost, byl stvořen nebeskými bytostmi světla."

krátký úvod



#########################################################################################################
#
# DAENISCH - DA
#
#########################################################################################################



Dråber af Guds kærlighed

Dråber af kærlighed
Livsrådgivning
Gud

Dråber af Guds kærlighed

Den frie, universelle, upersonlige kærlighedsånd - GUD - giver de hjertelige mennesker, der søger efter den guddommelige sandhed i nutiden, via en herold - som lever ydmygt og langt fra offentligheden og ikke tilhører nogen religion eller nogen åndelig eller verdslig interessegruppe - ny viden til deres bevidsthedsudvidelse, som går ind i forskellige jordiske og himmelske livssfærer og som indtil nu var delvis ukendt for menneskeheden.

Anbefaling af et særligt vigtigt budskab:

"Gud, det upersonlige, mest geniale og hjertelige væsen, blev skabt af himmelske lysvæsener."

kort briefing



#########################################################################################################
#
# DEUTSCH - DE
#
#########################################################################################################



Liebetröpfchen Gottes

Liebetröpfchen
Lebensrat
Gott

Liebetröpfchen Gottes

Der freie universelle, unpersönliche Liebegeist - GOTT - schenkt den herzlichen, nach der göttlichen Wahrheit suchenden Menschen in der Gegenwart über einen Künder - der demütig und fern der Öffentlichkeit lebt und keiner Religion und auch keiner spirituellen oder weltlichen Interessengruppe angehört - neues Wissen zu ihrer Bewusstseinserweiterung, das in verschiedene irdische und himmlische Lebensbereiche geht und bisher der Menschheit zum Teil unbekannt war.

Empfehlung einer besonders wichtigen Botschaft:

"Erschaffung des genialsten und herzlichsten unpersönlichen Wesens - Gott - durch himmlische Lichtwesen"

kurze Einführung



#########################################################################################################
#
# GRIECHISCH - EL
#
#########################################################################################################



Σταγονίδια της αγάπης του Θεού

Σταγονίδια της αγάπης
Συμβουλές ζωής
Θεός

Σταγονίδια της αγάπης του Θεού

Το ελεύθερο συμπαντικό απρόσωπο πνεύμα της αγάπης - ο ΘΕΟΣ - δίνει στους εγκάρδιους ανθρώπους που αναζητούν τη θεία αλήθεια στην παρούσα εποχή μέσω ενός κήρυκα - ο οποίος ζει ταπεινά και μακριά από το κοινό και δεν ανήκει σε καμία θρησκεία ούτε σε καμία ομάδα πνευματικών ή κοσμικών συμφερόντων - νέες γνώσεις για τη διεύρυνση της συνείδησής τους, οι οποίες εισχωρούν σε διάφορες γήινες και ουράνιες σφαίρες της ζωής και ήταν εν μέρει άγνωστες στην ανθρωπότητα μέχρι τώρα.

Σύσταση ενός ιδιαίτερα σημαντικού μηνύματος:

"Ο Θεός, το απρόσωπο, το πιο ιδιοφυές και εγκάρδιο ον, δημιουργήθηκε από ουράνια όντα του φωτός."

σύντομη εισαγωγή



#########################################################################################################
#
# ENGLISCH-GB - EN-GB
#
#########################################################################################################



Droplets of God's love

Droplets of love
Life advice
God

Droplets of God's Love

The free universal impersonal spirit of love - GOD - gives to the hearty people searching for the divine truth in the present time via a herald - who lives humbly and far from the public and does not belong to any religion nor to any spiritual or worldly interest group - new knowledge for their expansion of consciousness, which goes into different earthly and heavenly spheres of life and was partly unknown to mankind until now.

Recommendation of a particularly important message:

"God, the impersonal, most ingenious and most cordial being, was created by heavenly beings of light."

short introduction



#########################################################################################################
#
# ENGLISCH-US - EN-US
#
#########################################################################################################



Droplets of God's love

Droplets of love
Life advice
God

Droplets of God's love

The free universal, impersonal love spirit - GOD - gives to the hearty, after the divine truth searching people in the presence over a herald - who lives humbly and far from the public and belongs to no religion and also no spiritual or worldly interest group - new knowledge to their consciousness expansion, which goes into different earthly and heavenly areas of life and was partly unknown to the mankind up to now.

Recommendation of a particularly important message:

"God, the impersonal, most ingenious and most cordial being, was created by heavenly beings of light."

short introduction



#########################################################################################################
#
# SPANISCH - ES
#
#########################################################################################################



Gotas del amor de Dios

Gotas de amor
Consejos para la vida
Dios

Gotas del amor de Dios

El libre espíritu universal impersonal de amor - DIOS - da a las personas de corazón que buscan la verdad divina en el tiempo presente a través de un heraldo - que vive humildemente y lejos del público y no pertenece a ninguna religión ni a ningún grupo de interés espiritual o mundano - un nuevo conocimiento para su expansión de conciencia, que va a diferentes esferas terrestres y celestiales de la vida y que era parcialmente desconocido para la humanidad hasta ahora.

Recomendación de un mensaje especialmente importante:

"Dios, el ser impersonal, más ingenioso y más cordial, fue creado por seres celestiales de luz."

breve introducción



#########################################################################################################
#
# ESTNISCH - ET
#
#########################################################################################################



Jumala armastuse tilgad

Armastuse tilgad
Elu nõuanded
Jumal

Jumala armastuse tilgad

Vaba universaalne impersonaalne armastuse vaim - JUMAL - annab praegusel ajal jumalikku tõde otsivatele südamlikele inimestele kuulutaja kaudu - kes elab alandlikult ja avalikkusest eemal ega kuulu ühegi religiooni ega ühegi vaimse või maise huvigrupi juurde - uusi teadmisi nende teadvuse laiendamiseks, mis ulatuvad erinevatesse maistele ja taevastele eluvaldkondadele ja olid inimkonnale seni osaliselt tundmatud.

Eriti olulise sõnumi soovitus:

"Jumal, isikupäratu, kõige geniaalsem ja südamlikum olend, loodi taevaste valgusolendite poolt."

Sissejuhatus



#########################################################################################################
#
# FINNISCH - FI
#
#########################################################################################################



Jumalan rakkauden pisarat

Rakkauden pisarat
Neuvoja elämää varten
Jumala

Jumalan rakkauden pisarat

Vapaa, universaalinen ja persoonaton rakkauden henki - JUMALA - tarjoaa nykyhetkessä jumalallista totuutta etsiville sydämen ihmisille sanansaattajan välityksellä - joka elää nöyrästi ja kaukana julkisuudesta eikä kuulu mihinkään uskontoon tai hengelliseen tai maalliseen eturyhmään - heidän tietoisuutensa laajentamista varten uutta tietoa, joka ulottuu maallisen ja taivaallisen elämän eri alueille ja joka oli tähän asti ihmiskunnalle osittain tuntematon.

Suositus erityisen tärkeästä viestistä:

"Jumala, persoonaton, nerokkain ja ystävällisin olento, luotiin taivaallisten valo-olentojen toimesta."

Johdanto



#########################################################################################################
#
# FRANZOESISCH - FR
#
#########################################################################################################



Gouttelettes d'amour de Dieu

Gouttelettes d'amour
Conseil de vie
Dieu

Gouttelettes d'amour de Dieu

L'esprit d'amour libre, universel et impersonnel - DIEU - offre aux hommes de cœur en quête de la vérité divine dans le présent, par l'intermédiaire d'un messager - qui vit humblement et loin du public et n'appartient à aucune religion ni à aucun groupe d'intérêt spirituel ou mondain - de nouvelles connaissances pour l'élargissement de leur conscience, qui vont dans différents domaines de la vie terrestre et céleste et qui étaient jusqu'à présent en partie inconnues de l'humanité.

Recommandation d'un message particulièrement important :

"Dieu, l'être impersonnel, le plus ingénieux et le plus cordial, a été créé par des êtres célestes de lumière."

Introduction



#########################################################################################################
#
# UNGARISCH - HU
#
#########################################################################################################


Isten szeretetének cseppjei

A szerelem cseppjei
Életvezetési tanácsok
Isten

Isten szeretetének cseppjei

A szeretet szabad, egyetemes, személytelen szelleme - ISTEN - napjainkban az isteni igazság őszinte keresőinek egy szerényen és a társadalomtól távol élő, semmilyen valláshoz, szellemi vagy világi érdekcsoporthoz nem tartozó hírvivőn keresztül olyan új ismereteket ad át tudatuk bővítéséhez, amelyek átfogják az élet különböző, földi és égi szféráit, és amelyek eddig részben ismeretlenek voltak az emberiség számára.

Ajánlás egy különösen fontos üzenethez:

"Istent, a személytelen, legzseniálisabb és legszívélyesebb lényt a fény égi lényei teremtették."

rövid bevezetés



#########################################################################################################
#
# ITALIENISCH - IT
#
#########################################################################################################



Gocce d'amore di Dio

Gocce d'amore
Consigli per la vita
Dio

Gocce d'amore di Dio

Lo spirito d'amore libero, universale e impersonale - DIO - offre agli uomini di cuore alla ricerca della verità divina nel presente, attraverso un messaggero - che vive umilmente e lontano dal pubblico e non appartiene a nessuna religione o gruppo di interesse spirituale o mondano - una nuova conoscenza per l'ampliamento della loro coscienza, che va in diversi settori della vita terrena e celeste e che finora era in parte sconosciuta all'umanità.

Raccomandazione di un messaggio particolarmente importante:

"Dio, l'essere impersonale, più geniale e più cordiale, è stato creato da esseri celesti di luce."

Introduzione



#########################################################################################################
#
# JAPANISCH - JA
#
#########################################################################################################



神の愛のしずく

愛のしずく
人生相談
神様

神の愛のしずく

自由で普遍的な超越した愛の御霊である神は、今、宗教にも霊的・地上的な利益団体にも属さず、世間から離れて謙虚に生きる一人の説教師を通して、神の真理を真摯に求める人々に、意識の拡大のための新しい知識を与えている。この知識は、地上と天上の様々な生活圏に及んでおり、これまで人類には一部知られていませんでした。

特に重要なレポートの推薦。

"最も属人的で、最も心のこもった、人間離れした存在である神は、光の天人たちによって創造された。"

簡単な紹介



#########################################################################################################
#
# LITAUISCH - LT
#
#########################################################################################################



Dievo meilės lašeliai

Meilės lašeliai
Gyvenimo patarimai
Dievas

Dievo meilės lašeliai

Laisva visuotinė beasmenė meilės dvasia - DIEVAS - dabartiniais laikais nuoširdžiai ieškantiems dieviškosios tiesos žmonėms per pranašautoją, kuris gyvena kukliai ir toli nuo visuomenės, nepriklauso jokiai religijai, jokiai dvasinei ar pasaulietinei interesų grupei, suteikia naujų žinių jų sąmonei plėsti, kurios apima įvairias žemiškąsias ir dangiškąsias gyvenimo sritis ir iki šiol iš dalies buvo nežinomos žmonijai.

Ypač svarbaus pranešimo rekomendacija:

"Dievą, beasmenę, išradingiausią ir širdingiausią būtybę, sukūrė dangaus šviesos būtybės."

trumpas įvadas



#########################################################################################################
#
# LETTISCH - LV
#
#########################################################################################################



Dieva mīlestības pilieni

Mīlestības pilieni
Dzīves padomi
Dievs

Dieva mīlestības pilieni

Bezmaksas universālais bezpersoniskais mīlestības gars - DIEVS - mūsdienās ar vēstneša starpniecību, kurš dzīvo pieticīgi un tālu no sabiedrības, nepieder nevienai reliģijai, nevienai garīgai vai laicīgai interešu grupai, sniedz sirsnīgiem dievišķās patiesības meklētājiem jaunas zināšanas viņu apziņas paplašināšanai, kas aptver dažādas dzīves sfēras - gan zemes, gan debesu - un kuras līdz šim cilvēcei daļēji nav bijušas zināmas.

Īpaši svarīgas ziņas ieteikums:

"Dievu, bezpersonisko būtni, visģeniālāko un vismīlošāko, radīja debesu gaismas būtnes."

īss ievads



#########################################################################################################
#
# NIEDERLAENDISCH - NL
#
#########################################################################################################



Druppeltjes van God's liefde

Druppeltjes van liefde
Levensadvies
God

Druppeltjes van God's liefde

De vrije universele onpersoonlijke geest van liefde - GOD - geeft aan de hartelijke mensen die in de tegenwoordige tijd naar de goddelijke waarheid zoeken via een heraut - die nederig en ver van het publiek leeft en tot geen enkele godsdienst noch tot enige spirituele of wereldse belangengroep behoort - nieuwe kennis voor hun bewustzijnsverruiming, die in verschillende aardse en hemelse levenssferen doordringt en die de mensheid tot nu toe ten dele onbekend was.

Aanbeveling van een bijzonder belangrijke boodschap:

"God, het onpersoonlijke, meest geniale en meest hartelijke wezen, werd geschapen door hemelse wezens van licht."

korte introductie



#########################################################################################################
#
# POLNISCH - PL
#
#########################################################################################################



Kropelki Bożej miłości

Kropelki miłości
Porady życiowe
Bóg

Kropelki Bożej miłości

Wolny uniwersalny bezosobowy duch miłości - BÓG - daje serdecznym ludziom poszukującym boskiej prawdy w obecnym czasie poprzez zwiastuna - który żyje skromnie i z dala od społeczeństwa i nie należy do żadnej religii ani do żadnej duchowej czy światowej grupy interesów - nową wiedzę dla ich rozszerzenia świadomości, która wkracza w różne ziemskie i niebiańskie sfery życia i była dotychczas częściowo nieznana ludzkości.

Zalecenie szczególnie ważnego komunikatu:

"Bóg, bezosobowa, najbardziej genialna i najserdeczniejsza istota, został stworzony przez niebiańskie istoty światła."

krótkie wprowadzenie



#########################################################################################################
#
# PORTUGIESISCH - PT
#
#########################################################################################################



Gotas do amor de Deus

Gotas de amor
Conselhos para a vida
Deus

Gotas do amor de Deus

O livre espírito universal impessoal de amor - DEUS - dá às pessoas sinceras que procuram a verdade divina no tempo presente através de um arauto - que vive humildemente e longe do público e não pertence a nenhuma religião nem a nenhum grupo de interesse espiritual ou mundano - novos conhecimentos para a sua expansão da consciência, que vai para diferentes esferas da vida terrena e celestial e que era parcialmente desconhecida pela humanidade até agora.

Recomendação de uma mensagem particularmente importante:

"Deus, o ser impessoal, mais genial e mais cordial, foi criado por seres celestiais de luz."

breve introdução



#########################################################################################################
#
# PORTUGIESISCH-BR - PT-BR
#
#########################################################################################################



Gotas do amor de Deus

Gotas de amor
Conselhos para a vida
Deus

Gotas do amor de Deus

O livre espírito universal impessoal de amor - DEUS - dá às pessoas sinceras em busca da verdade divina no tempo presente através de um arauto - que vive humildemente e longe do público e não pertence a nenhuma religião nem a nenhum grupo de interesse espiritual ou mundano - novos conhecimentos para sua expansão da consciência, que vai para diferentes esferas da vida terrena e celestial e que era parcialmente desconhecida pela humanidade até agora.

Recomendação de uma mensagem particularmente importante:

"Deus, o ser impessoal, mais genial e mais cordial, foi criado por seres celestes de luz."

breve introdução



#########################################################################################################
#
# RUMAENISCH - RO
#
#########################################################################################################



Picături de iubire ale lui Dumnezeu

Picături de iubire
Sfaturi de viață
Dumnezeu

Picături de iubire ale lui Dumnezeu

Spiritul universal liber și impersonal al iubirii - DUMNEZEU - oferă oamenilor inimoși care caută adevărul divin în prezent, prin intermediul unui vestitor - care trăiește cu umilință și departe de public și nu aparține nici unei religii, nici unui grup de interese spirituale sau lumești - noi cunoștințe pentru extinderea conștiinței lor, care pătrund în diferite sfere pământești și cerești ale vieții și care au fost parțial necunoscute omenirii până acum.

Recomandarea unui mesaj deosebit de important:

"Dumnezeu, ființa impersonală, cea mai ingenioasă și mai inimoasă, a fost creat de ființe celeste de lumină."

scurtă introducere



#########################################################################################################
#
# RUSSISCH - RU
#
#########################################################################################################



Капельки Божьей любви

Капельки любви
Жизненные советы
Бог

Капельки Божьей любви

Свободный универсальный безличный дух любви - БОГ - дает сердечным людям, ищущим в наше время божественную истину, через вестника - живущего скромно и вдали от публики, не принадлежащего ни к какой религии, ни к какой духовной или мирской группе интересов - новые знания для расширения их сознания, которые входят в различные земные и небесные сферы жизни и были частично неизвестны человечеству до сих пор.

Рекомендация особо важного сообщения:

"Бог, безличное, самое гениальное и самое сердечное существо, был создан небесными существами света."

краткое введение



#########################################################################################################
#
# SLOWAKISCH - SK
#
#########################################################################################################



Kvapky Božej lásky

Kvapky lásky
Životné rady
Boh

Kvapky Božej lásky

Slobodný univerzálny neosobný duch lásky - BOH - dáva srdnatým ľuďom hľadajúcim božskú pravdu v súčasnosti prostredníctvom zvestovateľa - ktorý žije skromne a ďaleko od verejnosti a nepatrí k žiadnemu náboženstvu ani k žiadnej duchovnej či svetskej záujmovej skupine - nové poznatky na rozšírenie ich vedomia, ktoré zasahujú do rôznych pozemských i nebeských sfér života a boli ľudstvu doteraz čiastočne neznáme.

Odporúčanie obzvlášť dôležitého posolstva:

"Boha, neosobnú, najgeniálnejšiu a najsrdečnejšiu bytosť, stvorili nebeské bytosti svetla."

krátky úvod



#########################################################################################################
#
# SLOWENISCH - SL
#
#########################################################################################################



Kapljice Božje ljubezni

Kapljice ljubezni
Nasveti za življenje
Bog

Kapljice Božje ljubezni

Svobodni univerzalni neosebni duh ljubezni - BOG - današnjim srčnim ljudem, ki iščejo božansko resnico, prek glasnika, ki živi skromno in stran od družbe ter ne pripada nobeni religiji ali duhovni ali posvetni interesni skupini, daje novo znanje za razširitev njihove zavesti, ki prodira v različne zemeljske in nebeške sfere življenja in je bilo človeštvu doslej deloma neznano.

Priporočilo posebej pomembnega sporočila:

"Boga, najbolj genialno, najbolj srčno, neosebno bitje, so ustvarila nebesna bitja svetlobe."

kratek uvod



#########################################################################################################
#
# SCHWEDISCH - SV
#
#########################################################################################################



Droppar av Guds kärlek

Droppar av kärlek
Livsråd
Gud

Droppar av Guds kärlek

Den fria, universella, opersonliga kärleksanden - GUD - ger genom en härold - som lever ödmjukt och långt från allmänheten och inte tillhör någon religion eller någon andlig eller världslig intressegrupp - de hjärtliga människor som söker efter den gudomliga sanningen i vår tid ny kunskap för deras medvetandeutvidgning, som sträcker sig in i olika jordiska och himmelska livssfärer och som delvis har varit okänd för mänskligheten fram till nu.

Rekommendation av ett särskilt viktigt budskap:

"Gud, den opersonliga, mest geniala och hjärtligaste varelsen, skapades av himmelska ljusvarelser."

kortfattad introduktion



#########################################################################################################
#
# CHINESISCH - ZH
#
#########################################################################################################



上帝之爱的水滴

爱的水滴
生活建议
神

上帝之爱的水滴

自由的、普遍的、非个人的爱的精神--上帝--通过一个信使--他谦逊地生活在远离公众的地方，不属于任何宗教或精神或世俗的利益集团--为扩大他们的意识提供新的知识，这些知识深入到尘世和天堂生活的不同领域，是人类迄今为止部分未知的。

推荐一个特别重要的信息。

"上帝，这个非个人的、最巧妙的、最衷心的存在，是由天人合一的光明生物创造的。"

简短介绍



#########################################################################################################
#
# ENDE
#
#########################################################################################################
