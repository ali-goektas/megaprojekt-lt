<?php
# richtigen Zeichensatz benutzen
header('Content-type: text/html; charset=utf-8');
/*
iconv_set_encoding( 'input_encoding' , "UTF-8" );
iconv_set_encoding( 'internal_encoding' , "UTF-8" );
iconv_set_encoding( 'output_encoding' , "UTF-8" );
iconv_set_encoding( 'regex_encoding' , "UTF-8" );
#richtige Codierung benutzen
mb_internal_encoding("UTF-8");
mb_http_output("UTF-8");
mb_http_input("UTF-8");
mb_regex_encoding("UTF-8");
*/


#########################################################################################################
#
# KONSTANTEN
#
#########################################################################################################


# Pfade hier aendern
# Der Pfad zum lokalen Server
define( ROOTPFAD , "C:/xampp/htdocs/" );
# In den Botschaftenordner kommen alle Botschaften und Themensammlungen hinein
define( BOTSCHAFTEN , ROOTPFAD . "all-in-one/input/ordnerinhalt_gehoert_ins_htdocs-verzeichnis/botschaften/" );
# In den Eingangsordner kommen die Webseitentexte, in allen Sprachen, CSS, und die 'sprachspezifischen Textschnipsel'.
define( EINGANG , ROOTPFAD . "all-in-one/input/" );
# Im Ausgangsordner entstehen die 'fertigen' Dateien, die es auf den Server zu kopieren gilt
define( AUSGANG , ROOTPFAD . "all-in-one/output/" );
# Verzeichnisse + Ordner + Dateien
define( ORDNER_SCHABLONEN , "schablonen/" );
define( ORDNER_DATEN , "daten/" );
define( ORDNER_LABEL_BESCHRIFTUNGEN , "botschaften_link_beschriftungen/" );
define( ORDNER_DATEINAMEN , "botschaften_dateinamen/" );
define( ROHDATEN , "uebersetzungsdaten.txt" );
define( FUNKTIONEN , "hilfsprogramme.php" );
define( THEMENSAMMLUNG_INHALTSANGABEN , "themensammlungs_inhaltsangaben.txt" );

# Zeilenumbruch als Konstante
define( NEW_LINE , "<br>\n" );

define( DATUM_LETZTER_CSS_AENDERUNG , "2022-02-20" );


#########################################################################################################
#
# DATEN LADEN
#
#########################################################################################################


# Sprach"variablen" und Funktionen
ob_end_clean(); ob_start();
require_once( EINGANG . ORDNER_DATEN . ROHDATEN );
require_once( EINGANG . FUNKTIONEN );
ob_get_contents(); ob_end_clean();

# Anzahl Sprachen
$anzahl_sprachen = count( $liste_aller_sprachcodes );


#########################################################################################################
#
# DATENKONSISTENZ PRUEFEN
#
#########################################################################################################


# Botschaftenkonsistenz wird weiter unten ueberprueft, sobald alle zur Ueberpruefung notwendigen Daten "zusammengestellt" wurden.

# Ueberpruefen, ob die txt-Dateien in den Ordnern "daten" und "schablonen" und die Auflistung in der Liste "uebersetzungsdaten.txt" miteinander konform gehen.
ueberpruefen_ob_seitentextedateien_fehlen_oder_zuviel( $liste_aller_seiteninhaltedateinamen , EINGANG . ORDNER_DATEN , EINGANG . ORDNER_SCHABLONEN );

# Ueberpruefen, ob die Anzahl an Elementen in der Emai-"Liste" durch 2 teilbar ist. Wg. 'Kodierung <-> Wert'-Paar.
ueberpruefen_ob_anzahl_zeilen_in_email_durch_zwei_teilbar( $email_codierung );

# Arrays in "uebersetzungsdaten.txt" ueberpruefen
ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_aller_sprachen );
ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_aller_beschreibungen );
ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_aller_sprunglinks );
ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_aller_navigation_standardseiten );
ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_aller_navigation_sonderelemente );
ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_aller_infotexte );
ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_aller_datenschutztexte );
ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_aller_emailtexte );

# Ueberpruefen, ob die Anzahl an Zeilen in den Dateien mit Texten der Sprachuebersetzungen - glatt - durch die Anzahl an Sprachen teilbar ist.
$liste_aller_seitentexte = hole_alle_seitenuebersetzungen_texte( $liste_aller_seiteninhaltedateinamen );
for ( $i = 0 ; $i < count( $liste_aller_seitentexte ) ; $i++ ) {
  ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_aller_seitentexte[$i] );
}

# Ueberpruefen, ob die Anzahl an Zeilen an Inhaltsangaben der Botschaften in der Themensammlungsliste - glatt - durch die Anzahl an Sprachen teilbar ist.
$liste_der_inhaltsangaben_der_themensammlungsbotschaften = hole_alle_themensammlungen_inhaltsangaben( EINGANG . ORDNER_DATEN . THEMENSAMMLUNG_INHALTSANGABEN );

ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen( $liste_der_inhaltsangaben_der_themensammlungsbotschaften );


#########################################################################################################
#
# FEHLENDE ARRAYS ERSTELLEN
#
#########################################################################################################


$liste_der_sprachcodes_lateinischer_sprachen = array_von_array_abziehen( $liste_aller_sprachcodes , $liste_der_sprachcodes_nichtlateinischer_sprachen );
$liste_der_sprachcodes_nichtmeistgesprochener_sprachen = array_von_array_abziehen( $liste_aller_sprachcodes , $liste_der_sprachcodes_meistgesprochener_sprachen );


#########################################################################################################
#
# THEMENSAMMLUNGEN-INHALTSANGABEN-ARRAY
#
#########################################################################################################


$themensammlungen_inhaltsangaben_array = array();

For ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
  
  $sprachkuerzel = $liste_aller_sprachcodes[$i];
  # Sprachkuerzel ins Sprachverzeichnis im Seiteninhalte-Array hinzufuegen
  $themensammlungen_inhaltsangaben_array[$sprachkuerzel] = array();
  
  # Die Anzahl an Zeilen an Inhaltsangaben, die EINE Sprache hat, herausfinden
  $anzahl_zeilen_an_inhaltsangaben_je_sprache = count( $liste_der_inhaltsangaben_der_themensammlungsbotschaften ) / count( $liste_aller_sprachcodes );
  # Gehe alle Zeilen dieser EINEN Sprache EINZELN durch:
  for ( $j = 0 ; $j < $anzahl_zeilen_an_inhaltsangaben_je_sprache ; $j++ ) {
    # Trage Zeile fuer Zeile ins Seiteninhalte-Array ein
    $themensammlungen_inhaltsangaben_array[$sprachkuerzel][$j] = $liste_der_inhaltsangaben_der_themensammlungsbotschaften[ ( ( $i * $anzahl_zeilen_an_inhaltsangaben_je_sprache ) + $j ) ];
  }
  
}


#########################################################################################################
#
# GESAMT-ARRAY
#
#########################################################################################################


$gesamtarray = array();


For ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
  
  # Gesamtarray fuellen
  $gesamtarray[$liste_aller_sprachcodes[$i]] = array(
    
    'sprachkuerzel' => $liste_aller_sprachcodes[$i],
    'sprachkuerzel_nur_zwei_zeichen' => "",
    'ist_alternate' => "",
    'deutsch' => "",
    'englisch' => "",
    'in_eigener_sprache' => "",
    'ist_latein' => "",
    'beschreibung' => "",
    'sprunglink_zum_inhalt' => "",
    'info' => "",
    'datenschutz' => "",
    'aktualisiert' => "",
    'startseite' => "",
    'original' => "",
    'original_1' => "",
    'original_2' => "",
    'aufklappen' => "",
    'untermenue_aufklappen' => "",
    'email_1' => "",
    'email_2' => "",
    'email_3' => "",
    'email_4' => "",
    'email_5' => "",
    'email_6' => "",
    'email_7' => "",
    'email_8' => "",
    'email_9' => "",
    'email_10' => "",
    'email_11' => "",
    'email_12' => "",
    'email_13' => "",
    'email_14' => "",
    'email_15' => "",
    'email_16' => "",
    'email_17' => "",
    'email_18' => "",
    'email_19' => "",
    'email_20' => "",
    'email_21' => "",
    'email_22' => "",
    'ist_datumformat_int' => "",
    'datum_botschaft' => "",
    'datum_themensammlung' => "",
    'datum_startseite' => "",
    'anzahl_botschaften' => "",
    'anzahl_themensammlungen' => "",
    
  );
}


#########################################################################################################
#
# DATEN INS GESAMTARRAY EINTRAGEN
#
#########################################################################################################


# 2-Zeichen-Sprachkuerzel fuer "lang"-Attribut eintragen
$gesamtarray = sprachkuerzel_mit_nur_die_ersten_zwei_zeichen_eintragen( $gesamtarray );

# Ist es eine Sprache die aehnlich einer anderen Sprache ist (Dialekt) und deshalb doppelter Content eintragen
$gesamtarray = eintragen_ob_alternate( $gesamtarray , $liste_aller_alternate_sprachcodes );

# deutsche Bezeichnung eintragen
$gesamtarray = deutsche_bezeichnung_eintragen( $gesamtarray , $liste_aller_sprachen );

# englische Bezeichnung eintragen
$gesamtarray = englische_bezeichnung_eintragen( $gesamtarray , $liste_aller_sprachen );

# Bezeichnung der Sprache in der Sprache selbst eintragen
$gesamtarray = eigensprachliche_bezeichnung_eintragen( $gesamtarray , $liste_aller_sprachen );

# Sind es lateinische Buchstaben oder keine lateinischen Buchstaben eintragen
$gesamtarray = eintragen_ob_lateinische_buchstaben( $gesamtarray , $liste_der_sprachcodes_nichtlateinischer_sprachen );

# Beschreibungen (descriptions - fuer SEO) eintragen
$gesamtarray = beschreibungen_eintragen( $gesamtarray , $liste_aller_beschreibungen );

# "Zum Inhalt springen"-Texte eintragen
$gesamtarray = sprunglinks_eintragen( $gesamtarray , $liste_aller_sprunglinks );

# Infotext "von Freunden erstellt" eintragen
$gesamtarray = info_eintragen( $gesamtarray , $liste_aller_infotexte );

# Datenschutztext eintragen
$gesamtarray = datenschutz_eintragen( $gesamtarray , $liste_aller_datenschutztexte );

# "aktualisiert"-Begriffe eintragen
$gesamtarray = aktualisiert_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente );

# "Startseite"-Begriffe eintragen
$gesamtarray = startseite_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente );

# "Originalwebseiten"-Begriffe eintragen
$gesamtarray = original_webseiten_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente );

# "Originalwebseite 1"-Begriffe eintragen
$gesamtarray = original_webseite_1_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente );

# "Originalwebseite 2"-Begriffe eintragen
$gesamtarray = original_webseite_2_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente );

# "aufklappen"-Begriffe eintragen
$gesamtarray = aufklappen_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente );

# "Untermenue aufklappen"-Begriffe eintragen
$gesamtarray = untermenue_aufklappen_begriff_eintragen( $gesamtarray , $liste_aller_navigation_sonderelemente );

# E-Mail-Fragmente eintragen
$gesamtarray = email_texte_eintragen( $gesamtarray , $liste_aller_emailtexte , $email_codierung );

# Ob Sprache internationales Datumsformat verwendet eintragen
$gesamtarray = eintragen_ob_internationales_datumsformat( $gesamtarray , $liste_der_sprachcodes_der_sprachen_mit_internationalem_datumsformat );

# Datum der neuesten Themensammlungen eintragen
$gesamtarray = datum_neuester_themensammlung_eintragen( $gesamtarray , $themensammlungsdatum );

# Datum der neuesten Botschaften eintragen
$gesamtarray = datum_neuester_botschaft_eintragen( $gesamtarray , $in_allen_sprachen_das_selbe_botschaften_datum , $liste_jeder_sprache_botschaftsdatum );


#-------------------------------------------------------------
# BOTSCHAFTENKONSISTENZ PRUEFEN
#-------------------------------------------------------------

# Ueberpruefen, ob in den Listen "liste_der_themensammlungenreihenfolge" und "liste_der_botschaftenreihenfolge":
# alle vorhandenen Botschaften aufgelistet sind, alle aufgelisteten Botschaften vorhanden sind, in allen Sprachen.
ueberpruefen_ob_botschaften_zuviel_oder_fehlen( $liste_der_themensammlungenreihenfolge , $liste_der_botschaftenreihenfolge , BOTSCHAFTEN );

# Anzahl Themensammlungen eintragen
$gesamtarray = anzahl_themensammlungen_eintragen( $gesamtarray , $liste_der_themensammlungenreihenfolge , BOTSCHAFTEN );

# Anzahl Botschaften eintragen
$gesamtarray = anzahl_botschaften_eintragen( $gesamtarray , $liste_der_botschaftenreihenfolge , BOTSCHAFTEN );

# Ueberpruefen, ob die "Summe der 'Anzahl an Botschaften pro Jahr' aller Jahre" uebereinstimmt mit
# der TATSAECHLICHEN Anzahl an vorhandenen Botschaften in der Botschaftenliste (Botschaftenseite).
ueberpruefen_ob_anzahl_botschaften_uebereinstimmt_mit_summe_botschaften_pro_jahr( $gesamtarray , $anzahl_der_botschaften_pro_jahr );

# Ueberpruefen, ob die "Anzahl an Inhaltsangaben" (je Sprache) uebereinstimmt mit
# der TATSAECHLICHEN Anzahl an vorhandenen Inhaltsangaben in der Themensammlungenliste (Themensammlungenseite).
ueberpruefen_ob_anzahl_themensammlungs_inhaltsangaben_uebereinstimmt_mit_summe_themensammlungs_inhaltsangaben( $themensammlungen_inhaltsangaben_array , $anzahl_der_inhaltsangaben_pro_themensammlung );



#########################################################################################################
#
# NAVIGATIONS-URL-ARRAY
#
#########################################################################################################


$navigations_url_array = array();


For ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
  
  $sprachkuerzel = $liste_aller_sprachcodes[$i];
  # Sprachkuerzel ins Sprachverzeichis im Navigation-URL-Aarray (je Sprache) hinzufuegen
  #$navigations_url_array[$sprachkuerzel]['sprachkuerzel'] = $sprachkuerzel;
  
  $anzahl_zeilen_je_sprache = count( $liste_aller_navigation_standardseiten ) / count( $liste_aller_sprachcodes );
  
  # In der Navigations-Liste fuer jeden Button ein Array erzeugen
  $navigations_url_array = button_arrays_erschaffen( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache );
  
  # In der Navigations-Liste eine durchnummerierte Liste der Buttons erzeugen
  $navigations_url_array = felder_fuer_buttonarrays_erzeugen( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache );
  
  # Button-Typ festlegen (ob "Aufklapp-Button", "Hauptmenue-Button", oder "Untermenue-Button")
  
  # Untermenue-Buttons mit "unter" vermerken (dass sie Untermenue-Buttons sind)
  $navigations_url_array = untermenuebuttons_deklarieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache );
  
  # Aufklapp-Buttons mit "aufklapp" vermerken (dass sie Aufklappbuttons sind)
  $navigations_url_array = aufklappbuttons_deklarieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache );
  
  # Hauptmenue-Buttons mit "haupt" vermerken (dass sie Hauptmenue-Buttons sind)
  $navigations_url_array = hauptmenuebuttons_deklarieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache );
  
  # In der Navigations-Liste bei jedem Button "seine Reihenfolge" vermerken
  $navigations_url_array = buttons_durchnummerieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache );
  
  # In der Navigations-Liste bei jedem Button seine 'Bezeichnung' vermerken (inkl. Sonderzeichen. So wie es auch im Menue dargestellt wird)
  $navigations_url_array = button_beschriftungen_eintragen( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache );
  
  # In der URL muessen "Hauptmenue-Button-Links" und "Untermenue-Button-Links" aufrufbar sein.
  # Aber nicht "Aufklapp-Button(-Beschriftungen)". Weil diese KEINE Links sind.
  
  # In der Navigations-Liste bei jedem "Link-Button" (Hauptmenue-Buttons u. Untermenue-Buttons) "seine Reihenfolge" vermerken
  $navigations_url_array = url_links_durchnummerieren( $navigations_url_array , $sprachkuerzel , $liste_aller_navigation_standardseiten , $anzahl_zeilen_je_sprache );
  
}


#-------------------------------------------------------------
# fuer "sprechende URL" alle 'Sonderzeichen' ersetzen
#-------------------------------------------------------------

# Liste aller moeglichen "deutschen" Sprachbezeichnungen holen. Inkl. "alle" .
$liste_aller_sprachbezeichnungen_in_deutsch = hole_alle_deutschen_sprachbezeichnungen( $gesamtarray );
$liste_aller_sprachbezeichnungen_in_deutsch[] = "alle";

# Die Positionen, an denen die 'deutschen' Bezeichnungen stehen, holen. Und sortieren.
$array_der_positionen_der_vorkommenden_sprachen_in_deutsch = liste_der_positionen_deutscher_sprachbezeichnungen_erstellen( $liste_aller_sprachbezeichnungen_in_deutsch , $liste_zu_ersetzender_sonderzeichen );
sort( $array_der_positionen_der_vorkommenden_sprachen_in_deutsch);

# Ueberpruefen, ob *NACH* den Sprachbezeichnungen die zu 'ersetzenden' Zeichen immer *paarweise* vorkommen. (von -> zu)
ueberpruefen_ob_anzahl_sonderzeichen_paarweise_durch_zwei_teilbar( $array_der_positionen_der_vorkommenden_sprachen_in_deutsch , $liste_zu_ersetzender_sonderzeichen );

# Liste, nach welcher die Sonderzeichen - bezogen auf jeweilige Sprache - zu ersetzen sind, erzeugen.
$array_zu_ersetzender_sonderzeichen = array_mit_sprachen_und_deren_zu_ersetzende_sonderzeichen_erstellen( $array_der_positionen_der_vorkommenden_sprachen_in_deutsch , $liste_zu_ersetzender_sonderzeichen , $liste_der_sprachcodes_lateinischer_sprachen );

# Da URL eigene 'Liste aller Navigation Standardseiten' (ohne Sonderzeichen) braucht, eine *Kopie* dieser Liste erstellen.
$url_liste_aller_navigation_standardseiten = $liste_aller_navigation_standardseiten;

# Alle nicht-lateinischen Sprachen URL-konform in "Ziffern"-Ordner "umwandeln".
$url_liste_aller_navigation_standardseiten = url_konforme_liste_erstellen_nichtlateinische_sprachen( $url_liste_aller_navigation_standardseiten , $liste_der_sprachcodes_nichtlateinischer_sprachen , $navigations_url_array );

# In allen 'lateinischen' Sprachen die Sonderzeichen durch URL-konforme ( a-z , A-Z , 0-9 , - ) ersetzen. Und die Leerzeichen am Anfang entfernen.
$url_liste_aller_navigation_standardseiten = url_konforme_liste_erstellen_lateinische_sprachen( $url_liste_aller_navigation_standardseiten , $liste_der_sprachcodes_lateinischer_sprachen , $array_zu_ersetzender_sonderzeichen );

# Von Sonderzeichen befreiten URL-konformen Bezeichnungen ins Navigations-URL-Array eintragen
$navigations_url_array = url_bezeichnungen_ins_navigations_array_eintragen( $navigations_url_array , $url_liste_aller_navigation_standardseiten );

# Bei Aufklapp-Buttons URL-konforme Bezeichnungen leeren
$navigations_url_array = aufklapp_buttons_leeren( $navigations_url_array );


#########################################################################################################
#
# ANORDNUNG DER SPRACHEN IN UNTERMENUES
#
#########################################################################################################


# Sprachen zuweisen
$liste_meistgesprochener_sprachen = hole_eigensprachliche_sprachbezeichnungen( $liste_der_sprachcodes_meistgesprochener_sprachen );
$liste_nichtmeistgesprochener_sprachen = hole_eigensprachliche_sprachbezeichnungen( $liste_der_sprachcodes_nichtmeistgesprochener_sprachen );
$liste_nichtlateinischer_sprachen = hole_eigensprachliche_sprachbezeichnungen( $liste_der_sprachcodes_nichtlateinischer_sprachen );
$liste_lateinischer_sprachen = hole_eigensprachliche_sprachbezeichnungen( $liste_der_sprachcodes_lateinischer_sprachen );

# alphabetisch sortieren
sort( $liste_nichtmeistgesprochener_sprachen );
sort( $liste_lateinischer_sprachen );


#########################################################################################################
#
# SEITENINHALTE-ARRAY
#
#########################################################################################################


$seiteninhalte_array = array();

For ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
  
  $sprachkuerzel = $liste_aller_sprachcodes[$i];
  # Sprachkuerzel ins Sprachverzeichnis im Seiteninhalte-Array hinzufuegen
  $seiteninhalte_array[$sprachkuerzel] = array();
  
  # Gehe alle "Seiten" durch
  for ( $j = 0 ; $j < count( $liste_aller_seitentexte ) ; $j++ ) {
    $anzahl_zeilen_je_sprache_in_dieser_einen_textdatei = count( $liste_aller_seitentexte[$j] ) / count( $liste_aller_sprachcodes );
    # Gehe alle Zeilen dieser EINEN Sprache (in dieser EINEN Seitentext-Datei) EINZELN durch:
    for ( $k = 0 ; $k < $anzahl_zeilen_je_sprache_in_dieser_einen_textdatei ; $k++ ) {
      # Trage Zeile fuer Zeile ins Seiteninhalte-Array ein
      $seiteninhalte_array[$sprachkuerzel][$j][$k] = $liste_aller_seitentexte[$j][ ( ( $i * $anzahl_zeilen_je_sprache_in_dieser_einen_textdatei ) + $k ) ];
    }
  }
}


#########################################################################################################
#
# BOTSCHAFTEN-ARRAYS
#
#########################################################################################################


# Liste der auf der Botschaftenseite in der langen Liste zu lesenden Botschafts-"Umschreibungen" (nur die Links)
$liste_botschaften_beschriftungen_und_dateinamen = array();
$liste_botschaften_beschriftungen_und_dateinamen = botschaften_beschriftungen_und_dateinamen_eintragen( $liste_botschaften_beschriftungen_und_dateinamen , EINGANG . ORDNER_DATEN . ORDNER_LABEL_BESCHRIFTUNGEN , EINGANG . ORDNER_DATEN . ORDNER_DATEINAMEN , $liste_der_botschaftenreihenfolge , BOTSCHAFTEN );

# Liste der auf der Themensammlungenseite in der langen Liste zu lesenden Themensammlungs-"Umschreibungen" (nur die Links)
$liste_themensammlungen_beschriftungen_und_dateinamen = array();
$liste_themensammlungen_beschriftungen_und_dateinamen = themensammlungen_beschriftungen_und_dateinamen_eintragen( $liste_themensammlungen_beschriftungen_und_dateinamen , EINGANG . ORDNER_DATEN . ORDNER_LABEL_BESCHRIFTUNGEN , EINGANG . ORDNER_DATEN . ORDNER_DATEINAMEN , $liste_der_themensammlungenreihenfolge , BOTSCHAFTEN );









/*
#########################################################################################################
#
# BOTSCHAFTEN-SONDERMERKMALE-ARRAY
#
#########################################################################################################


$botschaften_sondermerkmale_array = array();

$botschaften_sondermerkmale_array = array(

'besonders' => array(),
'text_darueber' => array(),
'text_darunter' => array(),
'text_links_davon' => array(),
'text_rechts_davon' => array(),

);

# Liste aller 'besonderen' Botschaften (hellblauer Hintergrund) eintragen
$botschaften_sondermerkmale_array['besonders'] = $liste_aller_sondermerkmale_besondere_botschaften;

# In die 4 'Text'-Arrays ALLE Sprachkuerzel eintragen
for ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
$botschaften_sondermerkmale_array['text_darueber'][ $liste_aller_sprachcodes[$i] ] = array();
}

pre( $anzahl_botschaften );
pre($botschaften_sondermerkmale_array);


# In die 4 'Text'-Arrays ALLE Sprachkuerzel eintragen
$botschaften_sondermerkmale_array['text_darueber'] = $liste_aller_sprachcodes;
$botschaften_sondermerkmale_array['text_darunter'] = $liste_aller_sprachcodes;
$botschaften_sondermerkmale_array['text_links_davon'] = $liste_aller_sprachcodes;
$botschaften_sondermerkmale_array['text_rechts_davon'] = $liste_aller_sprachcodes;

# Keys und Values tauschen
foreach ( $botschaften_sondermerkmale_array['text_darueber'] as $aktuelles_kuerzel ) {
$botschaften_sondermerkmale_array['text_darueber'] = array_flip( $botschaften_sondermerkmale_array['text_darueber'] );
}

pre($botschaften_sondermerkmale_array);

# Alle Sprachcodes als 'Array' deklarieren
#foreach ( $botschaften_sondermerkmale_array[]
#for ( $i = 0 ; $i < count( $liste_aller_sprachcodes ) ; $i++ ) {
#  $botschaften_sondermerkmale_array['text_darueber']['liste_aller_sprachcodes'] = array();
#}
*/









#########################################################################################################
#
# WAS VORHANDEN IST
#
#########################################################################################################


#
# FUNKTIONEN:
# 
#   Darstellfunktionen:
#     pre
#     pre_dump
#     fehlermeldung
#
#   Ueberprueffunktionen:
#     ueberpruefen_ob_seitentextedateien_fehlen_oder_zuviel
#     ueberpruefen_ob_anzahl_zeilen_in_email_durch_zwei_teilbar
#     ueberpruefen_ob_anzahl_zeilen_ein_mehrfaches_von_anzahl_sprachen
#     ueberpruefen_ob_botschaften_zuviel_oder_fehlen
#     ueberpruefen_ob_anzahl_botschaften_uebereinstimmt_mit_summe_botschaften_pro_jahr
#
#   Getter:
#     hole_position_sprachkuerzel
#     hole_region_in_grossbuchstaben
#     hole_von_deutscher_sprachbezeichnung_dessen_sprachkuerzel
#     hole_navigationsblock_aktueller_sprache
#     hole_alle_deutschen_sprachbezeichnungen
#     hole_eigensprachliche_sprachbezeichnungen
#     hole_alle_seitenuebersetzungen_texte
#
#   Array-Bearbeitung:
#     array_leere_elemente_entfernen
#     array_sprachtextdatei_bereinigen
#     array_von_array_abziehen
#
#   Ins Gesamtarray eintragen:
#     sprachkuerzel_mit_nur_die_ersten_zwei_zeichen_eintragen
#     eintragen_ob_alternate
#     deutsche_bezeichnung_eintragen
#     englische_bezeichnung_eintragen
#     eigensprachliche_bezeichnung_eintragen
#     eintragen_ob_lateinische_buchstaben
#     beschreibungen_eintragen
#     sprunglinks_eintragen
#     info_eintragen
#     datenschutz_eintragen
#     aktualisiert_begriff_eintragen
#     startseite_begriff_eintragen
#     original_webseiten_begriff_eintragen
#     original_webseite_1_begriff_eintragen
#     original_webseite_2_begriff_eintragen
#     aufklappen_begriff_eintragen
#     untermenue_aufklappen_begriff_eintragen
#     email_texte_eintragen
#     eintragen_ob_internationales_datumsformat
#     datum_neuester_themensammlung_eintragen
#     datum_neuester_botschaft_eintragen
#     anzahl_themensammlungen_eintragen
#     anzahl_botschaften_eintragen
#
#   Ins Navigation-URL-Array eintragen:
#     button_arrays_erschaffen
#     felder_fuer_buttonarrays_erzeugen
#     untermenuebuttons_deklarieren
#     aufklappbuttons_deklarieren
#     hauptmenuebuttons_deklarieren
#     buttons_durchnummerieren
#     button_beschriftungen_eintragen
#     url_links_durchnummerieren
#   - Fuer URL Sonderzeichen ersetzen:
#     liste_der_positionen_deutscher_sprachbezeichnungen_erstellen
#     ueberpruefen_ob_anzahl_sonderzeichen_paarweise_durch_zwei_teilbar
#     array_mit_sprachen_und_deren_zu_ersetzende_sonderzeichen_erstellen
#     url_konforme_liste_erstellen_nichtlateinische_sprachen
#     url_konforme_liste_erstellen_lateinische_sprachen
#     aufklapp_buttons_leeren
#
#   In Botschaften-Arrays eintragen:
#     botschaften_beschriftungen_und_dateinamen_eintragen
#     themensammlungen_beschriftungen_und_dateinamen_eintragen
#
#
# KONSTANTEN:
#
#   ROOTPFAD
#   BOTSCHAFTEN
#   EINGANG
#   AUSGANG
#   ORDNER_SCHABLONEN
#   ORDNER_DATEN
#   ORDNER_LABEL_BESCHRIFTUNGEN
#   ORDNER_DATEINAMEN
#   ROHDATEN
#   FUNKTIONEN
#   NEW_LINE
#   DATUM_LETZTER_CSS_AENDERUNG
#
#
#
# ARRAYS:
#
#   $liste_aller_sprachcodes
#
#
#   $gesamtarray
#     'sprachkuerzel'
#     'sprachkuerzel_nur_zwei_zeichen'
#     'ist_alternate'
#     'deutsch'
#     'englisch'
#     'in_eigener_sprache'
#     'ist_latein'
#     'beschreibung'
#     'sprunglink_zum_inhalt'
#     'info'
#     'datenschutz'
#     'aktualisiert'
#     'startseite'
#     'original'
#     'original_1'
#     'original_2'
#     'aufklappen'
#     'untermenue_aufklappen'
#     'email_1'
#     'email_2'
#     'email_3'
#     'email_4'
#     'email_5'
#     'email_6'
#     'email_7'
#     'email_8'
#     'email_9'
#     'email_10'
#     'email_11'
#     'email_12'
#     'email_13'
#     'email_14'
#     'email_15'
#     'email_16'
#     'email_17'
#     'email_18'
#     'email_19'
#     'email_20'
#     'email_21'
#     'email_22'
#     'ist_datumformat_int'
#     'datum_botschaft'
#     'datum_themensammlung'
#     'datum_startseite'
#     'anzahl_themensammlungen'
#     'anzahl_botschaften'
#
#
#   $navigations_url_array
#
#     [sprachkuerzel]
#       'reihenfolge_button'
#       'beschriftung_button'
#       'reihenfolge_url'
#       'beschriftung_url'
#       'button_typ'
#
#     [sprachkuerzel]
#       ...
#
#
#   $seiteninhalte_array
#
#     [sprachkuerzel]
#       [wievielte_seite]
#         'titel-text'
#         'keyword-1-text'
#         'keyword-2-text'
#         'keyword-3-text'
#         'ueberschrift-text'
#         'text'
#         'text'
#         '...'
#       [wievielte_seite]
#         'titel-text'
#         'keyword-1-text'
#         'keyword-2-text'
#         'keyword-3-text'
#         'ueberschrift-text'
#         'text'
#         'text'
#         '...'
#       [wievielte_seite]
#         ...
#
#     [sprachkuerzel]
#       [wievielte_seite]
#         ...
#       [wievielte_seite]
#         ...
#
#     [sprachkuerzel]
#       [wievielte_seite]
#         ...
#       [wievielte_seite]
#         ...
#
#
#
# VARIABLEN:
#
#   $anzahl_sprachen
#   $liste_der_sprachcodes_meistgesprochener_sprachen
#   $liste_der_sprachcodes_nichtmeistgesprochener_sprachen
#   $liste_der_sprachcodes_nichtlateinischer_sprachen
#   $liste_der_sprachcodes_lateinischer_sprachen
#   $liste_meistgesprochener_sprachen
#   $liste_nichtmeistgesprochener_sprachen
#   $liste_nichtlateinischer_sprachen
#   $liste_lateinischer_sprachen
#
#   $liste_aller_seiteninhaltedateinamen
#
#   $liste_der_sprachcodes_der_sprachen_mit_internationalem_datumsformat
#


#########################################################################################################
#
# ERSTELLUNG DER HTML- UND CSS-DATEIEN
#
#########################################################################################################


#-------------------------------------------------------------
# Ordner erstellen
#-------------------------------------------------------------

For ( $i = 0 ; $i < $anzahl_sprachen ; $i++ ) {
  if ( !( is_dir( AUSGANG . $liste_aller_sprachcodes[$i] ) ) ) { mkdir( AUSGANG . $liste_aller_sprachcodes[$i] ); }
  chmod( AUSGANG . $liste_aller_sprachcodes[$i] , 0644 );
}

#-------------------------------------------------------------
# Vermittlung der Werte an die "Schablone"
#-------------------------------------------------------------

function datum_css()                                            { return DATUM_LETZTER_CSS_AENDERUNG; }
function seitenlink( $dateiname_zielseite )                     { global $sprachkuerzel; for ( $i = 0 ; $i < count( $GLOBALS['liste_aller_seiteninhaltedateinamen'] ) ; $i++ ) { if ( $GLOBALS['liste_aller_seiteninhaltedateinamen'][$i] == $dateiname_zielseite ) { $key_in_liste_aller_seiteninhaltedateinamen = $i; } } /* Wenn Startseite: */ if ( $key_in_liste_aller_seiteninhaltedateinamen == 0 ) { return ""; } /* Wenn nicht die Startseite */ for ( $j = 0 ; $j < count( $GLOBALS['navigations_url_array'][$sprachkuerzel] ) ; $j++ ) { if ( $GLOBALS['navigations_url_array'][$sprachkuerzel][$j]['reihenfolge_url'] == ( $key_in_liste_aller_seiteninhaltedateinamen + 1 ) ) { $zurueckzugebender_ordnername = $GLOBALS['navigations_url_array'][$sprachkuerzel][$j]['beschriftung_url']; } } return $zurueckzugebender_ordnername . "/"; }
function sprachkuerzel_meistgesprochen()                        { global $i_schablone; return $GLOBALS['liste_der_sprachcodes_meistgesprochener_sprachen'][$i_schablone]; }
function sprachkuerzel_nur_zwei_zeichen_meistgesprochen()       { global $i_schablone; $sprachkuerzel = $GLOBALS['liste_der_sprachcodes_meistgesprochener_sprachen'][$i_schablone]; return $GLOBALS['gesamtarray'][$sprachkuerzel]['sprachkuerzel_nur_zwei_zeichen']; }
function meistgesprochen_eigene_sprache()                       { global $i_schablone; $sprachkuerzel = $GLOBALS['liste_der_sprachcodes_meistgesprochener_sprachen'][$i_schablone]; return $GLOBALS['gesamtarray'][$sprachkuerzel]['in_eigener_sprache']; }
function sprachkuerzel_nicht_meistgesprochen()                  { global $i_schablone; return $GLOBALS['liste_der_sprachcodes_nichtmeistgesprochener_sprachen'][$i_schablone]; }
function sprachkuerzel_nur_zwei_zeichen_nicht_meistgesprochen() { global $i_schablone; $sprachkuerzel = $GLOBALS['liste_der_sprachcodes_nichtmeistgesprochener_sprachen'][$i_schablone]; return $GLOBALS['gesamtarray'][$sprachkuerzel]['sprachkuerzel_nur_zwei_zeichen']; }
function nicht_meistgesprochen_eigene_sprache()                 { global $i_schablone; $sprachkuerzel = $GLOBALS['liste_der_sprachcodes_nichtmeistgesprochener_sprachen'][$i_schablone]; return $GLOBALS['gesamtarray'][$sprachkuerzel]['in_eigener_sprache']; }
function sprachkuerzel_lateinisch()                             { global $i_schablone; return $GLOBALS['liste_der_sprachcodes_lateinischer_sprachen'][$i_schablone]; }
function sprachkuerzel_nur_zwei_zeichen_lateinisch()            { global $i_schablone; $sprachkuerzel = $GLOBALS['liste_der_sprachcodes_lateinischer_sprachen'][$i_schablone]; return $GLOBALS['gesamtarray'][$sprachkuerzel]['sprachkuerzel_nur_zwei_zeichen']; }
function lateinisch_eigene_sprache()                            { global $i_schablone; $sprachkuerzel = $GLOBALS['liste_der_sprachcodes_lateinischer_sprachen'][$i_schablone]; return $GLOBALS['gesamtarray'][$sprachkuerzel]['in_eigener_sprache']; }
function sprachkuerzel_nicht_lateinisch()                       { global $i_schablone; return $GLOBALS['liste_der_sprachcodes_nichtlateinischer_sprachen'][$i_schablone]; }
function sprachkuerzel_nur_zwei_zeichen_nicht_lateinisch()      { global $i_schablone; $sprachkuerzel = $GLOBALS['liste_der_sprachcodes_nichtlateinischer_sprachen'][$i_schablone]; return $GLOBALS['gesamtarray'][$sprachkuerzel]['sprachkuerzel_nur_zwei_zeichen']; }
function nicht_lateinisch_eigene_sprache()                      { global $i_schablone; $sprachkuerzel = $GLOBALS['liste_der_sprachcodes_nichtlateinischer_sprachen'][$i_schablone]; return $GLOBALS['gesamtarray'][$sprachkuerzel]['in_eigener_sprache']; }
function sprachkuerzel()                                        { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['sprachkuerzel']; }
function sprachkuerzel_nur_zwei_zeichen()                       { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['sprachkuerzel_nur_zwei_zeichen']; }
function alternate( $aktuelle_sprache )                         { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$aktuelle_sprache]['ist_alternate']; }
function text()                                                 { return array_shift( $GLOBALS['textzeilen_aktueller_seite'] ); }
function beschreibung()                                         { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['beschreibung']; }
function sprunglink_zum_inhalt()                                { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['sprunglink_zum_inhalt']; }
function startseite()                                           { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['startseite']; }
function original()                                             { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['original']; }
function original_1()                                           { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['original_1']; }
function original_2()                                           { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['original_2']; }
function info()                                                 { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['info']; }
function datenschutz()                                          { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['datenschutz']; }
function aufklappen()                                           { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['aufklappen']; }
function untermenue_aufklappen()                                { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['untermenue_aufklappen']; }
function url_ordner()                                           { $GLOBALS['liste_aller_url_ordner'][] = $GLOBALS['liste_aller_url_ordner'][0]; return array_shift( $GLOBALS['liste_aller_url_ordner'] ); }
function button_nummer()                                        { $GLOBALS['liste_aller_button_nummern'][] = $GLOBALS['liste_aller_button_nummern'][0]; return array_shift( $GLOBALS['liste_aller_button_nummern'] ); }
function button_beschriftung_label()                            { $GLOBALS['liste_aller_label_beschriftungen_dieser_sprache'][] = $GLOBALS['liste_aller_label_beschriftungen_dieser_sprache'][0]; return array_shift( $GLOBALS['liste_aller_label_beschriftungen_dieser_sprache'] ); }
function button_beschriftung_link()                             { $GLOBALS['liste_aller_link_beschriftungen_dieser_sprache'][] = $GLOBALS['liste_aller_link_beschriftungen_dieser_sprache'][0]; return array_shift( $GLOBALS['liste_aller_link_beschriftungen_dieser_sprache'] ); }
function email()                                                { /* $GLOBALS['liste_aller_email_textstuecke'][] = $GLOBALS['liste_aller_email_textstuecke'][0]; return array_shift( $GLOBALS['liste_aller_email_textstuecke'] ); */ }
function platzhalter_button_nummer()                            { global $i_schablone; return $GLOBALS['liste_aller_platzhalter_werte'][$i_schablone]['platzhalter_button_nummer']; }
function platzhalter_button_beschriftung()                      { global $i_schablone; return $GLOBALS['liste_aller_platzhalter_werte'][$i_schablone]['platzhalter_button_beschriftung']; }
function platzhalter_tag()                                      { global $i_schablone; return $GLOBALS['liste_aller_platzhalter_werte'][$i_schablone]['platzhalter_button_tag']; }
function anzahl_themensammlungen()                              { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['anzahl_themensammlungen']; }
function anzahl_botschaften()                                   { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['anzahl_botschaften']; }
function datum_themensammlung()                                 { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['datum_themensammlung']; }
function datum_botschaft()                                      { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['datum_botschaft']; }
function deutsche_bezeichnung()                                 { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['deutsch']; }
function themensammlung_dateiname()                             { global $i_schablone; return $GLOBALS['liste_aller_themensammlungen_dateinamen_dieser_sprache'][$i_schablone]; }
function themensammlung_label_beschriftung()                    { global $i_schablone; return $GLOBALS['liste_aller_themensammlungen_beschriftungen_dieser_sprache'][$i_schablone]; }
function botschaft_dateiname()                                  { global $i_schablone; return $GLOBALS['liste_aller_botschaften_dateinamen_dieser_sprache'][$i_schablone]; }
function botschaft_label_beschriftung()                         { global $i_schablone; return $GLOBALS['liste_aller_botschaften_beschriftungen_dieser_sprache'][$i_schablone]; }



# function aktuell()                                              { global $sprachkuerzel; return $GLOBALS['gesamtarray'][$sprachkuerzel]['aktualisiert']; }


# Platzhalter - Label fuellen


# Alle Sprachen durchgehen
foreach ( $liste_aller_sprachcodes as $sprachkuerzel ) {
  
  #-------------------------------------------------------------
  # sprachbezogene Arrays erstellen
  #-------------------------------------------------------------
  
  $liste_aller_label_beschriftungen_dieser_sprache = array();
  $liste_aller_link_beschriftungen_dieser_sprache = array();
  $liste_aller_email_textstuecke = array();
  $liste_aller_url_ordner = array();
  $liste_aller_button_nummern = array();
  $liste_aller_platzhalter_werte = array();
  $liste_aller_themensammlungen_beschriftungen_dieser_sprache = array();
  $liste_aller_themensammlungen_dateinamen_dieser_sprache = array();
  $liste_aller_botschaften_beschriftungen_dieser_sprache = array();
  $liste_aller_botschaften_dateinamen_dieser_sprache = array();
  
  # Liste aller Label-Beschriftungen (aufklappbar-Buttons)
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    if ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "aufklapp" ) {
      $liste_aller_label_beschriftungen_dieser_sprache[] = $navigations_url_array[$sprachkuerzel][$i]['beschriftung_button'];
    }
  }
  
  # Liste aller Link-Beschriftungen (Hauptmenue- und Untermenue-Buttons)
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    if ( ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "haupt" ) or ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "unter" ) ) {
      $liste_aller_link_beschriftungen_dieser_sprache[] = $navigations_url_array[$sprachkuerzel][$i]['beschriftung_button'];
    }
  }
  # Verdoppeln, da bis auf die Aufklapp-Buttons alle Buttons auch im Smartphone-Menue vorkommen
  $liste_aller_link_beschriftungen_dieser_sprache = array_merge( $liste_aller_link_beschriftungen_dieser_sprache , $liste_aller_link_beschriftungen_dieser_sprache );
  
  # Liste aller Email-Textstuecke
  for ( $i = 1 ; $i < 23 ; $i++ ) {
    $liste_aller_email_textstuecke[] = $gesamtarray[$sprachkuerzel]['email_' . $i];
  }
  
  # Liste aller URL-Ordner
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    # Wenn es sich nicht um einen "Aufklapp"-Button handelt
    if ( ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "haupt" ) or ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "unter" ) ) {
      $liste_aller_url_ordner[] = $navigations_url_array[$sprachkuerzel][$i]['beschriftung_url'];
    }
  }
  # Liste doppelt noetig: 1 mal fuer Desktop-/Tablet-Ansicht, 1 mal fuer Smartphone-Ansicht
  $liste_aller_url_ordner = array_merge( $liste_aller_url_ordner , $liste_aller_url_ordner );
  
  # Liste aller Button-Nummern
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    # Wenn es sich um einen "Aufklapp"-Button handelt ( 10 mal die 'selbe' Nummer )
    if ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "aufklapp" ) {
      for ( $j = 0 ; $j < 10 ; $j++ ) {
        $liste_aller_button_nummern[] = $navigations_url_array[$sprachkuerzel][$i]['reihenfolge_button'];
      }
    }
    # Wenn es sich nicht um einen "Aufklapp"-Button handelt ( 1 mal )
    if ( ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "haupt" ) or ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "unter" ) ) {
      $liste_aller_button_nummern[] = $navigations_url_array[$sprachkuerzel][$i]['reihenfolge_button'];
    }
  }
  
  # Platzhalter-'Fragmente'
  # Alle Buttons aktueller Sprache durchgehen
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    # Wenn es im Hauptmenue direkt sichtbar ist (sich also um ein Aufklapp- oder Hauptmenue-Button handelt)
    if ( ($navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "aufklapp" ) or ($navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "haupt" ) ) {
      # Array fuer diesen Button erzeugen
      $liste_aller_platzhalter_werte[] = array( "platzhalter_button_nummer" => "" , "platzhalter_button_beschriftung" => "" , "platzhalter_button_tag" => "" );
      $liste_aller_platzhalter_werte[ ( count( $liste_aller_platzhalter_werte ) - 1 ) ]['platzhalter_button_nummer'] = $navigations_url_array[$sprachkuerzel][$i]['reihenfolge_button'];
      $liste_aller_platzhalter_werte[ ( count( $liste_aller_platzhalter_werte ) - 1 ) ]['platzhalter_button_beschriftung'] = $navigations_url_array[$sprachkuerzel][$i]['beschriftung_button'];
    }
    if ($navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "aufklapp" ) {
      $liste_aller_platzhalter_werte[ ( count( $liste_aller_platzhalter_werte ) - 1 ) ]['platzhalter_button_tag'] = "label";
    }
    if ($navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "haupt" ) {
      $liste_aller_platzhalter_werte[ ( count( $liste_aller_platzhalter_werte ) - 1 ) ]['platzhalter_button_tag'] = "a";
    }
  }
  
  # Themensammlungen-Beschriftungen und Dateinamen
  $liste_aller_themensammlungen_beschriftungen_dieser_sprache = $liste_themensammlungen_beschriftungen_und_dateinamen[$sprachkuerzel]['label_beschriftung'];
  $liste_aller_themensammlungen_dateinamen_dieser_sprache = $liste_themensammlungen_beschriftungen_und_dateinamen[$sprachkuerzel]['dateiname'];
  
  # Botschaften-Beschriftungen und Dateinamen
  $liste_aller_botschaften_beschriftungen_dieser_sprache = $liste_botschaften_beschriftungen_und_dateinamen[$sprachkuerzel]['label_beschriftung'];
  $liste_aller_botschaften_dateinamen_dieser_sprache = $liste_botschaften_beschriftungen_und_dateinamen[$sprachkuerzel]['dateiname'];
  
  
  #-------------------------------------------------------------
  # HTML- und CSS-Dateien erstellen
  #-------------------------------------------------------------
  
  # Startseite
  $textzeilen_aktueller_seite = array();
  $textzeilen_aktueller_seite = $seiteninhalte_array[$sprachkuerzel][0];
   ob_start(); ob_end_clean(); ob_start();
  require( EINGANG . "schablonen/kopf.txt" );
  require( EINGANG . "schablonen/startseite.txt" );
  require( EINGANG . "schablonen/fuss.txt" );
  $generierte_datei = ob_get_contents(); ob_end_clean();
  file_put_contents( AUSGANG . $sprachkuerzel . "/index.html", $generierte_datei );
  
  # Alle uebrigen Seiten
  for ( $i = 0 ; $i < count( $navigations_url_array[$sprachkuerzel] ) ; $i++ ) {
    # Wenn es sich um ein "Verzeichnis" (also Haupt- oder Untermenue-Button) handelt, - nicht die Startseite -, und deshalb in der URL 'als Ordner' erscheint:
    if ( ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "haupt" ) or ( $navigations_url_array[$sprachkuerzel][$i]['button_typ'] == "unter" ) ) {
      $aktuelle_seitennummer = $navigations_url_array[$sprachkuerzel][$i]['reihenfolge_url'] - 1;
	  $aktueller_dateiname = $liste_aller_seiteninhaltedateinamen[ ( $aktuelle_seitennummer ) ];
      $textzeilen_aktueller_seite = array();
      $textzeilen_aktueller_seite = $seiteninhalte_array[$sprachkuerzel][$aktuelle_seitennummer];
       ob_start(); ob_end_clean(); ob_start();
      require( EINGANG . "schablonen/kopf.txt" );
      require( EINGANG . "schablonen/" . $aktueller_dateiname );
      require( EINGANG . "schablonen/fuss.txt" );
      $generierte_datei = ob_get_contents(); ob_end_clean();
      # "Unter"-Ordner erstellen
      $zu_erstellender_ordner = AUSGANG . $sprachkuerzel . "/" . $navigations_url_array[$sprachkuerzel][$i]['beschriftung_url'];
      if ( !( is_dir( $zu_erstellender_ordner ) ) ) { mkdir( $zu_erstellender_ordner ); }
      chmod( $zu_erstellender_ordner , 0644 );
      file_put_contents( AUSGANG . $sprachkuerzel . "/" . $navigations_url_array[$sprachkuerzel][$i]['beschriftung_url'] . "/index.html", $generierte_datei );
    }
  }
  
  # CSS-Datei
  ob_start(); ob_end_clean(); ob_start();
  require( EINGANG . "style.css" );
  $generierte_datei = ob_get_contents(); ob_end_clean();
  for ( $i_schablone = 0 ; $i_schablone < count( $liste_aller_platzhalter_werte ) ; $i_schablone++ ) {
    $generierte_datei .= "\r\n#platzhalter_button_" .  platzhalter_button_nummer() . " " . platzhalter_tag() . ' span:before {content:"' . platzhalter_button_beschriftung() . '";}';
  }
  file_put_contents( AUSGANG . $sprachkuerzel . "/style_" . DATUM_LETZTER_CSS_AENDERUNG . ".css", $generierte_datei );
  
}





#pre( $gesamtarray );

/*
ob_end_clean(); ob_start();
require_once( EINGANG . "schablonen/kopf.txt" );
require_once( EINGANG . "schablonen/startseite.txt" );
require_once( EINGANG . "schablonen/fuss.txt" );
$generierte_datei = ob_get_contents(); ob_end_clean();

#file_put_contents( AUSGANG . "ergebnis.txt", $generierte_datei );
*/










/*
$katze = "miauuu!";

ob_end_clean(); ob_start();
require_once( EINGANG . "test.php" );
$html = ob_get_contents(); ob_end_clean();

#file_put_contents( AUSGANG . "ergebnis.txt", $html );
*/


#########################################################################################################
#
# VORHANDENE LISTEN
#
#########################################################################################################
/*
$liste_aller_sprachen                                        ( Sprachkuerzel , deutsche Bezeichnung , englische Bezeichnung , Eigenbezeichnung )
$liste_nichtmeistgesprochener_sprachen                       ( alle nicht meistgesprochene Sprachen )
$liste_lateinischer_sprachen                                 ( alle Sprachen mit lateinischen Buchstaben )
$liste_aller_sprachcodes                                     ( alle Sprachkuerzel )
$liste_aller_sprachcodes_2_zeichen                           ( alle Sprachkuerzel - nur 2 Zeichen )
$liste_der_sprachcodes_meistgesprochener_sprachen            ( Sprachkuerzel der meistgesprochenen Sprachen )
liste_der_sprachcodes_aller_sprachen_ohne_meistgesprochenen  ( Sprachkuerzel der nicht-meistgesprochenen Sprachen )
$liste_der_sprachcodes_nichtlateinischer_sprachen            ( Sprachkuerzel der Sprachen mit nicht-lateinischen Buchstaben)
$liste_der_sprachcodes_lateinischer_sprachen                 ( Sprachkuerzel der Sprachen mit lateinischen Buchstaben)
$liste_aller_beschreibungen                                  ( description - nach <title> - fuer SEO )
$liste_aller_sprunglinks                                     ( zum Inhalt springen )
$url_liste_aller_navigation_standardseiten                   ( URL-konforme Bezeichnung (und Ordner-konform) )
$liste_aller_navigation_standardseiten                       ( Sprachkuerzel , deutsche Bezeichnung , Button-Beschriftungen - alle Sprachen )
$liste_aller_seiteninhaltedateinamen                         ( Liste Seiteninhalte-txt-Dateien )
$liste_aller_navigation_sonderelemente                       ( Sprachkuerzel, dt. Bezeichnung , Beschriftungen top-nav ,  alt-'aufklappen'-Texte )
$liste_aller_infotexte                                       ( Infotext - von Liebetroepfchen-Freunden erstellt )
$liste_aller_datenschutztexte                                ( Datenschutztext )
$email_codierung                                             ( Kodierung , richtiges Wort , ... )
$liste_aller_emailtexte                                      ( Email-Text fuer 'CSS abgeschaltet' )
$anzahl_sprachen                                             ( Anzahl Sprachen )
$datum_de                                                    ( dd-mm-YYYY )
$datum_int                                                   ( YYYY-mm-dd )


#pre ( $liste_aller_navigation_sonderelemente );


if ( $sprache_an_der_reihe == "latin" ) { $datum = $datum_de; }
if ( $sprache_an_der_reihe == "non-latin" ) { $datum = $datum_int; }

*/


# Zusammenfuegen von txt-Dateien (ein Array wird uebergeben)
function b( $array_zu_vereinender_textdateien ) {
  $anzahl_elemente_im_array = count( $array_zu_vereinender_textdateien );
  echo $anzahl_elemente_im_array;
  for ( $i = 0 ; $i < $anzahl_elemente_im_array ; $i++ ) {
    $array_zu_vereinender_textdateien[$i] = file_get_contents( EINGANG . $array_zu_vereinender_textdateien[$i] );
  }
  file_put_contents( AUSGANG . "ergebnis.txt", implode( $array_zu_vereinender_textdateien, NEW_LINE ));
}

# b( array( 'test.php' , 'test.php' ) );














/*

Nicht sauber:

lateinisch / nicht-lateinisch - Sprachen besser definieren.

*/

?>
<p>Zum Compilieren F5 drücken</p>
