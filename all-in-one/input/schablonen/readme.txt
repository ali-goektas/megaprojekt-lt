Datei- und Ordnernamen immer klein schreiben.
Alle Dateien im UTF-8-Format und mit der Zeilenende-Kodierung CR+LF abspeichern.
(Standardmäßig ist das der Fall. Man braucht also nur eine dieser Textdateien zu *kopieren*, und schon hat man eine im gewünschten Format.)


----------------------------------------------------------------


In die Schablone einzusetzende Begriffe:

Zuerst kommt ein "<?php",
dann ein Leerzeichen,
dann der Begriff "echo",
dann noch ein Leerzeichen,
dann ein Dollarzeichen ('$'),
dann die BEZEICHNUNG (direkt nach dem Dollarzeichen. Also KEIN Leerzeichen zwischen $-Zeichen und Begriff),
dann ein Strichpunkt (';') (wieder direkt nach dem Begriff. Also KEIN Leerzeichen zwischen dem Begriff und dem ;-Zeichen),
dann ein Leerzeichen,
und zum Schluss ein "?>".


Zusammengefasst sieht das so aus:

<?php echo $begriff; ?>




Die einzusetzenden Begriffe:

titel         oder  title                  für: Der Titel der Seite, welcher im Browsertab (Reiter) erscheint.                      Es muss   EXAKT    1 Titel je Seite existieren.
beschreibung                               für: Beschreibungstext für die Webseite.                                                 Es muss   EXAKT    1 Beschreibungstext je Seite existieren.
keywortliste                               für: Keywortliste für SEO - Suchmaschinenoptimierung                                     Es muss   EXAKT    1 Keywortliste je Seite existieren.
aufklappen                                 für: 'alt'-Text ("aufklappen beziehungsweise zuklappen" in jeweiliger Sprache)           Es dürfen MINIMAL  0 'alt'-Texte je Seite existieren.
untermenue_aufklappen                      für: 'alt'-Text ("Untermenü aufklappen beziehungsweise zuklappen" in jeweiliger Sprache) Es dürfen MINIMAL  0 'alt'-Texte je Seite existieren.
eigene_sprache_meistgesprochen             für: Sprachbezeichnung in der Sprache selbst (meistgesprochene Sprachen)                 Es dürfen MINIMAL  0 Sprachbezeichnungen je Seite existieren.
eigene_sprache_nicht_meistgesprochen       für: Sprachbezeichnung in der Sprache selbst (ohne meistgesprochene Sprachen)            Es dürfen MINIMAL  0 Sprachbezeichnungen je Seite existieren.
eigene_sprache_lateinisch                  für: Sprachbezeichnung in der Sprache selbst (lateinische Sprachen)                      Es dürfen MINIMAL  0 Sprachbezeichnungen je Seite existieren.
eigene_sprache_nicht_lateinisch            für: Sprachbezeichnung in der Sprache selbst (ohne lateinische Sprachen)                 Es dürfen MINIMAL  0 Sprachbezeichnungen je Seite existieren.
sprachkuerzel_nur_zwei_zeichen_meistgesprochen              für: Sprachkürzel (2 Zeichen - meistgesprochene Sprachen)                                Es dürfen MINIMAL  0 Sprachkürzel je Seite existieren.
sprachkuerzel_meistgesprochen              für: Sprachkürzel (inkl. Region - meistgesprochene Sprachen)                             Es dürfen MINIMAL  0 Sprachkürzel je Seite existieren.
sprachkuerzel_nur_zwei_zeichen_nicht_meistgesprochen        für: Sprachkürzel (2 Zeichen - ohne meistgesprochene Sprachen)                           Es dürfen MINIMAL  0 Sprachkürzel je Seite existieren.
sprachkuerzel_nicht_meistgesprochen für: Sprachkürzel (inkl. Region - ohne meistgesprochene Sprachen)                        Es dürfen MINIMAL  0 Sprachkürzel je Seite existieren.
sprachkuerzel_nur_zwei_zeichen_lateinisch                   für: Sprachkürzel (2 Zeichen - lateinische Sprachen)                                     Es dürfen MINIMAL  0 Sprachkürzel je Seite existieren.
sprachkuerzel_lateinisch                   für: Sprachkürzel (inkl. Region - lateinische Sprachen)                                  Es dürfen MINIMAL  0 Sprachkürzel je Seite existieren.
sprachkuerzel_nur_zwei_zeichen_nicht_lateinisch             für: Sprachkürzel (2 Zeichen - ohne lateinische Sprachen)                                Es dürfen MINIMAL  0 Sprachkürzel je Seite existieren.
sprachkuerzel_nicht_lateinisch             für: Sprachkürzel (inkl. Region - ohne lateinische Sprachen)                             Es dürfen MINIMAL  0 Sprachkürzel je Seite existieren.
sprachkuerzel_nur_zwei_zeichen             für: Sprachkürzel (nur die ersten 2 Zeichen - alle Sprachen)                             Es dürfen MINIMAL  0 Sprachkürzel je Seite existieren.
sprachkuerzel                              für: Sprachkürzel (inkl. der zugehörigen Region - z.B. "pt-br" - alle Sprachen)          Es dürfen MINIMAL  0 Regional-Sprachkürzel je Seite existieren.
aktuell                                    für: 'aktualisiert:'-Text (in jeweiliger Sprache)                                        Es dürfen MINIMAL  0 aktualisiert-Texte je Seite existieren.
startseite                                 für: Button-Beschriftung und 'alt'-Text (in jeweiliger Sprache)                          Es dürfen MINIMAL  0 Button-Beschriftungen/'alt'-Texte je Seite existieren.
original                                   für: Button-Beschriftung ("original Webseiten" in jeweiliger Sprache)                    Es dürfen MINIMAL  0 Button-Beschriftungen je Seite existieren.
original_1                                 für: Button-Beschriftung ("ich-bin-liebetroepfchen-gottes.de)                            Es dürfen MINIMAL  0 Button-Beschriftungen je Seite existieren.
original_2                                 für: Button-Beschriftung ("lebensrat-gottes.de")                                         Es dürfen MINIMAL  0 Button-Beschriftungen je Seite existieren.
button_beschriftung_label                  für: Button-Beschriftung (Hauptmenü - Aufklappbutton-Beschriftungen)                     Es dürfen MINIMAL  0 Button-Beschriftungen je Seite existieren.
button_beschriftung_link                   für: Button-Beschriftung (Hauptmenü - Hauptmenü-Link- u. Submenu-Link-Beschriftungen)    Es dürfen MINIMAL  0 Button-Beschriftungen je Seite existieren.
platzhalter_id                             für: Platzhalter-ID-Nummer (zwecks eindeutiger Zuordnung der Buttonbeschriftung)         Es dürfen MINIMAL  0 Platzhalter-IDs je Seite existieren.
platzhalter_tag                            für: Platzhalter-Tags (ob es sich um einen Link oder einen Aufklappbutton handelt)       Es dürfen MINIMAL  0 Platzhalter-Tags je Seite existieren.
sprunglink_inhalt                          für: zum Inhalt springen                                                                 Es dürfen MINIMAL  0 Inhalts-Sprunglinks je Seite existieren.
sprunglink_vertonungen                     für: zu den Vertonungen springen                                                         Es dürfen MINIMAL  0 Vertonungen-Sprunglinks je Seite existieren.
sprunglink_themensammlungen                für: zu den Themensammlungen springen                                                    Es dürfen MINIMAL  0 Themensammlungen-Sprunglinks je Seite existieren.
sprunglink_botschaften                     für: zu den (neuesten) Botschaften springen                                              Es dürfen MINIMAL  0 Botschaften-Sprunglinks je Seite existieren.
text                                       für: Seitentextabschnitt                                                                 Es dürfen MINIMAL  0 Seitentextabschnitte je Seite existieren.
botschaft                                  für: Botschaft (Themensammlungen sind auch Botschaften)                                  Es dürfen MINIMAL  0 Botschaften je Seite existieren.
datum                                      für: Datumsangabe                                                                        Es dürfen MINIMAL  0 Datumsangaben je Seite existieren.
anzahl_botschaften                         für: Anzahl an Botschaften                                                               Es dürfen MINIMAL  0 "Anzahl an Botschaften" je Seite existieren.
info                                       für: Info-Text                                                                           Es dürfen MINIMAL  0 Info-Texte je Seite existieren.
email                                      für: Email-Textfragmente                                                                 Es dürfen MINIMAL  0 Email-Textfragmente je Seite existieren.
url_ordner                                 für: Ordner (lateinische Sprachen: Name ; nicht-lateinische Sprachen: Nummer)            Es dürfen MINIMAL  0 Ordner je Seite existieren.
                                                In der URL wird der Ordnername abgebildet. Ergo: URL = Ordnerbezeichnung



Die HTML-Datei (Schablone) sähe dann so aus (als Beispiel):

<!DOCTYPE html>
<html lang="<?php echo $sprachkuerzel_nur_zwei_zeichen; ?>">
<title><?php echo $text; ?></title>
<meta name="keywords" content="<?php echo $keywortliste; ?>">
<h1><?php echo $h1; ?></h1>
<p><?php echo $text; ?></p>
...




Eine HTML-Datei mit folgendem Inhalt würde erzeugt werden:

In deutsch z.B.:

<!DOCTYPE html>
<html lang="de">
<title>Liebetröpfchen Gottes</title>
<meta name="keywords" content="Liebetröpfchen, Lebensrat, Gott">
<h1>Liebetröpfchen Gottes</h1>
<p>Die ICH BIN-Gottheit ...</p>
...

In anderen Sprachen jeweils der Inhalt *jener* Sprache.




Und im Browser bekäme man dann Folgendes zu sehen:

Liebetröpfchen Gottes            <-- im Browser-Tab

Liebetröpfchen Gottes            <-- Hauptüberschrift

Die ICH BIN-Gottheit ...         <-- Text

...


