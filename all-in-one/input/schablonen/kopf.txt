<!DOCTYPE html>
<html lang="<?php echo sprachkuerzel_nur_zwei_zeichen(); ?>">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="/<?php echo sprachkuerzel(); ?>/style_<?php echo datum_css(); ?>.css">
<title tabindex="0"><?php echo text(); ?></title>
<meta name="keywords" content="<?php echo text(); ?>, <?php echo text(); ?>, <?php echo text(); ?>">

<meta name="description" content="<?php echo beschreibung(); ?>">
<!-- <meta http-equiv="Cache-Control" content="max-age=0, must-revalidate">
<meta http-equiv="Cache-Control" content="no-cache"> -->

<?php for( $i_schablone = 0 ; $i_schablone < count( $liste_aller_sprachcodes ) ; $i_schablone++ ): ?>
<link <?php echo alternate( $liste_aller_sprachcodes[$i_schablone] ); ?>hreflang="<?php echo hole_region_in_grossbuchstaben( $liste_aller_sprachcodes[$i_schablone] ); ?>" href="https://liebetroepfchen-gottes.com/<?php echo $liste_aller_sprachcodes[$i_schablone]; ?>/" />
<?php endfor; ?>

<link hreflang="x-default" href="https://liebetroepfchen-gottes.com/en-gb/" /><!-- Default-Sprache -->
</head>
<body>

<!-- Skip-Link -->
<div id="skiplink_zum_seiteninhalt"> 
<a rel="noopener" tabindex="0" href="#seiteninhalt">
<div tabindex="-1" class="rahmen_fuer_scrollbalken">
<div class="maximal_1200_breit">
<span><?php echo sprunglink_zum_inhalt(); ?></span>
</div>
</div>
</a>
</div>

<!-- Top-Navigation, Haupt-Navigation, Platzhalter -->
<!--
top-nav: 1. Menue-Zeile oben
main-nav: 2. Menue-Zeile darunter
hauptmenuepunkt: Hauptnavigation
untermenuepunkt: Unternavigation (aufgeklappt)



gesamte Navigation - header -->
<header><!--
  --><div tabindex="-1" class="rahmen_fuer_scrollbalken"><!--
    --><div class="maximal_1200_breit"><!--
      
      
      --><div id="top-nav"><!--
        --><div id="startseite_als_haus_button" class="hauptmenuepunkt ansicht_tablet" ><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/"><span tabindex="-1" class="view"><img style="height:1.5rem;width:auto;border:none;" src="/global/bilder/home.png" alt="<?php echo startseite(); ?>"></span></a></div><!--
        --><div id="startseite_als_text_button" class="hauptmenuepunkt ansicht_desktop"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/"><span tabindex="-1" class="view"><?php echo startseite(); ?></span></a></div><!--
        
        
        Hauptnavigation in 2. Zeile - nav - main-nav
        --><nav id="main-nav" class="rahmen_fuer_scrollbalken"><!--
          --><div class="maximal_1200_breit"><!--
            --><div id="button_<?php echo button_nummer(); ?>" class="hauptmenuepunkt ansicht_tablet_desktop"><!--
              --><input tabindex="0" id="checkbox_button_<?php echo button_nummer(); ?>"  type="checkbox"><!--
              --><input tabindex="-1" id="radio_button_<?php echo button_nummer(); ?>_auf" type="radio" name="aufklappbar" class="radio_auf"><!--
              --><input tabindex="-1" id="radio_button_<?php echo button_nummer(); ?>_zu"  type="radio" name="aufklappbar"><!--
              --><span  tabindex="-1" class="aufklapp_button"><!--
                --><label tabindex="-1" for="checkbox_button_<?php echo button_nummer(); ?>"  id="label_button_<?php echo button_nummer(); ?>_checkbox" class="label_checkbox"><span class="view"><?php echo button_beschriftung_label(); ?><img class="dreieck" src="/global/bilder/dreieck.png" alt="<?php echo aufklappen(); ?>"></span></label><!--
                --><label tabindex="-1" for="radio_button_<?php echo button_nummer(); ?>_zu"  id="label_button_<?php echo button_nummer(); ?>_zu"       class="label_zu"      ><img src="/global/bilder/1-pixel.bmp" alt=""></label><!--
                --><label tabindex="-1" for="radio_button_<?php echo button_nummer(); ?>_auf" id="label_button_<?php echo button_nummer(); ?>_auf"      class="label_auf"     ><img src="/global/bilder/1-pixel.bmp" alt=""></label><!--
                --><span tabindex="-1" class="spalt"></span><!--
              --></span><!--
              --><div id="liste_einweisungsseiten" class="untermenue rahmen_fuer_scrollbalken"><!--
                --><div class="maximal_1200_breit"><!--
                  --><div id="button_<?php echo button_nummer(); ?>" class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
                  --><div id="button_<?php echo button_nummer(); ?>" class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
                  --><div id="button_<?php echo button_nummer(); ?>" class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
                  --><div id="button_<?php echo button_nummer(); ?>" class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
                  --><div id="button_<?php echo button_nummer(); ?>" class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
                  --><div id="button_<?php echo button_nummer(); ?>" class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
                --></div><!--
              --></div><!--
            --></div><!--
            --><div id="button_<?php echo button_nummer(); ?>" class="hauptmenuepunkt ansicht_tablet_desktop aktive_seite"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
            --><div id="button_<?php echo button_nummer(); ?>" class="hauptmenuepunkt ansicht_tablet_desktop"             ><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
            --><div id="button_<?php echo button_nummer(); ?>" class="hauptmenuepunkt ansicht_tablet_desktop"             ><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
          --></div><!--
        --></nav><!-- main-nav --><!--
        
        
        --><div id="dreipunkt_button" class="hauptmenuepunkt ansicht_smartphone"><!--
          --><input tabindex="0" id="checkbox_dreipunkt_button"  type="checkbox"><!--
          --><input tabindex="-1" id="radio_dreipunkt_button_auf" type="radio" name="aufklappbar" class="radio_auf"><!--
          --><input tabindex="-1" id="radio_dreipunkt_button_zu"  type="radio" name="aufklappbar"><!--
          --><span  tabindex="-1" class="aufklapp_button"><!--
            --><label tabindex="-1" for="checkbox_dreipunkt_button"  id="label_dreipunkt_button_checkbox" class="label_checkbox"><span class="view"><img style="height:0.1875rem;width:auto;border:none;" src="/global/bilder/dreipunkt.png" alt="<?php echo untermenue_aufklappen(); ?>"></span></label><!--
            --><label tabindex="-1" for="radio_dreipunkt_button_zu"  id="label_dreipunkt_button_zu"       class="label_zu"      ><img src="/global/bilder/1-pixel.bmp" alt=""></label><!--
            --><label tabindex="-1" for="radio_dreipunkt_button_auf" id="label_dreipunkt_button_auf"      class="label_auf"     ><img src="/global/bilder/1-pixel.bmp" alt=""></label><!--
            --><span tabindex="-1" class="spalt"></span><!--
          --></span><!--
          --><div id="dreipunkt_menu_inhalt" class="untermenue rahmen_fuer_scrollbalken"><!--
            --><div class="maximal_1200_breit"><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/"><span tabindex="-1" class="view"><?php echo startseite(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel(); ?>/<?php echo url_ordner(); ?>/"><span tabindex="-1" class="view"><?php echo button_beschriftung_link(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="https://www.ich-bin-liebetroepfchen-gottes.de"><span tabindex="-1" class="view"><?php echo original_1(); ?></span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="https://www.lebensrat-gottes.de"><span tabindex="-1" class="view"><?php echo original_2(); ?></span></a></div><!--
            --></div><!--
          --></div><!--
        --></div><!--
        --><!--<p id="aktuelles_datum" tabindex="0"><?php # echo aktuell(); ?><br><span class="rot"><?php # echo datum_botschaft(); ?></span></p>--><!--
        --><div id="originalseiten_button" class="hauptmenuepunkt ansicht_tablet_desktop"><!--
          --><input tabindex="0" id="checkbox_originalseiten_button"  type="checkbox"><!--
          --><input tabindex="-1" id="radio_originalseiten_button_auf" type="radio" name="aufklappbar" class="radio_auf"><!--
          --><input tabindex="-1" id="radio_originalseiten_button_zu"  type="radio" name="aufklappbar"><!--
          --><span  tabindex="-1" class="aufklapp_button"><!--
            --><label tabindex="-1" for="checkbox_originalseiten_button"  id="label_originalseiten_button_checkbox" class="label_checkbox"><span class="view"><?php echo original(); ?><img class="dreieck" src="/global/bilder/dreieck.png" alt="<?php echo aufklappen(); ?>"></span></label><!--
            --><label tabindex="-1" for="radio_originalseiten_button_zu"  id="label_originalseiten_button_zu"       class="label_zu"      ><img src="/global/bilder/1-pixel.bmp" alt=""></label><!--
            --><label tabindex="-1" for="radio_originalseiten_button_auf" id="label_originalseiten_button_auf"      class="label_auf"     ><img src="/global/bilder/1-pixel.bmp" alt=""></label><!--
            --><span tabindex="-1" class="spalt"></span><!--
          --></span><!--
          --><div id="originalseiten_liste" class="untermenue rahmen_fuer_scrollbalken"><!--
            --><div class="maximal_1200_breit"><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="https://www.ich-bin-liebetroepfchen-gottes.de"><span tabindex="-1" class="view">Ich-Bin-Liebetroepfchen-Gottes.de</span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="https://www.ich-bin-liebetroepfchen-gottes.com"><span tabindex="-1" class="view">Ich-Bin-Liebetroepfchen-Gottes.com</span></a></div><!--
              --><div class="untermenuepunkt"><a rel="noopener" tabindex="0" href="https://www.lebensrat-gottes.de"><span tabindex="-1" class="view">Lebensrat-Gottes.de</span></a></div><!--
            --></div><!--
          --></div><!--
        --></div><!--
        --><div id="flagge_button" class="hauptmenuepunkt"><!--
          --><input tabindex="0" id="checkbox_flagge_button"  type="checkbox"><!--
          --><input tabindex="-1" id="radio_flagge_button_auf" type="radio" name="aufklappbar" class="radio_auf"><!--
          --><input tabindex="-1" id="radio_flagge_button_zu"  type="radio" name="aufklappbar"><!--
          --><span  tabindex="-1" class="aufklapp_button"><!--
            --><label tabindex="-1" for="checkbox_flagge_button"  id="label_flagge_button_checkbox" class="label_checkbox"><span class="view"><img style="height:1.5rem;width:auto;border:none;" src="/global/bilder/flagge.png" alt="english 中文 español français русский português deutsch 日本語"><img class="dreieck" src="/global/bilder/dreieck.png" alt="<?php echo aufklappen(); ?>"></span></label><!--
            --><label tabindex="-1" for="radio_flagge_button_zu"  id="label_flagge_button_zu"       class="label_zu"      ><img src="/global/bilder/1-pixel.bmp" alt=""></label><!--
            --><label tabindex="-1" for="radio_flagge_button_auf" id="label_flagge_button_auf"      class="label_auf"     ><img src="/global/bilder/1-pixel.bmp" alt=""></label><!--
            --><span tabindex="-1" class="spalt"></span><!--
          --></span><!--
          --><div id="flagge_sprachen_liste" class="untermenue rahmen_fuer_scrollbalken"><!--
            --><div class="maximal_1200_breit"><!--
              
              <?php for( $i_schablone = 0 ; $i_schablone < count( $liste_der_sprachcodes_meistgesprochener_sprachen ) ; $i_schablone++ ): ?>
              --><div class="untermenuepunkt ansicht_smartphone"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel_meistgesprochen(); ?>/" lang="<?php echo sprachkuerzel_nur_zwei_zeichen_meistgesprochen(); ?>" hreflang="<?php echo hole_region_in_grossbuchstaben( sprachkuerzel_meistgesprochen() ); ?>"><span tabindex="-1" class="view"><?php echo meistgesprochen_eigene_sprache(); ?></span></a></div><!--
              <?php endfor; ?>
              
              --><div class="ansicht_smartphone sprachauswahl_trennlinie"></div><!--
              
              <?php for( $i_schablone = 0 ; $i_schablone < count( $liste_der_sprachcodes_nichtmeistgesprochener_sprachen ) ; $i_schablone++ ): ?>
              --><div class="untermenuepunkt ansicht_smartphone"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel_nicht_meistgesprochen(); ?>/" lang="<?php echo sprachkuerzel_nur_zwei_zeichen_nicht_meistgesprochen(); ?>" hreflang="<?php echo hole_region_in_grossbuchstaben( sprachkuerzel_nicht_meistgesprochen() ); ?>"><span tabindex="-1" class="view"><?php echo nicht_meistgesprochen_eigene_sprache(); ?></span></a></div><!--
              <?php endfor; ?>
              
              <?php for( $i_schablone = 0 ; $i_schablone < count( $liste_der_sprachcodes_lateinischer_sprachen ) ; $i_schablone++ ): ?>
              --><div class="untermenuepunkt ansicht_tablet_desktop"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel_lateinisch(); ?>/" lang="<?php echo sprachkuerzel_nur_zwei_zeichen_lateinisch(); ?>" hreflang="<?php echo hole_region_in_grossbuchstaben( sprachkuerzel_lateinisch() ); ?>"><span tabindex="-1" class="view"><?php echo lateinisch_eigene_sprache(); ?></span></a></div><!--
              <?php endfor; ?>
              
              <?php for( $i_schablone = 0 ; $i_schablone < count( $liste_der_sprachcodes_nichtlateinischer_sprachen ) ; $i_schablone++ ): ?>
              --><div class="untermenuepunkt ansicht_tablet_desktop"><a rel="noopener" tabindex="0" href="/<?php echo sprachkuerzel_nicht_lateinisch(); ?>/" lang="<?php echo sprachkuerzel_nur_zwei_zeichen_nicht_lateinisch(); ?>" hreflang="<?php echo hole_region_in_grossbuchstaben( sprachkuerzel_nicht_lateinisch() ); ?>"><span tabindex="-1" class="view"><?php echo nicht_lateinisch_eigene_sprache(); ?></span></a></div><!--
              <?php endfor; ?>
              
            --></div><!--
          --></div><!--
        --></div><!--
      --></div><!-- top-nav
      
      
     --></div><!--
  --></div><!--
--></header><!--



Platzhalter Main-Nav (2. Reihe)
--><div id="platzhalter" class="ansicht_tablet_desktop rahmen_fuer_scrollbalken"><!--
  --><div class="maximal_1200_breit"><!--
  
  <?php for( $i_schablone = 0 ; $i_schablone < count( $liste_aller_platzhalter_werte ) ; $i_schablone++ ): ?>
    
    <?php if ( platzhalter_tag() == "label" ): ?>
      --><div id="platzhalter_button_<?php echo platzhalter_button_nummer(); ?>" class="hauptmenuepunkt"><label tabindex="-1"><span tabindex="-1" class="view"><img tabindex="-1" class="dreieck" src="/global/bilder/1-pixel.bmp" alt=""></span></label></div><!--
    <?php endif; ?>
    
    <?php if ( platzhalter_tag() == "a" ): ?>
      --><div id="platzhalter_button_<?php echo platzhalter_button_nummer(); ?>" class="hauptmenuepunkt"><a rel="noopener" tabindex="-1"><span tabindex="-1" class="view"></span></a></div><!--
    <?php endif; ?>
    
  <?php endfor; ?>
  
  --></div><!--
--></div>

<!-- Inhaltsbereich -->
<main id="seiteninhalt">
<div class="relativrahmen_fuer_main">
<div class="rahmen_fuer_scrollbalken absolutrahmen_fuer_main">
<div class="maximal_1200_breit">

<br>
