# Megaprojekt LT


## Beschreibung

HTML-Generator_CMS_Framework_All-in-One-Paket (spezifisch für LT-Webseite)


## Ziele, die verfolgt werden mit diesem Projekt

Einladende Webseite, die die Menschen in ihren Herzen berühren und sie anregen soll, wenn sie wünschen, an sich zu arbeiten und ein besserer Mensch zu werden.  
(HTML-Generator)

Glz. für jemanden mit EINFACHEN HTML- und CSS-Kenntnissen erweiterbar,  
(CMS)

als auch für jemanden mit TIEFEREN Kenntnissen komplett um Funktionalitäten erweiterbar.  
(Framework)


## Schnellstart
Veröffentlicht auf [liebetroepfchen-gottes.com/de](https://liebetroepfchen-gottes.com/de).


## Im Detail

### Der "all-in-one"-Ordner:
Den Ordner "all-in-one" ins localhost-Verzeichnis kopieren.
In diesem Ordner befinden sich:
- den Ordner "input", welcher die zu bearbeitenden Dateien enthält,
- den Ordner "output", welcher die vom HTML-Generator generierten Dateien enthält.

### Der "input"-Ordner:
Der Inhalt des Ordners "ordnerinhalt_gehoert_ins_htdocs-verzeichnis" wird ebenfalls ins localhost-Verzeichnis kopiert.

Struktur: MVC-Modell:
- Ordner "daten" = Model
- Ordner "schablonen" = View
- Dateien "hauptprogramm.php" + "hilfsprogramme.php" = Controller

Ausserdem sind ebenfalls noch eine readme.txt und die CSS-Datei vorhanden.

### Der "output"-Ordner:
Im Ordner "output" erzeugt der HTML-Generator die 10 Seiten * 26 Sprachen. (je Sprache über 250 Dateien je Dateityp downloadbar)  
Diese einfach ins localhost-Verzeichnis verschieben. Dann ist das "output"-Verzeichnis wieder leer.

### Das "localhost"-Verzeichnis:
Via localhost-Aufruf im Browser kann man sich das Ergebnis anzeigen lassen.


## Fortschritt:
- [x] HTML-Generator  
- [ ] CMS
- [ ] Framework
